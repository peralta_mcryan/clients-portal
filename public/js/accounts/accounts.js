$(document).ready(function () {
    $('#country').on('change', function(e){
        var country_id = e.target.value;
 
        if (country_id != '') {
                $.get('/information/create/ajax-country-province/' + country_id, function(data) {
                $('#province').empty();

                $('#province').append($('<option>', { 
                    value: '0',
                    text : 'Select a Province' 
                }));

                $.each(data, function(index,subCatObj){
                    $('#province').append($('<option>', { 
                        value: subCatObj.id,
                        text : subCatObj.name 
                    }));
                });
            });
        } else {
            $('#province').append($('<option>', { 
                value: '0',
                text : 'Select a Province' 
            }));

            $('#province').empty();
        }
        $('#city').empty();
    });

    $('#province').on('change', function(e){
        var province_id = e.target.value;

        if (province_id != '') {
            $.get('/information/create/ajax-province-city/' + province_id, function(data) {
                $('#city').empty();

                $('#city').append($('<option>', { 
                    value: '0',
                    text : 'Select a City' 
                }));

                $.each(data, function(index,subCatObj){
                    $('#city').append($('<option>', { 
                        value: subCatObj.id,
                        text : subCatObj.name 
                    }));
                });
            });
        } else {
            $('#city').append($('<option>', { 
                value: '0',
                text : 'Select a City' 
            }));

            $('#city').empty();
        }
    });

    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});

function deleteClientAccount(clientId, clientName) {
    if (confirm('Are you sure you want to delete this client?')) {
        $.ajax({
            type:'GET',
            url:'/clients/delete/'+clientId,
            success:function(data){
                window.location = '/clients';
            }
        });
    }
}