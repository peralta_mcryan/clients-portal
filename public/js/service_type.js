$(document).ready(function () {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});


function deleteServiceType(serviceTypeId) {
	if (confirm('Are you sure you want to delete this service type?')) {
		$.ajax({
			type:'GET',
			url:'/service-types/delete/'+serviceTypeId,
			success:function(data){
				window.location = '/service-types';
			}
		});
	}
}