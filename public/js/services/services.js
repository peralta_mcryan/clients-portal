function deleteService(serviceId) {
    if (confirm('Are you sure you want to delete this service?')) {
        $.ajax({
            type:'GET',
            url:'/services/delete/'+serviceId,
            success:function(data){
                window.location = '/services';
            }
        });
    }
}