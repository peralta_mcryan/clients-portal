
function deleteTicket(ticketId) {
    if (confirm('Are you sure you want to delete this issue?')) {
        $.ajax({
            type: 'GET',
            url: '/technical/tickets/delete/' + ticketId,
            success: function (data) {
               
                window.location = backurl;
            }
        });
    }
}