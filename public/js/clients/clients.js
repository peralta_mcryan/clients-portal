function deleteClientAccount(clientId) {
    if (confirm('Are you sure you want to delete this client account?')) {
        $.ajax({
            type:'GET',
            url:'/clients/delete/'+clientId,
            success:function(data){
                window.location = '/clients';
            }
        });
    }
}