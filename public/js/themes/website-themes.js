
function editSite(theme,id)
{
    // $.ajax({
    //     type: 'GET',
    //     url: 'themes/' + id+'/edit',
    //     success: function (data) {
    //         $('#website_name').val(data.name);
    //         $('#website_link').val(data.website_links);
    //         $("#frm_themes").attr("action", "{{route('themes.update')}}");
    //     }
    // });
    window.location = '/themes?group='+theme+'&id=' +id;
}

function resetFields()
{
    // $('#website_name').val('');
    // $('#website_link').val('');
    // $("#frm_themes").attr("action", "{{route('themes.store')}}");
    // $("#submitButton").attr("value", "Add");
    window.location = '/themes';
    
}

function deleteSite(id) {
    if (confirm('Are you sure you want to delete this website?')) {
        $.ajax({
            type: 'GET',
            url: 'themes/delete/' + id,
            success: function (data) {
                window.location = '/themes';
            }
        });
    }
}

$('#theme_group').on('change',function () {
    var theme = $(this).val();
    // alert(theme)
    window.location = '/themes?group='+theme;
});