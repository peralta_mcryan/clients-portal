

function deleteDepartment(departmentId) {
    if (confirm('Are you sure you want to delete this department?')) {
        $.ajax({
            type: 'GET',
            url: '/departments/delete/' + departmentId,
            success: function (data) {
                window.location = backurl;
            }
        });
    }
}