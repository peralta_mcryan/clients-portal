$(document).ready(function () {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});


function deleteAdminAccount(adminId) {
	if (confirm('Are you sure you want to delete this admin account?')) {
		$.ajax({
			type:'GET',
			url:'/administrator-account/delete/'+adminId,
			success:function(data){
				window.location = '/administrator-accounts';
			}
		});
	}
}