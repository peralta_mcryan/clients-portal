$(document).ready(function () {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });

    $("#project_days").on('change',function () {
        var startDate = $("#project_start").val();
        var mandays = this.value;
        if (mandays == "" || startDate == "") {
            $("#project_end").val("");
        } else{  
            $.ajax({
                type: 'GET',
                url: '/projects/mandays-to-date?&startdate='+ startDate +'&mandays='+mandays,
                success: function (data) {
                   
                    $("#project_end").val(data);
                }
            });
        }
        
    });

    $("#project_start").on('change', function () {
        var mandays = $("#project_days").val();
        var startDate = this.value;
        if(mandays == "" || startDate == "" ){  
            $("#project_end").val("");
        }else{

            $.ajax({
                type: 'GET',
                url: '/projects/mandays-to-date?&startdate=' + startDate + '&mandays=' + mandays,
                success: function (data) {

                    $("#project_end").val(data);
                }
            });
          
        }
    

    });
});

function deleteProject(projectId) {
    if (confirm('Are you sure you want to delete this project?')) {
        $.ajax({
            type:'GET',
            url:'/projects/delete/'+projectId,
            success:function(data){
                window.location = backurl;
            }
        });
    }
}