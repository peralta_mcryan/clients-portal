$(document).ready(function () {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});

function deleteContact(contactId) {
    if (confirm('Are you sure you want to delete this contact?')) {
        $.ajax({
            type:'GET',
            url:'/contacts/delete/'+contactId,
            success:function(data){
                window.location = '/contacts';
            }
        });
    }
}