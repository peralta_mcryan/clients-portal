$(document).ready(function () {
    $(".clickable-row").click(function () {
        window.location = $(this).data("href");
    });
});


function deleteServiceType(adminTypeId) {
    if (confirm('Are you sure you want to delete this administrator role?')) {
        $.ajax({
            type: 'GET',
            url: '/admin-types/delete/' + adminTypeId,
            success: function (data) {
                // alert(data);
                window.location = '/admin-types';
            }
        });
    }
}