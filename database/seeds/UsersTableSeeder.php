<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                "name" => "Edward Joseph Clavito",
                "email" => "edward.clavito@imanila.com.ph",
                "password" => bcrypt("12345678"),
                "designation" => "Senior Software Engineer"
            ],
        ];

        foreach($users as $user){
            DB::table('users')->insert($user);
        }
    }
}
