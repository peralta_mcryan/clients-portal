<?php

use Illuminate\Database\Seeder;

class ServiceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $serviceTypes = [
            [
                "name" => "Wordpress",
            ],
            [
                "name" => "Hosting",
            ],
            [
                "name" => "iRepublic",
            ],
            [
                "name" => "Application Development",
            ],
            [
                "name" => "Digital Marketing",
            ],
            [
                "name" => "Internet",
            ],
            [
                "name" => "Technical",
            ],
            [
                "name" => "Others",
            ],
        ];

        foreach($serviceTypes as $serviceType){
            DB::table('service_types')->insert($serviceType);
        }
    }
}
