<?php

use Illuminate\Database\Seeder;

class DesignationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fileFolders = [
            [
                "id" => "1",
                "designation" => "Admin",
            ],
            [
                "id" => "2",
                "designation" => "Account Executive",
            ],
            [
                "id" => "3",
                "designation" => "Project Manager",
            ]
           
        ];
        foreach($fileFolders as $fileFolder){
            DB::table('designations')->insert($fileFolder);
        }
    }
}
