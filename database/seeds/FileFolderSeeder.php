<?php

use Illuminate\Database\Seeder;

class FileFolderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fileFolders = [
            [
                "id" => "1",
                "folder_name" => "General",
            ],
            [
                "id" => "2",
                "folder_name" => "Logo",
            ],
            [
                "id" => "3",
                "folder_name" => "Documents",
            ],
            [
                "id" => "4",
                "folder_name" => "Images",
            ],
            [
                "id" => "5",
                "folder_name" => "Products",
            ],
            [
                "id" => "7",
                "folder_name" => "Sitemap",
            ],
            [
                "id" => "8",
                "folder_name" => "[Digital Marketing] General Tasks",
            ],
            [
                "id" => "9",
                "folder_name" => "[Digital Marketing] Social Media Management",
            ],
            [
                "id" => "10",
                "folder_name" => "[Digital Marketing] Facebook Ads",
            ],
            [
                "id" => "11",
                "folder_name" => "[Digital Marketing] Google Ads",
            ],
            [
                "id" => "12",
                "folder_name" => " 	[Digital Marketing] Branding Collateral",
            ],
            [
                "id" => "13",
                "folder_name" => "Official Receipts",
            ],
            [
                "id" => "14",
                "folder_name" => "Mock-up",
            ],
            [
                "id" => "15",
                "folder_name" => "Contracts",
            ],
            [
                "id" => "16",
                "folder_name" => "Reports",
            ],
            [
                "id" => "17",
                "folder_name" => "Briefing Document",
            ],
            [
                "id" => "18",
                "folder_name" => "Social Media Calendar",
            ],
        ];
        foreach($fileFolders as $fileFolder){
            DB::table('file_folders')->insert($fileFolder);
        }
    }
}
