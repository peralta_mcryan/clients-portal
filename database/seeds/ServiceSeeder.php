<?php

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = [
            [
                "name" => "Customized Wordpress Website",
                "service_type_id" => "1",
                "email_template" => "<p>Hi Ms. / Mr. [CLIENT_NAME]</p>

                <p>Greetings from iManila!</p>
                
                <p>First of all, we would like to thank you for availing our&nbsp;<strong>iManila Customized WordPress Website&nbsp;package.</strong></p>
                
                <p>To recap, the package inclusions are as follows:</p>
                
                <p><strong>A. &nbsp; &nbsp;WordPress&nbsp;Website&nbsp;Development </strong><em>(please see the signed conforme for details)&nbsp;</em></p>
                
                <p><strong>B. &nbsp;&nbsp;WP Secure Silver Hosting Services&nbsp;and&nbsp;Server Support </strong><em>(please see the signed conforme for details)</em></p>
                
                <p><strong>C. &nbsp; &nbsp;Website&nbsp;Development&nbsp;Timeline&nbsp;(</strong><em>please see the signed conforme for details)</em></p>
                
                <p><br />
                <strong>INITIAL REQUIREMENTS FROM THE CLIENT</strong><em>&nbsp;</em></p>
                
                <p>To be able to proceed with the creation&nbsp;of&nbsp;your&nbsp;website, our creatives&nbsp;and&nbsp;development&nbsp;team will require the following information:</p>
                
                <ul>
                    <li><strong>Company Logo</strong>&nbsp;<br />
                    if available, please submit it in high-resolution, JPEG or PNG format</li>
                </ul>
                
                <ul>
                    <li><strong>Preferred Color/s</strong>&nbsp;<br />
                    if you have preferred colors, provide us 3 to 5 colors which we can use as your&nbsp;website&#39;s&nbsp;main color palette, if not, we will base the colors on your logo.<br />
                    &nbsp;</li>
                    <li><strong>Text Content</strong><br />
                    provide us&nbsp;all&nbsp;available content that we can use on your&nbsp;website, these will range from taglines, slogans, about the company, history, products or services offered, etc.&nbsp;</li>
                </ul>
                
                <ul>
                    <li><strong>Images&nbsp;and/or Videos</strong>&nbsp;<br />
                    submit any existing images or videos you have that we can use on your&nbsp;website&nbsp;(high-res. JPEG or PNG format for photos, MP4 format for videos); if you do not have any, we can utilize our stock photos&nbsp;and&nbsp;use images upon your approval</li>
                </ul>
                
                <ul>
                    <li><strong>Social Media Accounts</strong>&nbsp;<br />
                    send us links&nbsp;and&nbsp;URLs to your respective active social media accounts so we can integrate it on your&nbsp;website</li>
                </ul>
                
                <ul>
                    <li><strong>Contact Details</strong>&nbsp;<br />
                    contact details include: Physical address, Email address, Telephone, Fax&nbsp;and&nbsp;Mobile number, Hours&nbsp;of&nbsp;operation</li>
                </ul>
                
                <ul>
                    <li><strong>Content for the Web Pages</strong><br />
                    please provide us the&nbsp;corresponding content based on the sample site map/web pages below:</li>
                </ul>
                
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Home</p>
                
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - About Us</p>
                
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Products / Services</p>
                
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Our Clients</p>
                
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Contact Us<br />
                <br />
                <strong><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *</em></strong><em>A proposed Sitemap will be provided for your approval, based on the project requirements and materials.</em></p>
                
                <p><br />
                <strong>SUBMISSION OF MATERIALS AND CLIENT PORTAL ACCESS</strong></p>
                
                <p>All required&nbsp;content&nbsp;must be sent&nbsp;<strong>on or before&nbsp;[MATERIALS_DUE_DATE]</strong>.&nbsp; Please note that we can only begin the design&nbsp;and&nbsp;development&nbsp;of your project once all required contents and materials are submitted.&nbsp;</p>
                
                <p>You may upload your materials through this portal:</p>
                
                <p><strong>URL:</strong>&nbsp;[WEBSITE_LOGIN_URL]</p>
                
                <p><strong>Username:</strong> [CLIENT_USERNAME]</p>
                
                <p>Your password is sent in a separate email. If you&#39;re unable to retrieve your password, you may reset your password [RESET_PASSWORD_LINK].</p>
                
                <p>Feel free to reach us should you have any concerns.</p>
                
                <p>&nbsp;</p>
                
                <p>Sincerely,</p>
                
                <p>[ADMIN_NAME]</p>
                
                <p>[ADMIN_DESIGNATION]</p>",
            ],
            [
                "name" => "Database Hosting",
                "service_type_id" => "1",
                "email_template" => "<p>Hi Ms. / Mr. [CLIENT_NAME]</p>

                <p><br />
                Greetings from iManila!</p>
                
                <p>&nbsp;</p>
                
                <p>First of all, we would like to thank you for availing our servces in <strong>iManila Database Hosting.</strong></p>
                
                <p>To recap, the package inclusions are as follows:</p>
                
                <p><strong>A. &nbsp;&nbsp; </strong></p>
                
                <p>&nbsp;</p>
                
                <p><strong>INITIAL REQUIREMENTS FROM THE CLIENT</strong><em>&nbsp;</em></p>
                
                <p>To be able to proceed, the team will require the following information:</p>
                
                <ul>
                    <li><strong>Company Logo</strong>&nbsp;<br />
                    if available, please submit it in high-resolution, JPEG or PNG format</li>
                </ul>
                
                <p>&nbsp;</p>
                
                <p><strong>SUBMISSION OF MATERIALS AND CLIENT PORTAL ACCESS</strong></p>
                
                <p>All required&nbsp;content&nbsp;must be sent&nbsp;<strong>on or before&nbsp;[MATERIALS_DUE_DATE]</strong>.&nbsp; Please note that we can only begin once all required infromation are submitted.&nbsp;</p>
                
                <p>You may upload your materials through this portal:</p>
                
                <p><strong>URL:</strong>&nbsp;[WEBSITE_LOGIN_URL]</p>
                
                <p><strong>Username:</strong> [CLIENT_USERNAME]</p>
                
                <p>Your password is sent in a separate email. If you&#39;re unable to retrieve your password, you may reset your password [RESET_PASSWORD_LINK].</p>
                
                <p>Please compile (zip/rar) and upload your materials per web page for proper file identification and management. Refer to our &quot;How to upload&quot; video.</p>
                
                <p>&nbsp;</p>
                
                <p>Feel free to reach us should you have any concerns.</p>
                
                <p>&nbsp;</p>
                
                <p>Sincerely,</p>
                
                <p>[ADMIN_NAME]</p>
                
                <p>[ADMIN_DESIGNATION]</p>",
            ],
            [
                "name" => "eCommerce Wordpress Website",
                "service_type_id" => "1",
                "email_template" => "<p>Hi Ms. / Mr. [CLIENT_NAME]</p>

                <p>Greetings from iManila!</p>
                
                <p>First of all, we would like to thank you for availing our&nbsp;<strong>iManila eCommerce WordPress Website&nbsp;package.</strong></p>
                
                <p>To recap, the package inclusions are as follows:</p>
                
                <p><strong>A. &nbsp; &nbsp;WordPress&nbsp;Website&nbsp;Development </strong><em>(please see the signed conforme for details)&nbsp;</em></p>
                
                <p><strong>B. &nbsp;&nbsp;WP Secure Silver Hosting Services&nbsp;and&nbsp;Server Support </strong><em>(please see the signed conforme for details)</em></p>
                
                <p><strong>C. &nbsp; &nbsp;Website&nbsp;Development&nbsp;Timeline&nbsp;(</strong><em>please see the signed conforme for details)</em></p>
                
                <p><br />
                <strong>INITIAL REQUIREMENTS FROM THE CLIENT</strong><em>&nbsp;</em></p>
                
                <p>To be able to proceed with the creation&nbsp;of&nbsp;your&nbsp;website, our creatives&nbsp;and&nbsp;development&nbsp;team will require the following information:</p>
                
                <ul>
                    <li><strong>Company Logo</strong>&nbsp;<br />
                    if available, please submit it in high-resolution, JPEG or PNG format</li>
                </ul>
                
                <ul>
                    <li><strong>Preferred Color/s</strong>&nbsp;<br />
                    if you have preferred colors, provide us 3 to 5 colors which we can use as your&nbsp;website&#39;s&nbsp;main color palette, if not, we will base the colors on your logo.<br />
                    &nbsp;</li>
                    <li><strong>Text Content</strong><br />
                    provide us&nbsp;all&nbsp;available content that we can use on your&nbsp;website, these will range from taglines, slogans, about the company, history, products or services offered, etc.&nbsp;</li>
                </ul>
                
                <ul>
                    <li><strong>Images&nbsp;and/or Videos</strong>&nbsp;<br />
                    submit any existing images or videos you have that we can use on your&nbsp;website&nbsp;(high-res. JPEG or PNG format for photos, MP4 format for videos); if you do not have any, we can utilize our stock photos&nbsp;and&nbsp;use images upon your approval</li>
                </ul>
                
                <ul>
                    <li><strong>Social Media Accounts</strong>&nbsp;<br />
                    send us links&nbsp;and&nbsp;URLs to your respective active social media accounts so we can integrate it on your&nbsp;website</li>
                </ul>
                
                <ul>
                    <li><strong>Contact Details</strong>&nbsp;<br />
                    contact details include: Physical address, Email address, Telephone, Fax&nbsp;and&nbsp;Mobile number, Hours&nbsp;of&nbsp;operation</li>
                </ul>
                
                <ul>
                    <li><strong>Content for the Web Pages</strong><br />
                    please provide us the&nbsp;corresponding content based on the sample site map/web pages below:</li>
                </ul>
                
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Home</p>
                
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - About Us</p>
                
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Products / Services</p>
                
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Our Clients</p>
                
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Contact Us<br />
                <br />
                <strong><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *</em></strong><em>A proposed Sitemap will be provided for your approval, based on the project requirements and materials.</em></p>
                
                <p><br />
                <strong>SUBMISSION OF MATERIALS AND CLIENT PORTAL ACCESS</strong></p>
                
                <p>All required&nbsp;content&nbsp;must be sent&nbsp;<strong>on or before&nbsp;[MATERIALS_DUE_DATE]</strong>.&nbsp; Please note that we can only begin the design&nbsp;and&nbsp;development&nbsp;of your project once all required contents and materials are submitted.&nbsp;</p>
                
                <p>You may upload your materials through this portal:</p>
                
                <p><strong>URL:</strong>&nbsp;[WEBSITE_LOGIN_URL]</p>
                
                <p><strong>Username:</strong> [CLIENT_USERNAME]</p>
                
                <p>Your password is sent in a separate email. If you&#39;re unable to retrieve your password, you may reset your password [RESET_PASSWORD_LINK].</p>
                
                <p>Feel free to reach us should you have any concerns.</p>
                
                <p>&nbsp;</p>
                
                <p>Sincerely,</p>
                
                <p>[ADMIN_NAME]</p>
                
                <p>[ADMIN_DESIGNATION]</p>",
            ],
        ];
        foreach($services as $service){
            DB::table('services')->insert($service);
        }
    }
}
