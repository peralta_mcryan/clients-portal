<?php

use Illuminate\Database\Seeder;

class SiteContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$siteContents[] = [
            [
                "title" => "Acceptable User Policy",
                "content" => "<p>The following Acceptable Use Policy (AUP) applies to all iManila Internet Hosting services clients.</p>

                <p>Policies apply automatically to all iManila clients upon subscription to the Service/s including subscription through a third party or any commercial partner or reseller.</p>
                
                <h2>I.&nbsp;Legal</h2>
                
                <p>Client agrees to use the Service/s only for lawful purposes, in compliance with all applicable laws in the Philippines<em>.&nbsp; Specific illegal activities that are prohibited include but are not limited to, the following:</em></p>
                
                <ol>
                    <li>Threatening acts or language to persons or property or otherwise harassing behavior.</li>
                    <li>Misrepresentation or fraudulent representation of products and/or services.</li>
                    <li>Transmission, distribution or storage of any material in violation of any applicable law or regulation (i.e., Pornography, Pirated materials, etc.).</li>
                    <li>Transmission, distribution or storage of any material protected by copyright, trademark, trade secret or other intellectual property right without proper authorization, and material that is obscene, defamatory, slanderous, an invasion of privacy or constitutes an illegal threat, or is otherwise illegal.</li>
                    <li>Facilitation, aid, or encouragement of any of the above activities, whether using iManila network or service by itself or via a third party&rsquo;s network or service.</li>
                    <li>Interference with a third party&rsquo;s use of iManila network or service, or ability to connect to the Internet or provide services to Internet users.</li>
                    <li>Proxy server management</li>
                </ol>
                
                <h2>II.&nbsp;Email</h2>
                
                <p>iManila applies a strong anti-spam policy. This includes spamming through open proxy servers or any other kind of unsolicited email advertising.&nbsp; If Client is found to be in violation of our SPAM policy, iManila may, at its sole discretion, restrict, suspend or terminate client&rsquo;s account.&nbsp; Furthermore, iManila reserves the right to pursue civil remedies for any costs associated with the investigation of a substantiated policy violation.&nbsp; iManila will notify law enforcement officials if the violation is believed to be a criminal offense. Client acknowledges that he/she will be held responsible for any and all SPAM-related actions which come from their account.&nbsp; <em>Specific EMAIL activities that are prohibited include, but are not limited to the following:</em></p>
                
                <ol>
                    <li>Sending of unsolicited email messages, including, without limitation, commercial advertising and informational announcements, is explicitly prohibited even if the lists are said to be opt-in.</li>
                    <li>Use of another site&rsquo;s mail server to relay mail without the express permission of the site.</li>
                    <li>It is strictly forbidden to send out unsolicited email from any other network that advertises, promotes or in any way points to a location inside iManila network.</li>
                </ol>
                
                <p>We reserve the right to refuse or terminate the Service/s of any known spammer.&nbsp; Also we reserve the right to solely determine who violates this policy on Spamming.&nbsp; Any violation may result in termination of the Service/s without refund.</p>
                
                <h2>&nbsp;III.&nbsp;Frequent Attack Target Services (IRC networks, Camfrog, etc.):</h2>
                
                <p>It is absolutely forbidden to host an IRC server that is part of or connected to another IRC network or server.&nbsp; Servers found to be connected to or part of these networks will be immediately removed from our network without notice.&nbsp; The server will not be reconnected to the network until such time that you agree to completely remove any and all traces of the IRC server, as well as provide us access to your server to confirm content deletion. It is also forbidden to host Camfrog servers or other server applications that are frequent targets of Denial of Service attacks or other types of attacks.&nbsp; Any server guilty of a second violation will result in immediate account termination.</p>
                
                <h2>IV.&nbsp;Adult content</h2>
                
                <p>It is strictly forbidden to publish adult content on our services.&nbsp; The restriction applies, without limitation, to images, videos and text.</p>
                
                <h2>V.&nbsp;System and Network Security</h2>
                
                <p>Client is prohibited from utilizing iManila services to compromise the security or tamper with system resources or accounts on computers at the Premises or at any third party site. <em>Specific System and Network activities that are prohibited include, but are not limited to</em> <em>the following:</em></p>
                
                <ol>
                    <li>Threatening acts or language to persons or property or otherwise harassing behavior.</li>
                    <li>Use or distribution of tools designed for compromising security.</li>
                    <li>Unauthorized access to or use of data, systems or networks, including any attempt to probe, scan or test the vulnerability of a system or network or to breach security or authentication measures without express authorization of the owner of the system or network.</li>
                    <li>Unauthorized monitoring of data or traffic on any network or system without express authorization of the owner of the system or network.</li>
                    <li>Deliberate attempts to overload a system and broadcast attacks.</li>
                    <li>Forgery of any TCP-IP packet header or any part of the header information in an email or a newsgroup posting.</li>
                    <li>Intentional or negligent transmission of files containing a computer virus or corrupted data.</li>
                </ol>
                
                <h2>VI. Resource Usage</h2>
                
                <p>The Service/s is/are for corporate use.&nbsp; It is strictly forbidden to make your account resources available (whether for free or pay) to the general public.&nbsp; Without limitation, this applies to usage of an account to provide public online services like statistics, image hosting, free web hosting or any other similar service.&nbsp; At anytime, all hosting accounts must use an acceptable level of resources and stay below 2% of the total system resources.</p>
                
                <h2>VII.&nbsp;Violation</h2>
                
                <p>iManila, in its sole discretion, will determine what action shall be taken in response to a violation on a case-by-case basis. Violation of this AUP could also subject Client to criminal or civil liability.&nbsp; iManila may block access at the router level to the Client&rsquo;s Equipment involved. If iManila believes, in its sole discretion, that a violation of this AUP has occurred, such action may also include, but is not limited to, temporary or permanent blocking of access to Client&rsquo;s Equipment or data, and the suspension or termination of Client&rsquo;s services under this Agreement.&nbsp; iManila may involve, and if ordered by the courts, will also cooperate with law enforcement authorities in investigating suspected lawbreakers. iManila has no obligation to provide warnings under any circumstances, and can terminate the account without prior notice if the Client violates this policy.</p>
                
                <h2>VIII. Rights Reserve</h2>
                
                <p>iManila reserves the right to modify this AUP at any time without notice. Client is responsible for all use of the Service/s by itself, its employees, agents, contractors, invitees and clients, whether such use is with or without the consent of Client. iManila can require a signed copy of this document.</p>"
            ],

            [
                "title" => "Terms of Service",
                "content" => "<h1>Acceptable Use Policy for iManila Internet Hosting Services</h1>

                <p>By subscribing to the Internet Hosting Service/s of iManila, Client accepts the policies listed in this document and abide to respect and follow them.&nbsp; The service order described in this document or Conforme Letter is the package description presented in the website or service order document in case of collocation and dedicated server services.</p>
                
                <p>Unless otherwise specified, the use of &ldquo;us,&rdquo; &ldquo;we,&rdquo; and &ldquo;our&rdquo; shall refer to iManila and its Internet Hosting products, services and subsidiaries.</p>
                
                <h2>I.&nbsp;General</h2>
                
                <p>iManila agrees to provide services described in the Service Order/s signed by the parties herein called &ldquo;Service/s&rdquo; to Client, subject to the following Terms of Service (TOS).&nbsp; Use of the Services constitutes acceptance and agreement to these Terms of Service and all attachments and policies as Acceptable Use Policy (AUP), No Spam Policy (NSP) and Privacy Policy.</p>
                
                <p>iManila will provide Service/s for the agreed fees as specified in the website or on Service Order or Conforme Letter.&nbsp; Client acknowledges that the service fees have been properly communicated and will therefore agree to pay such service fees.&nbsp; In return, Client is allowed to use the Service/s for business and personal websites.</p>
                
                <p>iManila reserves the right to refuse or discontinue the Service/s at its own discretion.&nbsp; As such, Client information will be verified prior to account activation.&nbsp; Consequently, any violation of the Terms of Service and other policies could result in warning, suspension and probable termination of the account.&nbsp; Accounts terminated for policy violations shall not be refunded.</p>
                
                <h2>II.&nbsp;Account Activation</h2>
                
                <p>iManila will activate the account upon payment of agreed initial fees by Client.&nbsp; Additionally, we will also validate the client information for protection against fraud.&nbsp; Providing false client information may result in non-activation, cancellation and termination of the account.</p>
                
                <p>iManila will also require a valid email address which is NOT at the domain being registered with us.&nbsp; This 3rd party email address will be a primary contact email for any issues with the account (i.e., Billing, Abuse, Service Suspension, etc.).&nbsp; It is highly urged that this account be kept active during the duration of the Service/s.&nbsp; We will not be responsible for any expiration of Service/s due to noncurrent or inactive email address.&nbsp; Please keep contact information updated by emailing our service team.</p>
                
                <h2>III.&nbsp;Term</h2>
                
                <p>The client agrees to be bound by the service term selected in the Order Form or Service Order or Conforme Letter that requires a minimum duration of time.&nbsp; Client agrees to pay in advance for One (1) Year service term.</p>
                
                <h2>IV.&nbsp;Payment</h2>
                
                <p>iManila will require payment for the activation and continued use of the Service/s.&nbsp; Set-up fees may be charged if applicable in the service order.</p>
                
                <p>All payments, unless expressly provided, shall be payable in Philippine Pesos currency and Client agrees to pay all taxes applicable to the Service/s.</p>
                
                <p>Renewal Notice and Invoice shall be sent to the Client thirty (30) days before the expiration of the term.&nbsp; It shall be sent by email to the primary email contact provided by client. If payment is not made after 72 hours of lapse of expiry, your account will be terminated with no further notice.</p>
                
                <p>Accounts that have expired beyond thirty (30) days can no longer be renewed and all account records (email and website) are erased.&nbsp; Restoration beyond thirty (30) days, if possible, may require additional charges.</p>
                
                <h2>V.&nbsp;Delinquent Accounts</h2>
                
                <ol>
                    <li>Failure to pay Invoices due on Hosting Plans and Reseller Plans shall result to the&nbsp; suspension of the Service/s thirty (30) days after the due date, and then termination and erasure of accounts ninety (90) days after the due date.</li>
                    <li>&nbsp;iManila is not and shall not be held responsible for data integrity for any accounts that are suspended, interrupted, disconnected or terminated for non-payment of Service/s.</li>
                    <li>The termination or suspension of the Service/s shall not relieve you of responsibility for the payment of all accrued service fees, and any collection fees to which iManila may be entitled under the Agreements or under applicable law. Overdue amounts may be turned over to an external agency for collection, in which case you agree to pay to iManila a &ldquo;Processing and Collection&rdquo; Fee of not less than One Thousand Pesos (Php 1,000) or more than Five Thousand Pesos (Php 5,000). Returned checks shall be charged a service fee of One Thousand Pesos (Php 1,000)</li>
                </ol>
                
                <h2>VI.&nbsp;Account Cancellation</h2>
                
                <p>A. Clients may voluntarily cancel their account at any time, with or without any reasons, subject to the following:</p>
                
                <ol>
                    <li>Web Hosting. Request for cancellation can be sent anytime to <a href=\"mailto:support@localhost/imanila\">support@localhost/imanila</a></li>
                    <li>Virtual Servers and Dedicated Server. Request for cancellation shall be sent to <a href=\"mailto:support@localhost/imanila\">support@localhost/imanila</a> thirty (30) working days before the renewal date.</li>
                    <li>Colocation. Cancellation is subject to terms outlined in the colocation contract.</li>
                </ol>
                
                <p>B. You understand and agree that iManila does not provide any refunds, pro-rated or otherwise, in connection with cancellations. No more charges shall be billed to the account upon cancellation. However, cancellations made on or after the renewal date shall be subject to renewal fees.</p>
                
                <h2>VII.&nbsp;Backup and Restoration</h2>
                
                <p>Client is solely responsible for management and backup of all data, updates, patches and upgrades to any software being used in the Service/s.</p>
                
                <p>It is highly recommended that Client creates a backup of their account data in an off-site facility for contingency measures.&nbsp; iManila will not be held responsible for any loss of data under any circumstances.</p>
                
                <p>iManila does periodic backups of active and paying accounts for internal administration requirements.&nbsp; In the event that Client may need to recover data from backup, we will use utmost effort to restore data to their account. We will not provide any compensation for any kind for lost, outdated, incomplete or corrupted data in the event that our backups do not function properly.</p>
                
                <p>This recovery and restoration from our backup shall be charged at an agreed fee of Php 1,500.00 per instance. Other support services are available at additional fees.</p>
                
                <h2>VIII.&nbsp;Governing Law</h2>
                
                <p>The Agreements shall be governed by and construed in accordance with the laws of the Philippines, without regard to choice of law or conflicts of law provisions that would cause the application of the law of another jurisdiction.</p>
                
                <h2>IX.&nbsp;Notices</h2>
                
                <ol>
                    <li>From iManila to Client</li>
                </ol>
                
                <p>iManila will notify Client through e-mail of any notices that require us to do so under this Agreement. E-mail will be sent through Client&rsquo;s most current e-mail address as provided to iManila.</p>
                
                <p>By entering this Agreement, Client consents to receive notices via e-mail. Client is held responsible for ensuring that the e-mail address iManila has of Client is current and updated. iManila shall not be held responsible for any lost, misdirected, bounced, forwarded, or undeliverable e-mail that iManila sends to the most current email address Client has provided.</p>
                
                <p>From Client to iManila</p>
                
                <p>Unless otherwise specified in this Agreement, notices to iManila shall be sent to the following mailing address and email:</p>
                
                <ol>
                    <li>Mailing Address7/F Hanston Building<br />
                    F. Ortigas Jr. Avenue (formerly Emerald Avenue)<br />
                    Ortigas Center, Pasig City 1605<br />
                    Philippines</li>
                    <li>Email Address</li>
                </ol>
                
                <ul>
                    <li>Management: <a href=\"mailto:info@imanila.ph\">info@imanila.ph</a></li>
                    <li>Sales: <a href=\"mailto:sales@imanila.ph\">sales@imanila.ph</a></li>
                    <li>Billing: <a href=\"mailto:billing@imanila.ph\">billing@imanila.ph</a></li>
                    <li>Technical Support: <a href=\"mailto:support@imanila.ph\">support@imanila.ph</a></li>
                    <li>Marketing: <a href=\"mailto:marketing@imanila.ph\">marketing@imanila.ph</a></li>
                </ul>
                
                <h2>X.&nbsp;Entire Agreement</h2>
                
                <ol>
                    <li>The Agreement comprises the entire agreement between Client and iManila. This supersedes any prior or previous agreements between Client and iManila with respect to the subject matter of the Agreements; provided, however, that Client agrees to be subject to any additional terms and conditions of which iManila notifies from time to time, pursuant to the Agreements.</li>
                    <li>The Agreements may not be modified orally.</li>
                </ol>
                
                <h2>XI.&nbsp;Violation of Laws</h2>
                
                <ol>
                    <li>Client represents and warrants to iManila that it has complied, and will comply, with all applicable laws and regulations, of any applicable jurisdiction, including but not limited to the Philippines and the United States of America, and that each party has not engaged in, and shall not engage in, any illegal or improper action, and has not made, and shall not make, any improper payment to influence or effectuate any contracts regarding the Service/s and/or the Agreements.</li>
                    <li>Client agrees not to engage in any activity that violates any international or local laws applicable to the Service/s described in the Agreements. iManila reserves the right to discontinue the provision of Service/s to any client that it deems, in its sole discretion and/or upon notice by the proper party or government authority, to have violated any conditions of the Agreements or applicable laws. iManila will cooperate with all law enforcement agencies in relation to alleged violations of laws.</li>
                </ol>
                
                <h2>XII.&nbsp;Legal and Intellectual Property Rights</h2>
                
                <p>Any material or data used in the Service/s shall be the full responsibility of Client and may be subject to Philippine (or other countries&rsquo;) laws on copyright, trademarks and intellectual property.&nbsp;&nbsp; Such data or material will not contain anything leading to an abusive or unethical use of the Internet. All images and content published on the Internet should be rightfully owned and/or licensed to the Client.&nbsp; Abusive and unethical material include, but are not limited to, pornography, obscenity, nudity, violations of privacy, computer viruses, harassment, any illegal activity, spamming, advocacy of illegal activity, and any infringement of privacy.</p>
                
                <p>The use of a domain name that infringes or violates any trademark, service mark or similar rights of a third party are not acceptable to the Service/s and may be grounds for the termination of the Service/s.</p>
                
                <p>Client agrees to use the Service/s only for lawful purposes, in compliance with all applicable laws in the Philippines<em>.&nbsp; Specific illegal activities that are prohibited include but are not limited to, the following:</em></p>
                
                <ol>
                    <li>Threatening acts or language to persons or property or otherwise harassing behavior.</li>
                    <li>Misrepresentation or fraudulent representation of products and/or services.</li>
                    <li>Transmission, distribution or storage of any material in violation of any applicable law or regulation (i.e., Pornography, Pirated materials, etc.).</li>
                    <li>Transmission, distribution or storage of any material protected by copyright, trademark, trade secret or other intellectual property right without proper authorization, and material that is obscene, defamatory, slanderous, an invasion of privacy or constitutes an illegal threat, or is otherwise illegal.</li>
                    <li>Facilitation, aid, or encouragement of any of the above activities, whether using iManila network or service by itself or via a third party&rsquo;s network or service.</li>
                    <li>Interference with a third party&rsquo;s use of iManila network or service, or ability to connect to the Internet or provide services to Internet users.</li>
                    <li>Proxy server management</li>
                </ol>
                
                <h2>XIII.&nbsp;Indemnity</h2>
                
                <p>Client agrees to defend, indemnify and hold harmless iManila, as well as its parents, subsidiaries, officers, employees, agents, successors or assigns against any losses, claims, damages, liabilities, penalties, actions, proceedings or judgments (collectively, &ldquo;Losses&rdquo;) to which we may become subject to and which Losses arise out of, or relate to the Agreements or Client&rsquo;s use of the Service/s. Client also agrees to reimburse us for all legal and other expenses, including reasonable attorneys&rsquo; fees we may incur in connection with the investigation, defense, or settlement of any losses, whether or not in connection with pending or threatened litigation in which we are made a party.</p>
                
                <h2>XIV.&nbsp;&nbsp; Force Majeure</h2>
                
                <p>iManila shall not be liable or deemed to be in default for any delay or failure in performance of its obligations under the Agreements or other documents, as well as interruption of Service/s resulting directly or indirectly from acts of God, civil or military authority, any law, order proclamation, regulation, ordinance, demand or requirement of any governmental authority, acts of public enemy, war, terrorism, riot, civil disturbance, insurrection or other violence, explosion or other casualty or accident, fire, explosion, earthquake, flood, the elements, strike or labor dispute, shortage of suitable parts, materials, labor or transportation, magnetic interference, interruption of electrical power or other utility service, unavailability of any telecommunications or wireless service or connection to any telecommunications or wireless service, or any cause beyond the reasonable control of iManila.</p>
                
                <h2>XV.&nbsp;No Waiver of Rights by iManila</h2>
                
                <p>No course of dealing and no delay in exercising, or omission to exercise, any right, power or remedy accruing to iManila under the Agreements shall impair any such right, power or remedy or be construed to be a waiver thereof or an acquiescence therein; nor shall the action of iManila in respect of such default or circumstance, or any acquiescence by it thereto, affect or impair any right, power or remedy of iManila in respect of any other default or circumstance, whether similar or not.</p>
                
                <h2>XVI.&nbsp;Abuse</h2>
                
                <p>We here at iManila are proud of our excellent service because its aim is to &lsquo;improve lives through technology&rsquo;. Therefore, we respect all people, most especially our clients; so it is only natural that we expect the same.&nbsp; If anyone attempts to malign, threaten, or cause harm to us and our network, we will immediately terminate the Service/s without refund.&nbsp; In addition, we will pursue justice to the full extent of the law. <em>Abusive conduct includes, but is not limited to, the following behaviors:</em></p>
                
                <ol>
                    <li>Repeatedly addressing members of our staff in a demeaning or rude manner.</li>
                    <li>Using profanity in any oral or written communications with our staff, by any medium of communication, including but not limited to e-mail, instant messages, chat, text messaging, fax, postal mail, telephone, voice over Internet Protocol (VoIP), or in-person communication.</li>
                    <li>Yelling or shouting at our staff.</li>
                    <li>Deliberately using all capital (uppercase) letters in any written communication to our staff.</li>
                    <li>Insulting our staff because of their personal characteristics, or on the basis of their race, ethnicity, national origin, sex, sexual orientation, religion, or housing or economic status.</li>
                    <li>Deliberately providing false information to our staff for the purpose of harassing them or wasting their time.</li>
                </ol>
                
                <h2>XVII.&nbsp;Assignment</h2>
                
                <p>Client shall not assign or attempt to assign its obligations under the Agreements without iManila&rsquo;s prior and express written consent to such assignment. iManila may assign any or all of its rights and obligations under the Agreements at any time without prior notice to or consent of Client.</p>
                
                <h2>XVIII.&nbsp;&nbsp; Arbitration and Venue</h2>
                
                <ol>
                    <li>Any controversy or claim arising out of or relating to the Agreements or any breach thereof in excess of Fifteen Thousand Pesos (PhP 15,000) shall be settled by arbitration in any competent courts of Pasig City, Philippines.</li>
                    <li>Jurisdiction and venue for arbitration or litigation of any dispute, controversy, or claim arising out of, in connection with, or in relation to the Agreements, or the breach thereof, shall be proper only be in Pasig City, Philippines, to the exclusion of all other courts, venues and/or jurisdictions.</li>
                </ol>
                
                <h2>XIX. Severability</h2>
                
                <p>In the event that any portion of this Agreement is held to be unenforceable, the unenforceable portion shall be construed in accordance with applicable law as nearly as possible to reflect the original intentions of the parties hereto, and the remainder of the provisions shall remain in full force and effect.</p>
                
                <h2>XX.&nbsp;System Resources</h2>
                
                <p>iManila limits Clients on the system resources of Shared Web Hosting Service to assure the quality of the service.&nbsp; These system resources are consumed by applications and web services.</p>
                
                <p>Here are the detailed Email and Server Limits of the Service:</p>
                
                <ul>
                    <li>2% System Resource Limit (CPU &amp; RAM)</li>
                    <li>700 Email messages per domain per hour</li>
                    <li>Email Attachments is limited to 20MB via Webmail and 10MB via Email Client (Outlook, Apple Mail, etc.)</li>
                    <li>PHP upload limit of 20MB</li>
                    <li>PHP memory limit of 32MB</li>
                </ul>
                
                <p>Clients that go beyond the Email and Server limits shall be temporarily suspended to avoid any server load issues that may affect other clients hosted in the service.&nbsp; We shall notify the Client with the resource violation and they must resolve the issue within 72 hours with the following possible solutions:</p>
                
                <ul>
                    <li>Client must resolve the situation by using less resources.</li>
                    <li>Client must upgrade to dedicated server or VPS solution.</li>
                </ul>
                
                <p>If the violation is not resolved within 72 hours, we will have no choice but to terminate the service.&nbsp; This rule is to prevent abuse on our Service/s.</p>
                
                <p>iManila reserves the right to change or alter these limitations without prior notice.</p>
                
                <h2>XXI.&nbsp;Third-Party Applications Support</h2>
                
                <p>iManila does not guarantee or provide any free technical support for any open-source or free web application available in CPanel such as Fantastico and Softaculous.</p>
                
                <h2>XXII.&nbsp;Disclaimer</h2>
                
                <p>iManila shall not be held responsible for any harm or damages that the Client&rsquo;s business may suffer.&nbsp; We do not make any warranties of any kind and the Service/s are provided as is.&nbsp; We disclaim any and all warranties of non-infringement of any third party rights and any implied warranties including but not limited to implied warranties of merchantability and fitness for a particular purpose.&nbsp; This includes the loss of data brought about by delays, none or mistaken implementation, mistaken and service interruption even caused by our employees. Client is solely responsible for the selection, use and suitability of the Service/s.</p>
                
                <p>iManila expressly declares that by accepting the Service/s, Client will indemnify us for any violation of any law or our policies that will result in the loss for us or bringing of any claims against us.&nbsp; As such if we are sued because of your activities using our Service/s, you will pay for any damages awarded against us plus cost and legal fees.</p>",
            ],

            [
                "title" => "Service Contract",
                "content" => "<p>asdasd ad asd asdsdad asd</p>",
            ],

            [
                "title" => "How to pay your bills",
                "content" => "<p><strong>BUSINESS OFFICE</strong>: Accepts cash, check and credit card outright payments. Just drop on over to our office at 7/F Hanston Building, F. Ortigas Jr. Road, Ortigas Center, Pasig City, Philippines.</p>

                <p><strong>DIRECT PAYMENT PICK-UP:</strong> We have our team of mobile collectors, if you need the convenience of a payment pick-up, just call us at +632 490.0000 Local 3010 and let us know your pick-up details (Name, Address and Date/Time of pick-up) and we will send over our collectors to your doorstep.</p>
                
                <p><strong>BANKS/OVER-THE-COUNTER:</strong> Fill out the <strong>DEPOSIT SLIP</strong> for the account of <strong>iManila</strong>. Be sure to indicate clearly the amount to be deposited with the correct Account Information. See our Bank Details below.</p>
                
                <p><strong>PAYPAL</strong></p>
                
                <p>Email Address: <a href=\"mailto:paypal@imanila.ph\">paypal@imanila.ph</a></p>
                
                <table border=\"1\" style=\"width:100%\">
                    <tbody>
                        <tr>
                            <td>Account Name:</td>
                            <td>CLOUDWORX, INC. (IMANILA)</td>
                        </tr>
                        <tr>
                            <td>Account Number:</td>
                            <td>002-860-249-261</td>
                        </tr>
                        <tr>
                            <td>Service</td>
                            <td>Over-the-Counter Payment &ndash; Regular Deposit</td>
                        </tr>
                        <tr>
                            <td>Branch of Account</td>
                            <td>Strata 100 &ndash; Ortigas</td>
                        </tr>
                        <tr>
                            <td>Branches</td>
                            <td>Nationwide</td>
                        </tr>
                    </tbody>
                </table>
                
                <p><strong><em>*All Checks must be payable to CLOUDWORX, INC. (IMANILA)</em></strong></p>
                
                <p><strong>BDO PESO Savings Account</strong></p>
                
                <table border=\"1\" style=\"width:100%\">
                    <tbody>
                        <tr>
                            <td>Account Name:</td>
                            <td>CLOUDWORX,INC.</td>
                        </tr>
                        <tr>
                            <td>Account Number:</td>
                            <td>002860249261</td>
                        </tr>
                        <tr>
                            <td>Service</td>
                            <td>Over-the-Counter Payment &ndash; Regular Deposit</td>
                        </tr>
                        <tr>
                            <td>Branch</td>
                            <td>Strata 100 &ndash; Ortigas Branch</td>
                        </tr>
                    </tbody>
                </table>
                
                <p><strong>BDO DOLLAR Savings Account</strong></p>
                
                <table border=\"1\" style=\"width:100%\">
                    <tbody>
                        <tr>
                            <td>Account Name:</td>
                            <td>CLOUDWORX,INC.</td>
                        </tr>
                        <tr>
                            <td>Account Number:</td>
                            <td>102860248230</td>
                        </tr>
                        <tr>
                            <td>SWIFT CODE:</td>
                            <td>BNORPHMM &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Branch</td>
                            <td>Strata 100 &ndash; Ortigas Branch</td>
                        </tr>
                    </tbody>
                </table>
                
                <p><strong>EASTWEST BANK PESO Checking Account</strong></p>
                
                <table border=\"1\" style=\"width:100%\">
                    <tbody>
                        <tr>
                            <td>Account Name:</td>
                            <td>CLOUDWORX,INC. DBU THE NAME AND STYLE OF IMANILA</td>
                        </tr>
                        <tr>
                            <td>Account Number:</td>
                            <td>112-02-00667-3</td>
                        </tr>
                        <tr>
                            <td>Service</td>
                            <td>Over-the-Counter Payment &ndash; Regular Deposit</td>
                        </tr>
                        <tr>
                            <td>Branch</td>
                            <td>Emerald Branch</td>
                        </tr>
                    </tbody>
                </table>
                
                <p><strong>EASTWEST BANK DOLLAR Savings Account</strong></p>
                
                <table border=\"1\" style=\"width:100%\">
                    <tbody>
                        <tr>
                            <td>Account Name:</td>
                            <td>CLOUDWORX,INC. DBU THE NAME AND STYLE OF IMANILA</td>
                        </tr>
                        <tr>
                            <td>Account Number:</td>
                            <td>112-70-00069-1</td>
                        </tr>
                        <tr>
                            <td>SWIFT CODE:</td>
                            <td>EWBCPHMM</td>
                        </tr>
                        <tr>
                            <td>Branch</td>
                            <td>Emerald Branch</td>
                        </tr>
                    </tbody>
                </table>
                
                <p>To ensure proper payment postings, you may fax (+632 634.5139) or email (<a href=\"mailto:billing@imanila.ph\">billing@imanila.ph</a>) the Deposit Slips / Receipts. Kindly indicate your name and company name. For clarifications on your billings and payments, please feel free to call us at +632 490.000 local 3010 (Billing Department).</p>",
            ],

            [
                "title" => "Hosting FAQ",
                "content" => "<p>1)What are the hosting packages offered by iManila?</p>

                <p>You may choose from the Four (4) iManila Hosting Packages that may suit your need. There is the Bronze package, recommended for individuals that costs around Php 2,000. To view our complete package, go to our Services page.</p>
                
                <p>2)How much for a hosting account?</p>
                
                <p>Price will vary, depending on the amount of disk space you will avail.</p>
                
                <p>3)What is Monthly Transfer/Bandwidth?</p>
                
                <p>It is the amount of data moving in and out of our server due to your website. The number of visitors in your website and email usage will determine your bandwidth consumption.</p>
                
                <p>4)How do I manage my account?</p>
                
                <p>All iManila Hosting packages come along with an iManila CPanel (Control Panel) access to manage your account. Upon signing up,we provide you a temporary login and password to a CPanel Access for your management.</p>
                
                <p>5)What is CPanel?</p>
                
                <p>CPanel is a popular web-based control panel used to administer and/or manage your web site. All the facets in controlling your web site are available in CPanel like file management, logs, databases, email and so on. When you login to your CPanel screen, you&rsquo;ll see the account information which you can manage on your own.</p>
                
                <p>6What are iManila&rsquo;s DNS&rsquo;s?</p>
                
                <ul>
                    <li>ns6.imanila.ph (203.167.7.10)</li>
                    <li>ns7.imanila.ph (203.167.7.14)</li>
                </ul>",
            ],

            [
                "title" => "Domain FAQ",
                "content" => "<p>1)What is a Domain Name?</p>

                <p>It is your unique address online. Domain names are used to locate websites and/or email in the internet.</p>
                
                <p>2)Do I need a Domain Name?</p>
                
                <p>Yes, you need a domain name for a hosting account to upload your website, create your email address.</p>
                
                <p>3)What is the difference between Top Level domain (.com) from a Secondary Level domain (.com.ph)?</p>
                
                <p>Dot Com domains (.com) was the first domain style to be introduced, and is considered a Top Level Domain or TLD as it features only one suffix. Top level domains also cover any other domain name form that only has one suffix &ndash; for example; .net, .info, .biz, and so on.</p>
                
                <p>Second Level Domains or 2LDs are domain names that contain another level of qualification to this. For example; .com.au is a second level domain style as it contains an additional suffix after the .com.</p>
                
                <p>4)How long does it take for registering a new domain?</p>
                
                <p>Registry is within 24 hour; however, domain propagation may take 24-48 hours.</p>
                
                <p>5)Do you support sub-domain names for the hosting package availed?</p>
                
                <p>Yes, configured upon request.</p>
                
                <p>Sub-Domains are domain name extension placed at the beginning of the domain name. For example, wap.client.com is a sub-domain of client.com and &lsquo;wap&rsquo; being the sub-domain extension. You are allowed to create up to 5 sub-domains in our web hosting as long as you do not exceed your plan&rsquo;s disk space allocation.</p>
                
                <p>6)What happens if I exceed my disk space allocation?</p>
                
                <p>We will recommend upgrading client hosting plan. Or, you may simply delete unnecessary files to lower disk space usage.</p>
                
                <p>7)What happens if I exceed my bandwidth limits?</p>
                
                <p>Client hosting account will be suspended whenever the bandwidth limit is reached. It will be activated on the first day of the next month. We highly recommend upgrading clients hosting plan to provide ample bandwidth allocation for their website.</p>",
            ],

            [
                "title" => "Email Hosting FAQ",
                "content" => "<p>1)What are email accounts?</p>

                <p>It is the email address associated with your e-mail hosting account.</p>
                
                <p>2)How do I set up my email account?</p>
                
                <p>Through the CPanel, read the tutorial How to add email accounts to get an enhanced answer.</p>
                
                <p>3)What email server settings do I use to send email?</p>
                
                <p>Basically, if you want to send email you will have to use your ISP&rsquo;s mail server for your outgoing email since they know who you are based on your IP address.</p>
                
                <p>Email servers that will send email for anyone are known as &ldquo;open relays&rdquo;. If they can find them, spammers will abuse open relays to send their spam for them, so much so that nowadays any email server acting as an open relay risks being put on &ldquo;black hole lists&rdquo; so that any email originating from that server is refused.</p>
                
                <p>4)What is Web Mail Access?</p>
                
                <p>It is the ability to check email using your web browser.</p>",
            ],

            [
                "title" => "Website Hosting FAQ",
                "content" => "<p>1)How do I update my website?</p>

                <p>You may update your website through a CPanel (Control Panel) access, a browser based control management tool.</p>
                
                <p>For further information, contact us at 634-5140.</p>
                
                <p>2)What is a CGI-BIN/Perl/PHP?</p>
                
                <p>These are programming languages available in our web hosting service and a requirement for creating dynamic websites.</p>
                
                <p>3)What are the Web programming features supported by iManila Hosting?</p>
                
                <p>IManila Hosting packages support Linux Operating System, PHP 5.0 , MYSQL, CGI Center&rsquo;s, softaculous/fantastico. However, if you must use PHP, contact us and we&rsquo;ll see what we can do for you.</p>
                
                <p>4)What is Web Server SSL?</p>
                
                <p>SSL (&ldquo;Secure Sockets Layer&rdquo;) encrypts Internet TCP/IP connections allowing secure e-commerce transactions.</p>
                
                <p>5)Is it possible for site editors to administrate the website in a secure protocol (https)?</p>
                
                <p>Yes. This feature requires additional IP address and SSL Certificate. The whole procedure looks in the following way:</p>
                
                <p>Client conforme &ndash; Client should sign conforme and settle payment.<br />
                Required Documents &ndash; Client provides necessary documents required.<br />
                SSL Certificate &ndash; Our technical team will source out a SSL supplier from a reliable supplier.<br />
                IP address &ndash; We will provide an IP allocated for your use.<br />
                We configure server on the received IP and SSL Certificate. Now that everything works fine, we inform the client about it.</p>",
            ],

            [
                "title" => "Database Hosting FAQ",
                "content" => "<p>1)Is it possible to manage a database with my hosting package?</p>

                <p>Database Hosting are ONLY available on Gold and Platinum Packages. iManila Hosting Packages have only one MySQL database available, with no multiple databases support.</p>
                
                <p>2)What is a my SQL Database?</p>
                
                <p>MySQL is an open-source database. It is used to store information or serve dynamic data for your website.</p>",
            ],

            [
                "title" => "Client Email Notification",
                "content" => "<p>Hi Ms. / Mr. [CLIENT_NAME]</p>

                <p><br />
                Greetings from iManila!</p>
                
                <p>First of all, we would like to thank you for availing our&nbsp;services in <strong>iManila.</strong> We are pleased to inform you that you are now registered in our portal.</p>
                
                <p>&nbsp;</p>
                
                <p>You may check the status of your project and upload your materials through this portal:</p>
                
                <p><strong>URL:</strong>&nbsp;[WEBSITE_LOGIN_URL]</p>
                
                <p><strong>Username:</strong> [CLIENT_USERNAME]</p>
                
                <p><strong>Password:</strong>&nbsp;[CLIENT_PASSWORD]</p>
                
                <p>&nbsp;</p>
                
                <p>Feel free to reach us should you have any concerns.</p>
                
                <p>Sincerely,</p>
                
                <p>[ADMIN_NAME]</p>
                
                <p>[ADMIN_DESIGNATION]</p>",
            ],
        ];

        foreach($siteContents as $siteContent){
            DB::table('site_contents')->insert($siteContent);
        }
    }
}
