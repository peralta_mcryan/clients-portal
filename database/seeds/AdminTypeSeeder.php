<?php

use Illuminate\Database\Seeder;

class AdminTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
 public function run()
    {
        $fileFolders = [
            [
                "id" => "1",
                "name" => "Account Executive",
            ],
            [
                "id" => "2",
                "name" => "Project Manager",
            ],
            [
                "id" => "3",
                "name" => "API",
            ]
           
        ];
        foreach($fileFolders as $fileFolder){
            DB::table('admin_types')->insert($fileFolder);
        }
    }
}
