<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(FileFolderSeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(ServiceTypeSeeder::class);
        $this->call(SiteContentSeeder::class);

    }
}
