<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTaleIssueList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issue_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id');
            $table->integer('project_id');
            $table->text('control_no');
            $table->text('description');
            $table->integer('status');
            $table->integer('priority');
            $table->integer('assigned_to');
            $table->integer('created_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issue_lists');
    }
}
