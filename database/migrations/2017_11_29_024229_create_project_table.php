<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('status')->nullable();
            $table->integer('account_id')->nullable();
            $table->integer('assigned_ae')->nullable();
            $table->integer('user_id')->nullable();
            $table->text('inclusions')->nullable();
            $table->text('selected_theme')->nullable();
            $table->text('requirements')->nullable();
            $table->string('service_contract')->nullable();
            $table->integer('service_type_id')->nullable();
            $table->integer('assigned_ae')->nullable();
            $table->date('project_end')->nullable();
            $table->integer('project_days')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
