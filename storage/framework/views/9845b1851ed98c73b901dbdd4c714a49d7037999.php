<?php $__env->startSection('content'); ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><?php echo e($project->name); ?> <?php echo e($fileFolder->folder_name); ?></h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel tabbed-panel panel-primary">
                            <div class="panel-heading clearfix">
                                <div class="panel-title pull-left">
                                    <?php echo e($fileFolder->folder_name); ?>

                                </div>
                                <div class="pull-right">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Files</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab-primary-1">
                                        <form action="<?php echo e(route('client-upload-project-files', $project->id)); ?>" method="post" enctype="multipart/form-data">
                                            <?php echo e(csrf_field()); ?>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <span class="pull-left"><h4><?php echo e($fileFolder->folder_name); ?> Files</h4></span>
                                                    <a href="<?php echo e(route('my-project-show',$project->id)); ?>#tab-primary-2" class="btn btn-primary pull-right">Back</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <hr></hr>
                                                    <?php if(count($files) > 0): ?>
                                                        <?php $__currentLoopData = $files; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php
                                                                $fileUploaderId = 0;
                                                            ?>
                                                            <div class="col-lg-6">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <div class="row">
                                                                            <div class="col-xs-12" style="padding-bottom: 5px;">
                                                                                <div style="word-wrap: break-word;" class="imanila-color"><strong><?php echo e($file->filename); ?></strong></div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <a href="#">
                                                                        <div class="panel-footer">
                                                                            <a href="#" style="cursor:default">
                                                                                <span class="pull-left"><small>Uploaded By</small></span>
                                                                                <span class="pull-right">
                                                                                    <small>
                                                                                        <?php if($file->users): ?>
                                                                                            <?php echo e($file->users->name); ?>

                                                                                            <?php 
                                                                                                $fileUploaderId = $file->users->id;
                                                                                            ?>
                                                                                        <?php elseif($file->contacts): ?>
                                                                                            <?php echo e($file->contacts->first_name); ?> <?php echo e($file->contacts->last_name); ?>

                                                                                            <?php 
                                                                                                $fileUploaderId = $file->contacts->id;
                                                                                            ?>
                                                                                        <?php endif; ?>
                                                                                    </small>
                                                                                </span>
                                                                                <div class="clearfix"></div>
                                                                            </a>
                                                                            <a href="#" style="cursor:default;">
                                                                                <span class="pull-left"><small>Upload Date</small></span>
                                                                                <span class="pull-right">
                                                                                    <small>
                                                                                        <?php echo e($file->created_at); ?>

                                                                                    </small>
                                                                                </span>
                                                                                <div class="clearfix"></div>
                                                                            </a>
                                                                            <a href="<?php echo e(route('download-project-files', [$project->id, $fileFolder->id, $file->filename])); ?>">
                                                                                <span class="pull-left"><small>Download</small></span>
                                                                                <span class="pull-right"><i class="fa fa-download"></i></span>
                                                                                <div class="clearfix"></div>
                                                                            </a>
                                                                            <?php if((Auth::user()->id == $fileUploaderId) && ($file->user_id == 0)): ?>
                                                                            <a href="<?php echo e(route('client-delete-project-files', [$project->id, $fileFolder->id, $file->filename])); ?>" onclick="confirmDelete(event)">
                                                                                <span class="pull-left"><small>Remove</small></span>
                                                                                <span class="pull-right"><i class="fa fa-close imanila-color"></i></span>
                                                                                <div class="clearfix"></div>
                                                                            </a>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php else: ?>
                                                        <div class="alert alert-info">
                                                            <i class="fa fa-hand-o-right"></i>
                                                            <small>No file available.</small>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>
                <script src="<?php echo e(asset('js/projects/projects.js')); ?>"></script>
                <script>
                    function confirmDelete(e) {
                        if (confirm('Are you sure you want to delete this file?')) {
                            return true;
                        } else {
                            e.preventDefault();
                        }
                    }
                </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.clientlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>