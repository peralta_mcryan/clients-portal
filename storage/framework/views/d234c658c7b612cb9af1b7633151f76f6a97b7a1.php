<?php $__env->startSection('content'); ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Projects</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Account</th>
                                <th>Status</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Aging</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr class="clickable-row" data-href="<?php echo e(route('my-project-show', $project->id)); ?>">
                                    <td>
                                        <a href="<?php echo e(route('my-project-show', $project->id)); ?>">
                                            <?php echo e($project->name); ?>

                                        </a>
                                    </td>

                                    <td>
                                        <?php if(empty($project->accounts->name)): ?> 
                                            N / A
                                        <?php else: ?>
                                            <?php echo e($project->accounts->name); ?>

                                        <?php endif; ?>
                                    </td>

                                    <td>
                                        <?php echo e(config('constants.project_status.'.$project->status)); ?>

                                    </td>

                                    <td>
                                          <?php echo e(!empty($project->project_start)?\Carbon\Carbon::parse($project->project_start)->format('M d, Y'):'---'); ?>

                                    </td>
                                    <td>
                                        <?php echo e(\Carbon\Carbon::parse($project->project_end)->format('M d, Y')); ?>

                                    </td>


                                    <td>
                                        <?php echo e($project->getAgingDays()); ?> days
                                    </td>

                                    <td>
                                        <a href="<?php echo e(route('my-project-show', $project->id)); ?>#tab-primary-2">
                                            <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                            <span class="pull-left">&nbsp;Upload Files</span>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?php echo e(route('my-project-show', $project->id)); ?>#tab-primary-5">
                                            <span class="pull-left"><i class="fa fa-window-maximize"></i></span>
                                            <span class="pull-left">&nbsp;Select Theme</span>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <?php echo e($projects->links()); ?>

                    </div>
                </div>
                <script src="<?php echo e(asset('js/projects/projects.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.clientlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>