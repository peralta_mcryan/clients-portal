<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Service Types</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($pageName); ?>

                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-horizontal" method="POST" action="<?php echo e($route); ?>">
                                <?php echo e(csrf_field()); ?>


                                <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                    <label for="name" class="col-md-2 control-label"><span class="asterisk-marker">*</span> Name</label>

                                    <div class="col-md-10">
                                        <input id="name" type="text" class="form-control" name="name" value="<?php echo e($serviceType->name); ?>" required autofocus>

                                        <?php if($errors->has('name')): ?>
                                            <span class="help-block">
                                                <strong><?php echo e($errors->first('name')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                  <div class="form-group<?php echo e($errors->has('broad_type') ? ' has-error' : ''); ?>">
                                    <label for="name" class="col-md-2 control-label"></label>

                                    <div class="col-md-10">
                                        <?php if($serviceType->broad_type == 0): ?>
                                            <div class="col-md-2"><input type="radio" name="broad_type" value="0" checked>None</div>
                                        <?php else: ?>
                                            <div class="col-md-2"><input type="radio" name="broad_type" value="0">None</div>
                                        <?php endif; ?>

                                        <?php if($serviceType->broad_type == 1): ?>
                                            <div class="col-md-3"><input type="radio" name="broad_type" value="1" checked>Digital Marketing</div>
                                        <?php else: ?>
                                            <div class="col-md-3"><input type="radio" name="broad_type" value="1">Digital Marketing</div>
                                        <?php endif; ?>

                                        <?php if($serviceType->broad_type == 2): ?>
                                            <div class="col-md-3"><input type="radio" name="broad_type" value="2" checked>Website</div>
                                        <?php else: ?>
                                           <div class="col-md-3"><input type="radio" name="broad_type" value="2">Website</div>
                                        <?php endif; ?>

                                    </div>
                                    <?php if($errors->has('broad_type')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('broad_type')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                               

                                <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                    <label for="name" class="col-md-2 control-label"> Files</label>
                                    <?php $__currentLoopData = $fileFolders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div class="col-md-10">
                                            <input id="service_type_file_<?php echo e($key); ?>" type="checkbox" name="service_type_file[]" value="<?php echo e($key); ?>" <?php echo e((in_array($key, $serviceTypeFile) ? 'checked' : '')); ?>> <label for="service_type_file_<?php echo e($key); ?>">&nbsp;<?php echo e($value); ?></label>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-10 col-md-offset-2">
                                        <button type="submit" class="btn btn-primary">
                                            Save
                                        </button>
                                        <a href="<?php echo e(route('service-types.index')); ?>">
                                            <button type="button" class="btn btn-default">
                                                Cancel
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>