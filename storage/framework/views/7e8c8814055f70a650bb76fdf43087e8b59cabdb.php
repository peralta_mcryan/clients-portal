<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><?php echo e($pageName); ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Project Details
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form" method="POST" action="<?php echo e($route); ?>" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="name"><span class="asterisk-marker">*</span> Project Name</label>
                                    <input id="name" name="name" class="form-control" placeholder="Project Name" value="<?php echo e($project->name); ?>" required>
                                    <?php if($errors->has('name')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('name')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="description"><span class="asterisk-marker">*</span> Project Description</label>
                                    <textarea class="form-control" id="description" name="description" required><?php echo e($project->description); ?></textarea>
                                    <?php if($errors->has('description')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('description')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="project_owner">Project Owner</label>
                                    <br>
                                    <?php if($account->id): ?>
                                        <input type="hidden" name="project_owner" value="<?php echo e($account->id); ?>">
                                        <?php echo e($account->name); ?>

                                    <?php else: ?>
                                        <?php echo e(Form::select('project_owner', $accounts, $project->account_id,['id' => 'project_owner',  'class'=>''])); ?>

                                        <?php if($errors->has('project_owner')): ?>
                                            <span class="error-block">
                                                <strong><?php echo e($errors->first('project_owner')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="project_status">Project Status</label>
                                    <?php echo e(Form::select('project_status', $projectStatus, $project->status,['id' => 'project_status',  'class'=>''])); ?>

                                    <?php if($errors->has('project_status')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('project_status')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="assigned_project_manager">Assigned Project Manager</label>
                                    <br>
                                    <?php echo e(Form::select('assigned_project_manager', $users, $project->user_id,['id' => 'assigned_project_manager',  'class'=>''])); ?>

                                    <?php if($errors->has('assigned_project_manager')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('assigned_project_manager')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="assigned_ae">Assigned AE</label>
                                    <br>
                                    <?php echo e(Form::select('assigned_ae', $ae, $project->assigned_ae,['id' => 'assigned_ae',  'class'=>''])); ?>

                                    <?php if($errors->has('assigned_ae')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('assigned_ae')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="assigned_project_manager">Service Type</label>
                                    <br>
                                    <select multiple="multiple" name="service_type[]" id="service_type">
                                        <?php $__currentLoopData = $serviceTypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($key); ?>" <?php echo e(( in_array($key, $project->projectServiceType()->pluck('service_type_id')->toArray()) ? 'selected' : '')); ?>><?php echo e($value); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <?php if($errors->has('service_type')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('service_type')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 col-md-3">
                                        <label for="account"><span class="asterisk-marker">*</span> Service Contract
                                            <?php if(!empty($serviceContract)): ?>
                                                <a href="<?php echo e($serviceContract); ?>" class="imanila-color"><?php echo e($project->service_contract); ?></a>
                                            <?php endif; ?>
                                        </label>
                                        <input type="file" id="service_contract" name="service_contract" placeholder="Service Contract">
                                         <?php if($errors->has('service_contract')): ?>
                                            <span class="error-block">
                                                <strong><?php echo e($errors->first('service_contract')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label for="project_start"><span class="asterisk-marker">*</span> Project Start</label>
                                        <input type="date" id="project_start" name="project_start" class="form-control" placeholder="Project Start" value="<?php echo e($project->project_start == null ? Carbon\Carbon::now()->format('Y-m-d') : Carbon\Carbon::parse($project->project_start)->format('Y-m-d')); ?>" >
                                        <?php if($errors->has('project_start')): ?>
                                            <span class="error-block">
                                                <strong><?php echo e($errors->first('project_start')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label for="project_days"><span class="asterisk-marker">*</span> Project Man-Days</label>
                                        <?php echo e(Form::select('project_days',array(10=>10, 45 => 45, 60 =>60),$project->project_days,['id'=>'project_days','placeholder'=>'Man days'])); ?>

                                        <?php if($errors->has('project_days')): ?>
                                            <span class="error-block">
                                                <strong><?php echo e($errors->first('project_days')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label for="project_end">Project End</label>
                                        <input type="date" id="project_end" name="project_end" class="form-control disable" placeholder="Project End" value="<?php echo e(trim($project->project_end)); ?>" readonly>
                                        <?php if($errors->has('project_end')): ?>
                                            <span class="error-block">
                                                <strong><?php echo e($errors->first('project_end')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                   
                                </div>
                                <div class="form-group">
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label for="inclusions">Inclusions</label>
                                    <textarea name="inclusions" id="inclusions"><?php echo e($project->inclusions); ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="requirements">Requirements</label>
                                    <textarea name="requirements" id="requirements"><?php echo e($project->requirements); ?></textarea>
                                </div>

                                <?php if($formSettings['show_update_remarks_field']): ?> 
                                    <div class="form-group">
                                        <label for="update_remarks"><span class="asterisk-marker">*</span> Update Remarks</label>
                                        <textarea name="update_remarks" id="update_remarks" class="form-control" required></textarea>
                                    </div>
                                <?php endif; ?>

                                <div class="form-group">
                                    <input type="checkbox" name="send_welcome_email" id="send_welcome_email" value="1" <?php echo e($formSettings['welcome_email_default'] == 1 ? 'checked' : ''); ?>> Check this box to send the welcome email to the contacts associated to this project. Otherwise, remove the check mark.
                                </div>
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="<?= route('client-project-tabular') ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <script src="<?php echo e(asset('ckeditor/ckeditor.js')); ?>"></script>
    <script src="<?php echo e(asset('js/projects/projects.js')); ?>"></script>

    <script>
        $(document).ready(function () {
            CKEDITOR.replace( 'inclusions' );
            CKEDITOR.replace( 'requirements' );
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>