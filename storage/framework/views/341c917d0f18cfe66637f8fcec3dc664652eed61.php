            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo e(config('app.url', '#')); ?>"><?php echo e(config('app.name', 'iManila')); ?></a>
                </div>

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                </ul>

                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-plus fa-fw"></i> 
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="<?php echo e(route('new-client-account')); ?>">Add Client</a></li>
                            <li><a href="<?php echo e(route('new-project')); ?>">Add Project</a></li>
                            <li><a href="<?php echo e(route('new-contact')); ?>">Add Contact</a></li>
                            <li><a href="<?php echo e(route('new-service')); ?>">Add Service</a></li>
                            <li><a href="<?php echo e(route('service-types.create')); ?>">Add Service Type</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> 
                                <?php $__env->startComponent('components.who'); ?>
                                <?php echo $__env->renderComponent(); ?>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <!-- Authentication Links -->
                            <?php if(Auth::guest()): ?>
                                <li><a href="<?php echo e(route('login')); ?>">Login</a></li>
                                <li><a href="<?php echo e(route('register')); ?>">Register</a></li>
                            <?php else: ?>
                                <?php $__env->startComponent('components.logout'); ?>
                                <?php echo $__env->renderComponent(); ?>
                            <?php endif; ?>
                        </ul>
                    </li>
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <!-- <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                </span>
                                </div>
                            </li> -->
                            <!-- <li>
                                <a href="<?php echo e(route('home')); ?>" class="active"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li> -->
                            <li class="sidebar-search">
                                <form role="form" method="GET" action="<?php echo e(route('search')); ?>">
                                    <div class="input-group custom-search-form">
                                        <input type="text" name="search_text" class="form-control" placeholder="Search..." required>
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                                <!-- /input-group -->
                            </li>
                            <li>
                                <a href="tables.html"><i class="fa fa-handshake-o fa-fw"></i> Accounts<span class="fa arrow"></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo e(route('client-account')); ?>">Clients</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('client-project-tabular')); ?>">Projects</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('change-requests.index')); ?>">C.R.O</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="<?php echo e(route('client-contact-tabular')); ?>"><i class="fa fa-address-book fa-fw"></i> Contacts</a>
                            </li>
                            
                            <li>
                                <a href="<?php echo e(route('administrator-accounts-index')); ?>"><i class="fa fa-users fa-fw"></i> Administrator Accounts</a>
                            </li>
                            
                             <li>
                                <a href="<?php echo e(route('departments.index')); ?>"><i class="fa fa-building-o"></i> Department</a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('service')); ?>"><i class="fa fa-laptop fa-fw"></i> Services</a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('service-types.index')); ?>"><i class="fa fa-laptop fa-fw"></i> Service Types</a>
                            </li>
                            <li>
                                <a href="<?php echo e(route('themes.index')); ?>"><i class="fa fa-photo fa-fw"></i> Themes</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-file-code-o fa-fw"></i> Site Content<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo e(route('site-contents-edit-user-policy',1)); ?>">Acceptable User Policy</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('site-contents-edit-terms-and-conditions',2)); ?>">Terms of Service</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('site-contents-edit-service-contract',3)); ?>">Service Contract</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('site-contents-edit-how-to-pay-bills',4)); ?>">How to pay your bills</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('site-contents-edit-hosting-faq',5)); ?>">Hosting FAQ</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('site-contents-edit-domain-hosting-faq',6)); ?>">Domain FAQ</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('site-contents-edit-email-hosting-faq',7)); ?>">Email Hosting FAQ</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('site-contents-edit-website-hosting-faq',8)); ?>">Website Hosting FAQ</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('site-contents-edit-database-hosting-faq',9)); ?>">Database Hosting FAQ</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                           

                            <li>
                                <a href="<?php echo e(route('site-contents-edit-database-hosting-faq',10)); ?>"><i class="fa fa-file-code-o fa-fw"></i> Client Email Notification</a>
                            </li>
                           
                            
                        </ul>
                    </div>
                </div>
            </nav>