<?php $__env->startSection('content'); ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><?php echo e(Auth::user()->first_name); ?> <?php echo e(Auth::user()->last_name); ?></h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Hi <?php echo e(Auth::user()->first_name); ?>! Welcome to your iManila Client's Portal!</h2>
                        From this Client Portal, you will be able to view your account and billing information as well as revisit contracts and conformes anytime for reference. Updates on project status and requirements from both our side and yours can also be downloaded and uploaded through this portal. 
                    </div>
                </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.clientlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>