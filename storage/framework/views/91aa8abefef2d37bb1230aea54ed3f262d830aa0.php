<?php $__env->startSection('content'); ?>
<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Clients</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-3">
                            <p class="text-right">
                                <a href="<?php echo e(route('new-client-account')); ?>"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                            </p>
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $accountDetails): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr class="clickable-row" data-href="<?php echo e(route('show-client-account', $accountDetails->id)); ?>">
                                                <td>
                                                    <?php echo e($accountDetails->name); ?>

                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center">
                                <?php echo e($accounts->links()); ?>

                            </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="panel tabbed-panel panel-primary">
                            <div class="panel-heading clearfix">
                                <div class="panel-title pull-left">
                                    Account Details
                                </div>
                                <div class="pull-right">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Overview</a></li>
                                        <li class=""><a href="#tab-primary-2" data-toggle="tab" aria-expanded="false">Contacts</a></li>
                                        <li class=""><a href="#tab-primary-3" data-toggle="tab" aria-expanded="false">Projects</a></li>
                                        <!-- <li class=""><a href="#tab-primary-4" data-toggle="tab" aria-expanded="false">Files</a></li> -->
                                        <li class="dropdown">
                                            <a href="#" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="<?php echo e(route('update-client-account', $account->id)); ?>"><i class="fa fa-edit"></i> Edit</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:deleteClientAccount(<?php echo e($account->id); ?>)"><i class="fa fa-close"></i> Delete</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab-primary-1">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h3><?php echo e($account->name); ?> <a href="<?php echo e(route('update-client-account', $account->id)); ?>" class="text-right"><i class="fa fa-edit imanila-color"></i> </a></h3>
                                                <hr></hr>

                                                <div class="panel panel-green">
                                                    <div class="panel-heading" style="padding-bottom: 5px;">
                                                        Address
                                                    </div>
                                                    <div class="panel-body">
                                                        <address>
                                                            <strong><?php echo e($account->name); ?></strong>
                                                            <br>
                                                            <small>
                                                                <?php echo e($account->address); ?>

                                                                <br>
                                                                <abbr title="Work Phone">W:</abbr>
                                                                <?php echo e($account->work_phone); ?>

                                                                <br>
                                                                <abbr title="Mobile Phone">M:</abbr>
                                                                <?php echo e($account->mobile_phone); ?>

                                                            </small>
                                                        </address>

                                                        <small>
                                                        <?php if(!empty($city->name)): ?>
                                                            <?php echo e($city->name); ?>,
                                                        <?php endif; ?>

                                                        <?php if(!empty($province->name)): ?>
                                                            <?php echo e($province->name); ?>,
                                                        <?php endif; ?>

                                                        <?php if(!empty($country->name)): ?>
                                                            <?php echo e($country->name); ?>

                                                        <?php endif; ?>
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="hero-widget well well-sm">
                                                    <div class="icon">
                                                        <i class="fa fa-address-card-o"></i>
                                                    </div>
                                                    <div class="options">
                                                        <a href="<?php echo e(route('new-contact-account', $account->id)); ?>" class="btn btn-primary btn-lg"><i class="glyphicon glyphicon-plus"></i> New Contact</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="hero-widget well well-sm">
                                                    <div class="icon">
                                                        <i class="fa fa-suitcase"></i>
                                                    </div>
                                                    <div class="options">
                                                        <a href="<?php echo e(route('new-project-account', $account->id)); ?>" class="btn btn-primary btn-lg"><i class="glyphicon glyphicon-plus"></i> New Project</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="tab-pane fade" id="tab-primary-2">
                                        <div class="col-lg-12">
                                            <h3><?php echo e($account->name); ?> Contacts <a href="<?php echo e(route('new-contact-account', $account->id)); ?>" class="text-right"><i class="fa fa-plus imanila-color"></i> </a></h3>
                                            <hr></hr>
                                            <?php if($account->contacts->count() > 0): ?>
                                                <?php $__currentLoopData = $account->contacts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $contact): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-green">
                                                            <div class="panel-heading">
                                                                <div class="row">
                                                                    <div class="col-xs-3" style="padding-bottom: 5px;">
                                                                        <i class="fa fa-address-card-o fa-5x"></i>
                                                                    </div>
                                                                    <div class="col-xs-9 text-right">
                                                                        <div class="huge"><?php echo e($contact->first_name); ?> <?php echo e($contact->last_name); ?></div>
                                                                        <div><?php echo e($contact->email); ?></div>
                                                                        <div><?php echo e(config('constants.contact_type.'.$contact->contact_type)); ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <a href="#">
                                                                <div class="panel-footer">
                                                                    <a href="<?php echo e(route('show-contact', $contact->id)); ?>">
                                                                        <span class="pull-left">View Details</span>
                                                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                                        <div class="clearfix"></div>
                                                                    </a>
                                                                    <a href="<?php echo e(route('update-contact', $contact->id)); ?>">
                                                                        <span class="pull-left">Edit Details</span>
                                                                        <span class="pull-right"><i class="fa fa-edit"></i></span>
                                                                        <div class="clearfix"></div>
                                                                    </a>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php else: ?>
                                                <div class="alert alert-info">
                                                    <i class="fa fa-hand-o-right"></i>
                                                    <a href="<?php echo e(route('new-contact-account', $account->id)); ?>" class="alert-link"><small>Add <?php echo e($account->name); ?>'s first Contact Person</small></a>.
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab-primary-3">
                                        <div class="col-lg-12">
                                            <h3><?php echo e($account->name); ?> Projects <a href="<?php echo e(route('new-project-account', $account->id)); ?>" class="text-right"><i class="fa fa-plus imanila-color"></i> </a></h3>
                                            <hr></hr>
                                            <?php if($account->projects->count() > 0): ?>
                                                <?php $__currentLoopData = $account->projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="col-lg-6">
                                                        <div class="panel panel-yellow">
                                                            <div class="panel-heading">
                                                                <div class="row">
                                                                    <div class="col-xs-3">
                                                                        <i class="fa fa-suitcase fa-5x"></i>
                                                                    </div>
                                                                    <div class="col-xs-9 text-right">
                                                                        <div class="huge"><?php echo e($project->name); ?></div>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <a href="#">
                                                                <div class="panel-footer">
                                                                    <a href="<?php echo e(route('show-project', $project->id)); ?>">
                                                                        <span class="pull-left">View Details</span>
                                                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                                        <div class="clearfix"></div>
                                                                    </a>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php else: ?>
                                                <div class="alert alert-info">
                                                    <i class="fa fa-hand-o-right"></i>
                                                    <a href="<?php echo e(route('new-project-account', $account->id)); ?>" class="alert-link"><small>Add this <?php echo e($account->name); ?>'s first Project</small></a>.
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab-primary-4">
                                        <div class="col-lg-12">
                                            Files Here
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>

                <script src="<?php echo e(asset('js/accounts/accounts.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>