<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><?php echo e($pageName); ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Company Details
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" method="POST" action="<?php echo e($route); ?>">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="name"><span class="asterisk-marker">*</span> Company Name</label>
                                    <input id="name" name="name" class="form-control" placeholder="Company Name" value="<?php echo e($account->name); ?>" required>
                                    <?php if($errors->has('name')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('name')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="address"><span class="asterisk-marker">*</span> Company Address</label>
                                    <input id="address" name="address" class="form-control" placeholder="Company Address" value="<?php echo e($account->address); ?>" required>
                                    <?php if($errors->has('address')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('address')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="work_phone"><span class="asterisk-marker">*</span> Work Phone</label>
                                    <input id="work_phone" name="work_phone" class="form-control" placeholder="Work Phone" value="<?php echo e($account->work_phone); ?>" required>
                                    <?php if($errors->has('work_phone')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('work_phone')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="mobile_phone">Mobile Phone</label>
                                    <input id="mobile_phone" name="mobile_phone" class="form-control" placeholder="Mobile Phone" value="<?php echo e($account->mobile_phone); ?>">
                                    <?php if($errors->has('mobile_phone')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('mobile_phone')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="country">Country</label>
                                    <?php echo e(Form::select('country', $countries, $account->country_id,['id' => 'country',  'class'=>''])); ?>

                                    <?php if($errors->has('country')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('country')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="province">Province</label>
                                    <?php echo e(Form::select('province', $provinces, $account->province_id,['id' => 'province',  'class'=>''])); ?>

                                    <?php if($errors->has('province')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('province')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="city">City</label>
                                    <?php echo e(Form::select('city', $cities, $account->city_id,['id' => 'city',  'class'=>''])); ?>

                                    <?php if($errors->has('city')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('city')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="<?= route('client-account') ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <script src="<?php echo e(asset('js/accounts/accounts.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>