<?php $__env->startSection('content'); ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Projects</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <p class="text-left">
                                    <a href="<?php echo e(route('tech-account.index')); ?>"><i class="fa fa-columns fa-1x text-left"></i></a>
                                    <a href="<?php echo e(route('techTeam-project-tabular')); ?>"><i class="fa fa-list fa-1x text-left"></i></a>
                                </p>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-right">
                                    
                                </p>
                            </div>
                        </div>
                            <div class = "col-lg-12">
                            <form role="form" method="GET" action="<?php echo e(route('techTeam-project-tabular')); ?>" enctype="multipart/form-data">
                                

                                <div class="col-lg-4">
                                    <?php echo e(Form::select('client_accounts[]',$dropDowns['clientAccounts'],$searchFilters['client_accounts'],['id'=>'client_accounts','multiple'=>'multiple','placeholder'=>'All Client Accounts'])); ?>

                           
                                </div>

                                <div class="col-lg-4">
                                    <?php echo e(Form::select('project_status[]',$dropDowns['projectStatuses'],$searchFilters['project_status'],['id'=>'project_status','multiple'=>'multiple','placeholder'=>'All Status'])); ?>


                                </div>
                                    <div class="col-lg-4">
                                        <input type="submit" class="btn btn-success btn-rectangle pull-left" value="Search">
                                    </div>
                            </form>
                           
                            <?php if(count($projects->toArray())>0): ?>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Project Name</th>
                                            <th>Description</th>
                                            <th>Status</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Aging</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr class="clickable-row" data-href="<?php echo e(route('techTeam-show-project', $project->id)); ?>">
                                                <td>
                                                    <a href="<?php echo e(route('techTeam-show-project', $project->id)); ?>">
                                                        <?php echo e($project->name); ?>

                                                    </a>
                                                </td>

                                            <td title = "<?php echo e(empty($project->description)?'N / A':$project->description); ?>">
                                                    <?php if(empty($project->description)): ?> 
                                                        N / A
                                                    <?php else: ?>
                                                        <?php echo e($view = substr($project->description,0,50)); ?>

                                                    <?php echo e($view = strlen($project->description) > 50?'...':''); ?>

                                                    <?php endif; ?>
                                                </td>

                                                <td>
                                                    <?php echo e(config('constants.project_status.'.$project->status)); ?>

                                                </td>

                                                <td>
                                                    <?php echo e(!empty($project->project_start)?\Carbon\Carbon::parse($project->project_start)->format('M d, Y'):'---'); ?>

                                                </td>
                                                <td>
                                                    <?php echo e(\Carbon\Carbon::parse($project->project_end)->format('M d, Y')); ?>

                                                </td>


                                                <td>
                                                    <?php echo e($project->getAgingDays()); ?> days
                                                </td>

                                                <td>
                                                    <a href="<?php echo e(route('techTeam-show-project', $project->id)); ?>">
                                                        <span class="pull-left"> &nbsp;<i class="fa fa-arrow-circle-right" title="View Project"></i> &nbsp;</span>
                                                    </a> &nbsp;
                                                
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>   
                            <?php else: ?>
                                <h3>No Result Found.</h3>
                            <?php endif; ?>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                           <?php echo e($projects->appends($searchFilters)->links()); ?>

                    </div>
                </div>
                <script>
                    var backurl = "<?php echo e(route('client-project-tabular')); ?>";
                </script>
                <script src="<?php echo e(asset('js/projects/projects.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.techlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>