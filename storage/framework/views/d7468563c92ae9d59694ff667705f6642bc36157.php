<?php $__env->startSection('content'); ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Change Requests</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                            <div class="col-lg-6">
                                <p class="text-left">
                                    <a href="<?php echo e(route('client-project')); ?>"><i class="fa fa-columns fa-1x text-left"></i></a>
                                    <a href="<?php echo e(route('client-project-tabular')); ?>"><i class="fa fa-list fa-1x text-left"></i></a>
                                </p>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-right">
                                    <a href="<?php echo e(route('change-requests.create')); ?>"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                                </p>
                            </div>

                            <form  method="POST" action="<?php echo e(route('change-requests.index')); ?>">
                                <?php echo e(csrf_field()); ?>


                                <div class="col-lg-4">
                                    <?php echo e(Form::select('client_accounts[]',$searchFilters['clientAccounts'],old('client_accounts'),['multiple'=>'multiple','id'=>'client_accounts','placeholder'=>'All Accounts'])); ?>

                                    
                                </div>
                                <div class="col-lg-4">
                                    <?php echo e(Form::select('projects[]',$searchFilters['projects']->pluck('name','id'),old('projects'),['multiple'=> 'multiple','id'=>'projects','placeholder'=>'All Projects'])); ?>

                                    
                                </div>
    
                                <div class="col-lg-4">
                                    <input type="submit" class="btn btn-success btn-rectangle pull-left" value="Search">
                                </div>
                            </form>

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Account</th>
                                        <th>Project</th>
                                        <th colspan = "3">Task</th>
                                        <th>Man Hours</th>
                                        <th>Price</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $CRO; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item =>$cr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($cr->project->accounts->name); ?></td>
                                            <td><?php echo e($cr->project->name); ?></td>
                                            <td  colspan = "3">
                                                <a href = "#">Please view details... </a>
                                            </td>
                                            <td><?php echo e(number_format($cr->man_hours,2)); ?></td>
                                            <td><?php echo e(number_format($cr->price,2)); ?></td>
                                            <td>
                                                <?php
                                                    $changerequest = $cr->id;
                                                ?>
                                                <a href="<?php echo e(route('change-requests.show',$cr->id)); ?>">
                                                    <span class="pull-left"> &nbsp;<i class="fa fa-arrow-circle-right" title="View Request Details"></i> &nbsp;</span>
                                                </a> &nbsp;
                                                <a href="<?php echo e(route('change-requests.edit',$changerequest)); ?>">
                                                    <span class="pull-left"> &nbsp;<i class="fa fa-edit" title="Edit change request"></i> &nbsp;</span>
                                                </a> &nbsp;
                                                <a href="javasctript:void(0);" onclick="deleteRequest(<?php echo e($changerequest); ?>)" title="Delete">
                                                    <span class="pull-left"> &nbsp;<i class="fa fa-close imanila-color"></i></span>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        
                    </div>
                </div>
                <script>
                    function deleteRequest(reqId) {
                        if (confirm('Are you sure you want to delete this change request?')) {
                            $.ajax({
                                type:'GET',
                                url:'/change-requests/delete/'+reqId,
                                success:function(data){
                                    window.location = '<?php echo e(route('change-requests.index')); ?>';
                                }
                            });
                        }
                    }
                </script>
              
                
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>