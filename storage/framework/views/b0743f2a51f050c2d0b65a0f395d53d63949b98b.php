<?php if(Auth::guard('web')->check()): ?> 
<li>
    <a href="<?php echo e(route('administrator-accounts-change-pass', Auth::user()->id)); ?>">
        <i class="fa fa-gear fa-fw"></i> Change Password
    </a>
</li>
<li>
    <a href="<?php echo e(route('admin-logout')); ?>"
        onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();">
        <i class="fa fa-sign-out fa-fw"></i> Logout
    </a>

    <form id="logout-form" action="<?php echo e(route('admin-logout')); ?>" method="POST" style="display: none;">
        <?php echo e(csrf_field()); ?>

    </form>
</li>
<?php else: ?>
    <?php if(Auth::guard('ae')->check()): ?>
        <li>
            <a href="<?php echo e(route('administrator-accounts-change-pass', Auth::guard('ae')->user()->id)); ?>">
                <i class="fa fa-gear fa-fw"></i> Change Password
            </a>
        </li>
        <li>
            <a href="<?php echo e(route('ae-logout')); ?>"
                onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out fa-fw"></i> Logout
            </a>

            <form id="logout-form" action="<?php echo e(route('ae-logout')); ?>" method="POST" style="display: none;">
                <?php echo e(csrf_field()); ?>

            </form>
        </li>





    <?php else: ?>    
   
        <li>
            <a href="<?php echo e(route('client-contact-change-pass', Auth::user()->id)); ?>">
                <i class="fa fa-gear fa-fw"></i> Change Password
            </a>
        </li>
        <li>
            <a href="<?php echo e(route('client-logout')); ?>"
                onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out fa-fw"></i> Logout
            </a>

            <form id="logout-form" action="<?php echo e(route('client-logout')); ?>" method="POST" style="display: none;">
                <?php echo e(csrf_field()); ?>

            </form>
        </li>
    <?php endif; ?>
<?php endif; ?>