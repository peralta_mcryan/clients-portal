<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Contacts</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
            <div class="col-lg-6">
                <p class="text-left">
                    <a href="<?php echo e(route('client-contact')); ?>"><i class="fa fa-columns fa-1x text-left"></i></a>
                    <a href="<?php echo e(route('client-contact-tabular')); ?>"><i class="fa fa-list fa-1x text-left"></i></a>
                </p>
            </div>
            <p class="text-right">
                <a href="<?php echo e(route('new-contact')); ?>"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
            </p>
            <div class="dataTable_wrapper">
                <?php 
                    $ctr = 1;
                ?>
                <div class="row">
                    <?php $__currentLoopData = $contacts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $contact): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-lg-4 col-md-6">
                            <div class="panel panel-primary">
                                <a href="<?php echo e(route('show-contact', $contact->id)); ?>">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div><strong><?php echo e($contact->first_name); ?> <?php echo e($contact->last_name); ?></strong></div>
                                                <br>
                                                <div>
                                                    <small>
                                                        <i class="fa fa-building"></i> 
                                                        <?php if(!empty($contact->account->name)): ?>
                                                            <?php echo e($contact->account->name); ?>

                                                        <?php else: ?>
                                                            Not associated to any client
                                                        <?php endif; ?>
                                                    </small>
                                                </div>
                                                <div>
                                                    <small>
                                                        <i class="fa fa-phone"></i>
                                                        <?php if(!empty($contact->work_phone)): ?>
                                                            <?php echo e($contact->work_phone); ?>

                                                        <?php else: ?>
                                                            Not Available
                                                        <?php endif; ?>
                                                    </small>
                                                </div>
                                                <div>
                                                    <small>
                                                        <i class="fa fa-mobile-phone"></i> 
                                                        <?php if(!empty($contact->mobile_phone)): ?>
                                                            <?php echo e($contact->mobile_phone); ?>

                                                        <?php else: ?>
                                                            Not Available
                                                        <?php endif; ?>
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="panel-footer">
                                    <small>
                                        <a href="<?php echo e(route('update-contact', $contact->id)); ?>">
                                            <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                            <span class="pull-left">&nbsp;Edit Details</span>
                                        </a>
                                        <a class="pull-right ">
                                            <span class="pull-right imanila-color" style="cursor:pointer;"><i class="fa fa-close" onclick="javascript:deleteContact(<?php echo e($contact->id); ?>)"></i></span>
                                        </a>
                                    </small>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    <?php if($ctr % 3 == 0): ?>
                        </div>
                        <div class="row">
                    <?php endif; ?>
                    <?php $ctr++; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12 text-center">
        <?php echo e($contacts->links()); ?>

    </div>
</div>
<script src="<?php echo e(asset('js/contacts/contacts.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>