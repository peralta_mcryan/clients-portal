<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo e(asset('css/bootstrap.min.css')); ?>" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo e(asset('css/metisMenu.min.css')); ?>" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<?php echo e(asset('css/timeline.css')); ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo e(asset('css/startmin.css')); ?>" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo e(asset('css/morris.css')); ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo e(asset('css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <link href="<?php echo e(asset('css/dataTables/dataTables.bootstrap.css')); ?>" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<?php echo e(asset('css/dataTables/dataTables.responsive.css')); ?>" rel="stylesheet">

    <!-- Selectize CSS -->
    <link href="<?php echo e(asset('js/selectize/css/selectize.legacy.css')); ?>" rel="stylesheet">

    <link href="<?php echo e(asset('css/custom.css')); ?>" rel="stylesheet">

    <!-- jQuery -->
    <script src="<?php echo e(asset('js/jquery.min.js')); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo e(asset('js/metisMenu.min.js')); ?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo e(asset('js/startmin.js')); ?>"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo e(asset('js/dataTables/jquery.dataTables.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/dataTables/dataTables.bootstrap.min.js')); ?>"></script>

    <script src="<?php echo e(asset('js/selectize/js/standalone/selectize.js')); ?>"></script>
</head>
<body>
    <div id="wrapper">

        

        <div id="page-wrapper">
            <?php echo $__env->yieldContent('content'); ?>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('select').selectize();
        });
    </script>
</body>
</html>

<?php echo $__env->make('layouts.tech-navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>