<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Designations</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($pageName); ?>

                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-horizontal" method="POST" action="<?php echo e($route); ?>">
                                <?php echo e(csrf_field()); ?>


                                <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                    <label for="designation" class="col-md-2 control-label"><span class="asterisk-marker">*</span> Designation</label>

                                    <div class="col-md-10">
                                        <input id="designation" type="text" class="form-control" name="designation" value="<?php echo e(!empty(old('designation'))?old('designation'):$designation->designation); ?>" required autofocus>

                                        <?php if($errors->has('designation')): ?>
                                               <span class="error-block">
                                                <strong><?php echo e($errors->first('designation')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>          
                                <div class="form-group">
                                    <div class="col-md-10 col-md-offset-2">
                                        <button type="submit" class="btn btn-primary">
                                            Save
                                        </button>
                                        <a href="<?php echo e(route('designations.index')); ?>">
                                            <button type="button" class="btn btn-default">
                                                Cancel
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>