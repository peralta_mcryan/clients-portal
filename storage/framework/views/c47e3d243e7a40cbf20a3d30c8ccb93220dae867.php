<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><?php echo e($pageName); ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ticket Details
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" method="POST" action="<?php echo e($route); ?>" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="ctrl"> CTRL #</label>
                                    <input id="ctrl" name="ctrl" class="form-control" value="<?php echo e(!empty(old('ctrl'))?old('ctrl'):(!empty($issueList->control_no)?$issueList->control_no:'---')); ?>" required readonly>
                                    <?php if($errors->has('name')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('name')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>

                                 <div class="form-group">
                                    <label for="ctrl"><span class="asterisk-marker">*</span> Project</label>
                                    <?php echo e(Form::select('project',$projects,!empty(old('project'))?old('project'):(!empty($issueList->project_id)?$issueList->project_id:''),['placeholder'=>'Select Project','class'=>''])); ?>

                                    <?php if($errors->has('project')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('project')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group">
                                    <label for="ctrl"><span class="asterisk-marker">*</span> Assigned To</label>
                                    <?php echo e(Form::select('assigned_department',$users,!empty(old('assigned_department'))?old('assigned_department'):(!empty($issueList->assigned_to)?$issueList->assigned_to:''),['placeholder'=>'Select Assignee','class'=>''])); ?>

                                    <?php if($errors->has('assigned_department')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('assigned_department')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group">
                                    <label for="status"><span class="asterisk-marker">*</span> Status</label>
                                    <?php echo e(Form::select('status',config('constants.ticket_status_def'),!empty(old('status'))?old('status'):(!empty($issueList->status)?$issueList->status:''),['placeholder'=>'Select Status','class'=>''])); ?>

                                    <?php if($errors->has('status')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('status')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="priority"><span class="asterisk-marker">*</span> Priority</label>
                                    <?php echo e(Form::select('priority',config('constants.ticket_priority_def'),!empty(old('priority'))?old('priority'):(!empty($issueList->priority)?$issueList->priority:''),['placeholder'=>'Select Priority','class'=>''])); ?>

                                    <?php if($errors->has('priority')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('priority')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group">
                                    <label for="details"><span class="asterisk-marker">*</span> Details</label>
                                    <?php echo e(Form::textarea('details',!empty(old('details'))?old('details'):(!empty($issueList->description)?$issueList->description:''),['placeholder'=>'Detail goes here','class'=>'form-control'])); ?>

                                    <?php if($errors->has('details')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('details')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>

                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="<?= route('tickets.index') ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <script src="<?php echo e(asset('ckeditor/ckeditor.js')); ?>"></script>
    <script src="<?php echo e(asset('js/tickets.js')); ?>"></script>

    <script>
        $(document).ready(function () {
            $('select').selectize();
            CKEDITOR.replace('details');
        });
    </script>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('layouts.techlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>