<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><?php echo e($pageName); ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Contact Details
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form" method="POST" action="<?php echo e($route); ?>">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="first_name"><span class="asterisk-marker">*</span> First Name</label>
                                    <input id="first_name" name="first_name" class="form-control" placeholder="First Name" value="<?php echo e($contact->first_name); ?>" required>
                                    <?php if($errors->has('first_name')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('first_name')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="last_name"><span class="asterisk-marker">*</span> Last Name</label>
                                    <input id="last_name" name="last_name" class="form-control" placeholder="Last Name" value="<?php echo e($contact->last_name); ?>" required>
                                    <?php if($errors->has('last_name')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('last_name')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="email_address"><span class="asterisk-marker">*</span> Email Address</label>
                                    <input id="email_address" name="email_address" class="form-control" placeholder="Email Address" value="<?php echo e($contact->email); ?>" type="email" required>
                                    <?php if($errors->has('email_address')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('email_address')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="work_phone"><span class="asterisk-marker">*</span> Work Phone</label>
                                    <input id="work_phone" name="work_phone" class="form-control" placeholder="Work Phone" value="<?php echo e($contact->work_phone); ?>" required>
                                    <?php if($errors->has('work_phone')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('work_phone')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="mobile_phone"><span class="asterisk-marker">*</span> Mobile Phone</label>
                                    <input id="mobile_phone" name="mobile_phone" class="form-control" placeholder="Mobile Phone" value="<?php echo e($contact->mobile_phone); ?>" required>
                                    <?php if($errors->has('mobile_phone')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('mobile_phone')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="designation">Designation</label>
                                    <input id="designation" name="designation" class="form-control" placeholder="Designation" value="<?php echo e($contact->designation); ?>">
                                    <?php if($errors->has('designation')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('designation')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="department">Department</label>
                                    <input id="department" name="department" class="form-control" placeholder="Department" value="<?php echo e($contact->department); ?>">
                                    <?php if($errors->has('department')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('department')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="account">Account</label>
                                    <br>
                                    <?php if($account->id): ?>
                                        <input type="hidden" name="account" value="<?php echo e($account->id); ?>">
                                        <?php echo e($account->name); ?>

                                    <?php else: ?>
                                        <?php echo e(Form::select('account', $accounts, $contact->account_id,['id' => 'acccount',  'class'=>''])); ?>

                                        <?php if($errors->has('account')): ?>
                                            <span class="error-block">
                                                <strong><?php echo e($errors->first('account')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group" style="display:none;">
                                    <label for="portal_access">Portal Access</label>
                                    <?php echo e(Form::select('portal_access', $portalAccess, $contact->allow_access,['id' => 'portal_access',  'class'=>''])); ?>

                                    <?php if($errors->has('portal_access')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('portal_access')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group">
                                    <label for="portal_access">Contact Type</label>
                                    <?php echo e(Form::select('contact_type', $contactType, $contact->contact_type,['id' => 'contact_type',  'class'=>''])); ?>

                                    <?php if($errors->has('contact_type')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('contact_type')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>

                                <?php if($settings['include_password']): ?>
                                <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                                    <label for="password">Password</label>
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    <?php if($errors->has('password')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('password')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm">Confirm Password</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                                <?php endif; ?>

                                <?php if($settings['show_email_checkbox']): ?>
                                <div class="form-group">
                                    <input type="checkbox" name="send_welcome_email" id="send_welcome_email" value="1" <?php if($settings['show_email_default_value']): ?> <?php echo e("checked"); ?> <?php endif; ?>> Check this box to auto-generate a new password and send the welcome email. Otherwise, remove the check mark.
                                </div>
                                <?php endif; ?>
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="<?= route('client-contact') ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <script src="<?php echo e(asset('js/accounts/accounts.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>