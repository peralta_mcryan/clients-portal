<?php $__env->startSection('content'); ?>
<div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Designations</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                            <p class="text-right">
                                <a href="<?php echo e(route('designations.create')); ?>"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                            </p>
                            <div class="dataTable_wrapper">
                                <?php 
                                    $ctr = 1;
                                ?>
                                <div class="row">
                                    <?php if(count($designations) > 0): ?>
                                        <?php $__currentLoopData = $designations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $designation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="col-lg-4 col-md-6">
                                                <div class="panel panel-primary">
                                                    <a href="<?php echo e(route('service-types.show', $designation->id)); ?>">
                                                        <div class="panel-heading">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <div><strong><?php echo e($designation->designation); ?></strong></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="panel-footer">
                                                        <small>
                                                            <a href="<?php echo e(route('designations.edit', $designation->id)); ?>">
                                                                <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                                                <span class="pull-left">&nbsp;Edit Details</span>
                                                            </a>
                                                            <a class="pull-right ">
                                                                <span class="pull-right imanila-color" style="cursor:pointer;"><i class="fa fa-close" onclick="javascript:deletedesignation(<?php echo e($designation->id); ?>)"></i></span>
                                                            </a>
                                                        </small>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if($ctr % 3 == 0): ?>
                                                </div>
                                                <div class="row">
                                            <?php endif; ?>
                                            <?php $ctr++; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                                    <h2>Empty Designation List</h2>
                                    <?php endif; ?>
                                <div class="col-lg-12 text-center">
                                    <?php echo e($designations->links()); ?>

                                </div>
                            </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

<script>
    
function deletedesignation(designationId) {
	if (confirm('Are you sure you want to delete this designation?')) {
		$.ajax({
			type:'GET',
			url:'/designations/delete/'+designationId,
			success:function(data){
                alert(data)
				window.location = '/designations';
			}
		});
	}
}
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>