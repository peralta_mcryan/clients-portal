<?php $__env->startSection('content'); ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Departments</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                            <div class="col-lg-6">
                              
                            </div>
                            <div class="col-lg-6">
                                <p class="text-right">
                                    <a href="<?php echo e(route('departments.create')); ?>"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                                </p>
                            </div>
                            <div class="dataTable_wrapper">
                                <?php 
                                    $ctr = 1;
                                ?>
                                <div class="row">
                                    <?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $department): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="panel panel-primary">
                                                <a href="<?php echo e(route('show-project', $department->id)); ?>">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div><strong><?php echo e($department->name); ?></strong></div>
                                                                <div><small>Email: <?php echo e($department->email); ?></small></div>
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="panel-footer">
                                                    <small>
                                                        <a href="<?php echo e(route('departments.edit', $department->id)); ?>">
                                                            <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <span class="pull-left">&nbsp;Edit Details</span>
                                                        </a>
                                                    </small>
                                                   
                                                    <br>
                                                    <small>
                                                        <a href="#" onclick="javascript:deleteDepartment(<?php echo e($department->id); ?>)">
                                                            <span class="pull-left"><i class="fa fa-close imanila-color"></i></span>
                                                            <span class="pull-left">&nbsp;Delete</span>
                                                        </a>
                                                    </small>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php if($ctr % 3 == 0): ?>
                                        </div>
                                        <div class="row">
                                    <?php endif; ?>
                                    <?php $ctr++; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <?php echo e($departments->links()); ?>

                    </div>
                </div>
                <script>
                    var backurl = "<?php echo e(route('departments.index')); ?>";
                </script>
                <script src="<?php echo e(asset('js/department.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>