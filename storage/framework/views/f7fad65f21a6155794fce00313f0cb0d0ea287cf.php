<?php $__env->startSection('content'); ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Account Information</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel tabbed-panel panel-primary">
                            <div class="panel-heading clearfix">
                                <div class="panel-title pull-left">
                                    Company Details
                                </div>
                                <div class="pull-right">
                                    <?php if(!empty($contact->account)): ?>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Overview</a></li>
                                        <li class=""><a href="#tab-primary-2" data-toggle="tab" aria-expanded="false">Contacts</a></li>
                                    </ul>
                                    <?php else: ?>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Overview</a></li>
                                    </ul>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <?php if(!empty($contact->account)): ?>
                                    <div class="tab-pane fade active in" id="tab-primary-1">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h3><?php echo e($contact->account->name); ?> </h3>
                                                <hr></hr>

                                                <div class="panel panel-green">
                                                    <div class="panel-heading" style="padding-bottom: 5px;">
                                                        Address
                                                    </div>
                                                    <div class="panel-body">
                                                        <address>
                                                            <strong><?php echo e($contact->account->name); ?></strong>
                                                            <br>
                                                            <small>
                                                                <?php echo e($contact->account->address); ?>

                                                                <br>
                                                                <abbr title="Work Phone">W:</abbr>
                                                                <?php echo e($contact->account->work_phone); ?>

                                                                <br>
                                                                <abbr title="Mobile Phone">M:</abbr>
                                                                <?php echo e($contact->account->mobile_phone); ?>

                                                            </small>
                                                        </address>

                                                        <small>
                                                        <?php if(!empty($city->name)): ?>
                                                            <?php echo e($city->name); ?>,
                                                        <?php endif; ?>

                                                        <?php if(!empty($province->name)): ?>
                                                            <?php echo e($province->name); ?>,
                                                        <?php endif; ?>

                                                        <?php if(!empty($country->name)): ?>
                                                            <?php echo e($country->name); ?>

                                                        <?php endif; ?>
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab-primary-2">
                                        <div class="col-lg-12">
                                            <h3><?php echo e($contact->account->name); ?> </h3>
                                            <hr></hr>

                                            <?php if(!empty($contact->account->contacts)): ?>
                                                <?php $__currentLoopData = $contact->account->contacts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $contact): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-green">
                                                            <div class="panel-heading">
                                                                <div class="row">
                                                                    <div class="col-xs-3" style="padding-bottom: 5px;">
                                                                        <i class="fa fa-address-card-o fa-5x"></i>
                                                                    </div>
                                                                    <div class="col-xs-9 text-right">
                                                                        <div class="huge"><?php echo e($contact->first_name); ?> <?php echo e($contact->last_name); ?></div>
                                                                        <div><small><?php echo e($contact->email); ?></small></div>
                                                                        <div><small><?php echo e($contact->work_phone); ?></small></div>
                                                                        <div><small><?php echo e($contact->mobile_phone); ?></small></div>
                                                                        <div><small><?php echo e(config('constants.contact_type.'.$contact->contact_type)); ?></small></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <a href="#">
                                                                <div class="panel-footer">
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php else: ?>
                                                <div class="alert alert-info">
                                                    <i class="fa fa-hand-o-right"></i> You don't have a contact associated to this account.
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <?php else: ?>
                                    <div class="tab-pane fade active in" id="tab-primary-1">
                                        <div class="col-lg-12">
                                            <div class="alert alert-info">
                                                <i class="fa fa-hand-o-right"></i> You are not assicated to any account. Please wait until the administrator is finished updating your account. 
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.clientlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>