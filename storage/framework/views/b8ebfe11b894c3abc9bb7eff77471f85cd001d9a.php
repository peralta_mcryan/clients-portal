<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-12">
        <h4 class="page-header">Search Results for <strong><i><?php echo e($searchText); ?></i></strong></h4>
    </div>
</div>
<!-- /.row -->
<div class="row search-result">
    <div class="col-lg-12">
        <div class="panel tabbed-panel panel-primary">
            <div class="panel-heading clearfix">
                <div class="pull-left">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Projects (<?php echo e(count($projectSearchResults)); ?>)</a></li>
                        <li class=""><a href="#tab-primary-2" data-toggle="tab" aria-expanded="false">Files (<?php echo e(count($fileSearchResults)); ?>)</a></li>
                        <li class=""><a href="#tab-primary-3" data-toggle="tab" aria-expanded="false">Tickets (<?php echo e(count($ticketSearchResults)); ?>)</a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-primary-1">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php if(count($projectSearchResults) > 0): ?>
                                    <?php $__currentLoopData = $projectSearchResults; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $projectSearchResult): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <a href="<?php echo e(route('techTeam-show-project', $projectSearchResult->id)); ?>">
                                        <div class="col-lg-12 col-md-12">
                                            <strong><?php echo e($projectSearchResult->name); ?></strong>
                                            <hr></hr>
                                        </div>
                                    </a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <strong class="imanila-color">No results found.</strong>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab-primary-2">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php if(count($fileSearchResults) > 0): ?>
                                    <?php $__currentLoopData = $fileSearchResults; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fileSearchResult): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <a href="<?php echo e(route('download-project-files', [$fileSearchResult->project_id, $fileSearchResult->file_folder_id, $fileSearchResult->filename])); ?>">
                                            <div class="col-lg-12 col-md-12">
                                                <strong><?php echo e($fileSearchResult->filename); ?></strong>
                                                <hr></hr>
                                            </div>
                                        </a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <strong class="imanila-color">No results found.</strong>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                     <div class="tab-pane" id="tab-primary-3">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php if(count($ticketSearchResults) > 0): ?>
                                    <?php $__currentLoopData = $ticketSearchResults; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ticketSearchResult): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <a href="<?php echo e(route('tickets.show', $ticketSearchResult->id)); ?>">
                                            <div class="col-lg-12 col-md-12">
                                                <strong><?php echo e($ticketSearchResult->control_no); ?></strong>
                                                <hr></hr>
                                            </div>
                                        </a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <strong class="imanila-color">No results found.</strong>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.techlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>