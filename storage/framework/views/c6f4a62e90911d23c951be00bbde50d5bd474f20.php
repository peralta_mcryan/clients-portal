<?php $__env->startSection('content'); ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Projects</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                            <div class="col-lg-6">
                                <p class="text-left">
                                    <a href="<?php echo e(route('ae-card-project')); ?>"><i class="fa fa-columns fa-1x text-left"></i></a>
                                    <a href="<?php echo e(route('my-account.index')); ?>"><i class="fa fa-list fa-1x text-left"></i></a>
                                </p>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-right">
                                    &nbsp;
                                </p>
                            </div>
                            <div class="dataTable_wrapper">
                                <?php 
                                    $ctr = 1;
                                ?>
                                <div class="row">
                                    <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="panel panel-primary">
                                                <a href="<?php echo e(route('ae-show-project', $project->id)); ?>">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div><strong><?php echo e($project->name); ?></strong></div>
                                                                <br>
                                                                <div>
                                                                    <small>
                                                                        <?php if(empty($project->description)): ?>
                                                                            No project description
                                                                            <?php else: ?>
                                                                            <span title = "<?php echo e(empty($project->description)?'No project description':'Project Description: '.$project->description); ?>">
                                                                                 <?php echo e($view = substr($project->description,0,50)); ?>

                                                                                <?php echo e($view = strlen($project->description) > 50?'...':''); ?>

                                                                            </span>
                                                                        <?php endif; ?>
                                                                    </small>
                                                                </div>
                                                                <div><small>Status: <?php echo e(config('constants.project_status.'.$project->status)); ?></small></div>
                                                                <div><small>Start Date:  <?php echo e(!empty($project->project_start)?\Carbon\Carbon::parse($project->project_start)->format('M d, Y'):'---'); ?></small></div>
                                                                <div><small>End Date: <?php echo e(\Carbon\Carbon::parse($project->project_end)->format('M d, Y')); ?></small></div>
                                                                <div><small>Aging: <?php echo e($project->getAgingDays()); ?> days</small></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="panel-footer">
                                                    <small>
                                                        <a href="<?php echo e(route('ae-show-project', $project->id)); ?>">
                                                            <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <span class="pull-left">&nbsp;View Details</span>
                                                        </a>
                                                    </small>
                                                   
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php if($ctr % 3 == 0): ?>
                                        </div>
                                        <div class="row">
                                    <?php endif; ?>
                                    <?php $ctr++; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <?php echo e($projects->links()); ?>

                    </div>
                </div>
                <script>
                    var backurl = "<?php echo e(route('client-project-tabular')); ?>";
                </script>
                <script src="<?php echo e(asset('js/projects/projects.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.aelayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>