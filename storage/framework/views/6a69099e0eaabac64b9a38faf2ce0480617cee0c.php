<?php if(Auth::guard('web')->check()): ?> 
<span id = "currentuser"><?php echo e(Auth::user()->name); ?></span>
<?php else: ?>
    <?php if(Auth::guard('ae')->check()): ?>
       <span id = "currentuser"> <?php echo e(Auth::guard('ae')->user()->name); ?></span>
    <?php elseif(Auth::guard('techTeam')->check()): ?>
       <span id = "currentuser"> <?php echo e(Auth::guard('techTeam')->user()->name); ?></span>
    <?php else: ?>
    <span id = "currentuser"><?php echo e(Auth::user()->first_name); ?> <?php echo e(Auth::user()->last_name); ?></span>
    <?php endif; ?> 

<?php endif; ?>