<?php $__env->startSection('content'); ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><?php echo e($issueList->project->name); ?></h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-10">
                        <div class="panel tabbed-panel panel-primary">
                            <div class="panel-heading clearfix">
                                <div class="panel-title pull-left">
                                    Issue Details
                                </div>
                                <div class="pull-right">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Overview</a></li>
                                        <li ><a href="#tab-primary-2" data-toggle="tab" aria-expanded="false">Logs</a></li>
                                        <li ><a href="#tab-primary-3" data-toggle="tab" aria-expanded="false">New Topic</a></li>
                                        
                                       
                                    </ul>
                                </div>
                            </div>
                           
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab-primary-1">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="col-lg-8">
                                                    <h3>Issue Ctrl #:&nbsp;<?php echo e($issueList->control_no); ?>  &nbsp;<a href="<?php echo e(route('admin-tickets.edit',$issueList->id)); ?>" class="text-right"><i class="fa fa-edit imanila-color"></i></a></h3>
                                                </div>
                                                 <div class="pull-right">
                                                    <?php if(!empty($alerts)): ?>
                                                    <label for="alert"> Notify Me! </label>
                                                    <form id="change-status" action="<?php echo e(route('update-alert-status',$issueList->id)); ?>" method="post">
                                                        <?php echo e(csrf_field()); ?>

                                                        <label class="switch">
                                                            <?php if($alerts->status == 1): ?>
                                                                <input type="checkbox" name = "alert" id = "alert" onchange="submitForm();" value = 1 checked>
                                                                 <span class="slider round"></span>
                                                            <?php else: ?>
                                                                <input type="checkbox" name = "alert" id = "alert" onchange="submitForm();" value = 1>
                                                                <span class="slider round"></span>
                                                            <?php endif; ?>
                                                          
                                                        </label>
                                                    </form>
                                                    <?php endif; ?>
                                                </div>

                                                <div class="row">
                                                     <div class="col-md-12">
                                                        <div class="col-xs-12 col-lg-12">
                                                            <small>
                                                                <strong>Project Name</strong>
                                                                <br>
                                                            
                                                                <?php echo e($issueList->project->name); ?>

                                                        
                                                            </small>
                                                            <hr></hr>
                                                        </div> 
                                                   
                                                        <div class="col-xs-12 col-lg-4">
                                                            <small>
                                                                <strong>Date Issued</strong>
                                                                <br>
                                                                    <?php echo e($issueList->created_at); ?>

                                                                </small>
                                                        </div>
        
                                                        <div class="col-xs-12 col-lg-4">
                                                            <small>
                                                                <strong>Created By</strong>
                                                                <br>
                                                                <?php echo e($issueList->user->name); ?>

                                                            </small>
                                                        </div>
                                                        
                                                        <div class="col-xs-12 col-lg-4">
                                                            <small>
                                                                <strong>Assigned To</strong>
                                                                <br>
                                                                <?php echo e($issueList->assignee->name); ?>

                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <p>
                                                        &nbsp;
                                                    </p>
                                                </div>  
                                                <div class="col-xs-12 col-lg-12">
                                                    <small>
                                                        <strong>Description</strong>
                                                        <br>
                                                        <div class="border-container">
                                                        <?php echo $issueList->description; ?>

                                                        </div>
                                                    </small>
                                                    <hr></hr>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade in" id="tab-primary-2"> 
                                        <div id = "mainpanel" class="panel-body">
                                            <?php $__currentLoopData = $issueLogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="panel panel-primary">    
                                                    <div class="panel-heading">
                                                                                                    
                                                        <span calss ="text-left"><?php echo e(date('F d, Y',strtotime($log->created_at))); ?></span>
                                                    
                                                    </div>
                                                    <div class = "panel-body">
                                                        <div class="col-lg-12">
                                                        <?php $logs = json_decode($log->details)?>       
                                                        <ul>
                                                            <?php $__currentLoopData = $logs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $logdetail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <li><?php echo e($logdetail); ?></li>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </ul>
                                                        </div>
                                                    </div>
                                                </div>  
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade in" id="tab-primary-3">                              
                                        <div class="panel-body">
                                                <form id = "frm_forum" action="<?php echo e(route('admin-issue-thread.store',$issueList->id)); ?>" method="post">
                                                   <?php echo e(csrf_field()); ?>    
                                                    <textarea name="details" id="details"></textarea>
                                                    <br>
                                                    <div class="pull-right">
                                                        <input type="submit" value="Submit" class = "btn btn-success"/>
                                                    </div>
                                                </form>  
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                         <div class="col-lg-10">
                            <?php $__env->startComponent('components.chat',['issueThread' => $issueThread]); ?>
                            <?php echo $__env->renderComponent(); ?>
                        </div>
  
                        <!-- /.panel -->
                    </div>


<script src="<?php echo e(asset('ckeditor/ckeditor.js')); ?>"></script>
<script>
     
        $(document).ready(function () {
            CKEDITOR.replace( 'details' );
        });
</script>
<script>
    $(document).ready(function(){
        
        $('#employee_sched').slideDown(300); 
    });

    function showFooter(id) {
        if($('#footer_'+id).is(':visible')){
            $('#footer_'+id).slideUp(300);
        }else{
            $('#footer_'+id).slideDown(300);
        }
    }

    function addReply(id,forum) {
        var  ctr = $('#current_message_'+id).html()
         $('#reply_to_'+forum).val(id);
        // $('#addComment_'+forum).val($('#current_message_'+id).html())
        if($('#reply_to_'+forum).val() == 0){
            $('#label_'+forum).slideUp(300);
        }else{
            $('#reply_for_'+forum).html(ctr);
            $('#label_'+forum).slideDown(300);
        }
        
    
    }
    function resetReply(forum)
    {
       
        $('#reply_to_'+forum).val(0);
        $('#label_'+forum).slideUp(300);
    }

    function submitForm()
    {
        $('#change-status').submit();
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>