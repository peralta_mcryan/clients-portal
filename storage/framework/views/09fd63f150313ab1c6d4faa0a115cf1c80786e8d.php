<?php $__env->startSection('content'); ?>
<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Service Types</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-3">
                            <p class="text-right">
                                <a href="<?php echo e(route('service-types.create')); ?>"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                            </p>
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $serviceTypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $serviceTypeList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr class="clickable-row" data-href="<?php echo e(route('service-types.show', $serviceTypeList->id)); ?>">
                                                <td>
                                                    <?php echo e($serviceTypeList->name); ?>

                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                                <?php echo e($serviceTypes->links()); ?>

                            </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Service Type Details
                             </div>
                            <div class="panel-body">
                                <p class="text-right">
                                    <a href="<?php echo e(route('service-types.edit', $serviceType->id)); ?>">
                                        <i class="fa fa-edit"> Edit</i></a>
                                        | <a href="javascript:deleteServiceType(<?php echo e($serviceType->id); ?>)">
                                        <i class="fa fa-close"> Delete</i></a>
                                </p>
                                <p class="lead">
                                    <?php echo e($serviceType->name); ?>

                                </p>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>

                <script src="<?php echo e(asset('js/service_type.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>