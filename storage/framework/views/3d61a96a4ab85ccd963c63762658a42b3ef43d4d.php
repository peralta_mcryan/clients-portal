<?php $__env->startComponent('mail::message'); ?>

<p>Hi Ms. / Mr. <?php echo e($userName); ?></p>

<p><br />
Greetings from iManila!</p>

<p>Your iManila Client Portal Credentials:</p>

<p><strong>URL:</strong>&nbsp;<?php echo $userLoginUrl; ?></p>
<p><strong>Username:</strong> <?php echo e($userUsername); ?></p>
<p><strong>Password:</strong>&nbsp;<?php echo e($userPassword); ?></p>

<p>&nbsp;</p>

Thanks,<br>
<?php echo e(config('app.name')); ?>

<?php echo $__env->renderComponent(); ?>
