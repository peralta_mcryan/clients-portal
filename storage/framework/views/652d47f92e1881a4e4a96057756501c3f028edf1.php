<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><?php echo e($pageName); ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Change Request Details
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" method="POST" action="<?php echo e($route); ?>" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="name"><span class="asterisk-marker">*</span>Project</label>
                                  <?php echo e(Form::select('project',$projectArray->pluck('name','id'),!empty(old('project'))?old('project'):((!empty($changerequest->project_id)?$changerequest->project_id:'')))); ?>

                                    </select>
                                      <?php if($errors->has('project')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('project')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label for="task">Task</label>
                                <textarea name="task" id="task" class="form-control" rows="4" placeholder = "Enter task here..."><?php echo e(!empty(old('task'))?old('task'):(((!empty($changerequest->task)?$changerequest->task:'')))); ?></textarea>
                                      <?php if($errors->has('task')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('task')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>

                                  <div class="form-group">
                                    <label for="man_hours">Man Hours</label>
                                  <input name="man_hours" id="man_hours" class="form-control" value = "<?php echo e(!empty(old('man_hours'))?old('man_hours'):((!empty($changerequest->man_hours)?str_replace(',','',$changerequest->man_hours):'0.00'))); ?>">
                                      <?php if($errors->has('man_hours')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('man_hours')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group">
                                    <label for="price">Price</label>
                                    <input name="price" id="price" class="form-control" value = "<?php echo e(!empty(old('price'))?old('price'):((!empty($changerequest->price)?str_replace(',','',$changerequest->price):'0.00'))); ?>">
                                    <?php if($errors->has('price')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('price')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="<?= route('change-requests.index') ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <script src="<?php echo e(asset('ckeditor/ckeditor.js')); ?>"></script>

    <script>
        $('select').selectize();
        $(document).ready(function () {
            CKEDITOR.replace( 'task' );
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>