<?php $__env->startComponent('mail::message'); ?>

<?php echo $emailBody; ?>

<?php if(isset($selectedThemes)): ?>
<?php echo $selectedThemes; ?>

<?php endif; ?>
<?php echo $remarks; ?>


Thanks,<br>
<?php echo e(config('app.name')); ?>

<?php echo $__env->renderComponent(); ?>
