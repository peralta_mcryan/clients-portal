<?php
 $loggedUser = '';
 if(Auth::guard('web')->check())
    $loggedUser = Auth::guard('web')->user()->id;
 elseif (Auth::guard('techTeam')->check())
    $loggedUser = Auth::guard('techTeam')->user()->id;
 elseif (Auth::guard('ae')->check())
    $loggedUser = Auth::guard('ae')->user()->id;
 ?>
<div class="panel tabbed-panel panel-primary">
    <div class="panel-heading clearfix">
        <div class="panel-title text-center">
            Topics
        </div>
    </div>
    
    <div class="panel-body">
        <div class=" pull-right">
            
        </div>
        <br>
        <br>
        <?php ($threads = $issueThread->toArray()); ?>
        <?php if(count($threads['data'])> 0): ?>
            <?php $__currentLoopData = $issueThread; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kry => $forum): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div  class="panel tabbed-panel panel-primary">
                    <div class="panel-heading  discussion-head clearfix">
                        <div class="panel-title  pull-left">
                            <?php echo e(date('F d, Y',strtotime($forum->created_at))); ?>

                        </div>
                    </div>
                    <div class="panel-body">
                        <?php echo $forum->details; ?>

                    </div>
                    <div style="display:none;" id = "footer_<?php echo e($forum->id); ?>" class="panel-footer issue-thread-expand">
                        <form action="<?php echo e(route('issue-thread.sendReply',$forum->id)); ?>" method="post">
                            <?php echo e(csrf_field()); ?>

                            <?php $__currentLoopData = $forum->thread_discussion()->orderBy('created_at')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $discussion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            
                            <?php if($discussion->created_by == $loggedUser): ?>    
                                <div class="col-lg-12 chat-logs-me">
                                    <div class="col-lg-8">
                                        <div class="panel tabbed-panel">
                            <?php else: ?>   
                                <div class="col-lg-12 chat-logs-others">
                                    <div class="col-lg-8">     
                                        <div class="panel tabbed-panel">      
                            <?php endif; ?>    
                                            <div class=" panel-discussion panel-heading clearfix">
                                                <div class="panel-title text-center">
                                                    <?php echo e(date('F d, Y',strtotime($discussion->created_at))); ?> -- <?php echo e($discussion->user->name); ?>

                                                </div>
                                            </div>
                                                <div class="panel-body thread_discussion">
                                                <?php if($discussion->reply_to > 0): ?>
                                                    <div class="panel panel-body">
                                                        <span style = "font-style: italic;">&nbsp;<?php echo App\ThreadDiscussion::where('id',$discussion->reply_to)->pluck('details')->first(); ?>&nbsp;</span>
                                                    </div>  
                                                <?php endif; ?>
                                                <span id = "current_message_<?php echo e($discussion->id); ?>"><?php echo $discussion->details; ?></span>
                                            </div>
                                            <div class="panel-footer">
                                                <a href="javascript::void(0);" class = "text-right" onclick="addReply(<?php echo e($discussion->id); ?>,<?php echo e($forum->id); ?>);">Reply</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

                            <div id = "label_<?php echo e($forum->id); ?>" style="display:none;" class="panel panel-body thread-reply">
                                Reply for: <span id = "reply_for_<?php echo e($forum->id); ?>"></span> <span class = "pull-right"><a href = "javascript:void(0);" onclick="resetReply(<?php echo e($forum->id); ?>)"><i class="fa fa-close imanila-color"></i></a></span>
                            </div>
                                <input type="hidden" name = "reply_to_<?php echo e($forum->id); ?>" id = "reply_to_<?php echo e($forum->id); ?>" value="0">
                                    <textarea name="addComment_<?php echo e($forum->id); ?>" id="addComment_<?php echo e($forum->id); ?>" cols="30" rows="10"></textarea>
                                        <br>
                                            <input type="submit" value = "Post" class = "btn btn-info"></button>
                    
                                                <script>
                                                    $(document).ready(function () {
                                                        CKEDITOR.replace( 'addComment_'+"<?php echo e($forum->id); ?>" );
                                                    });
                                                </script>

                        </form>
                    </div>
                    <div class="discussion-footer panel-footer text-center">
                        <a href="javascript:void(0);" onclick="showFooter(<?php echo e($forum->id); ?>);">Show Discussions</a>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <div class="text-center">
                <?php echo $issueThread->links(); ?>

            </div>
            <div class="panel-body">
            </div>

        <?php else: ?>
            <h3>No Topics</h3>
       <?php endif; ?>
    </div>
</div>






 