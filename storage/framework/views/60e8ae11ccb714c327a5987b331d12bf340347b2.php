<?php $__env->startSection('content'); ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Ticket List</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-6">
                                
                            </div>
                            <div class="col-lg-6">
                                <p class="text-right">
                                    <a href="<?php echo e(route('tickets.create')); ?>"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                                </p>
                            </div>
                        </div>
                         <form role="form" method="GET" action="<?php echo e(route('tickets.index')); ?>">
                            <?php echo e(csrf_field()); ?>

                            <div class="row">
                                <div class="col-lg-4">
                                    
                                    <?php echo e(Form::select('project[]',$projects,old('project'),['di'=>'project','multiple'=>'multiple','placeholder'=>'All Projects'])); ?>

                                </div>

                                <div class="col-lg-2">
                                    
                                    <?php echo e(Form::select('status[]',config('constants.ticket_status_def'),old('status'),['id'=>'status','multiple'=>'multiple','placeholder'=>'All Status'])); ?>

                                </div>

                                <div class="col-lg-2">
                                    
                                    <?php echo e(Form::select('priority[]',config('constants.ticket_priority_def'),old('priority'),['id'=>'priority','multiple'=>'multiple','placeholder'=>'All Priority'])); ?>

                                </div>
                                <div class="col-lg-2">
                                      <input type="submit" class="btn btn-success btn-rectangle pull-left" value="Search">
                                </div>
                            </div>
                        </form>
                         <br>
                            <div class="dataTable_wrapper">
                                <?php 
                                    $ctr = 1;
                                ?>
                                <div class="row">
                                   
                                    <?php $__currentLoopData = $issues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $issue): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="panel panel-primary">
                                                <a href="<?php echo e(route('tickets.show', $issue->id)); ?>">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <div><strong><?php echo e($issue->control_no); ?></strong></div>                     
                                                            </div>
                                                            <?php if($issue->status == config('constants.ticket_status.active')): ?>                                               
                                                                <span class="ticket-status ticket-status-active pull-right"><span class = "ticket-code">&nbsp;&nbsp;<?php echo e(config('constants.ticket_status_def.'.$issue->status)); ?>&nbsp;&nbsp;</span></span>
                                                            <?php elseif($issue->status == config('constants.ticket_status.onhold')): ?>
                                                                <span class="ticket-status ticket-status-hold pull-right"><span class = "ticket-code">&nbsp;&nbsp;<?php echo e(config('constants.ticket_status_def.'.$issue->status)); ?>&nbsp;&nbsp;</span></span>
                                                            <?php elseif($issue->status == config('constants.ticket_status.cancelled')): ?>
                                                                <span class="ticket-status ticket-status-cancelled pull-right"><span class = "ticket-code">&nbsp;&nbsp;<?php echo e(config('constants.ticket_status_def.'.$issue->status)); ?>&nbsp;&nbsp;</span></span>
                                                            <?php endif; ?>
                                                              
                                                            &nbsp;
                                                            <?php if($issue->priority == config('constants.ticket_priority.high')): ?>                                               
                                                                <span class="ticket-priority ticket-priority-high pull-right"><span class = "ticket-code">&nbsp;&nbsp;<?php echo e(config('constants.ticket_priority_def.'.$issue->priority)); ?>&nbsp;&nbsp;</span></span>
                                                            <?php elseif($issue->priority == config('constants.ticket_priority.medium')): ?>
                                                                <span class="ticket-priority ticket-priority-medium pull-right"><span class = "ticket-code">&nbsp;&nbsp;<?php echo e(config('constants.ticket_priority_def.'.$issue->priority)); ?>&nbsp;&nbsp;</span></span>
                                                            <?php elseif($issue->priority == config('constants.ticket_priority.low')): ?>
                                                                <span class="ticket-priority ticket-priority-low pull-right"><span class = "ticket-code">&nbsp;&nbsp;<?php echo e(config('constants.ticket_priority_def.'.$issue->priority)); ?>&nbsp;&nbsp;</span></span>
                                                            <?php endif; ?>
                                                            
                                                         
                                                            <div class="col-xs-12">
                                                              
                                                                <div><small>Project: <?php echo e($issue->project->name); ?></small></div>
                                                                <div><small>Assigned to: <?php echo e($issue->assignee->name); ?></small></div>
                                                                <div><small>Created Date: <?php echo e(\Carbon\Carbon::parse($issue->created_at)->format('M d, Y')); ?></small></div>
                                                                <div><small>Aging:&nbsp;<?php echo e($issue->getAgingDays()); ?> days</small></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="panel-footer">
                                                    <small>
                                                        <a href="<?php echo e(route('tickets.edit', $issue->id)); ?>">
                                                            <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <span class="pull-left">&nbsp;Edit Details</span>
                                                        </a>
                                                    </small>    
                                                    <br>
                                                    <small>
                                                        <a href="javascript:void(0);" onclick="javascript:deleteTicket(<?php echo e($issue->id); ?>)">
                                                            <span class="pull-left"><i class="fa fa-close imanila-color"></i></span>
                                                            <span class="pull-left">&nbsp;Delete</span>
                                                        </a>
                                                    </small>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php if($ctr % 3 == 0): ?>
                                        </div>
                                        <div class="row">
                                    <?php endif; ?>
                                    <?php $ctr++; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <?php echo e($issues->appends($searchFilters)->links()); ?>

                    </div>
                </div>
                <script>
                    var backurl = "<?php echo e(route('tickets.index')); ?>";
                </script>
                <script src="<?php echo e(asset('js/tickets.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.techlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>