<?php if(Auth::guard('web')->check()): ?> 
<?php echo e(Auth::user()->name); ?>

<?php else: ?>
    <?php if(Auth::guard('ae')->check()): ?>
        <?php echo e(Auth::guard('ae')->user()->name); ?>

    <?php else: ?>
    <?php echo e(Auth::user()->first_name); ?> <?php echo e(Auth::user()->last_name); ?>

    <?php endif; ?> 

<?php endif; ?>