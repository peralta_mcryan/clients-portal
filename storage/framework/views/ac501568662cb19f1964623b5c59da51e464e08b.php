<?php $__env->startSection('content'); ?>
<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Administrator Accounts</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-3">
                            <p class="text-right">
                                <a href="<?php echo e(route('administrator-accounts-create')); ?>"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                            </p>
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $userList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr class="clickable-row" data-href="<?php echo e(route('administrator-accounts-show', $userList->id)); ?>">
                                                <td>
                                                    <?php echo e($userList->name); ?>

                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center">
                                <?php echo e($users->links()); ?>

                            </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Account Details
                             </div>
                            <div class="panel-body">
                                <p class="text-right">
                                    <a href="<?php echo e(route('administrator-accounts-edit', $user->id)); ?>">
                                        <i class="fa fa-edit"> Edit</i></a> | 
                                    <a href="<?php echo e(route('administrator-accounts-change-pass', $user->id)); ?>">
                                        <i class="fa fa-key"> Change Password</i></a> 
                                    <?php if(Auth::user()->id == $user->id): ?> 
                                        | <a href="javascript:deleteAdminAccount(<?php echo e($user->id); ?>)">
                                        <i class="fa fa-close"> Delete</i></a>
                                    <?php endif; ?>
                                </p>
                                <p class="lead">
                                    <?php echo e($user->name); ?>

                                </p>
                                <p>
                                    <strong>
                                        <?php echo e($user->email); ?>

                                    </strong>
                                </p>
                                <p>
                                    <strong>
                                        <?php if(!empty($user->userDesignation->name)): ?>
                                        <?php echo e($user->userDesignation->name); ?>

                                        <?php else: ?>
                                        ---
                                        <?php endif; ?>
                                    </strong>
                                </p>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>

                <script src="<?php echo e(asset('js/admin_account.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>