<?php $__env->startSection('content'); ?>
<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><?php echo e($siteContents->title); ?></h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <?php echo $siteContents->content; ?>

                    </div>
                </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.clientlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>