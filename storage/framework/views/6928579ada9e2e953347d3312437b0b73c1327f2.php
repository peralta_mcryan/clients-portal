            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo e(config('app.url', '#')); ?>"><?php echo e(config('app.name', 'iManila')); ?></a>
                </div>

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                </ul>

                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> 
                                <?php $__env->startComponent('components.who'); ?>
                                <?php echo $__env->renderComponent(); ?>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <!-- Authentication Links -->
                            <?php if(Auth::guest()): ?>
                                <li><a href="<?php echo e(route('login')); ?>">Login</a></li>
                                <li><a href="<?php echo e(route('register')); ?>">Register</a></li>
                            <?php else: ?>
                                <?php $__env->startComponent('components.logout'); ?>
                                <?php echo $__env->renderComponent(); ?>
                            <?php endif; ?>
                        </ul>
                    </li>
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <!-- <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                </span>
                                </div>
                            </li> -->
                            <li>
                                <a href="<?php echo e(route('account-information', Auth::user()->id )); ?>"><i class="fa fa-user fa-fw"></i> Account Information</a>
                            </li>

                            <li>
                                <a href="#"><i class="fa fa-bank fa-fw"></i> Billing<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo e(route('official-receipts', Auth::user()->id)); ?>">Official Receipts</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('client-site-contents-edit-how-to-pay-bills',4)); ?>">How to pay your bills</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>

                            <li>
                                <a href="<?php echo e(route('my-project', Auth::user()->id)); ?>"><i class="fa fa-suitcase fa-fw"></i> Projects</a>
                            </li>
                            <?php ($count = session('DM')); ?>
                            <?php if($count > 0): ?>
                                 <li>
                                    <a href="<?php echo e(route('reports', Auth::user()->id)); ?>"><i class="fa fa-file-word-o fa-fw"></i> Digital Marketing Reports</a>
                                </li>
                            
                            <?php else: ?>
                               <li class = "li-no-hover">
                                    <a href="#" style="cursor:default; color: inherit; text-mute"><i class="fa fa-file-word-o fa-fw"></i> Digital Marketing Reports</a>
                                </li>
                            <?php endif; ?>
                                
                           
                            <!-- <li>
                                <a href="#"><i class="fa fa-comments fa-fw"></i> Request For Sales Meeting</a>
                            </li> -->

                            <li>
                                <a target="_blank" href="http://development.imanila.ph/imanilastockphotos/"><i class="fa fa-photo fa-fw"></i> Stock Photos</a>
                            </li>
                            
                            

                             <li>
                                <a href="#"><i class="fa fa-file-text fa-fw"></i> Terms & Conditions<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo e(route('client-site-contents-edit-user-policy',1)); ?>"> Acceptable User Policy</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('client-site-contents-edit-terms-and-conditions',2)); ?>"> Terms of Service</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>

                          <li>
                                <a href="#"><i class="fa fa-question fa-fw"></i> FAQ<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo e(route('client-site-contents-edit-hosting-faq',5)); ?>"> Hosting FAQ</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('client-site-contents-edit-domain-hosting-faq',6)); ?>"> Domain FAQ</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('client-site-contents-edit-email-hosting-faq',7)); ?>"> Email Hosting FAQ</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('client-site-contents-edit-website-hosting-faq',8)); ?>"> Website Hosting FAQ</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('client-site-contents-edit-database-hosting-faq',9)); ?>"> Database Hosting FAQ</a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://services.imanila.ph/knowledgebase.php"> Services FAQ</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>