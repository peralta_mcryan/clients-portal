<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Contacts</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-6">
            <p class="text-left">
                <a href="<?php echo e(route('client-contact')); ?>"><i class="fa fa-columns fa-1x text-left"></i></a>
                <a href="<?php echo e(route('client-contact-tabular')); ?>"><i class="fa fa-list fa-1x text-left"></i></a>
            </p>
        </div>
            <p class="text-right">
                <a href="<?php echo e(route('new-contact')); ?>"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
            </p>
            <div class="dataTable_wrapper">
                <?php 
                    $ctr = 1;
                ?>
                <div class="row">
                    <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Contact Name</th>
                            <th>Department</th>
                            <th>Designation</th>
                            <th>Email</th>
                            <th>Work Phone</th>
                            <th>Mobile Phone</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $contacts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $contact): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($contact->first_name); ?> <?php echo e($contact->last_name); ?> </td>
                            <td><?php echo e(!empty($contact->department)?$contact->department:"N/A"); ?></td>
                            <td><?php echo e(!empty($contact->designation)?$contact->designation:"N/A"); ?></td>
                            <td><?php echo e($contact->email); ?></td>
                            <td><?php echo e(!empty($contact->work_phone)?$contact->work_phone:"N/A"); ?></td>
                            <td><?php echo e(!empty($contact->mobile_phone)?$contact->mobile_phone:"N/A"); ?></td>
                            <td> 
                         
                                        <a href="<?php echo e(route('update-contact', $contact->id)); ?>">
                                            <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                            <span class="pull-left"></span>
                                        </a>
                                        <a class="pull-right ">
                                            <span class="pull-right imanila-color" style="cursor:pointer;"><i class="fa fa-close" onclick="javascript:deleteContact(<?php echo e($contact->id); ?>)"></i></span>
                                        </a>
                                    
                           </td>
                           <td>&nbsp; &nbsp; &nbsp;</td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>                
            </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12 text-center">
        <?php echo e($contacts->links()); ?>

    </div>
</div>
<script src="<?php echo e(asset('js/contacts/contacts.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>