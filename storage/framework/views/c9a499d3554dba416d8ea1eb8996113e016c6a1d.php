<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-12">
        <h4 class="page-header">Search Results for <strong><i><?php echo e($searchText); ?></i></strong></h4>
    </div>
</div>
<!-- /.row -->
<div class="row search-result">
    <div class="col-lg-12">
        <div class="panel tabbed-panel panel-primary">
            <div class="panel-heading clearfix">
                <div class="pull-left">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Clients (<?php echo e(count($clientSearchResults)); ?>)</a></li>
                        <li class=""><a href="#tab-primary-2" data-toggle="tab" aria-expanded="false">Projects (<?php echo e(count($projectSearchResults)); ?>)</a></li>
                        <li class=""><a href="#tab-primary-3" data-toggle="tab" aria-expanded="false">Contacts (<?php echo e(count($contactSearchResults)); ?>)</a></li>
                        <li class=""><a href="#tab-primary-4" data-toggle="tab" aria-expanded="false">Administrator Accounts (<?php echo e(count($userSearchResults)); ?>)</a></li>
                        <li class=""><a href="#tab-primary-5" data-toggle="tab" aria-expanded="false">Services (<?php echo e(count($serviceSearchResults)); ?>)</a></li>
                        <li class=""><a href="#tab-primary-6" data-toggle="tab" aria-expanded="false">Service Types (<?php echo e(count($serviceTypeSearchResults)); ?>)</a></li>
                        <li class=""><a href="#tab-primary-7" data-toggle="tab" aria-expanded="false">Files (<?php echo e(count($fileSearchResults)); ?>)</a></li>
                        <li class=""><a href="#tab-primary-8" data-toggle="tab" aria-expanded="false">Tickets (<?php echo e(count($ticketSearchResults)); ?>)</a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab-primary-1">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php if(count($clientSearchResults) > 0): ?>
                                    <?php $__currentLoopData = $clientSearchResults; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $clientSearchResult): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <a href="<?php echo e(route('show-client-account', $clientSearchResult->id)); ?>">
                                            <div class="col-lg-12 col-md-12">
                                                <strong><?php echo e($clientSearchResult->name); ?></strong>
                                                <hr></hr>
                                            </div>
                                        </a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <strong class="imanila-color">No results found.</strong>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-primary-2">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php if(count($projectSearchResults) > 0): ?>
                                    <?php $__currentLoopData = $projectSearchResults; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $projectSearchResult): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <a href="<?php echo e(route('show-project', $projectSearchResult->id)); ?>">
                                        <div class="col-lg-12 col-md-12">
                                            <strong><?php echo e($projectSearchResult->name); ?></strong>
                                            <hr></hr>
                                        </div>
                                    </a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <strong class="imanila-color">No results found.</strong>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-primary-3">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php if(count($contactSearchResults) > 0): ?>
                                    <?php $__currentLoopData = $contactSearchResults; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $contactSearchResult): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <a href="<?php echo e(route('show-contact', $contactSearchResult->id)); ?>">
                                            <div class="col-lg-12 col-md-12">
                                                <strong><?php echo e($contactSearchResult->first_name); ?> <?php echo e($contactSearchResult->last_name); ?></strong>
                                                <hr></hr>
                                            </div>
                                        </a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <strong class="imanila-color">No results found.</strong>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-primary-4">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php if(count($userSearchResults) > 0): ?>
                                    <?php $__currentLoopData = $userSearchResults; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $userSearchResult): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <a href="<?php echo e(route('administrator-accounts-edit', $userSearchResult->id)); ?>">
                                            <div class="col-lg-12 col-md-12">
                                                <strong><?php echo e($userSearchResult->name); ?></strong>
                                                <hr></hr>
                                            </div>
                                        </a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <strong class="imanila-color">No results found.</strong>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-primary-5">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php if(count($serviceSearchResults) > 0): ?>
                                    <?php $__currentLoopData = $serviceSearchResults; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $serviceSearchResult): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <a href="<?php echo e(route('update-service', $serviceSearchResult->id)); ?>">
                                            <div class="col-lg-12 col-md-12">
                                                <strong><?php echo e($serviceSearchResult->name); ?></strong>
                                                <hr></hr>
                                            </div>
                                        </a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <strong class="imanila-color">No results found.</strong>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-primary-6">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php if(count($serviceTypeSearchResults) > 0): ?>
                                    <?php $__currentLoopData = $serviceTypeSearchResults; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $serviceTypeSearchResult): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <a href="<?php echo e(route('update-service-type', $serviceTypeSearchResult->id)); ?>">
                                            <div class="col-lg-12 col-md-12">
                                                <strong><?php echo e($serviceTypeSearchResult->name); ?></strong>
                                                <hr></hr>
                                            </div>
                                        </a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <strong class="imanila-color">No results found.</strong>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab-primary-7">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php if(count($fileSearchResults) > 0): ?>
                                    <?php $__currentLoopData = $fileSearchResults; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fileSearchResult): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <a href="<?php echo e(route('download-project-files', [$fileSearchResult->project_id, $fileSearchResult->file_folder_id, $fileSearchResult->filename])); ?>">
                                            <div class="col-lg-12 col-md-12">
                                                <strong><?php echo e($fileSearchResult->filename); ?></strong>
                                                <hr></hr>
                                            </div>
                                        </a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <strong class="imanila-color">No results found.</strong>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-primary-8">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php if(count($ticketSearchResults) > 0): ?>
                                    <?php $__currentLoopData = $ticketSearchResults; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ticketSearchResult): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <a href="<?php echo e(route('admin-tickets.show', $ticketSearchResult->id)); ?>">
                                            <div class="col-lg-12 col-md-12">
                                                <strong><?php echo e($ticketSearchResult->control_no); ?></strong>
                                                <hr></hr>
                                            </div>
                                        </a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <strong class="imanila-color">No results found.</strong>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>