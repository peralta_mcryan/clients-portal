<?php $__env->startSection('content'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><?php echo e($project->name); ?></h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-8">
                        <div class="panel tabbed-panel panel-primary">
                            <div class="panel-heading clearfix">
                                <div class="panel-title pull-left">
                                    Project Details
                                </div>
                                <div class="pull-right">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Overview</a></li>
                                        <li class=""><a href="#tab-primary-2" data-toggle="tab" aria-expanded="false">Files</a></li>
                                        <li class=""><a href="#tab-primary-3" data-toggle="tab" aria-expanded="false">Inclusions</a></li>
                                        <li class=""><a href="#tab-primary-4" data-toggle="tab" aria-expanded="false">Requirements</a></li>
                                        <li class=""><a href="#tab-primary-5" data-toggle="tab" aria-expanded="false">Themes</a></li>
                                        <li class=""><a href="#tab-primary-6" data-toggle="tab" aria-expanded="false">Tickets</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab-primary-1">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h3><?php echo e($project->name); ?>  &nbsp;<a href="<?php echo e(route('update-project', $project->id)); ?>" class="text-right"><i class="fa fa-edit imanila-color"></i> </a></h3>
                                                <hr></hr>

                                                <div class="col-xs-12 col-lg-12">
                                                        <small>
                                                            <strong>Description</strong>
                                                            <br>
                                                            <?php echo e($project->description); ?>

                                                        </small>
                                                        <hr></hr>
    
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-xs-12 col-lg-4">
                                                                <small>
                                                                    <strong>Project Owner</strong>
                                                                    <br>
                                                                    <?php if(empty($project->accounts->name)): ?> 
                                                                        N/A
                                                                    <?php else: ?>
                                                                        <?php echo e($project->accounts->name); ?>

                                                                    <?php endif; ?>
                                                                </small>
                                                            </div>
            
                                                            <div class="col-xs-12 col-lg-4">
                                                                <small>
                                                                    <strong>Project Status</strong>
                                                                    <br>
                                                                    <?php echo e($projectStatus); ?>

                                                                </small>
                                                            </div>
            
                                                            <div class="col-xs-12 col-lg-4">
                                                                <small>
                                                                    <strong>Project Manager</strong>
                                                                    <br>
                                                                    <?php if(empty($project->users->name)): ?> 
                                                                        N/A
                                                                    <?php else: ?>
                                                                        <?php echo e($project->users->name); ?>

                                                                    <?php endif; ?>
                                                                </small>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <p>
                                                            &nbsp;
                                                        </p>
                                                    </div>

                                                    <div class="col-xs-12 col-lg-4">
                                                        <small>
                                                            <strong>Service Contract</strong>
                                                            <br>
                                                            <?php if(!empty($serviceContract)): ?>
                                                                <a href="<?php echo e($serviceContract); ?>" class="imanila-color"><?php echo e($project->service_contract); ?></a>
                                                            <?php else: ?>
                                                                N/A
                                                            <?php endif; ?>
                                                        </small>
                                                    </div>
    
                                                    <div class="col-xs-12 col-lg-4">
                                                        <small>
                                                            <strong>Project Start</strong>
                                                            <br>
                                                           <?php echo e((!empty($project->project_start)?(\Carbon\Carbon::parse($project->project_start)->format('F j, Y, g:i a')):'---')); ?>

                                                        </small>
                                                    </div>
    
                                                    <div class="col-xs-12 col-lg-4">
                                                        <small>
                                                            <strong>Project End</strong>
                                                            <br>
                                                            <?php echo e(\Carbon\Carbon::parse($project->project_end)->format('F j, Y, g:i a')); ?>

                                                        </small>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <p>
                                                            &nbsp;
                                                        </p>
                                                    </div>

                                                    <div class=
                                                    "col-xs-12 col-lg-4">
                                                        <small>
                                                            <strong>Assigned AE</strong>
                                                            <br>
                                                            <?php echo e(!empty($ae->name)?$ae->name:'---'); ?>

                                                        </small>
                                                    </div>
                                                    <div class="col-xs-12 col-lg-4">
                                                        <small>
                                                            <strong>Selected Themes</strong>
                                                            <br>
                                                            <?php ($display = '---'); ?>
                                                            <?php $__currentLoopData = $themes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $theme): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php
                                                                if(in_array($theme->id,$selected))
                                                                {
                                                                    if($display == '---')
                                                                    {
                                                                        $display = $theme->name;
                                                                    }else{
                                                                        $display .= ','.$theme->name;
                                                                    }
                                                                }
                                                                
                                                            ?> 
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            <?php echo e($display); ?>

                                                        </small>
                                                    </div>
                                                <div class="col-xs-12">
                                                    <hr>
                                                        <h4 class="text-center">Timeline</h4>
                                                    <hr>
                                                </div>

                                                <?php if($chartData != '[]'): ?>
                                                <div class="col-lg-11">
                                                    <div id="chart_div"></div>
                                                </div>
                                                <?php else: ?>
                                                    <div class="col-lg-12 text-center">
                                                        No Project Timeline
                                                        <hr>
                                                    </div>
                                                <?php endif; ?>
                                                <?php 
                                                    $ctr = 1;
                                                ?>
                                                <div class="row">
                                                <div class="col-lg-12">
                                                <?php $__currentLoopData = $projectScheduleList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $projectSchedule): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="col-sm-6">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <span class="pull-left"><strong><?php echo e($projectSchedule->phase_name); ?></strong></span> 
                                                                <span class="pull-right"><strong><?php echo e($projectSchedule->working_days); ?> Days</strong></span>
                                                                &nbsp;
                                                                
                                                                
                                                            </div>
                                                            <div class="panel-body">
                                                                <?php echo $projectSchedule->description; ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php if($ctr % 2 == 0): ?>
                                                        </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                    <?php endif; ?>
                                                    <?php $ctr++; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-primary-2">
                                        <form action="<?php echo e(route('upload-project-files', $project->id)); ?>" method="post" enctype="multipart/form-data">
                                            <?php echo e(csrf_field()); ?>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-4">
                                                        <?php echo e(Form::select('file_folder', $fileFolders, '',['id' => 'file_folder',  'class'=>''])); ?>

                                                    </div>
                                                    <div class="col-lg-4">
                                                        <input class="pull-left" type="file" name="file_projects[]" id="file_projects" multiple>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div><a href="<?php echo e(route('download-project-files-all', $project->id)); ?>" class="btn btn-primary pull-right">Download All</a></div>
                                                        <div><button  style="margin-right: 10px;" type="submit" class="btn btn-primary pull-right">Upload</button></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <hr></hr>
                                                    <?php if(count($fileFolders) > 0): ?>
                                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                        <thead>
                                                            <tr>
                                                                <th>File Category</th>
                                                                <th>File Count</th>
                                                                <th>Download</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $__currentLoopData = $fileFolders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <tr class="clickable-row" data-href="<?php echo e(route('view-files', [$project->id, $key])); ?>">
                                                                    <td>
                                                                        <?php echo e($name); ?>

                                                                    </td>

                                                                    <td>
                                                                        <?php echo e($project->attachments->where('file_folder_id',$key)->count()); ?>

                                                                    </td>

                                                                    <td class="text-center">
                                                                        <?php if($project->attachments->where('file_folder_id',$key)->count() > 0): ?> 
                                                                            <a href="<?php echo e(route('download-project-files-category', [$project->id, $key])); ?>"><span class=""><small>Download Files</small></span></a>
                                                                        <?php else: ?> 
                                                                        <?php endif; ?>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </tbody>
                                                    </table>
                                                    <?php else: ?>
                                                        <div class="alert alert-info">
                                                            <i class="fa fa-hand-o-right"></i>
                                                            <small>No file available.</small>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="tab-primary-3">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <?php echo $project->inclusions; ?>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-primary-4">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <?php echo $project->requirements; ?>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-primary-5">
                                        <form action="<?php echo e(route('my-project-themes', $project->id)); ?>" method="post" enctype="multipart/form-data">
                                            <?php echo e(csrf_field()); ?>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-6">
                                                        
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="col-lg-12">
                                                            
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="col-lg-6">&nbsp;</div>
                                                        <div class="col-lg-6">
                                                            <div class="col-lg-12">   
                                                            <p>
                                                                
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
    
                                            <?php if(count($themes->toArray())>0): ?>
                                                <?php $__currentLoopData = $themes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $theme): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <h4><?php echo e($theme->name); ?> 
                                                                        <a target = "_blank" href="http://<?php echo e($theme->website_links); ?>">
                                                                            <span class="pull-right"> &nbsp;<i class="fa fa-external-link" title = "Open Link"></i> &nbsp;</span>
                                                                        </a> &nbsp;
                                                                    </h4>
                                                                </div>   
                                                            </div>
                                                        </div>
                                                        <div class="panel-body">
                                                             <div class="col-lg-12">
                                                                <?php echo e($theme->website_links); ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            <?php else: ?>
                                                <h2>No Selected Themes</h2>
                                            <?php endif; ?>  
                                        </form>
                                        <div class="row">
                                            <div class="col-lg-12 text-center">
                                               
                                            </div>
                                        </div>

                                    </div>
                                     <div class="tab-pane" id="tab-primary-6">
                                          <?php $__currentLoopData = $project->issue_list()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $issue): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="col-md-6">
                                                <div  class="panel tabbed-panel panel-danger">
                                                    <div class="panel-heading clearfix">
                                                        <div class="panel-title pull-left">
                                                            <?php echo e(date('F d, Y',strtotime($issue->created_at))); ?>

                                                        </div>
                                                    </div>
                                                <a href="<?php echo e(route('admin-tickets.show',$issue->id)); ?>">
                                                        <div class="panel-body">
                                                            
                                                            <h4><strong> Issue Ctrl #: <?php echo e($issue->control_no); ?> </strong></h4>
                                                            Issue date : <?php echo e(date('F d, Y',strtotime($issue->created_at))); ?>

                                                            <br>
                                                            Created by : <?php echo e($issue->user->name); ?>

                                                        </div>
                                                    </a>
                                               </div>
                                            </div>
                                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>     
                                        <div class="row">
                                            <div class="col-lg-12 text-center">
                                               
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <div class="col-lg-4">
                            <div class="dataTable_wrapper">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        Project Activity
                                    </div>
                                    <div class="panel-body">
                                        <?php if(!empty($projectActivity)): ?>
                                            <?php $__currentLoopData = $projectActivity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="panel panel-primary">
                                                    <div class="panel-body">
                                                        <small><?php echo e($value->created_at->format('F j, Y, g:i a')); ?></small>
                                                        <br>
                                                        <small><?php echo e($value->remarks); ?></small>
                                                    </div>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script src="<?php echo e(asset('js/projects/projects.js')); ?>"></script>
                <script>
                    $(document).ready(function () {
                        var url = window.location.href;
                        var activeTab = url.substring(url.indexOf("#") + 1);
                        if (url.indexOf("#") >= 0) {
                            $(".tab-pane").removeClass("active in");
                            $('a[href="#'+ activeTab +'"]').tab('show')
                        }
                    });
                </script>
                <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

                <script>
                    $(document).ready(function () {
                        <?php if($chartData != '[]'): ?>
                            google.charts.load('visualization', '1', {packages: ['corechart']});
                            google.charts.setOnLoadCallback(drawChart);            
                        <?php endif; ?>
                    });
            
                    function drawChart() {
                        var jsonChartData = <?php echo $chartData; ?>;
                        var data = new google.visualization.DataTable();
                        data.addColumn('string', 'Name');
                        data.addColumn('number', 'spacer');
                        data.addColumn('number', 'Working Days');
                        data.addRows(jsonChartData);

                        
                        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
                        chart.draw(data, {
                            isStacked: true,
                            series: {
                                0: {
                                    visibleInLegend: false,
                                    color: 'transparent'
                                }
                            },
                            hAxis: {
                                title: 'Working Days',
                                viewWindow: {
                                    min: 1
                                },
                                gridlines: {
                                    color: '#000'
                                }
                            },
                            vAxis: {
                                title: 'Phases',
                                gridlines: {
                                    color: '#000'
                                }
                            }

            
                        });
                    }
            
                    function setWorkingDays() {
                        var startDate = $('#start_date').val();
                        var endDate = $('#end_date').val();
            
                        if ((startDate != '') && (endDate != '')) {
                            // $()
                        }
                    }
            
                    function deleteProjectSchedule(projectId, projectScheduleId) {
                        if (confirm('Are you sure you want to delete this schedule?')) {
                            $.ajax({
                                type:'GET',
                                url:'/project-timeline/' + projectScheduleId + '/destroy',
                                success:function(data){
                                    window.location = '/project-timeline/'+projectId;
                                }
                            });
                        }
                    }
                </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>