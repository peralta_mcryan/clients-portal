<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><?php echo e($pageName); ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Schedule Details
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form" method="POST" action="<?php echo e($route); ?>" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group col-md-4">
                                    <label for="order"><span class="asterisk-marker">*</span> Order</label>
                                    <input id="order" name="order" class="form-control" placeholder="Order" value="<?php echo e($projectSchedule->phase_no); ?>" required>
                                    <?php if($errors->has('order')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('order')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="phase_name"><span class="asterisk-marker">*</span> Phase Name</label>
                                    <input id="phase_name" name="phase_name" class="form-control" placeholder="Phase Name"  value="<?php echo e($projectSchedule->phase_name); ?>" required>
                                    <?php if($errors->has('phase_name')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('phase_name')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="working_days"><span class="asterisk-marker">*</span> Working Days</label>
                                    <input id="working_days" name="working_days" class="form-control" placeholder="Working Days"  value="<?php echo e($projectSchedule->working_days); ?>" required>
                                    <?php if($errors->has('working_days')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('working_days')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="description"> Description</label>
                                    <textarea name="description" id="description"><?php echo e($projectSchedule->description); ?></textarea>
                                    <?php if($errors->has('description')): ?>
                                        <span class="error-block">
                                            <strong><?php echo e($errors->first('description')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group col-md-4">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                    <a href="<?= route('client-project-tabular') ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                                </div>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <div class="col-lg-12">
            <hr>
        </div>

        <?php if($chartData != '[]'): ?>
            <div class="col-lg-11">
                <div id="chart_div"></div>
            </div>
        <?php else: ?>
            <div class="col-lg-12 text-center">
                No Project Timeline
                <hr>
            </div>
        <?php endif; ?>
        <?php 
            $ctr = 1;
        ?>
        <div class="row">
        <div class="col-lg-12">
        <?php $__currentLoopData = $projectScheduleList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $projectSchedule): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="pull-left"><strong><?php echo e($projectSchedule->phase_name); ?></strong></span> 
                        <span class="pull-right"><strong><?php echo e($projectSchedule->working_days); ?> Days</strong></span>
                        &nbsp;
                        
                        
                    </div>
                    <div class="panel-body">
                        <?php echo $projectSchedule->description; ?>

                    </div>
                    <div class="panel-footer">
                        <small>
                            <a href="<?php echo e(route('project-timeline.edit', [$projectSchedule->id])); ?>">
                                <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                <span class="pull-left">&nbsp;Edit Details</span>
                            </a>
                            <a class="pull-right ">
                                <span class="pull-right imanila-color" style="cursor:pointer;"><i class="fa fa-close" onclick="javascript:deleteProjectSchedule(<?php echo e($project->id); ?>, <?php echo e($projectSchedule->id); ?>)"></i></span>
                            </a>
                        </small>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <?php if($ctr % 3 == 0): ?>
                </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
            <?php endif; ?>
            <?php $ctr++; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <script src="<?php echo e(asset('ckeditor/ckeditor.js')); ?>"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script>
        $(document).ready(function () {
            CKEDITOR.replace( 'description' );
            <?php if($chartData != '[]'): ?>
                google.charts.load('visualization', '1', {packages: ['corechart']});
                google.charts.setOnLoadCallback(drawChart);            
            <?php endif; ?>
        });

        function drawChart() {
            var jsonChartData = <?php echo $chartData; ?>;
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Name');
            data.addColumn('number', 'spacer');
            data.addColumn('number', 'Working Days');
            data.addRows(jsonChartData);
            
            var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
            chart.draw(data, {
                isStacked: true,
                series: {
                    0: {
                        visibleInLegend: false,
                        color: 'transparent'
                    }
                },
                hAxis: {
                    minValue: 1
                }

            });
        }

        function setWorkingDays() {
            var startDate = $('#start_date').val();
            var endDate = $('#end_date').val();

            if ((startDate != '') && (endDate != '')) {
                // $()
            }
        }

        function deleteProjectSchedule(projectId, projectScheduleId) {
            if (confirm('Are you sure you want to delete this schedule?')) {
                $.ajax({
                    type:'GET',
                    url:'/project-timeline/' + projectScheduleId + '/destroy',
                    success:function(data){
                        window.location = '/project-timeline/'+projectId;
                    }
                });
            }
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>