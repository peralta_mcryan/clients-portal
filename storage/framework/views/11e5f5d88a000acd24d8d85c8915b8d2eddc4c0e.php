<?php $__env->startSection('content'); ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Client Accounts</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                            <p class="text-right">
                                <a href="<?php echo e(route('new-client-account')); ?>"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                            </p>
                            <div class="dataTable_wrapper">
                                <?php 
                                    $ctr = 1;
                                ?>
                                <div class="row">
                                <?php $__currentLoopData = $accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <div class="col-lg-4 col-md-6">
                                            <div class="panel panel-primary">
                                                <a href="<?php echo e(route('show-client-account', $account->id)); ?>">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div><strong><?php echo e($account->name); ?></strong></div>
                                                                <br>
                                                                <div>
                                                                    <small>
                                                                        <i class="fa fa-phone"></i>
                                                                        <?php if(!empty($account->work_phone)): ?>
                                                                            <?php echo e($account->work_phone); ?>

                                                                        <?php else: ?>
                                                                            Not Available
                                                                        <?php endif; ?>
                                                                    </small>
                                                                </div>
                                                                <div>
                                                                    <small>
                                                                        <i class="fa fa-mobile-phone"></i> 
                                                                        <?php if(!empty($account->mobile_phone)): ?>
                                                                            <?php echo e($account->mobile_phone); ?>

                                                                        <?php else: ?>
                                                                            Not Available
                                                                        <?php endif; ?>
                                                                    </small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="panel-footer">
                                                    <small>
                                                        <a href="<?php echo e(route('update-client-account', $account->id)); ?>">
                                                            <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <span class="pull-left">&nbsp;Edit Details</span>
                                                        </a>
                                                        <a class="pull-right ">
                                                            <span class="pull-right imanila-color" style="cursor:pointer;"><i class="fa fa-close" onclick="javascript:deleteClientAccount(<?php echo e($account->id); ?>)"></i></span>
                                                        </a>
                                                    </small>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>

                                    <?php if($ctr % 3 == 0): ?>
                                        </div>
                                        <div class="row">
                                    <?php endif; ?>
                                    <?php $ctr++; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <?php echo e($accounts->links()); ?>

                    </div>
                </div>
                <script src="<?php echo e(asset('js/accounts/accounts.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>