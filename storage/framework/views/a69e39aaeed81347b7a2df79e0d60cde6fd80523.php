<?php $__env->startSection('content'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><?php echo e($changerequest->project->name); ?></h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-8">
                        <div class="panel tabbed-panel panel-primary">
                            <div class="panel-heading clearfix">
                                <div class="panel-title pull-left">
                                    Change Request Task   &nbsp; <a href="<?php echo e(route('change-requests.edit', $changerequest->id)); ?>" class="text-right"><i class="fa fa-edit "></i> </a>
                                </div>
                                <div class="pull-right">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Overview</a></li>
                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab-primary-1">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-lg-12">  
                                                    <div class="panel panel-primary">
                                                        <div class="panel-body">
                                                            <?php echo $changerequest->task; ?>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <div class="col-lg-4">
                            <div class="dataTable_wrapper">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                       Change Request Details
                                    </div>
                                    <div class="panel-body">
                                        <div class="panel panel-primary">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-5">
                                                        Created Date:
                                                    </div>
                                                    <div class="col-lg-7">
                                                       <strong> <?php echo e($changerequest->created_at->format('F j, Y, g:i a')); ?></strong>
                                                    </div>
                                                </div>
                                              
                                                <div class="row">
                                                    <div class="col-lg-5">
                                                           Account:
                                                    </div>
                                                    <div class="col-lg-7">
                                                          <strong>  <?php echo e($changerequest->project->accounts->name); ?></strong>
                                                    </div>
                                                </div>
                                        
                                                <div class="row">
                                                    <div class="col-lg-5">
                                                           Project Involve:
                                                    </div>
                                                    <div class="col-lg-7">
                                                           <strong> <?php echo e($changerequest->project->name); ?></strong>
                                                    </div>
                                                </div>
                                                
                                           
                                                <br>
                                              
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script src="<?php echo e(asset('js/projects/projects.js')); ?>"></script>
                <script>
                    $(document).ready(function () {
                        var url = window.location.href;
                        var activeTab = url.substring(url.indexOf("#") + 1);
                        if (url.indexOf("#") >= 0) {
                            $(".tab-pane").removeClass("active in");
                            $('a[href="#'+ activeTab +'"]').tab('show')
                        }
                    });
                </script>
           
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>