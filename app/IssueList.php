<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\CarbonPeriod;
use Carbon\Carbon;

class IssueList extends Model
{
     use SoftDeletes;

    protected $fillable = ['account_id','project_id','control_no','description','status','priority','assigned_to','created_by'];

   function project()
    {
        return $this->belongsTo('App\Project', 'project_id');
    }

   function assignee()
    {
        return $this->hasOne('App\User', 'id','assigned_to');
    }

   function user()
    {
        return $this->hasOne('App\User', 'id','created_by');
    }

   function issue_thread()
    {
        return $this->hasMany('App\IssueThread', 'issue_id','id');
    }

    function getAgingDays() {
        $start = new \DateTime($this->created_at);
        $end = new \DateTime(Carbon::now());
        // otherwise the  end date is excluded (bug?)
        $end->modify('+1 day');
        
        $interval = $end->diff($start);
        
        // total days
        $days = $interval->days;
        
        // create an iterateable period of date (P1D equates to 1 day)
        $period = new \DatePeriod($start, new \DateInterval('P1D'), $end);
        
        // best stored as array, so you can add more than one
        $holidays = array('2012-09-07');
        
        foreach($period as $dt) {
            $curr = $dt->format('D');
        
            // substract if Saturday or Sunday
            if ($curr == 'Sat' || $curr == 'Sun') {
                $days--;
            }
        
            // (optional) for the updated question
            elseif (in_array($dt->format('Y-m-d'), $holidays)) {
                $days--;
            }
        }
        return $days;
    }
}
