<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Contact;
use App\Project;
use App\User;
use Illuminate\Http\Request;
use Auth;
class SelectedTheme extends Mailable
{
    public $request;
    public $contact;
    public $user;
    public $project;
    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request, Contact $contact, User $user, Project $project, $details )
    {
        $this->request = $request;
        $this->contact = $contact;
        $this->user = $user;
        $this->project = $project;
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(count($this->details) > 0 )
            $emailBody = "Hi Ms. / Mr. [ADMIN_NAME] <br><br> Greetings from iManila! <br><br> A set of themes was selected by [CLIENT_NAME] for the project [PROJECT_NAME]: <br>";
        else
            $emailBody = "Hi Ms. / Mr. [ADMIN_NAME] <br><br> Greetings from iManila! <br><br> [CLIENT_NAME] has removed current selected themes for the project [PROJECT_NAME]. <br>";
        

        $remarks = '';
        if($this->request->remarks != '')
        {
            $remarks .= "With Remarks:<br>";
            $remarks .= '<span style = "font-style:italic;">';
            $remarks .= $this->request->remarks;
            $remarks .= "</span>";
        }
        $selectedThemes = '';
        $contactName = $this->contact->first_name.' '.$this->contact->last_name;
        $adminName = $this->user->name;

        if(count($this->details) > 0 ){
            $selectedThemes .= '<ul>';
            foreach ($this->details as $key => $value) {  
                    $selectedThemes .= '<li>';
                    $selectedThemes .= '<a href="http://'.$value->website_links.'">';
                    $selectedThemes .= $value->name;
                    $selectedThemes .= '</a>';
                    $selectedThemes .= '</li>';         
            }
            $selectedThemes .= '</ul>';
        }
        $emailBody = str_replace('[CLIENT_NAME]', $contactName, $emailBody);
        $emailBody = str_replace('[ADMIN_NAME]', $adminName, $emailBody);
        $emailBody = str_replace('[PROJECT_NAME]', $this->project->name, $emailBody);
        if(count($this->details) > 0 ){
            $emailMarkdown = $this->markdown('emails.projects.themeSelection')
                ->subject('[CLIENT PORTAL] Client Portal Theme Selection Notification for '.$this->project->name)
                ->bcc('system@clientsportal.imanila.ph')
                ->bcc('projects@clientsportal.imanila.ph')
                ->with([
                    'emailBody' => $emailBody,
                    'selectedThemes' => $selectedThemes,
                    'remarks'       => $remarks
                ]);
        }else{
              $emailMarkdown = $this->markdown('emails.projects.themeSelection')
                ->subject('[CLIENT PORTAL] Client Portal Theme Selection Notification for '.$this->project->name)
                ->bcc('system@clientsportal.imanila.ph')
                ->with([
                    'emailBody' => $emailBody,
                    'remarks'       => $remarks
                ]);
        }
        if (!empty($this->project->assignedAe->email)) {
            $emailMarkdown = $emailMarkdown->cc($this->project->assignedAe->email);
        }

        return $emailMarkdown;
    }
}
