<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Contact;
use App\Project;
use App\User;
use Illuminate\Http\Request;
use Auth;

class FileUploaded extends Mailable
{
    use Queueable, SerializesModels;
    public $request;
    public $contact;
    public $user;
    public $project;
    public $uploadedFiles;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request, Contact $contact, User $user, Project $project, $uploadedFiles)
    {
        $this->request = $request;
        $this->contact = $contact;
        $this->user = $user;
        $this->project = $project;
        $this->uploadedFiles = $uploadedFiles;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $emailBody = "Hi Ms. / Mr. [ADMIN_NAME] <br><br> Greetings from iManila! <br><br> New file/s has been uploaded by [CLIENT_NAME] for the project [PROJECT_NAME]: <br> [NEW_FILES]";
        $newFiles = '';
        $contactName = $this->contact->first_name.' '.$this->contact->last_name;
        $adminName = $this->user->name;

        foreach ($this->uploadedFiles as $key => $value) {
            $newFiles .= '<ul>';
            foreach ($value as $fileKey => $filename) {
                $newFiles .= '<li>';
                $newFiles .= '<a href="'.route('download-project-files', [$this->project->id, $key, $filename]).'">';
                $newFiles .= $filename;
                $newFiles .= '</a>';
                $newFiles .= '</li>';
            }
            $newFiles .= '</ul>';
        }

        $emailBody = str_replace('[CLIENT_NAME]', $contactName, $emailBody);
        $emailBody = str_replace('[ADMIN_NAME]', $adminName, $emailBody);
        $emailBody = str_replace('[NEW_FILES]', $newFiles, $emailBody);
        $emailBody = str_replace('[PROJECT_NAME]', $this->project->name, $emailBody);

        $emailMarkdown = $this->markdown('emails.files.upload')
            ->subject('[CLIENT PORTAL] Client Portal File Upload Notification for '.$this->project->name)
            ->bcc('system@clientsportal.imanila.ph')
            ->cc('projects@imanila.ph')
            ->with([
                'emailBody' => $emailBody
            ]);

        if (!empty($this->project->assignedAe->email)) {
            $emailMarkdown = $emailMarkdown->cc($this->project->assignedAe->email);
        }

        return $emailMarkdown;
    }
}
