<?php

namespace App\Mail;

use App\Project;
use App\Contact;
use App\Service;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Auth;

class ProjectWelcome extends Mailable
{
    use Queueable, SerializesModels;
    public $project;
    public $contact;
    public $serviceTypeIds;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Project $project, Contact $contact,$servicetype)
    {
        $this->project = $project;
        $this->contact = $contact;
        $this->serviceTypeIds = $servicetype;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $emailBody = '';
        $service = Service::where('service_type_id',$this->serviceTypeIds)->first();
        if (!empty($service->email_template)) {
            $emailBody = $service->email_template;
        }
         
        $contactName = $this->contact->first_name.' '.$this->contact->last_name;
        $materialsDue = date('F d, Y', strtotime('+15 days'));
        $clientLoginUrl = '<a href="'.route('client-login').'">iManila Client Portal</a>';
        $clientUsername = $this->contact->email;
        $clientResetPasswordUrl = '<a href="'.route('client-password-request').'">here</a>';
        $adminName = Auth::user()->name;
        $adminDesignation = Auth::user()->designation;

        $emailBody = str_replace('[CLIENT_NAME]', $contactName, $emailBody);
        $emailBody = str_replace('[MATERIALS_DUE_DATE]', $materialsDue, $emailBody);
        $emailBody = str_replace('[WEBSITE_LOGIN_URL]', $clientLoginUrl, $emailBody);
        $emailBody = str_replace('[CLIENT_USERNAME]', $clientUsername, $emailBody);
        $emailBody = str_replace('[RESET_PASSWORD_LINK]', $clientResetPasswordUrl, $emailBody);
        $emailBody = str_replace('[ADMIN_NAME]', $adminName, $emailBody);
        $emailBody = str_replace('[ADMIN_DESIGNATION]', $adminDesignation, $emailBody);

        $emailMarkdown = $this->markdown('emails.projects.welcome')
                            ->subject('[CLIENT PORTAL] iManila Client Portal - Project Registration')
                            ->bcc('system@clientsportal.imanila.ph')
                            ->cc('projects@imanila.ph')
                            // ->bcc('salessupport@imanila.ph')
                            ->with([
                                'emailBody' => $emailBody,
                            ]);

        if (!empty($this->project->assignedAe->email)) {
            $emailMarkdown = $emailMarkdown->cc($this->project->assignedAe->email);
        }

        return $emailMarkdown;
    }
}
