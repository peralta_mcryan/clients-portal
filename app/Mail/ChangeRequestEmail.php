<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChangeRequestEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $changerequest;
    

    /**
     * Create a new message instance.
     *
     * @return void
     */

      public function __construct( $changerequest)
    {
        $this->changerequest = $changerequest;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         
        $emailBody = "Hi Ms. / Mr. ".$this->changerequest->project->assignedAe->name." <br><br> Greetings from iManila! <br><br> A change request order is created under  your project: &nbsp; <strong>".$this->changerequest->project->name."<strong> <br><br> Details are as follows:";
        
        $subject = '[CLIENT PORTAL] Client Portal New Change Request for '.$this->changerequest->project->name;
        if(empty($cr->project->assignedAe->email))
        {
             $subject .=' (No AE Recepient)';
        }
         $emailMarkdown = $this->markdown('emails.projects.changeRequest')
                ->subject($subject)
                ->bcc('system@clientsportal.imanila.ph')
                ->bcc('projects@clientsportal.imanila.ph')
                ->with([
                    'emailBody'     =>      $emailBody,
                    'task'          =>      $this->changerequest->task,
                    'price'         =>      number_format($this->changerequest->price,2),
                    'man_hours'     =>      number_format($this->changerequest->man_hours,2),

                ]);

    
          return $emailMarkdown;
    }
}
