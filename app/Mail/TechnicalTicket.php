<?php

namespace App\Mail;

use App\IssueList;
use App\AlertStatus;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Auth;

class TechnicalTicket extends Mailable
{
    use Queueable, SerializesModels;
    public $issueList;
    public $email_type;
    public $ticket;
    public $thread;
    public $reply;

    /**
     * Create a new message instance.
     *
     * @return void
     */
   public function __construct(IssueList $issueList, $email_type,$ticket,$thread,$reply = '')
    {
        $this->issueList = $issueList;
        $this->email_type = $email_type;
        $this->ticket = $ticket;
        $this->thread = $thread;
        $this->reply = $reply;
        
   
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {       
        $stats =config('constants.ticket_status_def');
        $priority =config('constants.ticket_priority_def');

        $subject = '';
        $emailBody = '';
        $emailBody .= 'Good day!<br><br>';
        $departmentEmail = '';
        if ($this->email_type == 1)
        {       
            $subject  = '[CLIENT PORTAL] New Ticket Issue for '.$this->issueList->project->name;    
            $emailBody .= 'An issue ticket is released under your project: <strong>'.$this->issueList->project->name.'</strong>';
            $emailBody .= '<br><br>Details are as follows:';
            $details ='<ul>';
            $details.= '<li> CTRL #:'.$this->issueList->control_no.'</li>';
            $details.= '<li> Status:'.$stats[$this->issueList->status].'</li>';
            $details.= '<li> Priority:'.$priority[$this->issueList->priority].'</li>';
            $details.= '<li> Details:'.$this->issueList->description.'</li>';
            $details.='</ul>';
            $departmentEmail = $this->issueList->assignee->email;
        }
        elseif($this->email_type == 2)
        {
            $subject  = '[CLIENT PORTAL]['.$this->issueList->control_no.'] Ticket Issue Update for '.$this->issueList->project->name;
            $emailBody .= 'This email is to notify that the Issue <strong>'.$this->issueList->control_no.'</strong> under <strong>'.$this->issueList->project->name.'</strong> is updated.';
            $emailBody .= '<br><br>Details are as follows:';
            $details ='<ul>';
            $details.= '<li> CTRL #:'.$this->issueList->control_no.'</li>';
            $details.= '<li> Priority:'.$stats[$this->issueList->status].'</li>';
            $details.= '<li> Status:'.$priority[$this->issueList->priority].'</li>';
            $details.= '<li> Details:'.$this->issueList->description.'</li>';
            $details.='</ul>';
            $departmentEmail = $this->issueList->assignee->email;
        }
        elseif($this->email_type == 3)
        {
            $subject  = '[CLIENT PORTAL]['.$this->issueList->control_no.'] Ticket Issue Update for '.$this->issueList->project->name;
            $emailBody .= 'This email is to notify that the Issue '.$this->issueList->control_no.' under '.$this->issueList->project->name.' is deleted.';
            $details = '';
            $departmentEmail = $this->issueList->assignee->email;
        } 
        elseif($this->email_type == 4)
        {
            $subject  = '[CLIENT PORTAL]['.$this->issueList->control_no.'] New Forum Topic.';
            $emailBody .= 'This email is to notify that the Issue '.$this->issueList->control_no.' under '.$this->issueList->project->name.' has new forum topic.';
            $details = '';
            $departmentEmail = $this->issueList->assignee->email;
        }   
        elseif($this->email_type == 5)
        {
            $subject  = '[CLIENT PORTAL]['.$this->thread->issue->control_no.'] New Reply to Topic';
            $emailBody .= 'This email is to notify that the a topic under Issue '.$this->thread->issue->control_no.' of project '.$this->thread->issue->project->name.' has new reply.<br><br>';
            $details = '<div style = "background:beige; padding:5px;">'.$this->reply->details.'</div><br><br>';
            $departmentEmail = $this->thread->issue->assignee->email;
        }   
          $emailMarkdown = $this->markdown('emails.tickets.ticket_mail')
                ->subject($subject)
                ->with([
                    'emailBody' => $emailBody,
                    'issueDetails' => $details,
                ]);

        $recievers = AlertStatus::where('issue_id',$this->issueList->id)->get();
        foreach ($recievers as $key => $reciever) {
            if($reciever->status == 1){
                 $emailMarkdown->to($reciever->user->email);
            }
            
        }

      
        
                return $emailMarkdown;

    }
}
