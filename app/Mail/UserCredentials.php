<?php

namespace App\Mail;

use App\User;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

use Auth;

class UserCredentials extends Mailable
{
    use Queueable, SerializesModels;
    public $request;
    public $user;
    public $randomPassword;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request, User $user, $randomPassword)
    {
        $this->request = $request;
        $this->user = $user;
        $this->randomPassword = $randomPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $userName = $this->user->name;
        if($this->user->admin_type_id == config('constants.role_type.ae'))
        {
            $userLoginUrl = '<a href="'.route('ae-login').'">iManila Client Portal</a>';    
        }elseif($this->user->admin_type_id == config('constants.role_type.tech')){
             $userLoginUrl = '<a href="'.route('techTeam-login').'">iManila Client Portal</a>';    
        }else{
            $userLoginUrl = '<a href="'.route('login').'">iManila Client Portal</a>';
        }
        
        $userUsername = $this->user->email;
        $userPassword = $this->randomPassword;

        return $this->markdown('emails.users.credentials')
            ->subject('Your Client Portal Account')
            ->bcc('system@clientsportal.imanila.ph')
            ->cc('projects@imanila.ph')
            ->with([
                'userName' => $userName,
                'userLoginUrl' => $userLoginUrl,
                'userUsername' => $userUsername,
                'userPassword' => $userPassword,
            ]);
    }
}
