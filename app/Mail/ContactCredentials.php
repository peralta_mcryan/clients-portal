<?php

namespace App\Mail;

use App\Contact;
use App\SiteContents;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

use Auth;

class ContactCredentials extends Mailable
{
    use Queueable, SerializesModels;
    public $request;
    public $contact;
    public $randomPassword;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request, Contact $contact, $randomPassword)
    {
        $this->request = $request;
        $this->contact = $contact;
        $this->randomPassword = $randomPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $service = SiteContents::find(10);
        $emailBody = $service->content;

        $contactName = $this->contact->first_name.' '.$this->contact->last_name;
        $clientLoginUrl = '<a href="'.route('client-login').'">iManila Client Portal</a>';
        $clientUsername = $this->contact->email;
        $clientPassword = $this->randomPassword;
        $adminName = Auth::user()->name;
        $adminDesignation = Auth::user()->designation;

        $emailBody = str_replace('[CLIENT_NAME]', $contactName, $emailBody);
        $emailBody = str_replace('[WEBSITE_LOGIN_URL]', $clientLoginUrl, $emailBody);
        $emailBody = str_replace('[CLIENT_USERNAME]', $clientUsername, $emailBody);
        $emailBody = str_replace('[CLIENT_PASSWORD]', $clientPassword, $emailBody);
        $emailBody = str_replace('[ADMIN_NAME]', $adminName, $emailBody);
        $emailBody = str_replace('[ADMIN_DESIGNATION]', $adminDesignation, $emailBody);

        return $this->markdown('emails.contacts.credentials')
            ->subject('Welcome to iManila - Your Portal Account')
            ->bcc('system@clientsportal.imanila.ph')
            ->cc('projects@imanila.ph')
            ->with([
                'contactName' => $contactName,
                'clientLoginUrl' => $clientLoginUrl,
                'clientUsername' => $clientUsername,
                'clientPassword' => $clientPassword,
                'adminName' => $adminName,
                'adminDesignation' => $adminDesignation,
                'emailBody' => $emailBody
            ]);
    }
}
