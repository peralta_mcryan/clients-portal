<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
	use SoftDeletes;

    function cities()
    {
        return $this->hasMany('App\City', 'province_id');
    }
}
