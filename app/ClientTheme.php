<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientTheme extends Model
{
    
    protected $fillable = [ 'project_id', 'themes', 'remarks'];
}
