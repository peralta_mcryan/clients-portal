<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ContactResetPasswordNotification;


class Contact extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $guard = 'contact';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = 	[
	    						'first_name', 
	    						'last_name', 
	    						'work_phone', 
	    						'mobile_phone',
	    						'email',
	    						'password',
                                'designation',
	    						'department',
	    						'main',
	    						'allow_access',
	    						'account_id',
                                'contact_type',
    						];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ContactResetPasswordNotification($token));
    }

    function account()
    {
        return $this->belongsTo('App\Account', 'account_id');
    }
}
