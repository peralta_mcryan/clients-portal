<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Attachment extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['filename', 'project_id', 'file_folder_id', 'user_id', 'contact_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    function contacts()
    {
        return $this->belongsTo('App\Contact', 'contact_id');
    }


}
