<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'designation', 'api_token', 'admin_type_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function generateToken()
    {
        $this->api_token = str_random(60);
        $this->save();

        return $this->api_token;
    }

    public function adminType()
    {
        return $this->belongsTo('App\AdminType');
    }

    
    public function userDesignation()
    {
        return $this->hasOne('App\Department','id','designation');
    }


     function projects()
    {
        return $this->hasMany('App\Project', 'assigned_ae');
    }

   
}
