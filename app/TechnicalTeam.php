<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\TechTeamResetPasswordNotification;

class TechnicalTeam extends Authenticatable
{
    use Notifiable;
     protected $table = 'users';


    public function sendPasswordResetNotification($token)
    {
        $this->notify(new TechTeamResetPasswordNotification($token));
    }

}
