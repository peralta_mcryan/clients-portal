<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IssueThread extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['issue_id','details','created_by'];

    function user() {

        return $this->belongsTo('App\User','created_by','id');
    }

    function issue(){
        
        return $this->belongsTo('App\IssueList','issue_id');
    }

    function thread_discussion(){
        
        return $this->hasMany('App\ThreadDiscussion','thread_id','id');
    }
}
