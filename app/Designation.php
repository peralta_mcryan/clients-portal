<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Designation extends Model
{
        use SoftDeletes;
        protected $fillable = [ 'designation'];


//  function project()
//     {
//         return $this->belongsTo('App\Project')->withDefault();
//     }

function user()
    {
        return $this->belongsTo('App\User','id','designation');
    }

}

