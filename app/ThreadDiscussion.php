<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThreadDiscussion extends Model
{
   protected $fillable = ['thread_id','details','created_by','reply_to'];

    function user() {

        return $this->belongsTo('App\User','created_by','id');
    }
  

    function forum_topic(){
        
        return $this->belongsTo('App\Issuethread','thread_id','id');
    }

    function replied_to(){
        
        return $this->belongsTo('App\Issuethread','reply_to','id');
    }
}
