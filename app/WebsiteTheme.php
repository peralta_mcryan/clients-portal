<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteTheme extends Model
{
    //
     protected $fillable = ['name','website_links','theme_group'];


    //  public function groupTheme($group_id)
    //  {
    //      return $group_id->where('group_theme','=',$group_id);
    //  }
}
