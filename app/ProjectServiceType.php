<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectServiceType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['service_type_id', 'project_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    function project()
    {
        return $this->belongsTo('App\Project')->withDefault();
    }

    function serviceType()
    {
        return $this->belongsTo('App\ServiceType')->withDefault();
    }

    
    function serviceType2()
    {
        return $this->hasOne('App\ServiceType','id','service_type_id');
    }
}
