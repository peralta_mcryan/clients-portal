<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IssueLog extends Model
{
      protected $fillable = ['issue_id','details','created_by'];

    
   function user()
    {
        return $this->hasOne('App\User', 'id','created_by');
    }
}
