<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlertStatus extends Model
{
    protected $fillable = ['user_id','issue_id','status'];


    function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    function issue()
    {
        return $this->belongsTo('App\IssueList', 'issue_id');
    }
}
