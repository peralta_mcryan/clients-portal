<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChangeRequest extends Model
{
    use SoftDeletes;

    protected $fillable = ['project_id', 'account_id', 'task','man_hours','price'];

    
    function project()
    {
          return $this->belongsTo('App\Project')->withDefault();
    }
}
