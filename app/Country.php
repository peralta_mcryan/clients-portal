<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	use SoftDeletes;

    function provinces()
    {
        return $this->hasMany('App\Province', 'country_id');
    }

}
