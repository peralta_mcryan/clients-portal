<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

use Carbon\CarbonPeriod;
use Carbon\Carbon;

class Project extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =   [
                                'name', 
                                'description', 
                                'status', 
                                'account_id', 
                                'assigned_ae', 
                                'user_id', 
                                'inclusions', 
                                'requirements', 
                                'service_contract', 
                                'service_type_id',
                                'project_start',
                                'project_end',
                                'project_days',
                            ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    function clientTheme()
    {
        return $this->hasOne('App\ClientTheme', 'project_id');
    }


    function accounts()
    {
        return $this->belongsTo('App\Account', 'account_id');
    }

    function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    function attachments()
    {
        return $this->hasMany('App\Attachment', 'project_id');
    }

    function assignedAe()
    {
        return $this->belongsTo('App\User', 'assigned_ae')->withDefault();
    }
    
    function issue_list()
    {
        return $this->hasMany('App\IssueList', 'project_id');
    }
    
    function projectSchedules()
    {
        return $this->hasMany('App\ProjectSchedule', 'project_id');
    }

    function projectServiceType()
    {
        return $this->hasMany('App\ProjectServiceType');
    }

    function projectActivity()
    {
        return $this->hasMany('App\ProjectActivity');
    }
    
    function changeRequests()
    {
          return $this->hasMany('App\ChangeRequest','project_id');
    }


    function getAgingDays() {
        $start = new \DateTime($this->project_start);
        $end = new \DateTime(Carbon::now());
        // otherwise the  end date is excluded (bug?)
        $end->modify('+1 day');
        
        $interval = $end->diff($start);
        
        // total days
        $days = $interval->days;
        
        // create an iterateable period of date (P1D equates to 1 day)
        $period = new \DatePeriod($start, new \DateInterval('P1D'), $end);
        
        // best stored as array, so you can add more than one
        $holidays = array('2012-09-07');
        
        foreach($period as $dt) {
            $curr = $dt->format('D');
        
            // substract if Saturday or Sunday
            if ($curr == 'Sat' || $curr == 'Sun') {
                $days--;
            }
        
            // (optional) for the updated question
            elseif (in_array($dt->format('Y-m-d'), $holidays)) {
                $days--;
            }
        }
        
        
        return $days; // 4
    }
}