<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceTypeFile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['service_type_id', 'file_folder_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    function serviceType()
    {
        return $this->belongsTo('App\ServiceType')->withDefault();
    }
}
