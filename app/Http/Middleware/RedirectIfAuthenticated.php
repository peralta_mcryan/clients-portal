<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        switch ($guard) {
            case 'contact':
                if (Auth::guard($guard)->check()) {
                    return redirect('client-dashboard');
                }
                break;
            case 'ae':
                if (Auth::guard($guard)->check()) {
                    return redirect('ae/my-account');   
                    
                }
                break;
                
            case 'techTeam':
                if (Auth::guard($guard)->check()) {
                    return redirect('technical/my-account');   
                    
                }
                break;
            default:
                if (Auth::guard($guard)->check()) {
                        return redirect('/home');   
                        
                }
                break;
        }

        // if (Auth::guard($guard)->check()) {
        //     return redirect('/home');
        // }
        return $next($request);
    }
}
