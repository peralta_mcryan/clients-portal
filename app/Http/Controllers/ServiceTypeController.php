<?php

namespace App\Http\Controllers;

use App\ServiceType;
use App\FileFolder;
use App\ServiceTypeFile;
use Illuminate\Http\Request;

class ServiceTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $serviceTypes = ServiceType::orderBy('name')->paginate(config('constants.pagination'));
        return view('admin.service_type.index', compact('serviceTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $serviceType = new ServiceType();
        $fileFolders = FileFolder::all()->pluck('folder_name', 'id');
        $serviceTypeFile = $serviceType->serviceTypeFile()->pluck('file_folder_id')->toArray();
        $serviceType->name = old('name');
        $pageName = 'New Service Type';
        $route = route('service-types.store');
        return view('admin.service_type.form', compact('serviceTypeFile','serviceType', 'pageName', 'route', 'fileFolders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:service_types,name',
        ]);
       
        ServiceType::create([
            'name' => $request->name,
            'broad_type'    =>$request->broad_type
        ]);

        return redirect()->route('service-types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceType  $serviceType
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceType $serviceType)
    {
        $serviceTypes = ServiceType::orderBy('name')->paginate(12);
        return view('admin.service_type.view',compact('serviceTypes', 'serviceType'));    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceType  $serviceType
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceType $serviceType)
    {
        $serviceType->name = old('name') ? old('name') : $serviceType->name;
         $serviceType->broad_type = old('broad_type') ? old('broad_type') : $serviceType->broad_type;
        $fileFolders = FileFolder::all()->pluck('folder_name', 'id');

        $serviceTypeFile = $serviceType->serviceTypeFile()->pluck('file_folder_id')->toArray();

        $pageName = 'Edit Service Type';
        $route = route('service-types.edit', $serviceType->id);
        return view('admin.service_type.form', compact('serviceType', 'pageName', 'route', 'fileFolders', 'serviceTypeFile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceType  $serviceType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceType $serviceType)
    {
        // dd($request->service_type_file);
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:service_types,name,'.$serviceType->id,
        ]);

        $serviceType->name = $request->get('name');
        $serviceType->broad_type = $request->broad_type;
        $serviceType->save();

        $serviceType->serviceTypeFile()->delete();

        if (!empty($request->service_type_file)) {
            $serviceTypeFile = [];
            foreach ($request->service_type_file as $key => $value) {
                $serviceTypeFile[] = ['file_folder_id' => $value, 'service_type_id' => $serviceType->id];
            }
            $serviceType->serviceTypeFile()->createMany($serviceTypeFile);
        }

        

        return redirect()->route('service-types.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceType  $serviceType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ServiceType $serviceType)
    {
        $serviceType->delete();
        $request->session()->flash('message.level', 'success');
        $request->session()->flash('message.content', 'Service Type was successfully deleted!');
    }
}
