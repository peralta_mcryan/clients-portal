<?php

namespace App\Http\Controllers;

use App\ServiceTypeFile;
use Illuminate\Http\Request;

class ServiceTypeFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceTypeFile  $serviceTypeFile
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceTypeFile $serviceTypeFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceTypeFile  $serviceTypeFile
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceTypeFile $serviceTypeFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceTypeFile  $serviceTypeFile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceTypeFile $serviceTypeFile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceTypeFile  $serviceTypeFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceTypeFile $serviceTypeFile)
    {
        //
    }
}
