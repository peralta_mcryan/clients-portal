<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\User;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
        $this->middleware('guest', ['except' => ['logout','userLogout']]);
    }

    public function userLogout()
    {
        Auth::guard('web')->logout();
        
        // $request->session()->invalidate();

        return redirect('/');
    }

    public function login(Request $request)
    {
    	$this->validate($request, [
    		'email' => 'required|email',
    		'password' => 'required',
    	]);

        if ($request->wantsJson()) {
            if ($this->attemptLogin($request)) {
                $user = $this->guard()->user();
                $user->generateToken();
        
                return response()->json([
                    'data' => $user->toArray(),
                ]);
            }
        
            return $this->sendFailedLoginResponse($request);
        } else {
            
            $user = User::where('email',$request->email)->pluck('admin_type_id')->first();
           
            if($user == config('constants.role_type.api') || $user == config('constants.role_type.pm') )
            {

            
                if (Auth::guard()->attempt(array(
                    'email'    => $request->email,
                    'password' => $request->password,

                )
                // ['email' => $request->email, 'password' => $request->password, 'designation' => config('constants.designation.ae')]
                ,  !empty($request->remember)?$request->remember:false)) {

                    return redirect()->intended(route('home'));
                }
            }
        
            return redirect()->back()->withInput($request->only('email', 'remember'));
        }
    }

    public function logout(Request $request)
    {
        if ($request->wantsJson()) {
            $user = Auth::guard('api')->user();

            if ($user) {
                $user->api_token = null;
                $user->save();
            }
            return response()->json(['data' => 'User logged out.'], 200);
        } else {
            Auth::guard()->logout();
            return redirect('/');
        }
    }
}
