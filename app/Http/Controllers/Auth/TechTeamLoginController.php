<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\User;
use Auth;


class TechTeamLoginController extends Controller
{
 use AuthenticatesUsers;

    public function __construct()
    {
    	$this->middleware('guest:techTeam', ['except' => ['logout']]);
    }

    public function showLoginForm()
    {
    	return view('auth.techTeam-login');
    }

    public function login(Request $request)
    {
       
    	$this->validate($request, [
    		'email' => 'required|email',
    		'password' => 'required',
    	]);

    	if (Auth::guard('techTeam')->attempt(array(
                'email'    => $request->email,
                'password' => $request->password,
                'admin_type_id' => [
                    'value'    => config('constants.role_type.tech'),
                    'operator' => '='
                ]
            )
            // ['email' => $request->email, 'password' => $request->password, 'designation' => config('constants.designation.ae')]
            ,  !empty($request->remember)?$request->remember:false)) {
            // return redirect()->intended(route('client-dashboard'));
         
            return redirect()->intended(route('tech-account.index'));
        }else{
              return $this->sendFailedLoginResponse($request);
        }
        // dd($request);
    	return back()->withInput();
    }

    public function logout()
    {
        Auth::guard('techTeam')->logout();

        // $request->session()->invalidate();

        return redirect('/');
    }
}
