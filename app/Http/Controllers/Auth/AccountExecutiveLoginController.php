<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use Auth;


class AccountExecutiveLoginController extends Controller
{
    public function __construct()
    {
    	$this->middleware('guest:ae', ['except' => ['logout']]);
    }

    public function showLoginForm()
    {
    	return view('auth.ae-login');
    }

    public function login(Request $request)
    {
        // dd('malie');
    	$this->validate($request, [
    		'email' => 'required|email',
    		'password' => 'required',
    	]);

    	if (Auth::guard('ae')->attempt(array(
                'email'    => $request->email,
                'password' => $request->password,
                'admin_type_id' => [
                    'value'    => config('constants.role_type.ae'),
                    'operator' => '='
                ]
            )
            // ['email' => $request->email, 'password' => $request->password, 'designation' => config('constants.designation.ae')]
            ,  !empty($request->remember)?$request->remember:false)) {
            // return redirect()->intended(route('client-dashboard'));
         
            return redirect()->intended(route('my-account.index'));
        }
        // dd($request);
    	return back()->withInput();
    }

    public function logout()
    {
        Auth::guard('ae')->logout();

        // $request->session()->invalidate();

        return redirect('/');
    }
}
