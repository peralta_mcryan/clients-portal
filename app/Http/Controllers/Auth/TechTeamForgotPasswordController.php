<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\User;
use Password;
use Validator;

class TechTeamForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:techTeam');
    }

    public function showLinkRequestForm()
    {
        return view('auth.passwords.techTeam-email-contact');
    }
    
    protected function broker()
    {
        return Password::broker('techTeam');
    }

    public function sendResetLinkEmail(Request $request)
    {
       $validator = $this->validateEmail($request);
        if ($validator->fails()) {
                    // dd($validator);
                  return back()
                                ->withErrors($validator)
                                ->withInput();
                }else{
                    // We will send the password reset link to this user. Once we have attempted
                    // to send the link, we will examine the response then see the message we
                    // need to show to the user. Finally, we'll send out a proper response.
                    $response = $this->broker()->sendResetLink(
                        $request->only('email')
                    );

                    return $response == Password::RESET_LINK_SENT
                                ? $this->sendResetLinkResponse($response)
                                : $this->sendResetLinkFailedResponse($request, $response);
                }
        
    }

    protected function validateEmail(Request $request)
    {
        $admin = User::where('email','=',$request->email)->pluck('admin_type_id')->first();
        $data = $request->all();
        $data['admin'] = $admin;
        // dd($data);
        $validator = Validator::make($data, ['email' => ['required','email'],'admin'=> Rule::in([4])]);
       
        return $validator;
        
    }


}
