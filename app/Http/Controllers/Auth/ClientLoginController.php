<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use Auth;


class ClientLoginController extends Controller
{
    public function __construct()
    {
    	$this->middleware('guest:contact', ['except' => ['logout']]);
    }

    public function showLoginForm()
    {
    	return view('auth.client-login');
    }

    public function login(Request $request)
    {
    	$this->validate($request, [
    		'email' => 'required|email',
    		'password' => 'required',
    	]);

    	if (Auth::guard('contact')->attempt(['email' => $request->email, 'password' => $request->password ], !empty($request->remember)?$request->remember:false)) {
            // return redirect()->intended(route('client-dashboard'));
            $contact = Contact::find(Auth::guard('contact')->id());
            $projects = $contact->account->projects()->orderBy('name')->get();
            $dmCount = 0;
            foreach ($projects as $key => $project) {
                $sevices  = $project->projectServiceType()->get();
                foreach ($sevices as $k => $service) {
                    $serviceType = $service->serviceType2()->first();
                    
                    if($serviceType->broad_type == 1)
                    {
                         $dmCount+=1;
                    }
                }
                   
                
            }
            session(['DM' => $dmCount]);
         
            return redirect()->intended(route('account-information',Auth::guard('contact')->id()));
        }

    	return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function logout()
    {
        Auth::guard('contact')->logout();

        // $request->session()->invalidate();

        return redirect('/');
    }
}
