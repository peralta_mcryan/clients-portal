<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Account;
use Auth;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:contact');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $ch = curl_init(); 
        // curl_setopt($ch, CURLOPT_URL, "http://localhost:8000/api/projects"); 
        // curl_setopt($ch, CURLOPT_POST, 1);

        // // set post data to true 
        // curl_setopt($ch, CURLOPT_POSTFIELDS,$payload); 

        // // post data 
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        
        // $json = curl_exec($ch); 
        // dd($json);
        // curl_close ($ch);
        return redirect(route('account-information', Auth::user()->id));
        // return view('client.home');
    }
}
