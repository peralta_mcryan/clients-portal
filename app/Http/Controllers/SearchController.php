<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Account;
use App\AlertStatus;
use App\Contact;
use App\Project;
use App\IssueList;
use App\Service;
use App\ServiceType;
use App\User;
use App\Attachment;
use Auth;
class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchText = $request->search_text;
        $clientSearchResults = [];
        $projectSearchResults = [];
        $contactSearchResults = [];
        $userSearchResults = [];
        $serviceSearchResults = [];
        $serviceTypeSearchResults = [];
        
        if(Auth::guard('web')->check()){
                if (!empty($searchText)) {
                        $clientSearchResults = Account::orderBy('name')
                                                        ->where('name', 'like', '%' . $searchText . '%')
                                                        ->orWhere('address', 'like', '%' . $searchText . '%')
                                                        ->orWhere('work_phone', 'like', '%' . $searchText . '%')
                                                        ->orWhere('mobile_phone', 'like', '%' . $searchText . '%')
                                                        ->get();

                        $projectSearchResults = Project::orderBy('name')
                                                        ->where('name', 'like', '%' . $searchText . '%')
                                                        ->orWhere('description', 'like', '%' . $searchText . '%')
                                                        ->get();

                        $contactSearchResults = Contact::orderBy('first_name')
                                                        ->where('first_name', 'like', '%' . $searchText . '%')
                                                        ->orWhere('last_name', 'like', '%' . $searchText . '%')
                                                        ->orWhere('work_phone', 'like', '%' . $searchText . '%')
                                                        ->orWhere('mobile_phone', 'like', '%' . $searchText . '%')
                                                        ->orWhere('email', 'like', '%' . $searchText . '%')
                                                        ->orWhere('designation', 'like', '%' . $searchText . '%')
                                                        ->orWhere('department', 'like', '%' . $searchText . '%')
                                                        ->get();

                        $userSearchResults = User::orderBy('name')
                                                        ->where('name', 'like', '%' . $searchText . '%')
                                                        ->orWhere('email', 'like', '%' . $searchText . '%')
                                                        ->orWhere('designation', 'like', '%' . $searchText . '%')
                                                        ->get();

                                                        
                        $ticketSearchResults = IssueList::orderBy('control_no')
                                                        ->where('control_no', 'like', '%' . $searchText . '%')
                                                        ->get();                  

                        $serviceSearchResults = Service::orderBy('name')->where('name', 'like', '%' . $searchText . '%')->get();

                        $serviceTypeSearchResults = ServiceType::orderBy('name')->where('name', 'like', '%' . $searchText . '%')->get();
                        
                        $fileSearchResults = Attachment::orderBy('filename')
                                        ->where('filename', 'like', '%' . $searchText . '%')
                                        ->get();
                
                
                }
                return view('admin.search.result',compact('clientSearchResults', 'projectSearchResults', 'contactSearchResults', 'userSearchResults', 'serviceSearchResults', 'serviceTypeSearchResults', 'searchText', 'fileSearchResults','ticketSearchResults'));

        }
        if(Auth::guard('ae')->check()){
                if (!empty($searchText)) {
                        $projectHandled = Project::where('assigned_ae',Auth::guard('ae')->user()->id)->pluck('id');
                       
                        // $clientSearchResultsQueue = Account::orderBy('name')
                        //                                 ->where('name', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('address', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('work_phone', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('mobile_phone', 'like', '%' . $searchText . '%');
                        // $clientSearchResults = $clientSearchResultsQueue->get();
                        $projectSearchResultsQueue = Project::orderBy('name')
                                                        ->where(function($query) use ($searchText){
                                                                $query->where('name', 'like', '%' . $searchText . '%');
                                                                $query->orWhere('description', 'like', '%' . $searchText . '%');
                                                        })
                                                        ->where('assigned_ae',Auth::guard('ae')->user()->id);
                        $projectSearchResults = $projectSearchResultsQueue->get(); 

                        // $contactSearchResults = Contact::orderBy('first_name')
                        //                                 ->where('first_name', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('last_name', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('work_phone', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('mobile_phone', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('email', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('designation', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('department', 'like', '%' . $searchText . '%')
                        //                                 ->get();

                        // $userSearchResults = User::orderBy('name')
                        //                                 ->where('name', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('email', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('designation', 'like', '%' . $searchText . '%')
                        //                                 ->get();

                        $ticketSearchResults = IssueList::orderBy('control_no')
                                                        ->where('control_no', 'like', '%' . $searchText . '%')
                                                        ->whereIn('project_id',$projectHandled->toArray())
                                                        ->get();                                                        
                                                       
                        // $serviceSearchResults = Service::orderBy('name')->where('name', 'like', '%' . $searchText . '%')->get();

                        // $serviceTypeSearchResults = ServiceType::orderBy('name')->where('name', 'like', '%' . $searchText . '%')->get();
                        
                        $fileSearchResults = Attachment::orderBy('filename')
                                        ->where('filename', 'like', '%' . $searchText . '%')
                                       ->whereIn('project_id',$projectHandled->toArray())
                                        ->get();
                
                
                }
                return view('ae.search.result',compact('clientSearchResults', 'projectSearchResults', 'contactSearchResults', 'userSearchResults', 'serviceSearchResults', 'serviceTypeSearchResults', 'searchText', 'fileSearchResults','ticketSearchResults'));

        }

        if(Auth::guard('techTeam')->check()){
                if (!empty($searchText)) {
                        // $clientSearchResults = Account::orderBy('name')
                        //                                 ->where('name', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('address', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('work_phone', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('mobile_phone', 'like', '%' . $searchText . '%')
                        //                                 ->get();

                        $projectSearchResults = Project::orderBy('name')
                                                        ->where('name', 'like', '%' . $searchText . '%')
                                                        ->orWhere('description', 'like', '%' . $searchText . '%')
                                                        ->get();

                        // $contactSearchResults = Contact::orderBy('first_name')
                        //                                 ->where('first_name', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('last_name', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('work_phone', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('mobile_phone', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('email', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('designation', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('department', 'like', '%' . $searchText . '%')
                        //                                 ->get();

                        // $userSearchResults = User::orderBy('name')
                        //                                 ->where('name', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('email', 'like', '%' . $searchText . '%')
                        //                                 ->orWhere('designation', 'like', '%' . $searchText . '%')
                        //                                 ->get();

                                                        
                        $ticketSearchResults = IssueList::orderBy('control_no')
                                                        ->where('control_no', 'like', '%' . $searchText . '%')
                                                        ->get();                  

                        // $serviceSearchResults = Service::orderBy('name')->where('name', 'like', '%' . $searchText . '%')->get();

                        // $serviceTypeSearchResults = ServiceType::orderBy('name')->where('name', 'like', '%' . $searchText . '%')->get();
                        
                        $fileSearchResults = Attachment::orderBy('filename')
                                        ->where('filename', 'like', '%' . $searchText . '%')
                                        ->get();
                
                
                }
                return view('technical_team.search.result',compact('clientSearchResults', 'projectSearchResults', 'contactSearchResults', 'userSearchResults', 'serviceSearchResults', 'serviceTypeSearchResults', 'searchText', 'fileSearchResults','ticketSearchResults'));

        }

     

        
    
}

 public function email_alert(Request $request,IssueList $issueList){
       $user = 0;
        if(Auth::guard('web')->check()){
                $user = Auth::guard('web')->user()->id;
        }
        elseif(Auth::guard('ae')->check()){
                $user = Auth::guard('ae')->user()->id;
        }
        elseif(Auth::guard('techTeam')->check()){
                    $user = Auth::guard('techTeam')->user()->id; 
        }
        $status = !empty($request->alert) ?  $request->alert : 0;

        $isExisting = AlertStatus::where('user_id',$user)->where('issue_id',$issueList->id)->first();
        if(!empty($isExisting))
        {
                AlertStatus::where('user_id',$user)
                           ->where('issue_id',$issueList->id)
                           ->update(['status'=>$status]);
        }else{
                  AlertStatus::create([
                          'user_id' => $user,
                          'issue_id'=> $issueList->id,
                          'status' => $status
                  ]);

                  
        }
        return redirect()->back();
    }

}