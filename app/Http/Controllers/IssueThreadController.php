<?php

namespace App\Http\Controllers;

use App\IssueList;
use App\ThreadDiscussion;
use App\IssueThread;
use App\User;
use Illuminate\Http\Request;
use App\Events\ChatEvent;
use Illuminate\Support\Facades\Mail;
use App\Mail\TechnicalTicket;
    
use Auth;

class IssueThreadController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth.techTeam');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return IssueThread::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,IssueList $issueList)
    {
   
        $this->validate($request,[
                'details' => 'required'
            ]);
       
        $issue = IssueThread::create([
             'issue_id' => $issueList->id,
             'details' => $request->details,
             'created_by' => Auth::guard('techTeam')->user()->id,
        ]);

        $newIssue = IssueThread::where('id',$issue->id)->first();
        Mail::to($issueList->project->assignedAe->email)->send(new TechnicalTicket($issueList,4,$newIssue,''));
            
        //Announce the posted message
        // event(new ChatEvent($request->details, User::where('id',Auth::guard('techTeam')->user()->id)->first()));
        // broadcast(new ChatEvent( $issueList->id,$request->details, User::where('id',Auth::guard('techTeam')->user()->id)->first()))->toOthers();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IssueList  $issueList
     * @return \Illuminate\Http\Response
     */
    public function show(IssueList $issueList)
    {
     
    //    $issueThread = $issueList->issue_thread()->with('user')->orderBy('created_at','DSC')->get();
    //   return $issueThread;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IssueList  $issueList
     * @return \Illuminate\Http\Response
     */
    public function edit(IssueList $issueList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IssueList  $issueList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IssueList $issueList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IssueList  $issueList
     * @return \Illuminate\Http\Response
     */
    public function destroy(IssueList $issueList)
    {
        //
    }

    public function sendReply(Request $request, IssueThread $issueThread)
    {
            $user = '';
            if(Auth::guard('web')->check()){
                 $user = Auth::guard('web')->user()->id;
            }elseif(Auth::guard('techTeam')->check()){
                 $user = Auth::guard('techTeam')->user()->id;
            }elseif(Auth::guard('ae')->check()){
                 $user = Auth::guard('ae')->user()->id;
            }


            $msg = 'addComment_'.$issueThread->id;
            $reply = 'reply_to_'.$issueThread->id;

            $this->validate($request,[
                $msg => 'required'
            ]);
            $reply = ThreadDiscussion::create([
                'thread_id' => $issueThread->id,
                 'details'=> $request->get($msg),
                 'created_by' => $user ,
                 'reply_to'    => $request->get($reply)
            ]);

            $newIssue = IssueThread::where('id',$issueThread->id)->first();
            $reply = ThreadDiscussion::where('id',$reply->id)->first();
            Mail::to('projects@clientsportal.imanila.ph')->send(new TechnicalTicket(new IssueList,5,$issueThread,$newIssue,$reply));
         
           return  redirect()->back();
    }
}
