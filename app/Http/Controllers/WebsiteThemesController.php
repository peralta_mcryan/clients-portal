<?php

namespace App\Http\Controllers;

use App\WebsiteTheme;
use Illuminate\Http\Request;
use Auth;
use JonnyW\PhantomJs\Client;
class WebsiteThemesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
        $data = !empty($_GET['id'])?$_GET['id']:0;
        $group  = !empty($_GET['group'])?$_GET['group']:1;
        // $themes = WebsiteTheme::orderBy('name')->paginate(config('constants.pagination'));
        if($data == 0)
        {
            $themeSite = new WebsiteTheme();
            $route = route('themes.store');
            $drName = 'theme_group';
            $buttonName = 'Add';
            $themes = WebsiteTheme::where('theme_group','=',($group==0)?1:$group)->paginate(config('constants.pagination'));
            
        }else
        {
            
            $themeSite = WebsiteTheme::find($data);
            
            $route = route('update-themes',$themeSite->id);
            $buttonName = 'Update';
            
            $drName = 'theme_group2';
            $themes = WebsiteTheme::where('theme_group','=',($group==0)?1:$group)->paginate(config('constants.pagination'));
           
        }

      
        return view('admin.themes.index',compact('group','drName','themes','route','themeSite','buttonName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'website_name' => 'required|string|max:45|unique:website_themes,name',
                'website_link' => 'required',
                'theme_group' => 'required',
            ]);

           $id =  WebsiteTheme::create([
                'name' => $request->website_name,
                'website_links' => $request->website_link,
                'theme_group' => $request->theme_group,
            ]);
               $client = Client::getInstance();
    
            $request  = $client->getMessageFactory()->createCaptureRequest($request->website_link);
            $response = $client->getMessageFactory()->createResponse();
            $path = 'themes/'.$id.'/theme.jpg';
            $width  = 800;
            $height = 600;
            $top    = 0;
            $left   = 0;
            return redirect()->route('themes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WebsiteTheme  $websiteTheme
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteTheme $websiteTheme)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WebsiteTheme  $websiteTheme
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteTheme $websiteTheme)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WebsiteTheme  $websiteTheme
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteTheme $websiteTheme)
    {
        $this->validate($request, [
                'website_name' => 'required|string|max:45',
                'website_link' => 'required',
                'theme_group2' => 'required',
            ]);

        $websiteTheme->name = $request->website_name;
        $websiteTheme->theme_group = $request->theme_group2;
        $websiteTheme->website_links = $request->website_link;
        $websiteTheme->save();
        
         return redirect()->route('themes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WebsiteTheme  $websiteTheme
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteTheme $websiteTheme)
    {
        $websiteTheme->delete();
        return 1;
    }

}
