<?php

namespace App\Http\Controllers;

use App\Project;
use App\IssueList;
use App\ProjectServiceType;
use App\WebsiteTheme;
use App\ProjectActivity;
use App\Account;
use App\FileFolder;
use App\ServiceTypeFile;
use App\User;
use App\Service;
use App\ServiceType;
use App\Attachment;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Mail;
use LinkPreview\LinkPreview;

use App\Mail\ProjectWelcome;

use Carbon\Carbon;
use Carbon\CarbonPeriod;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $projectsQuery = Project::orderBy('created_at', 'desc');
        $projects = $projectsQuery->paginate(config('constants.pagination'));
        
        if ($request->wantsJson()) {
            return response()->json($projects, 200);
        }
        // $details = $projectsQuery->get();
        // foreach ($details as $key => $value) {
        //     $pro = Project::find($value->id);
        //     $pro->project_start = $pro->created_at;

        //     $pro->save();
        // }
        return view('admin.project.index', compact('projects'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_tabular(Request $request)
    {
        $dropDowns['clientAccounts'] = Account::pluck('name','id')->toArray();
        $dropDowns['projectStatuses'] = config('constants.project_status');
        $projectsQuery = Project::orderBy('created_at', 'desc');
        
        $searchFilters['client_accounts'] = $request->client_accounts;
        $searchFilters['project_status'] = $request->project_status;
        // dd($searchFilters);
        if (!empty( $searchFilters['client_accounts'])) {
            $projectsQuery->whereIn('account_id', $searchFilters['client_accounts']);
        }

        if (!empty($searchFilters['project_status'])) {
            $projectsQuery->whereIn('status',$searchFilters['project_status']);
        }
        $projects = $projectsQuery->paginate(config('constants.pagination'));

        if ($request->wantsJson()) {    
            return response()->json($projects, 200);
        }

        return view('admin.project.index_tabular', compact('projects', 'searchFilters','dropDowns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Account $account)
    {
        $serviceContract = '';
        $accounts = Account::orderBy('name')->pluck('name','id');
        $accounts->prepend('Select the Project Owner', 0);
        $projectStatus = config('constants.project_status');

        $users = User::orderBy('name')->pluck('name','id');
        $ae = User::orderBy('name')->pluck('name','id');

        $serviceTypes = ServiceType::orderBy('name')->pluck('name','id');

        $project = new Project();
        $project->name = old('name');
        $project->description = old('description');
        $project->status = old('status');
        $project->account_id = old('project_owner');
        $project->project_days = old('project_days');
        $project->assigned_ae = old('assigned_ae');
        $project->user_id = old('assigned_project_manager');
        $project->service_type_id = old('service_type');
        $project->inclusions = old('inclusions');
        $project->requirements = old('requirements');
        $project->project_end = old('project_end');

        if ($account->id) {
            $pageName = 'New Project for '.$account->name;
            $route = route('new-project-account', $account->id);
        } else {
            $pageName = 'New Project';
            $route = route('new-project');
        }

        $formSettings['welcome_email_default'] = false;
        $formSettings['show_update_remarks_field'] = false;

        return view('admin.project.form', compact('route', 'pageName', 'project', 'accounts', 'projectStatus', 'account', 'users', 'serviceTypes', 'serviceContract', 'ae', 'formSettings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rename = '';
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'project_status' => 'required|numeric',
            'project_owner' => 'required|numeric',
            'service_contract' => 'required',
            'project_start' => 'required|date',
            'project_days' => 'required',
            'project_end' => 'required|date',
            'service_type' => 'required',
        ]);

        if ($request->file('service_contract')) {
            $timestamp = date('ynjGis');
            $basename = pathinfo($request->file('service_contract')->getClientOriginalName(), PATHINFO_FILENAME );
            $fileExtension = pathinfo($request->file('service_contract')->getClientOriginalName(), PATHINFO_EXTENSION);
            $rename = $basename.'_'.$timestamp.'.'.$fileExtension;
        }

        $project = Project::create([
            'name' => $request->name,
            'description' => $request->description,
            'inclusions' => $request->inclusions,
            'requirements' => $request->requirements,
            'status' => $request->project_status,
            'account_id' => $request->project_owner,
            'assigned_ae' => $request->assigned_ae,
            'user_id' => $request->assigned_project_manager,
            // 'service_type_id' => $request->service_type,
            'service_contract' => $rename,
            'project_start' => $request->project_start,
            'project_end' => $request->project_end,
            'project_days' => $request->project_days,
        ]);

        if ($project->id) {
            $projectServiceTypes = [];

            foreach ($request->service_type as $key => $value) {
                $projectServiceTypes[] = ['project_id' => $project->id, 'service_type_id' => $value];
            }

            $project->projectServiceType()->createMany($projectServiceTypes);

            ProjectActivity::create([
                'project_id' => $project->id,
                'activity_type' => config('constants.project_activity_type.project')['id'],
                'remarks' => 'Project created '.Carbon::now()->format('F j, Y, g:i a'),
            ]);
        }

        if ($request->file('service_contract')) {
            $path = $request->file('service_contract')->storeAs(
                'file_projects/'.$project->id.'/15', $rename
            );

            Attachment::create([
                'filename' => $rename,
                'file_folder_id' => 15,
                'project_id' => $project->id,
                'user_id' => Auth::user()->id,
                'contact_id' => 0,
            ]);
        }

        if (!empty($request->send_welcome_email)) {
            $this->sendWelcomeEmail($project->id);
        }

        if ($request->wantsJson()) {
            return response()->json($project, 201);
        }

        return redirect()->route('client-project-tabular');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Project $project)
    {
        $serviceContract = '';
        $ae = User::where('id',$project->assigned_ae)->first();
        $accounts = Account::find($project->account_id);
        $projectStatus = config('constants.project_status.'.$project->status);
        $serviceTypeIds = $project->projectServiceType->pluck('service_type_id')->toArray();
        $allowedFiles = ServiceTypeFile::whereIn('service_type_id',$serviceTypeIds)->pluck('file_folder_id')->toArray();
        if (empty($allowedFiles)) {
            $allowedFiles[] = 0;
        }
        $fileFolders = FileFolder::whereIn('id',$allowedFiles)->pluck('folder_name', 'id');

        $projects = Project::orderBy('name')->paginate(config('constants.pagination'));

        if (!empty($project->service_contract)) {
            $serviceContract = route('download-project-files', [$project->id,'15',$project->service_contract]);
        }

        $prevWorkingDays = 1;
        $projectScheduleList = $project->projectSchedules()->orderBy('phase_no')->get();
        $chartData = $projectScheduleList->map(function ($item) use(&$prevWorkingDays) {
            $newItem = [$item->phase_name, $prevWorkingDays, $item->working_days];
            $prevWorkingDays = $item->working_days + $prevWorkingDays;
            return $newItem;
        });
         $selected = []; 
        if(!empty($project->clientTheme->themes))
        {
            $selected = unserialize($project->clientTheme->themes);
        }
        $themes = WebsiteTheme::whereIn('id',$selected)->orderBy('name')->get();

        if (!$chartData->isEmpty()) {
            $chartData = json_encode($chartData);
        }

        if ($request->wantsJson()) {
            return response()->json($project, 200);
        }
        

        $projectActivity = $project->projectActivity()->orderBy('created_at', 'desc')->get();

        return view('admin.project.view', compact('content','themes','selected','ae','projects' ,'project', 'accounts', 'projectStatus', 'files', 'fileFolders', 'serviceContract', 'chartData', 'projectScheduleList', 'projectActivity'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, Account $account)
    {
        $serviceContract = '';
        $accounts = Account::orderBy('name')->pluck('name','id');
        $accounts->prepend('Select the Project Owner', 0);
        $projectStatus = config('constants.project_status');

        $users = User::orderBy('name')->pluck('name','id');
        $ae = User::orderBy('name')->pluck('name','id');

        $serviceTypes = ServiceType::orderBy('name')->pluck('name','id');

        $project->name = old('name') ? old('name') : $project->name;
        $project->description = old('description') ? old('description') : $project->description;
        $project->status = old('status') ? old('status') : $project->status;
        $project->account_id = old('project_owner') ? old('project_owner') : $project->account_id;
        $project->assigned_ae = old('project_owner', $project->assigned_ae);
        $project->user_id = old('assigned_project_manager') ? old('assigned_project_manager') : $project->user_id;
        $project->service_type_id = old('service_type') ? old('service_type') : $project->service_type_id;
        $project->inclusions = old('inclusions') ? old('inclusions') : $project->inclusions;
        $project->requirements = old('requirements') ? old('requirements') : $project->requirements;
        $project->project_days = old('project_days') ? old('project_days') : $project->project_days;
        $project->project_end = old('project_end', $project->project_end);
        $project->project_start = old('project_start', $project->project_start);

        if (!empty($project->service_contract)) {
            $serviceContract = route('download-project-files', [$project->id,'15',$project->service_contract]);
        }

        $formSettings['welcome_email_default'] = false;
        $formSettings['show_update_remarks_field'] = true;

        $pageName = 'Update Project';
        $route = route('update-project', $project->id);
        return view('admin.project.form', compact('route', 'pageName', 'project', 'accounts', 'projectStatus', 'account', 'users', 'serviceTypes', 'serviceContract', 'ae', 'formSettings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $filename = '';
        if ($request->file('service_contract')) {
            $timestamp = date('ynjGis');
            $basename = pathinfo($request->file('service_contract')->getClientOriginalName(), PATHINFO_FILENAME );
            $fileExtension = pathinfo($request->file('service_contract')->getClientOriginalName(), PATHINFO_EXTENSION);
            $rename = $basename.'_'.$timestamp.'.'.$fileExtension;
            $project->service_contract = $rename;
        }

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'project_status' => 'required|numeric',
            'project_owner' => 'required|numeric',
            'assigned_project_manager' => 'required|numeric',
            'service_type' => 'required',
            'project_days' => 'required',
            'project_end' => 'required|date',
            'project_start' => 'required|date',
        ]);

        $project->name = $request->name;
        $project->description = $request->description;
        $project->inclusions = $request->inclusions;
        $project->requirements = $request->requirements;
        $project->status = $request->project_status;
        $project->account_id = $request->project_owner;
        $project->assigned_ae = $request->assigned_ae;
        $project->user_id = $request->assigned_project_manager;
        // $project->service_type_id = $request->service_type;
        $project->project_days = $request->project_days;
        $project->project_end = $request->project_end;
        $project->project_start = $request->project_start;
        $project->save();

        $project->projectServiceType()->delete();

        $projectServiceTypes = [];

        if (!empty($request->service_type)) {
            foreach ($request->service_type as $key => $value) {
                $projectServiceTypes[] = ['project_id' => $project->id, 'service_type_id' => $value];
            }

            $project->projectServiceType()->createMany($projectServiceTypes);
        }

        ProjectActivity::create([
            'project_id' => $project->id,
            'activity_type' => config('constants.project_activity_type.project')['id'],
            'remarks' => $request->update_remarks,
        ]);

        if ($request->file('service_contract')) {
            $path = $request->file('service_contract')->storeAs(
                'file_projects/'.$project->id.'/15', $rename
            );

            Attachment::create([
                'filename' => $rename,
                'file_folder_id' => 15,
                'project_id' => $project->id,
                'user_id' => Auth::user()->id,
                'contact_id' => 0,
            ]);
        }

        if (!empty($request->send_welcome_email)) {
            $this->sendWelcomeEmail($project->id);
        }   

        if ($request->wantsJson()) {
            return response()->json($project, 200);
        }

        return redirect()->route('client-project-tabular');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Project $project)
    {
        $project->delete();

        if ($request->wantsJson()) {
            return response()->json(null, 204);
        }

        $request->session()->flash('message.level', 'success');
        $request->session()->flash('message.content', 'Project was successfully deleted!');
    }

    public function upload(Request $request, Project $project)
    {
        if ($request->file('file_projects')) {
            foreach ($request->file('file_projects') as $file_projects) {
                $timestamp = date('ynjGis');
                $basename = pathinfo($file_projects->getClientOriginalName(), PATHINFO_FILENAME );
                $fileExtension = pathinfo($file_projects->getClientOriginalName(), PATHINFO_EXTENSION);
                $rename = $basename.'_'.$timestamp.'.'.$fileExtension;
                $path = $file_projects->storeAs(
                    'file_projects/'.$project->id.'/'.$request->file_folder, $rename
                );

                Attachment::create([
                    'filename' => $rename,
                    'file_folder_id' => $request->file_folder,
                    'project_id' => $project->id,
                    'user_id' => Auth::user()->id,
                    'contact_id' => 0,
                ]);

            }
        }
        return redirect(URL::previous());
        // return redirect()->route('show-project', $project->id);
    }

    public function download($filename) 
    {
        return response()->download(storage_path($filename));
    }

    public function viewFiles(Request $request, Project $project, FileFolder $fileFolder) 
    {
        $accounts = Account::find($project->account_id);
        $projectStatus = config('constants.project_status.'.$project->status);
        $fileFolders = FileFolder::all()->pluck('folder_name', 'id');

        $projects = Project::orderBy('name')->paginate(config('constants.pagination'));

        // $files = Storage::files('file_projects/'.$project->id.'/'.$fileFolder->id);
        $files = $project->attachments->where('file_folder_id',$fileFolder->id);

        return view('admin.project.view_files', compact('projects' ,'project', 'accounts', 'projectStatus', 'files', 'fileFolders', 'fileFolder'));
    }

    public function deleteFile($projectid, $filefolder, $filename)
    {
        $attachments = Attachment::where('project_id', $projectid)->where('file_folder_id',$filefolder)->where('filename', 'like', '%' . $filename . '%')->get();

        foreach ($attachments as $key => $attachment) {
            if ((Auth::user()->id == $attachment->user_id) && $attachment->contact_id == 0) {
                Attachment::destroy($attachment->id);
            }
        }
        File::delete(storage_path('app/file_projects/'.$projectid.'/'.$filefolder.'/'.$filename));
        return redirect(URL::previous());
    }

    private function sendWelcomeEmail($projectId) 
    {
        $empy = [];
        if (!empty($projectId)) {
            $project = Project::find($projectId);
            if (!empty($project->accounts->contacts)) {
                foreach ($project->accounts->contacts as $key => $contact) {
                        $serviceTypeIds = $project->projectServiceType->pluck('service_type_id')->toArray();
                        if (!empty($serviceTypeIds)) {
                            foreach($serviceTypeIds as $k => $v) {
                                $service = Service::where('service_type_id',$v)->first();
                                // dd($service);
                                if(!empty($service)){
                                    // dd($serviceTypeIds);
                                    // $empy['correct'] = $service;
                                    if (!empty($service->email_template) ) {
                                        if($service->email_template != "")
                                            Mail::to($contact->email)->send(new ProjectWelcome($project, $contact, $v));
                                    }else{
                                        // $empy[] = $service;
                                    }

                                }else{
                                    //   $empy[] = $v;
                                }
                            }
                            
                        }
                   
                }
            }
        }
        // dd($empy);
    }

    public function timelineDate()
    {
             
        $startDate = (!empty($_GET['startdate']))?$_GET['startdate']:'0000-00-00';
        $mandays = (!empty($_GET['mandays']))?$_GET['mandays']:0;
        $setcount = 1;
        $finalDate = $startDate;
        $sample = "";
        while(true)
        {
            if($setcount >= $mandays)
            {
                break;
            }
            $startDate = date('Y-m-d',strtotime($startDate." + 1 day"));  
            $day = date('w',strtotime($startDate)); 
            if($day == 6 || $day == 0)
            {
                
            }else{
                
                $finalDate = $startDate;
                $setcount +=1;
            }
           
        }
      
        return  $finalDate;
    }
}
