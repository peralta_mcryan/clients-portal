<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SiteContents;

class MySiteContentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:contact');
    }

    public function show(SiteContents $siteContents)
    {
    	return view('client.site_contents.view',compact('siteContents'));	

    }
}
