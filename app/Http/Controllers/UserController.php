<?php

namespace App\Http\Controllers;

use App\User;
use App\AdminType;
use App\Designation;
use App\Department;

use Validator;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Mail\UserCredentials;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('name')->paginate(config('constants.pagination'));
        // $users =User::where('id',18)->paginate(config('constants.pagination'));
        // foreach ($users as $key => $value) {
        //     // dd($value->userDesignation->designation);
        // }
        
        return view('admin.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $adminType = AdminType::orderBy('name')->pluck('name','id');
        $designations = Department::orderBy('name')->pluck('name','id');
      
        $user = new User();
        $user->name = old('name');
        $user->email = old('email');
        $user->designation = old('designation');
        $user->admin_type_id = old('role');
        
        $settings = ['include_password' => 0];
        $pageName = 'New Administrator Account';
        $route = route('administrator-accounts-create');
        return view('admin.user.form', compact('designations','user', 'settings', 'pageName', 'route', 'adminType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'role' => 'required|integer',
        ]);

        $randomPassword = $this->randomStr(8);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'designation' => $request->designation,
            'admin_type_id' => $request->role,
            'password' => bcrypt($randomPassword),
        ]);

        $user->generateToken();

        $this->sendWelcomeEmail($request, $user, $randomPassword);

        return redirect()->route('administrator-accounts-index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $users = User::orderBy('name')->paginate(config('constants.pagination'));
        return view('admin.user.view',compact('users', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $adminType = AdminType::orderBy('name')->pluck('name','id');
        $designations = Department::orderBy('name')->pluck('name','id');

        $user->name = old('name') ? old('name') : $user->name;
        $user->email = old('email') ? old('email') : $user->email;
        $user->designation = old('designation') ? old('designation') : $user->designation;
        $user->admin_type_id = old('role', $user->admin_type_id);
        $settings = ['include_password' => 0];
        $pageName = 'Edit Administrator Account';
        $route = route('administrator-accounts-edit', $user->id);
        return view('admin.user.form', compact('designations','user', 'settings', 'pageName', 'route', 'adminType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
            'role' => 'required|integer',
        ]);

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->designation = $request->get('designation');
        $user->admin_type_id = $request->get('role');
        $user->save();

        return redirect()->route('administrator-accounts-index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        $user->delete();
        $request->session()->flash('message.level', 'success');
        $request->session()->flash('message.content', 'User was successfully deleted!');

        return redirect()->route('administrator-accounts-index');
    }

    /**
     * Change the user's password
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request, User $user)
    {
        $route = route('administrator-accounts-change-pass', $user->id);
        return view('admin.user.changepassword',compact('user', 'route'));
    }

    /**
     * Update the user password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request, User $user)
    {
        Validator::extendImplicit('current_password', function ($attribute, $value, $parameters, $validator) 
        {
            if (empty($value)) 
            {
                return false;
            }

            if (Hash::check($value, Auth::user()->password)) { 
                return true;
            }

            return false;
        });

        $this->validate($request, [
            // 'current_password' => 'required|current_password',
            // 'password' => 'required|confirmed|min:8|max:45|different:current_password',
            'password' => 'required|confirmed|min:8|max:45',
        ]);

       // if (Hash::check(Input::get('current_password'), Auth::user()->password)) { 
       //      echo 'done';
       //  }


        $user->password = bcrypt($request->get('password'));
        $user->save();

        return redirect()->route('administrator-accounts-index');
    }

    private function sendWelcomeEmail(Request $request, User $user, $randomPassword) 
    {
        if (!empty($user)) {
            Mail::to($user->email)->send(new UserCredentials($request, $user, $randomPassword));
        }
    }

    private function randomStr(
    $length,
    $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ) {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        if ($max < 1) {
            throw new Exception('$keyspace must be at least two characters long');
        }
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }


}
