<?php

namespace App\Http\Controllers;

use App\User;
use App\AdminType;

use Validator;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Mail\UserCredentials;
use Illuminate\Support\Facades\Mail;

class AeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

     public function changePassword(Request $request, User $user)
    {
        $route = route('ae-accounts-change-pass', $user->id);
        return view('ae.user.changepassword',compact('user', 'route'));
    }

    /**
     * Update the user password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request, User $user)
    {
        Validator::extendImplicit('current_password', function ($attribute, $value, $parameters, $validator) 
        {
            if (empty($value)) 
            {
                return false;
            }

            if (Hash::check($value, Auth::user()->password)) { 
                return true;
            }

            return false;
        });

        $this->validate($request, [
            // 'current_password' => 'required|current_password',
            // 'password' => 'required|confirmed|min:8|max:45|different:current_password',
            'password' => 'required|confirmed|min:8|max:45',
        ]);

       // if (Hash::check(Input::get('current_password'), Auth::user()->password)) { 
       //      echo 'done';
       //  }


        $user->password = bcrypt($request->get('password'));
        $user->save();

        return redirect()->route('my-account.index');
    }
}
