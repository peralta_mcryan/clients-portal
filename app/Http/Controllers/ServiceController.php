<?php

namespace App\Http\Controllers;

use App\Service;
use App\ServiceType;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::orderBy('name')->paginate(config('constants.pagination'));
        return view('admin.service.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $serviceTypes = ServiceType::orderBy('name')->pluck('name', 'id');

        $service = new Service();
        $service->name = old('name');
        $service->service_type = old('service_type');
        $service->email_template = old('email_template');
        $pageName = 'New Service';
        $route = route('new-service');
        return view('admin.service.form', compact('service', 'pageName', 'route', 'serviceTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:45',
            'service_type' => 'required',
        ]);

        Service::create([
            'name' => $request->name,
            'service_type_id' => $request->service_type,
            'email_template' => $request->email_template
        ]);

        return redirect()->route('service');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $serviceTypes = ServiceType::orderBy('name')->pluck('name', 'id');

        $service->name = old('name') ? old('name') : $service->name;
        $service->service_type_id = old('service_type_id') ? old('service_type_id') : $service->service_type_id;
        $service->email_emplate = old('email_emplate') ? old('email_emplate') : $service->email_emplate;
        $pageName = 'Edit Service';
        $route = route('update-service', $service->id);
        return view('admin.service.form', compact('service', 'pageName', 'route', 'serviceTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $this->validate($request, [
            'name' => 'required|string|max:45|unique:services,name,'.$service->id,
            'service_type' => 'required',
        ]);

        $service->name = $request->get('name');
        $service->service_type_id = $request->get('service_type');
        $service->email_template = $request->get('email_template');
        $service->save();

        return redirect()->route('service');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Service $service)
    {
        $service->delete();
        $request->session()->flash('message.level', 'success');
        $request->session()->flash('message.content', 'Service was successfully deleted!');
    }
}
