<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;

class DepartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deptQuery = Department::orderBy('name', 'desc');
        $departments = $deptQuery->paginate(config('constants.pagination'));

        return view('admin.department.index', compact('departments'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $route = route('departments.store');
        $pageName = 'New Department';

        return view('admin.department.form',compact('route','pageName'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'      => 'required|unique:departments',
            'email'     => 'required|email|unique:departments',
        ]);

        Department::create([
            'name' => $request->name,
            'email' => $request->email,
            ]);

            return redirect(route('departments.index'));

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        $pageName = 'Edit Department';
        $route = route('departments.update',$department->id);
         return view('admin.department.form',compact('route','pageName','department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
           $this->validate($request,[
            'name'      => 'required|unique:departments,name,'.$department->id,
            'email'     => 'required|email|unique:departments,email,'.$department->id
        ]);

        $department->name = $request->name;
        $department->email = $request->email;
        $department->save();

        return redirect(route('departments.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
       $department->delete();
    }
}
