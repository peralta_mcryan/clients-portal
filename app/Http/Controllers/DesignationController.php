<?php

namespace App\Http\Controllers;

use App\Designation;
use Illuminate\Http\Request;

class DesignationController extends Controller
{

   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     */
    public function index()
    {
        $designationsQeury = Designation::orderBy('created_at');
        $designations = $designationsQeury->paginate(config('constants.pagination'));

        return view('admin.designation.view',compact('designations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $designation = new Designation;
        $pageName = 'New Designation';
        $route = route('designations.store');
        return view('admin.designation.form',compact('designation','pageName','route'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'designation' => 'required|min:3|unique:designations'
        ]);

        $designation = Designation::create(['designation'=>$request->designation]);

        return redirect()->route('designations.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function show(Designation $designation)
    {
        dd($designation->user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function edit(Designation $designation)
    {
        $pageName = 'Edit Designation';
        $route = route('designations.update',$designation->id);
        return view('admin.designation.form',compact('pageName','route','designation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Designation $designation)
    {
        
         $this->validate($request,[
            'designation' => 'required|min:3|unique:designations,designation,'.$designation->id
        ]);

        $designation->designation = $request->designation;
        $designation->save();

        return redirect()->route('designations.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Designation $designation)
    {
        if(!empty($designation->user))
        {
        
        }else{
            $designation->delete();
            return 1;
        }
      
    }
}
