<?php

namespace App\Http\Controllers;

use App\SiteContents;
use Illuminate\Http\Request;

class SiteContentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SiteContents  $siteContents
     * @return \Illuminate\Http\Response
     */
    public function show(SiteContents $siteContents)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SiteContents  $siteContents
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, SiteContents $siteContents)
    {
        $pageName = 'Site Content';
        // $route = route('site-contents-edit-user-policy', $siteContents->id);
        $route = $request->pathInfo;
        return view('admin.site_contents.form', compact('route', 'siteContents', 'pageName', 'siteContents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SiteContents  $siteContents
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiteContents $siteContents)
    {
        $siteContents->content = $request->page_content;
        $siteContents->save();

        // return redirect(URL::previous());
        $pageName = 'Site Content';
        // $route = route('site-contents-edit-user-policy', $siteContents->id);
        $route = $request->pathInfo;
        return view('admin.site_contents.form', compact('route', 'siteContents', 'pageName', 'siteContents'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SiteContents  $siteContents
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteContents $siteContents)
    {
        //
    }
}
