<?php

namespace App\Http\Controllers;

use App\ClientTheme;
use Illuminate\Http\Request;

class ClientWebsiteThemeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClientTheme  $clientTheme
     * @return \Illuminate\Http\Response
     */
    public function show(ClientTheme $clientTheme)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClientTheme  $clientTheme
     * @return \Illuminate\Http\Response
     */
    public function edit(ClientTheme $clientTheme)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClientTheme  $clientTheme
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientTheme $clientTheme)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClientTheme  $clientTheme
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClientTheme $clientTheme)
    {
        //
    }
}
