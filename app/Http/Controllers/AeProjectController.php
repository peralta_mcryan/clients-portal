<?php

namespace App\Http\Controllers;

use App\Project;
use App\ProjectServiceType;
use App\WebsiteTheme;
use App\ProjectActivity;
use App\Account;
use App\FileFolder;
use App\ServiceTypeFile;
use App\User;
use App\ServiceType;
use App\Attachment;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Mail;

use App\Mail\ProjectWelcome;

use Carbon\Carbon;
use Carbon\CarbonPeriod;

use Auth;
class AeProjectController extends Controller
{

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:ae');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd(Auth::user());
        $dropDowns['clientAccounts'] = Account::pluck('name','id')->toArray();
        $dropDowns['projectStatuses'] = config('constants.project_status');

        $projectsQuery = User::find(Auth::guard('ae')->user()->id)->projects()->orderBy('created_at', 'desc');
        
        $searchFilters['client_accounts'] = $request->client_accounts;
        $searchFilters['project_status'] = $request->project_status;
        // dd($searchFilters);
        if (!empty( $searchFilters['client_accounts'])) {
            $projectsQuery->whereIn('account_id', $searchFilters['client_accounts']);
        }

        if (!empty($searchFilters['project_status'])) {
            $projectsQuery->whereIn('status',$searchFilters['project_status']);
        }

        $projects = $projectsQuery->paginate(config('constants.pagination'));

        // $projectsQuery = Project::orderBy('created_at', 'desc');
        // $projects = $projectsQuery->paginate(config('constants.pagination'));

        // if ($request->wantsJson()) {
        //     return response()->json($projects, 200);
        // }

        return view('ae.project.project', compact('projects','searchFilters','dropDowns'));
    }

     public function index_card(Request $request)
    {
        // dd(Auth::user());
        $searchFilters['clientAccounts'] = Account::pluck('name','id')->toArray();
        $searchFilters['projectStatuses'] = config('constants.project_status');

        $projectsQuery = User::find(Auth::guard('ae')->user()->id)->projects()->orderBy('created_at', 'desc');
        
         if (!empty($request->client_accounts)) {
            $projectsQuery->whereIn('account_id',$request->client_accounts);
        }

        if (!empty($request->project_status)) {
            $projectsQuery->whereIn('status',$request->project_status);
        }


        $projects = $projectsQuery->paginate(config('constants.pagination'));

        // $projectsQuery = Project::orderBy('created_at', 'desc');
        // $projects = $projectsQuery->paginate(config('constants.pagination'));

        // if ($request->wantsJson()) {
        //     return response()->json($projects, 200);
        // }

        return view('ae.project.project-card', compact('projects','searchFilters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $serviceContract = '';
        $ae = User::where('id',$project->assigned_ae)->first();
        $accounts = Account::find($project->account_id);
        $projectStatus = config('constants.project_status.'.$project->status);
        $serviceTypeIds = $project->projectServiceType->pluck('service_type_id')->toArray();
        $allowedFiles = ServiceTypeFile::whereIn('service_type_id',$serviceTypeIds)->pluck('file_folder_id')->toArray();
        if (empty($allowedFiles)) {
            $allowedFiles[] = 0;
        }
        $fileFolders = FileFolder::whereIn('id',$allowedFiles)->pluck('folder_name', 'id');

        $projects = Project::orderBy('name')->paginate(config('constants.pagination'));

        if (!empty($project->service_contract)) {
            $serviceContract = route('download-project-files', [$project->id,'15',$project->service_contract]);
        }

        $prevWorkingDays = 1;
        $projectScheduleList = $project->projectSchedules()->orderBy('phase_no')->get();
        $chartData = $projectScheduleList->map(function ($item) use(&$prevWorkingDays) {
            $newItem = [$item->phase_name, $prevWorkingDays, $item->working_days];
            $prevWorkingDays = $item->working_days + $prevWorkingDays;
            return $newItem;
        });
        $selected = []; 
        if(!empty($project->clientTheme->themes))
        {
            $selected = unserialize($project->clientTheme->themes);
        }
        $themes = WebsiteTheme::whereIn('id',$selected)->orderBy('name')->get();

        if (!$chartData->isEmpty()) {
            $chartData = json_encode($chartData);
        }
      
        $projectActivity = $project->projectActivity()->orderBy('created_at', 'desc')->get();
        // dd($project);
        return view('ae.project.view', compact('themes','selected','ae','projects' ,'project', 'accounts', 'projectStatus', 'files', 'fileFolders', 'serviceContract', 'chartData', 'projectScheduleList', 'projectActivity'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }

    
    public function upload(Request $request, Project $project)
    {
        if ($request->file('file_projects')) {
            foreach ($request->file('file_projects') as $file_projects) {
                $timestamp = date('ynjGis');
                $basename = pathinfo($file_projects->getClientOriginalName(), PATHINFO_FILENAME );
                $fileExtension = pathinfo($file_projects->getClientOriginalName(), PATHINFO_EXTENSION);
                $rename = $basename.'_'.$timestamp.'.'.$fileExtension;
                $path = $file_projects->storeAs(
                    'file_projects/'.$project->id.'/'.$request->file_folder, $rename
                );

                Attachment::create([
                    'filename' => $rename,
                    'file_folder_id' => $request->file_folder,
                    'project_id' => $project->id,
                    'user_id' => Auth::guard('ae')->user()->id,
                    'contact_id' => 0,
                ]);

            }
        }
        return redirect(URL::previous());
        // return redirect()->route('show-project', $project->id);
    }

    public function viewFiles(Request $request, Project $project, FileFolder $fileFolder) 
    {
        $contactsId = array_column($project->accounts->contacts->toArray(), 'id');

        $accounts = Account::find($project->account_id);
        $projectStatus = config('constants.project_status.'.$project->status);
        $fileFolders = FileFolder::all()->pluck('folder_name', 'id');

        $projects = $project->accounts->projects()->orderBy('name')->paginate(config('constants.pagination'));

        $files = $project->attachments->where('file_folder_id',$fileFolder->id);

        return view('ae.project.view_files', compact('projects' ,'project', 'accounts', 'projectStatus', 'fileFolders', 'files', 'fileFolder'));
    }
}
