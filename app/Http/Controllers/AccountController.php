<?php

namespace App\Http\Controllers;

use App\Account;
use App\Country;
use App\Province;
use App\City;

use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $accounts = Account::orderBy('name')->paginate(config('constants.pagination'));
        if ($request->wantsJson()) {
            return response()->json($accounts, 200);
        }

        return view('admin.account.index', compact('accounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all()->pluck('name','id');
        $countries->prepend('Select a Country', 0);
        $provinces = [];        
        $cities = [];

        $account = new Account();
        $account->name = old('name');
        $account->address = old('address');
        $account->work_phone = old('work_phone');
        $account->mobile_phone = old('mobile_phone');
        $account->country_id = old('country');
        $account->province_id = old('province');
        $account->city_id = old('city');

        if (!empty($account->country_id)) {
            $country = Country::find($account->country_id);
            $provinces = $country->provinces->pluck('name','id');
        }

        if (!empty($account->province_id)) {
            $province = Province::find($account->province_id);
            $cities = $province->cities->pluck('name','id');
        }

        $pageName = 'New Client Account';
        $route = route('new-client-account');
        return view('admin.account.form', compact('countries', 'route', 'pageName', 'account', 'provinces', 'cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:accounts',
            'address' => 'required|max:255',
            'work_phone' => 'required|max:255',
        ]);

        $account = Account::create([
            'name' => $request->name,
            'address' => $request->address,
            'work_phone' => $request->work_phone,
            'mobile_phone' => $request->mobile_phone,
            'country_id' => $request->country,
            'province_id' => $request->province,
            'city_id' => $request->city,
        ]);

        if ($request->wantsJson()) {
            return response()->json($account, 201);
        }

        return redirect()->route('client-account');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Account $account)
    {
        $country = Country::find($account->country_id);
        $province = Province::find($account->province_id);
        $city = City::find($account->city_id);
        $accounts = Account::orderBy('name')->paginate(config('constants.pagination'));

        if ($request->wantsJson()) {
            return response()->json($account, 200);
        }

        return view('admin.account.view', compact('accounts' ,'account', 'country', 'province', 'city'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {
        $countries = Country::all()->pluck('name','id');
        $countries->prepend('Select a Country', 0);
        $provinces = [];        
        $cities = [];

        $account->name = old('name') ? old('name') : $account->name;
        $account->address = old('address') ? old('address') : $account->address;
        $account->work_phone = old('work_phone') ? old('work_phone') : $account->work_phone;
        $account->mobile_phone = old('mobile_phone') ? old('mobile_phone') : $account->mobile_phone;
        $account->country_id = old('country') ? old('country') : $account->country_id;
        $account->province_id = old('province') ? old('province') : $account->province_id;
        $account->city_id = old('city') ? old('city') : $account->city_id;

        if (!empty($account->country_id)) {
            $country = Country::find($account->country_id);
            $provinces = $country->provinces->pluck('name','id');
        }

        if (!empty($account->province_id)) {
            $province = Province::find($account->province_id);
            $cities = $province->cities->pluck('name','id');
        }

        $pageName = 'Update Client Account';
        $route = route('update-client-account', $account->id);
        return view('admin.account.form', compact('countries', 'route', 'pageName', 'account', 'provinces', 'cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $account)
    {
        $this->validate($request, [
            'name' => 'required|max:255|unique:accounts,name,'.$account->id,
            'address' => 'required|max:255',
            'work_phone' => 'required|max:255',
        ]);

        $account->name = $request->name;
        $account->address = $request->address;
        $account->work_phone = $request->work_phone;
        $account->mobile_phone = $request->mobile_phone;
        $account->country_id = $request->country;
        $account->province_id = $request->province;
        $account->city_id = $request->city;
        $account->save();

        if ($request->wantsJson()) {
            return response()->json($account, 200);
        }

        return redirect()->route('client-account');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Account $account)
    {
        $account->delete();
        $request->session()->flash('message.level', 'success');
        $request->session()->flash('message.content', 'Account was successfully deleted!');

        if ($request->wantsJson()) {
            return response()->json(null, 204);
        }

        return redirect()->route('client-account');
    }
}
