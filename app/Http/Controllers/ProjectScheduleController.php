<?php

namespace App\Http\Controllers;

use App\ProjectSchedule;
use Illuminate\Http\Request;

use App\Project;

class ProjectScheduleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project)
    {
        $prevWorkingDays = 1;
        $projectScheduleList = $project->projectSchedules()->orderBy('phase_no')->get();
        $chartData = $projectScheduleList->map(function ($item) use(&$prevWorkingDays) {
            $newItem = [$item->phase_name, $prevWorkingDays, $item->working_days];
            $prevWorkingDays = $item->working_days + $prevWorkingDays;
            return $newItem;
        });

        if (!$chartData->isEmpty()) {
            $chartData = json_encode($chartData);
        }

        $projectSchedule = new ProjectSchedule();
        $projectSchedule->phase_no = old('order', count($projectScheduleList)+1);
        $projectSchedule->phase_name = old('phase_name');
        $projectSchedule->working_days = old('working_days');
        $projectSchedule->description = old('description');

        $pageName = 'Project Timeline';
        $route = route('project-timeline.create', $project->id);
        return view('admin.project-schedule.form', compact('pageName','route', 'projectScheduleList', 'project', 'projectSchedule', 'chartData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Project $project)
    {
        $this->validate($request, [
            'order' => 'required|integer',
            'phase_name' => 'required|max:255',
            'working_days' => 'required|numeric',
        ]);

        $projectSchedule = ProjectSchedule::create([
            'project_id' => $project->id,
            'phase_no' => $request->order,
            'phase_name' => $request->phase_name,
            'description' => $request->description,
            'working_days' => $request->working_days,
        ]);

        return redirect()->route('project-timeline.index',$project->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProjectSchedule  $projectSchedule
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectSchedule $projectSchedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectSchedule  $projectSchedule
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectSchedule $projectSchedule)
    {
        $project = $projectSchedule->project;
        $projectScheduleList = $project->projectSchedules()->orderBy('phase_no')->get();

        $prevWorkingDays = 1;
        $chartData = $projectScheduleList->map(function ($item) use(&$prevWorkingDays) {
            $newItem = [$item->phase_name, $prevWorkingDays, $item->working_days];
            $prevWorkingDays = $item->working_days + $prevWorkingDays;
            return $newItem;
        });

        if (!$chartData->isEmpty()) {
            $chartData = json_encode($chartData);
        }

        $projectSchedule->phase_no = old('order', $projectSchedule->phase_no);
        $projectSchedule->phase_name = old('phase_name', $projectSchedule->phase_name);
        $projectSchedule->working_days = old('working_days', $projectSchedule->working_days);
        $projectSchedule->description = old('description', $projectSchedule->description);

        $pageName = 'Project Timeline';
        $route = route('project-timeline.update', $projectSchedule->id);
        return view('admin.project-schedule.form', compact('pageName','route', 'projectScheduleList', 'project', 'projectSchedule', 'chartData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectSchedule  $projectSchedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectSchedule $projectSchedule)
    {
        $this->validate($request, [
            'order' => 'required|integer',
            'phase_name' => 'required|max:255',
            'working_days' => 'required|numeric',
        ]);

        $projectSchedule->phase_no = $request->order;
        $projectSchedule->phase_name = $request->phase_name;
        $projectSchedule->description = $request->description;
        $projectSchedule->working_days = $request->working_days;
        $projectSchedule->save();

        return redirect()->route('project-timeline.index',$projectSchedule->project_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectSchedule  $projectSchedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectSchedule $projectSchedule)
    {
        $projectSchedule->delete();
    }
}
