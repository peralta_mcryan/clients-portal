<?php

namespace App\Http\Controllers;

use App\AdminType;
use Illuminate\Http\Request;

class AdminTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('disabledesignation');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adminTypes = AdminType::orderBy('name')->paginate(12);
        return view('admin.admin_type.index', compact('adminTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $adminType = new AdminType();
        $adminType->name = old('name');
        $pageName = 'New Administrator Type';
        $route = route('admin-types.store');
        return view('admin.admin_type.form', compact('adminType', 'pageName', 'route'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
        ]);

        AdminType::create([
            'name' => $request->name
        ]);

        return redirect()->route('admin-types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdminType  $adminType
     * @return \Illuminate\Http\Response
     */
    public function show(AdminType $adminType)
    {
        $adminTypes = AdminType::orderBy('name')->paginate(12);
        return view('admin.admin_type.view',compact('adminTypes', 'adminType'));    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdminType  $adminType
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminType $adminType)
    {
        $adminType->name = old('name') ? old('name') : $adminType->name;
        $pageName = 'Edit Service Type';
        $route = route('update-admin-type', $adminType->id);
        return view('admin.admin_type.form',compact('adminType', 'pageName', 'route'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminType  $adminType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminType $adminType)
    {
    
       $this->validate($request, [
            'name' => 'required|string|max:255|unique:admin_types,name,'.$adminType->id,
        ]);

         $adminType->name = $request->name;
         $adminType->save();

         return redirect()->route('admin-types.index');
    }
       


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminType  $adminType
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminType $adminType)
    {
        $adminType->delete();
    }
}
