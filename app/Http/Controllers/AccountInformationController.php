<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contact;
use Auth;
use Validator;
use Hash;

class AccountInformationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:contact');
    }

    public function index(Contact $contact)
    {
		if ($contact->id != Auth::user()->id) {
            return redirect(route('account-information', Auth::user()->id));
        }
    	return view('client.account_information.index', compact('contact'));
    }

    public function changePassword(Request $request, Contact $contact)
    {
    	if ($contact->id != Auth::user()->id) {
            return redirect(route('client-dashboard'));
        }

        $route = route('client-contact-change-pass', $contact->id);
        return view('client.account_information.changepassword',compact('contact', 'route'));
    }

    public function updatePassword(Request $request, Contact $contact)
    {
    	if ($contact->id != Auth::user()->id) {
            return redirect(route('client-dashboard'));
        }

        Validator::extendImplicit('current_password', function ($attribute, $value, $parameters, $validator) 
        {
            if (empty($value)) 
            {
                return false;
            }

            if (Hash::check($value, Auth::user()->password)) { 
                return true;
            }

            return false;
        });

        $this->validate($request, [
            'current_password' => 'required|current_password',
            'password' => 'required|confirmed|min:8|max:45|different:current_password',
        ]);

        $contact->password = bcrypt($request->get('password'));
        $contact->save();

        return redirect()->route('client-dashboard');
    }

    public function officialReceipts(Request $request, Contact $contact)
    {

    }
}
