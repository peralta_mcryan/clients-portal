<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Account;
use App\Service;

use Validator;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use App\Mail\ContactCredentials;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $contacts = Contact::orderBy('first_name')->paginate(config('constants.pagination'));

        if ($request->wantsJson()) {
            return response()->json($contacts, 200);
        }

        return view('admin.contact.index', compact('contacts'));
    }
    

    public function index_tabular(Request $request)
    {
        $contacts = Contact::orderBy('first_name')->paginate(config('constants.pagination'));

        if ($request->wantsJson()) {
            return response()->json($contacts, 200);
        }

        return view('admin.contact.index_tabular', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Account $account)
    {
        $accounts = Account::all()->pluck('name','id');
        $accounts->prepend('Select the Account', 0);
        $portalAccess = config('constants.portal_access');
        $contactType = config('constants.contact_type');

        $contact = new Contact();
        $contact->first_name = old('first_name');
        $contact->last_name = old('last_name');
        $contact->work_phone = old('work_phone');
        $contact->mobile_phone = old('mobile_phone');
        $contact->email = old('email_address');
        $contact->designation = old('designation');
        $contact->department = old('department');
        $contact->account = old('account');
        
        if ($account->id) {
            $pageName = 'New Contact for '.$account->name;
            $route = route('new-contact-account', $account->id);
        } else {
            $pageName = 'New Contact';
            $route = route('new-contact');
        }

        $settings = ['include_password' => 0, 'show_email_checkbox' => 1, 'show_email_default_value' => true];

        return view('admin.contact.form', compact('contact', 'pageName', 'route', 'accounts', 'portalAccess', 'account', 'settings', 'contactType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->department);

        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email_address' => 'required|string|max:255|unique:contacts,email',
            'work_phone' => 'required|string|max:255',
            'mobile_phone' => 'required|string|max:255',
            'designation' => 'max:255',
            'department' => 'max:255',
            'account' => 'numeric',
            'portal_access' => 'numeric',
            'contact_type' => 'required|numeric',
            // 'password' => 'required|string|min:6|confirmed',
        ]);

        $randomPassword = $this->randomStr(8);

        $contact = Contact::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email_address,
            'work_phone' => $request->work_phone,
            'mobile_phone' => $request->mobile_phone,
            'designation' => $request->designation,
            'department' => $request->department,
            'account_id' => $request->account,
            'allow_access' => $request->portal_access,
            'contact_type' => $request->contact_type,
            'main' => 0,
            'password' => bcrypt($randomPassword),
        ]);

        if (!empty($request->send_welcome_email)) {
            $this->sendWelcomeEmail($request, $contact, $randomPassword);
        }

        if ($request->wantsJson()) {
            return response()->json($contact, 201);
        }

        return redirect()->route('client-contact');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Contact $contact)
    {
        $accounts = Account::all()->pluck('name','id');
        $accounts->prepend('Select the Account', 0);
        $portalAccess = config('constants.portal_access');

        $contacts = Contact::orderBy('first_name')->paginate(config('constants.pagination'));
        
        if ($request->wantsJson()) {
            return response()->json($contact, 200);
        }

        return view('admin.contact.view', compact('contacts' ,'contact', 'portalAccess'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact, Account $account)
    {
        $accounts = Account::all()->pluck('name','id');
        $accounts->prepend('Select the Account', 0);
        $portalAccess = config('constants.portal_access');
        $contactType = config('constants.contact_type');

        $contact->first_name = old('first_name') ? old('first_name') : $contact->first_name;
        $contact->last_name = old('last_name') ? old('last_name') : $contact->last_name;
        $contact->work_phone = old('work_phone') ? old('work_phone') : $contact->work_phone;
        $contact->mobile_phone = old('mobile_phone') ? old('mobile_phone') : $contact->mobile_phone;
        $contact->email = old('email_address') ? old('email_address') : $contact->email;
        $contact->designation = old('designation') ? old('designation') : $contact->designation;
        $contact->department = old('department') ? old('department') : $contact->department;
        $contact->account = old('account') ? old('account') : $contact->account;
        
        if ($account->id) {
            $pageName = 'Update Contact for '.$account->name;
            $route = route('update-contact-account', $contact->id, $account->id);
        } else {
            $pageName = 'Update Contact';
            $route = route('update-contact', $contact->id);
        }

        $settings = ['include_password' => 0, 'show_email_checkbox' => 1, 'show_email_default_value' => false];

        return view('admin.contact.form', compact('contact', 'pageName', 'route', 'accounts', 'portalAccess', 'account', 'settings', 'contactType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email_address' => 'required|string|max:255|unique:contacts,email,'.$contact->id,
            'work_phone' => 'required|string|max:255',
            'mobile_phone' => 'required|string|max:255',
            'designation' => 'max:255',
            'department' => 'max:255',
            'account' => 'numeric',
            'portal_access' => 'numeric',
            'contact_type' => 'required|numeric',
        ]);

        $randomPassword = $this->randomStr(8);

        $contact->first_name = $request->first_name;
        $contact->last_name = $request->last_name;
        $contact->email = $request->email_address;
        $contact->work_phone = $request->work_phone;
        $contact->mobile_phone = $request->mobile_phone;
        $contact->designation = $request->designation;
        $contact->department = $request->department;
        $contact->account_id = $request->account;
        $contact->allow_access = $request->portal_access;
        $contact->contact_type = $request->contact_type;

        if ($request->wantsJson()) {
            return response()->json($contact, 200);
        }

        if (!empty($request->send_welcome_email)) {
            $contact->password = bcrypt($randomPassword);
            $this->sendWelcomeEmail($request, $contact, $randomPassword);
        }

        $contact->save();

        return redirect()->route('client-contact');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Contact $contact)
    {
        $contact->delete();

        if ($request->wantsJson()) {
            return response()->json(null, 204);
        }

        $request->session()->flash('message.level', 'success');
        $request->session()->flash('message.content', 'Contact was successfully deleted!');

        return redirect()->route('client-contact');
    }

    public function changePassword(Request $request, Contact $contact)
    {
        $route = route('contact-change-pass', $contact->id);
        return view('admin.contact.changepassword',compact('contact', 'route'));
    }

    public function updatePassword(Request $request, Contact $contact)
    {
        Validator::extendImplicit('current_password', function ($attribute, $value, $parameters, $validator) 
        {
            if (empty($value)) 
            {
                return false;
            }

            if (Hash::check($value, Auth::user()->password)) { 
                return true;
            }

            return false;
        });

        $this->validate($request, [
            'current_password' => 'required|current_password',
            'password' => 'required|confirmed|min:8|max:45|different:current_password',
        ]);

        $contact->password = bcrypt($request->get('password'));
        $contact->save();

        return redirect()->route('client-contact');
    }

    private function sendWelcomeEmail(Request $request, Contact $contact, $randomPassword) 
    {
        if (!empty($contact)) {
            Mail::to($contact->email)->send(new ContactCredentials($request, $contact, $randomPassword));
        }
    }

    private function randomStr(
    $length,
    $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ) {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        if ($max < 1) {
            throw new Exception('$keyspace must be at least two characters long');
        }
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }
}
