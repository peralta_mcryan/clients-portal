<?php

namespace App\Http\Controllers;

use App\IssueList;
use App\User;
use App\IssueLog;
use App\AlertStatus;
use App\IssueThread;
use App\Project;
use App\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\TechnicalTicket;

use Auth;

class AdminTicketController extends Controller
{

      public function __construct()
    {
        $this->middleware('auth');
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        // $projects = Project::orderBy('name')->pluck('name','id');
        // $issuesQuery = IssueList::orderBy('created_at');
        
        
        // if (!empty($request->project)) {
        //     $issuesQuery->whereIn('project_id',$request->project);
        // }

        // if (!empty($request->status)) {
        //     $issuesQuery->whereIn('status',$request->status);
        // }

        // if (!empty($request->priority)) {
        //     $issuesQuery->whereIn('priority',$request->priority);
        // }
        
        // $issues = $issuesQuery->paginate(config('constants.pagination'));
        
        // return view('technical_team.tickets.index',compact('issues','projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $count = IssueList::get()->count();
        // if($count > 0 ){
        //     $count = date('Ymd').sprintf('%04s',$count+1);
        // }else{
        //     $count = date('Ymd').'0001';
        // }
        // session(['ctrl' => $count]);
        // $issueList = new IssueList();
        // $issueList->control_no = $count;
        // $route = route('tickets.store');
        // $projects = Project::orderBy('name')->pluck('name','id');
        // $departments = Department::orderBy('name')->pluck('name','id');

        // $pageName = 'Add New Ticket';
        //  return view('technical_team.tickets.form',compact('route','pageName','projects','departments','issueList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request,[
        //     'ctrl'=>'required',
        //     'assigned_department'=>'required',
        //     'status'=>'required',
        //     'project'=>'required',
        //     'priority'=>'required',
        //     'details'=>'required',
        // ]);

        // $project = Project::find($request->project);

        // $issue = IssueList::create([
        //     'control_no' => session('ctrl'),
        //     'account_id' => $project->accounts->id,
        //     'project_id' => $request->project,
        //     'status' => $request->status,
        //     'priority' => $request->priority,
        //     'assigned_to' => $request->assigned_department,
        //     'description' => $request->details,
        //     'created_by' => Auth::guard('techTeam')->user()->id,
            
        // ]);
        // $issue = IssueList::find($issue->id);
        // $logs = [];
        // $logs[] = 'Issue created: '.date('F d, Y H:m:s');
        // $jsonLog = json_encode($logs);
        
        // IssueLog::create([
        //     'issue_id' => $issue->id,
        //     'details' =>  $jsonLog,
        //     'created_by' =>  Auth::guard('techTeam')->user()->id,
        // ]);

        // Mail::to($project->assignedAe->email)->send(new TechnicalTicket($issue,1));
        // $request->session()->forget('ctrl');
        // return redirect(route('tickets.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IssueList  $issueList
     * @return \Illuminate\Http\Response
     */
    public function show(IssueList $issueList)
    {
        $user = 0;
        $user = Auth::guard('web')->user()->id;
       
        $alerts = AlertStatus::where('issue_id',$issueList->id)->where('user_id',$user)->first();
        $issueLogs = IssueLog::where('issue_id',$issueList->id)->orderBy('created_at')->get();
        $issueThread = $issueList->issue_thread()->orderBy('created_at','DSC')->paginate(config('constants.pagination'));
        // foreach ($issueThread as $key => $value) {
        //   if (!empty($value->thread_discussion())){
        //       var_dump($value);
        //       dd($value->thread_discussion()->get());
        //     }
        // } 
        
        return view('admin.tickets.view',compact('issueList','issueLogs','issueThread','alerts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IssueList  $issueList
     * @return \Illuminate\Http\Response
     */
    public function edit(IssueList $issueList)
    { 
        $route = route('admin-tickets.update',$issueList->id);
        $pageName = 'Edit Ticket';

        $projects = Project::orderBy('name')->pluck('name','id');
        $users = User::orderBy('name')->pluck('name','id');

         return view('admin.tickets.form',compact('route','pageName','projects','users','issueList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IssueList  $issueList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IssueList $issueList)
    { 
       $this->validate($request,[
            'ctrl'=>'required',
            'assigned_department'=>'required',
            'status'=>'required',
            'project'=>'required',
            'priority'=>'required',
            'details'=>'required',
        ]);
       $status = config('constants.ticket_status_def');
        $priority = config('constants.ticket_priority_def');
        $issueList2 = IssueList::where('id',$issueList->id)->first();
        $logs = [];
        $assignee = $issueList->assignee->name;
        $project = Project::find($request->project);
        $issueList->assigned_to = $request->assigned_department;
        $issueList->status = $request->status;
        $issueList->project_id = $request->project;
        $issueList->priority = $request->priority;
        $issueList->description = $request->details;
        $issueList->save();

        $newCreated = IssueList::where('id',$issueList->id)->first();
        if($issueList2->assigned_to != $newCreated->assigned_to){
            $logs[] = 'Change assigned assignee from '.$assignee.' to '.$newCreated->assignee->name;
        }
        
        if($issueList2->status != $newCreated->status){
            $logs[] = 'Change status from '.$status[$issueList2->status].' to '.$status[$request->status];
        }
        
        if($issueList2->priority != $newCreated->priority){
           $logs[] = 'Change priority from '.$priority[$issueList2->priority].' to '.$priority[$request->priority];
       }
        
        if($issueList2->description != $newCreated->description){
            $logs[] = 'Change details';
        }

        $jsonLog = json_encode($logs);
        
        if($jsonLog != ''){
            IssueLog::create([
                'issue_id' => $issueList->id,
                'details' =>  $jsonLog,
                'created_by' =>  Auth::user()->id,
            ]);

           $isExists = AlertStatus::where('issue_id',$issueList->id)->where('user_id',$issueList->assigned_to)->first();
            if(empty($isExists)){
                    AlertStatus::create([
                    'issue_id' => $issueList->id,
                    'user_id' => $issueList->assigned_to,
                    'status' => 1
                ]);
            }

            Mail::to('projects@clientsportal.imanila.ph')->send(new TechnicalTicket($issueList,2,'',''));
        }
        return redirect(route('admin-tickets.show',$issueList->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IssueList  $issueList
     * @return \Illuminate\Http\Response
     */
    public function destroy(IssueList $issueList)
    {
        $id = $issueList->id;
        $issueList->delete();

        $logs = [];
        $logs[] = 'Issue deleted: '.date('F d, Y H:m:s');
        $jsonLog = json_encode($logs);
        
        IssueLog::create([
            'issue_id' => $id,
            'details' =>  $jsonLog,
            'created_by' =>  Auth::guard('techTeam')->user()->id,
        ]);


         Mail::to('projects@clientsportal.imanila.ph')->send(new TechnicalTicket($issueList,3,'',''));
        return 1;
    }
}
