<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Mail;

use App\Contact;
use App\User;
use App\Project;
use App\Account;
use App\FileFolder;
use App\Attachment; 
use App\ServiceTypeFile; 
use App\WebsiteTheme; 
use App\ProjectActivity; 
use App\ClientTheme; 

use App\Mail\FileUploaded;
use App\Mail\SelectedTheme;

use Auth;

class MyProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:contact');
    }

    public function index(Contact $contact)
    {
 
        $projects = array();
        if(empty($contact))
            return redirect(route('client-dashboard'));

        if ($contact->id != Auth::user()->id) {
            return redirect(route('client-dashboard'));
        }

        if (!empty($contact->account->projects)) {
            $projects = $contact->account->projects()->orderBy('name')->paginate(config('constants.pagination'));
        }

    	return view('client.project.index',compact('projects'));
    }

    public function show(Project $project)
    {
        $serviceContract ='';

        $ae = User::where('id',$project->assigned_ae)->first();

        if (!isset($project->accounts->contacts)) {
            return redirect(route('client-dashboard'));
        }

        $contactsId = array_column($project->accounts->contacts->toArray(), 'id');
        if (!in_array(Auth::user()->id, $contactsId)) {
            return redirect(route('client-dashboard'));
        }

        $selected = []; 
        if(!empty($project->clientTheme->themes))
        {
            $selected = unserialize($project->clientTheme->themes);
        }
       
        if(!empty($_GET['group']))
        {
            $group = $_GET['group'];
            
        }else{
            if(count($selected) > 0)
            {
                $group = WebsiteTheme::where('id','=',$selected[0])->pluck('theme_group');
            }else{
                $group = 1;
            }
        }
        $themesQuery = WebsiteTheme::where('theme_group','=',$group)->orderBy('name')->get();
        $clientTheme = $project->clientTheme;

        $themes = [];
        foreach ($themesQuery as $key => $value) {
            // $result = File::exists($myfile);
            // $previews = $previewClient->getParsed();
            // foreach ($previews as $key => $preview) {
            //     $data = $preview->getPictures();   
            // }
            $list = [];
            $list['theme'] = $value;
            // $list['preview'] = !empty($data[3])?$data[3]:'';
            $themes[] = $list;
     
        }
        

        // $parsed = $linkPreview->getParsed();
        $accounts = Account::find($project->account_id);
        $projectStatus = config('constants.project_status.'.$project->status);
        $projectStatus = config('constants.project_status.'.$project->status);
        $serviceTypeIds = $project->projectServiceType->pluck('service_type_id')->toArray();
        $allowedFiles = ServiceTypeFile::whereIn('service_type_id',$serviceTypeIds)->pluck('file_folder_id')->toArray();
        if (empty($allowedFiles)) {
            $allowedFiles[] = 0;
        }
        $fileFolders = FileFolder::whereIn('id',$allowedFiles)->pluck('folder_name', 'id');

        $projects = $project->accounts->projects()->orderBy('name')->paginate(config('constants.pagination'));
        
        if (!empty($project->service_contract)) {
            $serviceContract = route('download-project-files', [$project->id,'15',$project->service_contract]);
        }

        $prevWorkingDays = 1;
        $projectScheduleList = $project->projectSchedules()->orderBy('phase_no')->get();
        $chartData = $projectScheduleList->map(function ($item) use(&$prevWorkingDays) {
            $newItem = [$item->phase_name, $prevWorkingDays, $item->working_days];
            $prevWorkingDays = $item->working_days + $prevWorkingDays;
            return $newItem;
        });
        
        if (!$chartData->isEmpty()) {
            $chartData = json_encode($chartData);
        }

        
        $projectActivity = $project->projectActivity()->orderBy('created_at', 'desc')->get();

        return view('client.project.view', compact('group','clientTheme','selected','themes','ae','projects' ,'project', 'accounts', 'projectStatus', 'fileFolders', 'serviceContract', 'chartData', 'projectScheduleList', 'projectActivity'));
    }

    public function upload(Request $request, Project $project)
    {
        $uploadedFiles = [];
        if ($request->file('file_projects')) {
            foreach ($request->file('file_projects') as $file_projects) {
                $timestamp = date('ynjGis');
                $basename = pathinfo($file_projects->getClientOriginalName(), PATHINFO_FILENAME );
                $fileExtension = pathinfo($file_projects->getClientOriginalName(), PATHINFO_EXTENSION);
                $rename = $basename.'_'.$timestamp.'.'.$fileExtension;
                $path = $file_projects->storeAs(
                    'file_projects/'.$project->id.'/'.$request->file_folder, $rename
                );

                $uploadedFiles[$request->file_folder][] = $rename;

                Attachment::create([
                    'filename' => $rename,
                    'file_folder_id' => $request->file_folder,
                    'project_id' => $project->id,
                    'contact_id' => Auth::user()->id,
                    'user_id' => 0,
                ]);

            }

            $this->sendUploadEmail($request, $project, $uploadedFiles);
        }
        return redirect(URL::previous());
        // return redirect()->route('my-project-show', $project->id);
    }

    public function viewFiles(Request $request, Project $project, FileFolder $fileFolder) 
    {
        if (!isset($project->accounts->contacts)) {
            return redirect(route('client-dashboard'));
        }

        $contactsId = array_column($project->accounts->contacts->toArray(), 'id');

        if (!in_array(Auth::user()->id, $contactsId)) {
            return redirect(route('client-dashboard'));
        }

        $accounts = Account::find($project->account_id);
        $projectStatus = config('constants.project_status.'.$project->status);
        $fileFolders = FileFolder::all()->pluck('folder_name', 'id');

        $projects = $project->accounts->projects()->orderBy('name')->paginate(config('constants.pagination'));

        $files = $project->attachments->where('file_folder_id',$fileFolder->id);

        return view('client.project.view_files', compact('projects' ,'project', 'accounts', 'projectStatus', 'fileFolders', 'files', 'fileFolder'));
    }

    public function viewReceipts(Contact $contact)
    {
        if ($contact->id != Auth::user()->id) {
            return redirect(route('client-dashboard'));
        }

        $projects = $contact->account->projects;
        return view('client.project.index_receipts',compact('projects'));
    }

    public function viewReports(Contact $contact)
    {
        if ($contact->id != Auth::user()->id) {
            return redirect(route('client-dashboard'));
        }

        $projects = $contact->account->projects;
        return view('client.project.index_reports',compact('projects'));
    }

    public function deleteFile($projectid, $filefolder, $filename)
    {
        $attachments = Attachment::where('project_id', $projectid)->where('file_folder_id',$filefolder)->where('filename', 'like', '%' . $filename . '%')->get();

        foreach ($attachments as $key => $attachment) {
            if ((Auth::user()->id == $attachment->contact_id) && $attachment->user_id == 0) {
                Attachment::destroy($attachment->id);
            }
        }
        File::delete(storage_path('app/file_projects/'.$projectid.'/'.$filefolder.'/'.$filename));
        return redirect(URL::previous());
    }

    private function sendUploadEmail($request, $project, $uploadedFiles) 
    {
        if (!empty($project)) {
            $contact = Auth::user();

            if (!empty($project->users)) {
                $user = $project->users;
                Mail::to('salessupport@imanila.ph')->send(new FileUploaded($request, $contact, $user, $project, $uploadedFiles));
            }
        }
    }

    private function sendselectedThemeEmail($request, $project) 
    {
        if (!empty($project)) {
            $contact = Auth::user();
            $details = [];
            if(isset($request->themeSel))
            {
              
                    $details = WebsiteTheme::whereIn('id',$request->themeSel)->get();
             
            } 
            // dd($details);
            if (!empty($project->users)) {
                $user = $project->users;
                Mail::to('projects@imanila.ph')->send(new SelectedTheme($request, $contact, $user, $project, $details));
            }
        }
    }

    public function saveThemes(Request $request,Project $project)
    {  
        
        $selected = [];
        // dd($request->themeSel);
        if(isset($request->themeSel))
        {
            foreach ($request->themeSel as $key => $value) {
                $selected[] =$value; 
            }
        }
        $same = 0;
        $themeNames = WebsiteTheme::whereIn('id',$selected)->pluck('name');
        $themename = 'None';
        if(count($themeNames) > 0)
        {
            foreach ($themeNames as $key => $value) {
                  if($themename == 'None')
                    {
                        $themename = $value;
                    }else{
                        $themename .= ', '.$value;
                    }
            }
          
        }


        $selected = serialize($selected);

        if(empty($project->clientTheme)){
            $same = 1;
            $project->clientTheme()->create([
                    'project_id' => $project->id,
                    'themes'     => $selected,
                    'remarks'   => $request->remarks

            ]);
            ProjectActivity::create([
                'project_id' => $project->id,
                'activity_type' => config('constants.project_activity_type.project')['id'],
                'remarks' => 'Selected new set of themes: '.$themename,
            ]);
        }else{

            if(($selected != $project->clientTheme->themes) || (trim($project->clientTheme->remarks) != trim($request->remarks)))
                $same = 1;
            // dd($selected.'    -------    '.$project->clientTheme->themes);
            $project->clientTheme()->update([
                'themes'     =>  $selected,
                'remarks'   => $request->remarks

            ]);

            if($same == 1)
            {
                ProjectActivity::create([
                    'project_id' => $project->id,
                    'activity_type' => config('constants.project_activity_type.project')['id'],
                    'remarks' => 'Updated theme selection: '.$themename,
                ]);
            }
            
        }
        

        if($same == 1)
            $this->sendselectedThemeEmail($request, $project);

    
        return redirect(URL::previous());
        
    }
}
