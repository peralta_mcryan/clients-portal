<?php

namespace App\Http\Controllers;

use App\ProjectServiceType;
use Illuminate\Http\Request;

class ProjectServiceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProjectService  $projectService
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectServiceType $projectServiceType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectService  $projectService
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectServiceType $projectServiceType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectService  $projectService
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectServiceType $projectServiceType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectService  $projectService
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectServiceType $projectServiceType)
    {
        //
    }
}
