<?php

namespace App\Http\Controllers;

use App\ChangeRequest;
use App\Account;
use App\Project;
use App\Mail\ChangeRequestEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ChangeRequestController extends Controller
{


      public function __construct()
    {
        $this->middleware('auth:web');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
  
        $searchFilters['clientAccounts'] = Account::pluck('name','id')->toArray();
        $searchFilters['projects'] =  Project::orderBy('created_at', 'desc');
    
        $croQuery = ChangeRequest::orderBy('created_at');
        if(!empty($request->client_accounts))
        {
            $croQuery->whereIn('account_id',$request->client_accounts);
        }
        
        if(!empty($request->projects))
        {
            $croQuery->whereIn('project_id',$request->projects);
        }

        $CRO = $croQuery->paginate(config('constant.pagination'));
      
        
        return view('admin.change_request.index',compact('searchFilters','CRO'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
        $pageName = 'New Change Request';
        $route = route('change-requests.store');
        $projectArray =  Project::orderBy('created_at', 'desc');
        return view('admin.change_request.form',compact('projectArray','pageName','route'));
    }
    
    
    /* Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $message = [
            'man_hours.between' =>'Man hours must not be equal to 0.',
            'price.between' =>'Price must not be equal to 0.',
        ];

    


        $this->validate($request,[

                'project'  =>  'required',
                'task'      =>  'required',
                'man_hours' =>  'required|between:0.01,9999999999999999999999999999.99999999999999999999|numeric|max:100000000',
                'price'     =>  'required|between:0.01,9999999999999999999999999999.99999999999999999999|numeric|max:100000000'   
        ],$message);

        $client = Project::find($request->project);
  
        $cr = ChangeRequest::create([
                'project_id'    =>      $request->project,
                'account_id'    =>      $client->account_id,
                'task'          =>      $request->task,
                'man_hours'     =>      $request->man_hours,
                'price'         =>      $request->price,
        ]);
            
        $request->session()->flash('message.level', 'success');
        $request->session()->flash('message.content', 'Change Request was successfully created!');


        Mail::to(!empty($cr->project->assignedAe->email)?$cr->project->assignedAe->email:'salessupport@imanila.ph')->send(new ChangeRequestEmail($cr));
        
        return redirect(route('change-requests.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(ChangeRequest $changerequest)
    {    
        // dd($changerequest);
        $pageName = "Change Request";
        return view('admin.change_request.view',compact('changerequest','pageName'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(ChangeRequest $changerequest)
    {
        $pageName = "Edit Change request";
        $route = route('change-requests.update',$changerequest->id);
        $projectArray =  Project::orderBy('created_at', 'desc');
        $changerequest->price = number_format( $changerequest->price,2);
        $changerequest->man_hours = number_format( $changerequest->man_hours,2);

        return view('admin.change_request.form',compact('pageName','route','projectArray','changerequest'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChangeRequest $changerequest)
    {
        $message = [
            'man_hours.between' =>'Man hours must not be equal to 0.',
            'price.between' =>'Price must not be equal to 0.',
        ];
      
          $this->validate($request,[

                'project'  =>  'required',
                'task'      =>  'required',
                'man_hours' =>  'required|between:0.01,9999999999999999999999999999.99999999999999999999|numeric',
                'price'     =>  'required|between:0.01,9999999999999999999999999999.99999999999999999999|numeric'   
        ],$message);

        $changerequest->project_id = $request->project;
        $changerequest->task = $request->task;
        $changerequest->man_hours = $request->man_hours;
        $changerequest->price = $request->price;

        $changerequest->save();
        return redirect(route('change-requests.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChangeRequest $changerequest)
    {
        $changerequest->delete();
    }
}
