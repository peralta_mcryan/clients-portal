<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'address', 'work_phone', 'mobile_phone', 'country', 'province', 'city'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    function contacts()
    {
        return $this->hasMany('App\Contact', 'account_id')->orderBy('contact_type');
    }

    function projects()
    {
        return $this->hasMany('App\Project', 'account_id');
    }
}
