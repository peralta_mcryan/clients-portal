<?php

use App\Project;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

/*************************
* Projects               *
*************************/
Route::group(['middleware' => 'auth:api'], function() {
    Route::get('/projects', 'ProjectController@index')->name('client-project');
    Route::post('/projects/new', 'ProjectController@store')->name('new-project');
    Route::post('/projects/new/{account}', 'ProjectController@store')->name('new-project-account');
    Route::post('/projects/edit/{project}', 'ProjectController@update')->name('update-project');
    Route::get('/projects/show/{project}', 'ProjectController@show')->name('show-project');
    Route::get('/projects/delete/{project}', 'ProjectController@destroy')->name('delete-project');

    Route::get('/clients', 'AccountController@index')->name('client-account');
    Route::post('/clients/new', 'AccountController@store')->name('new-client-account'); 
    Route::post('/clients/edit/{account}', 'AccountController@update')->name('update-client-account');
    Route::get('/clients/show/{account}', 'AccountController@show')->name('show-client-account');
    Route::get('/clients/delete/{account}', 'AccountController@destroy')->name('delete-client-account');
    
    /*************************
    * Contacts               *
    *************************/
    Route::get('/contacts', 'ContactController@index')->name('client-contact');
    Route::post('/contacts/new', 'ContactController@store')->name('new-contact');
    Route::post('/contacts/new/{account}', 'ContactController@store')->name('new-contact-account');
    Route::post('/contacts/edit/{contact}', 'ContactController@update')->name('update-contact');

    Route::post('/contacts/edit/{contact}/{account}', 'ContactController@update')->name('update-contact-account');

    Route::get('/contacts/show/{contact}', 'ContactController@show')->name('show-contact');
    Route::get('/contacts/delete/{contact}', 'ContactController@destroy')->name('delete-contact');

    Route::post('/contact-account/change-pass/{contact}', 'ContactController@updatePassword')->name('contact-change-pass');
});
