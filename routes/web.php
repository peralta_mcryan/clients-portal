<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group whichd
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/home', 'DashboardController@index')->name('home');
Route::get('/client-dashboard', 'ClientController@index')->name('client-dashboard');


Route::prefix('client')->group(function () {

	/*************************
	* Clients Login          *
	*************************/
	Route::get('/login', 'Auth\ClientLoginController@showLoginform')->name('client-login');
	Route::post('/login', 'Auth\ClientLoginController@login')->name('client-login');

	/*************************
	* Clients Password Reset *
	*************************/
	Route::post('/password/email', 'Auth\ContactForgotPasswordController@sendResetLinkEmail')->name('client-password-email');
	Route::get('/password/reset', 'Auth\ContactForgotPasswordController@showLinkRequestForm')->name('client-password-request');
	Route::post('/password/reset', 'Auth\ContactResetPasswordController@reset');
	Route::get('/password/reset/{token}', 'Auth\ContactResetPasswordController@showResetForm')->name('client-password-reset');

});



Route::prefix('technical')->group(function () {

	/*************************
	* Technical Team         *
	*************************/
	Route::get('/login', 'Auth\TechTeamLoginController@showLoginform')->name('techTeam-login');
	Route::post('/login', 'Auth\TechTeamLoginController@login')->name('techTeam-login');
	
	Route::get('/my-account/card', 'TechTeamProjectController@index_card')->name('techTeam-card-project');
	Route::post('/my-account/card','TechTeamProjectController@index_card')->name('techTeam-card-project');	

	Route::get('/my-account/projects/show/{project}', 'TechTeamProjectController@show')->name('techTeam-show-project');
	Route::get('/my-account/projects/download/{project}', 'TechTeamProjectController@download')->name('techTeam-download-project');
	Route::post('/my-account/projects/uploads/{project}', 'TechTeamProjectController@upload')->name('techTeam-upload-project-files');
	Route::get('/my-account','TechTeamProjectController@index')->name('tech-account.index');
	Route::post('/my-account','TechTeamProjectController@index')->name('tech-account.index');
	

	Route::get('/my-account/change-pass/{user}', 'TechTeamProjectController@changePassword')->name('techTeam-accounts-change-pass');
	Route::post('/my-account/change-pass/{user}', 'TechTeamProjectController@updatePassword')->name('techTeam-accounts-change-pass');


	Route::post('/password/email', 'Auth\TechTeamForgotPasswordController@sendResetLinkEmail')->name('techTeam-password-email');
	Route::get('/password/reset', 'Auth\TechTeamForgotPasswordController@showLinkRequestForm')->name('techTeam-password-request');
	Route::post('/password/reset', 'Auth\TechTeamResetPasswordController@reset');
	Route::get('/password/reset/{token}', 'Auth\TechTeamResetPasswordController@showResetForm')->name('techTeam-password-reset');

	Route::get('/my-account/change-pass/{user}', 'TechTeamController@changePassword')->name('techTeam-accounts-change-pass');
	Route::post('/my-account/change-pass/{user}', 'TechTeamController@updatePassword')->name('techTeam-accounts-change-pass');

	Route::get('/my-account/tabular', 'TechTeamProjectController@index_tabular')->name('techTeam-project-tabular');
	Route::post('/my-account/tabular', 'TechTeamProjectController@index_tabular')->name('techTeam-project-tabular');


	Route::get('/tickets','IssueListController@index')->name('tickets.index');
	Route::post('/tickets','IssueListController@index')->name('tickets.index');

	Route::get('/tickets/create','IssueListController@create')->name('tickets.create');
	Route::post('/tickets/store','IssueListController@store')->name('tickets.store');

	Route::get('/tickets/show/{issueList}','IssueListController@show')->name('tickets.show');
	Route::get('/tickets/{issueList}/edit','IssueListController@edit')->name('tickets.edit');
	Route::post('/tickets/{issueList}','IssueListController@update')->name('tickets.update');
	Route::get('/tickets/delete/{issueList}','IssueListController@destroy')->name('tickets.delete');
	
	Route::get('issue-thread/{issueList}','IssueThreadController@show')->name('issue-thread.show');
	Route::post('issue-thread/{issueList}','IssueThreadController@store')->name('issue-thread.store');	
	
});

Route::post('/alert-status/{issueList}','SearchController@email_alert')->name('update-alert-status');
Route::post('/tickets/{issueList}/issue-thread/store','IssueListController@postTopic')->name('admin-issue-thread.store');
Route::post('/tickets/{issueList}','IssueListController@update')->name('tickets.update');
//Admin tickets 
Route::get('/tickets/show/{issueList}','AdminTicketController@show')->name('admin-tickets.show');
Route::get('/tickets/{issueList}/edit','AdminTicketController@edit')->name('admin-tickets.edit');
Route::post('/tickets/{issueList}','AdminTicketController@update')->name('admin-tickets.update');
Route::get('/tickets/delete/{issueList}','AdminTicketController@destroy')->name('admin-tickets.delete');

Route::get('issue-thread','IssueThreadController@index')->name('issue-thread.index');
Route::get('issue-thread','IssueThreadController@index')->name('issue-thread.index');
Route::post('issue-thread/reply/{issueThread}','IssueThreadController@sendReply')->name('issue-thread.sendReply');
// Route::resource('issue-thread','IssueThreadController');


Route::prefix('ae')->group(function () {

	/*************************
	* Ae Account             *
	*************************/
	Route::get('/login', 'Auth\AccountExecutiveLoginController@showLoginform')->name('ae-login');
	Route::post('/login', 'Auth\AccountExecutiveLoginController@login')->name('ae-login');
	
	Route::get('/my-account/card', 'AeProjectController@index_card')->name('ae-card-project');
	Route::post('/my-account/card','AeProjectController@index_card')->name('ae-card-project');	

	Route::get('/my-account/projects/show/{project}', 'AeProjectController@show')->name('ae-show-project');
	Route::get('/my-account/projects/download/{project}', 'AeProjectController@download')->name('ae-download-project');
	Route::post('/my-account/projects/uploads/{project}', 'AeProjectController@upload')->name('ae-upload-project-files');
	Route::get('/my-account','AeProjectController@index')->name('my-account.index');
	Route::post('/my-account','AeProjectController@index')->name('my-account.index');
	

	Route::get('/my-account/change-pass/{user}', 'AeController@changePassword')->name('ae-accounts-change-pass');
	Route::post('/my-account/change-pass/{user}', 'AeController@updatePassword')->name('ae-accounts-change-pass');

	Route::get('/my-account/ticket/{issueList}','AeTicketController@show')->name('ae-ticket-show');
	Route::post('/my-account/ticket/forum/{issueList}','AeTicketController@postTopic')->name('ae-ticket-post-topic');
	Route::post('/my-account/ticket/reply/{issueThread}','AeTicketController@sendReply')->name('ae-ticket-post-reply');
});
	/*************************
	* AE Password Reset *
	*************************/
	Route::post('/ae/password/email', 'Auth\AeForgotPasswordController@sendResetLinkEmail')->name('ae-password-email');
	Route::get('/ae/password/reset', 'Auth\AeForgotPasswordController@showLinkRequestForm')->name('ae-password-request');
	Route::post('/ae/password/reset', 'Auth\AeResetPasswordController@reset');
	Route::get('/ae/password/reset/{token}', 'Auth\AeResetPasswordController@showResetForm')->name('ae-password-reset');


/*************************
* Designation         	 *
*************************/
Route::post('designations/{designation}', 'DesignationController@update');
Route::get('designations/delete/{designation}', 'DesignationController@destroy');
Route::resource('designations', 'DesignationController');


/*************************
* Clients Logout         *
*************************/
Route::get('/client-logout', 'Auth\ClientLoginController@logout')->name('client-logout');
Route::get('/admin-logout', 'Auth\LoginController@userLogout')->name('admin-logout');
Route::get('/ae-logout', 'Auth\AccountExecutiveLoginController@userLogout')->name('ae-logout');
Route::get('/technical-logout', 'Auth\TechTeamLoginController@userLogout')->name('techTeam-logout');
Route::post('/ae-logout', 'Auth\AccountExecutiveLoginController@logout')->name('ae-logout');
Route::post('/client-logout', 'Auth\ClientLoginController@logout')->name('client-logout');
Route::post('/admin-logout', 'Auth\LoginController@userLogout')->name('admin-logout');
Route::post('/technical-logout', 'Auth\TechTeamLoginController@logout')->name('techTeam-logout');

/*************************
* Clients                *
*************************/
Route::get('/clients', 'AccountController@index')->name('client-account');
Route::get('/clients/new', 'AccountController@create')->name('new-client-account');
Route::post('/clients/new', 'AccountController@store')->name('new-client-account');
Route::get('/clients/edit/{account}', 'AccountController@edit')->name('update-client-account');
Route::post('/clients/edit/{account}', 'AccountController@update')->name('update-client-account');
Route::get('/clients/show/{account}', 'AccountController@show')->name('show-client-account');
Route::get('/clients/delete/{account}', 'AccountController@destroy')->name('delete-client-account');


/*************************
* Change Request         *
*************************/
Route::get('/change-requests','ChangeRequestController@index')->name('change-requests.index');
Route::post('/change-requests','ChangeRequestController@index')->name('change-requests.index');
Route::post('/change-requests/store','ChangeRequestController@store')->name('change-requests.store');
Route::get('/change-requests/create','ChangeRequestController@create')->name('change-requests.create');
Route::get('/change-requests/{changerequest}/edit','ChangeRequestController@edit')->name('change-requests.edit');
Route::post('/change-requests/{changerequest}','ChangeRequestController@update')->name('change-requests.update');
Route::get('/change-requests/{changerequest}','ChangeRequestController@show')->name('change-requests.show');
Route::get('/change-requests/delete/{changerequest}','ChangeRequestController@destroy')->name('change-requests.delete');

/*************************
* Projects                *
*************************/
Route::get('/projects', 'ProjectController@index')->name('client-project');
Route::get('/projects/tabular', 'ProjectController@index_tabular')->name('client-project-tabular');
Route::post('/projects/tabular', 'ProjectController@index_tabular')->name('client-project-tabular');

Route::get('/projects/mandays-to-date', 'ProjectController@timelineDate')->name('mandays-to-date');

Route::get('/projects/new', 'ProjectController@create')->name('new-project');
Route::post('/projects/new', 'ProjectController@store')->name('new-project');

Route::get('/projects/new/{account}', 'ProjectController@create')->name('new-project-account');
Route::post('/projects/new/{account}', 'ProjectController@store')->name('new-project-account');

Route::get('/projects/edit/{project}', 'ProjectController@edit')->name('update-project');
Route::post('/projects/edit/{project}', 'ProjectController@update')->name('update-project');
Route::get('/projects/show/{project}', 'ProjectController@show')->name('show-project');
Route::get('/projects/delete/{project}', 'ProjectController@destroy')->name('delete-project');

Route::post('/projects/uploads/{project}', 'ProjectController@upload')->name('upload-project-files');
Route::get('/projects/download/{projectid}/{filefolder}/{filename}', function($projectid, $filefolder, $filename) {
	return response()->download(storage_path('app/file_projects/'.$projectid.'/'.$filefolder.'/'.$filename));
})->name('download-project-files');

Route::get('/projects/download/{projectid}', function($projectid) {

	// shell_exec( $cmd,$output);
	$zipName = 'files.zip';
	$rootPath = storage_path('app/file_projects/'.$projectid);

	if (file_exists(storage_path('app/file_projects/'.$projectid.'/'.$zipName))) {
		unlink(storage_path('app/file_projects/'.$projectid.'/'.$zipName));
	}

	$zip = new ZipArchive();
	$zip->open($rootPath.'/'.$zipName, ZipArchive::CREATE | ZipArchive::OVERWRITE);

	// Create recursive directory iterator
	/** @var SplFileInfo[] $files */
	$files = new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator($rootPath),
		RecursiveIteratorIterator::LEAVES_ONLY
	);
	$cnt= 0;
	foreach ($files as $name => $file)
	{
		// Skip directories (they would be added automatically)
		if (!$file->isDir())
		{
			// Get real and relative path for current file
			$filePath = $file->getRealPath();
			$relativePath = substr($filePath, strlen($rootPath) + 1);
			if ( $cnt == 10 ) {
				$cnt = 0;
				set_time_limit ( 30 ); //if it doesnt work try to use ini_set('max_execution_time', 30);
			}
			// Add current file to archive
			$zip->addFile($filePath, $relativePath);
		}
		$cnt++;
	}

	// Zip archive will be created only after closing object
	$zip->close();

	return response()->download(storage_path('app/file_projects/'.$projectid.'/'.$zipName));
})->name('download-project-files-all');

Route::get('/projects/category/download/{projectid}/{categoryid}', function($projectid, $categoryid) {

	$zipName = 'filesCategory'.$categoryid.'.zip';
	$rootPath = storage_path('app/file_projects/'.$projectid.'/'.$categoryid);

	if (file_exists(storage_path('app/file_projects/'.$projectid.'/'.$categoryid.'/'.$zipName))) {
		unlink(storage_path('app/file_projects/'.$projectid.'/'.$categoryid.'/'.$zipName));
	}

	$zip = new ZipArchive();
	$zip->open($rootPath.'/'.$zipName, ZipArchive::CREATE | ZipArchive::OVERWRITE);

	// Create recursive directory iterator
	/** @var SplFileInfo[] $files */
	$files = new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator($rootPath),
		RecursiveIteratorIterator::LEAVES_ONLY
	);

	foreach ($files as $name => $file)
	{
		// Skip directories (they would be added automatically)
		if (!$file->isDir())
		{
			// Get real and relative path for current file
			$filePath = $file->getRealPath();
			$relativePath = substr($filePath, strlen($rootPath) + 1);

			// Add current file to archive
			$zip->addFile($filePath, $relativePath);
		}
	}

	// Zip archive will be created only after closing object
	$zip->close();

	return response()->download(storage_path('app/file_projects/'.$projectid.'/'.$categoryid.'/'.$zipName));
})->name('download-project-files-category');

Route::get('/projects/delete/{projectid}/{filefolder}/{filename}', 'ProjectController@deleteFile')->name('delete-project-files');
Route::get('/projects/client/delete/{projectid}/{filefolder}/{filename}', 'MyProjectController@deleteFile')->name('client-delete-project-files');
Route::get('/projects/ae/delete/{projectid}/{filefolder}/{filename}', 'AeProjectController@deleteFile')->name('ae-delete-project-files');


/*************************
* Contacts               *
*************************/
Route::get('/contacts', 'ContactController@index')->name('client-contact');
Route::get('/contacts/tabular', 'ContactController@index_tabular')->name('client-contact-tabular');
Route::get('/contacts/new', 'ContactController@create')->name('new-contact');
Route::post('/contacts/new', 'ContactController@store')->name('new-contact');
Route::get('/contacts/new/{account}', 'ContactController@create')->name('new-contact-account');
Route::post('/contacts/new/{account}', 'ContactController@store')->name('new-contact-account');
Route::get('/contacts/edit/{contact}', 'ContactController@edit')->name('update-contact');
Route::post('/contacts/edit/{contact}', 'ContactController@update')->name('update-contact');

Route::get('/contacts/edit/{contact}/{account}', 'ContactController@edit')->name('update-contact-account');
Route::post('/contacts/edit/{contact}/{account}', 'ContactController@update')->name('update-contact-account');

Route::get('/contacts/show/{contact}', 'ContactController@show')->name('show-contact');
Route::get('/contacts/delete/{contact}', 'ContactController@destroy')->name('delete-contact');

Route::get('/contact-account/change-pass/{contact}', 'ContactController@changePassword')->name('contact-change-pass');
Route::post('/contact-account/change-pass/{contact}', 'ContactController@updatePassword')->name('contact-change-pass');

/*************************
* Administrator Accounts *
*************************/
Route::get('/administrator-accounts/show/{user}', 'UserController@show')->name('administrator-accounts-show');
Route::get('/administrator-accounts', 'UserController@index')->name('administrator-accounts-index');
Route::get('/administrator-account/new', 'UserController@create')->name('administrator-accounts-create');
Route::post('/administrator-account/new', 'UserController@store')->name('administrator-accounts-create');
Route::get('/administrator-accounts/edit/{user}', 'UserController@edit')->name('administrator-accounts-edit');
Route::post('/administrator-accounts/edit/{user}', 'UserController@update')->name('administrator-accounts-edit');
Route::get('/administrator-account/change-pass/{user}', 'UserController@changePassword')->name('administrator-accounts-change-pass');
Route::post('/administrator-account/change-pass/{user}', 'UserController@updatePassword')->name('administrator-accounts-change-pass');
Route::get('/administrator-account/delete/{user}', 'UserController@destroy')->name('administrator-accounts-delete');

Route::get('/information/create/ajax-country-province/{country}', 'CountryController@ajaxCountryProvince')->name('ajax-country-province');
Route::get('/information/create/ajax-province-city/{province}', 'ProvinceController@ajaxProvinceCity')->name('ajax-province-city');

/*************************
* Site Contents *
*************************/
Route::get('/acceptable-user-policy/{siteContents}', 'SiteContentsController@edit')->name('site-contents-edit-user-policy');
Route::post('/acceptable-user-policy/{siteContents}', 'SiteContentsController@update')->name('site-contents-edit-user-policy');

Route::get('/terms-and-conditions/{siteContents}', 'SiteContentsController@edit')->name('site-contents-edit-terms-and-conditions');
Route::post('/terms-and-conditions/{siteContents}', 'SiteContentsController@update')->name('site-contents-edit-terms-and-conditions');

Route::get('/service-contract/{siteContents}', 'SiteContentsController@edit')->name('site-contents-edit-service-contract');
Route::post('/service-contract/{siteContents}', 'SiteContentsController@update')->name('site-contents-edit-service-contract');

Route::get('/how-to-pay-bills/{siteContents}', 'SiteContentsController@edit')->name('site-contents-edit-how-to-pay-bills');
Route::post('/how-to-pay-bills/{siteContents}', 'SiteContentsController@update')->name('site-contents-edit-how-to-pay-bills');

Route::get('/hosting-faq/{siteContents}', 'SiteContentsController@edit')->name('site-contents-edit-hosting-faq');
Route::post('/hosting-faq/{siteContents}', 'SiteContentsController@update')->name('site-contents-edit-hosting-faq');

Route::get('/domain-faq/{siteContents}', 'SiteContentsController@edit')->name('site-contents-edit-domain-hosting-faq');
Route::post('/domain-faq/{siteContents}', 'SiteContentsController@update')->name('site-contents-edit-domain-hosting-faq');

Route::get('/email-hosting-faq/{siteContents}', 'SiteContentsController@edit')->name('site-contents-edit-email-hosting-faq');
Route::post('/email-hosting-faq/{siteContents}', 'SiteContentsController@update')->name('site-contents-edit-email-hosting-faq');

Route::get('/website-hosting-faq/{siteContents}', 'SiteContentsController@edit')->name('site-contents-edit-website-hosting-faq');
Route::post('/website-hosting-faq/{siteContents}', 'SiteContentsController@update')->name('site-contents-edit-website-hosting-faq');

Route::get('/database-hosting-faq/{siteContents}', 'SiteContentsController@edit')->name('site-contents-edit-database-hosting-faq');
Route::post('/database-hosting-faq/{siteContents}', 'SiteContentsController@update')->name('site-contents-edit-database-hosting-faq');

Route::get('/client-email-notification/{siteContents}', 'SiteContentsController@edit')->name('site-contents-edit-client-email-notification');
Route::post('/client-email-notification/{siteContents}', 'SiteContentsController@update')->name('site-contents-edit-client-email-notification');

/*************************
* Client Projects        *
*************************/
Route::get('/projects/client/{contact}', 'MyProjectController@index')->name('my-project');
Route::get('/projects/client/show/{project}', 'MyProjectController@show')->name('my-project-show');
Route::post('/projects/client/themes/{project}', 'MyProjectController@saveThemes')->name('my-project-themes');
Route::post('/projects/client/uploads/{project}', 'MyProjectController@upload')->name('client-upload-project-files');

/*************************
* Client  Site Contents  *
*************************/
Route::get('/acceptable-user-policy/client/{siteContents}', 'MySiteContentController@show')->name('client-site-contents-edit-user-policy');
Route::get('/terms-and-conditions/client/{siteContents}', 'MySiteContentController@show')->name('client-site-contents-edit-terms-and-conditions');
Route::get('/service-contract/client/{siteContents}', 'MySiteContentController@show')->name('client-site-contents-edit-service-contract');
Route::get('/how-to-pay-bills/client/{siteContents}', 'MySiteContentController@show')->name('client-site-contents-edit-how-to-pay-bills');
Route::get('/hosting-faq/client/{siteContents}', 'MySiteContentController@show')->name('client-site-contents-edit-hosting-faq');

Route::get('/domain-hosting-faq/client/{siteContents}', 'MySiteContentController@show')->name('client-site-contents-edit-domain-hosting-faq');
Route::get('/email-hosting-faq/client/{siteContents}', 'MySiteContentController@show')->name('client-site-contents-edit-email-hosting-faq');
Route::get('/website-hosting-faq/client/{siteContents}', 'MySiteContentController@show')->name('client-site-contents-edit-website-hosting-faq');
Route::get('/database-hosting-faq/client/{siteContents}', 'MySiteContentController@show')->name('client-site-contents-edit-database-hosting-faq');

/*************************
* Account Information    *
*************************/
Route::get('/account-infromation/{contact}', 'AccountInformationController@index')->name('account-information');

/*************************
* Client Change Password *
*************************/
Route::get('/account-information/client/change-pass/{contact}', 'AccountInformationController@changePassword')->name('client-contact-change-pass');
Route::post('/account-information/client/change-pass/{contact}', 'AccountInformationController@updatePassword')->name('client-contact-change-pass');

/**********************
* View Files          *
**********************/
Route::get('/viewFiles/{project}/{fileFolder}', 'ProjectController@viewFiles')->name('view-files');
Route::get('/viewFiles/client/{project}/{fileFolder}', 'MyProjectController@viewFiles')->name('client-view-files');
Route::get('/viewFiles/ae/{project}/{fileFolder}', 'AeProjectController@viewFiles')->name('ae-view-files');
Route::get('/official-receipts/{contact}', 'MyProjectController@viewReceipts')->name('official-receipts');
Route::get('/reports/{contact}', 'MyProjectController@viewReports')->name('reports');

/*************************
* Services               *
*************************/
Route::get('/services', 'ServiceController@index')->name('service');
Route::get('/services/new', 'ServiceController@create')->name('new-service');
Route::post('/services/new', 'ServiceController@store')->name('new-service');
Route::get('/services/edit/{service}', 'ServiceController@edit')->name('update-service');
Route::post('/services/edit/{service}', 'ServiceController@update')->name('update-service');
Route::get('/services/delete/{service}', 'ServiceController@destroy')->name('delete-service');

/*************************
* Admin Service Types    *
*************************/
Route::resource('service-types', 'ServiceTypeController');
Route::get('/service-types/delete/{serviceType}', 'ServiceTypeController@destroy')->name('delete-service-type');
Route::post('/service-types/{serviceType}/edit', 'ServiceTypeController@update')->name('update-service-type');

/*************************
* Admin Types            *
*************************/

Route::get('/admin-types/delete/{adminType}', 'AdminTypeController@destroy')->name('delete-admin-type');
Route::post('/admin-types/{adminType}/edit', 'AdminTypeController@update')->name('update-admin-type');
Route::resource('admin-types', 'AdminTypeController');
/*************************
* Search    *
*************************/
Route::get('/search', 'SearchController@index')->name('search');


/*************************
* Project Schedules      *
*************************/
Route::get('project-timeline/{project}', 'ProjectScheduleController@index')->name('project-timeline.index');
Route::get('project-timeline/{project}/create', 'ProjectScheduleController@create')->name('project-timeline.create');
Route::post('project-timeline/{project}/create', 'ProjectScheduleController@store')->name('project-timeline.update');
Route::get('project-timeline/{projectSchedule}/edit', 'ProjectScheduleController@edit')->name('project-timeline.edit');
Route::post('project-timeline/{projectSchedule}/edit', 'ProjectScheduleController@update')->name('project-timeline.update');
Route::get('project-timeline/{projectSchedule}/destroy', 'ProjectScheduleController@destroy')->name('project-timeline.destroy');


/*************************
 * Website Themes         *
*************************/
Route::get('/themes/delete/{websiteTheme}', 'WebsiteThemesController@destroy')->name('delete-themes');
Route::post('/themes/update/{websiteTheme}', 'WebsiteThemesController@update')->name('update-themes');
Route::get('/themes/{websiteTheme}/edit', 'WebsiteThemesController@edit')->name('edit-themes');
Route::resource('/themes', 'WebsiteThemesController');


/*************************
* Departments	         *
*************************/
Route::get('/departments/delete/{department}', 'DepartmentsController@destroy')->name('delete-department');
Route::post('/departments/{department}', 'DepartmentsController@update')->name('departments.update');
Route::resource('/departments','DepartmentsController');
