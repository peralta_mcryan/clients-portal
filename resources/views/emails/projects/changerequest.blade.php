@component('mail::message')

{!! $emailBody !!}

{!!$task!!}
<br>

<Strong>Man Hours:&nbsp;{!! $man_hours !!} hours</Strong><br>
<Strong>Price:&nbsp;{!! $price !!}</Strong><br><br>
Thanks,<br>
{{ config('app.name') }}
@endcomponent
