@component('mail::message')

{!! $emailBody !!}
@if(isset($selectedThemes))
{!! $selectedThemes !!}
@endif
{!!$remarks!!}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
