@component('mail::message')

{!! $emailBody !!}

{!!$issueDetails!!}
Thanks,<br>
{{ config('app.name') }}
@endcomponent