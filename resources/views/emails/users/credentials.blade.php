@component('mail::message')

<p>Hi Ms. / Mr. {{ $userName }}</p>

<p><br />
Greetings from iManila!</p>

<p>Your iManila Client Portal Credentials:</p>

<p><strong>URL:</strong>&nbsp;{!! $userLoginUrl !!}</p>
<p><strong>Username:</strong> {{ $userUsername }}</p>
<p><strong>Password:</strong>&nbsp;{{ $userPassword }}</p>

<p>&nbsp;</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
