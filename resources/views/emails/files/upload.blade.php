@component('mail::message')

{!! $emailBody !!}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
