@extends('layouts.adminlayout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Services</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
            <p class="text-right">
                <a href="{{ route('new-service') }}"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
            </p>
            <div class="dataTable_wrapper">
                <?php 
                    $ctr = 1;
                ?>
                <div class="row">
                    @foreach ($services as $service)
                        <div class="col-lg-4 col-md-6">
                            <div class="panel panel-primary">
                                <a href="#">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div><strong>{{ $service->name }}</strong></div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="panel-footer">
                                    <small>
                                        <a href="{{ route('update-service', $service->id) }}">
                                            <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                            <span class="pull-left">&nbsp;Edit Details</span>
                                        </a>
                                        <a class="pull-right ">
                                            <span class="pull-right imanila-color" style="cursor:pointer;"><i class="fa fa-close" onclick="javascript:deleteService({{ $service->id }})"></i></span>
                                        </a>
                                    </small>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    @if($ctr % 3 == 0)
                        </div>
                        <div class="row">
                    @endif
                    <?php $ctr++; ?>
                @endforeach
            </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12 text-center">
        {{ $services->links() }}
    </div>
</div>
<script src="{{ asset('js/services/services.js') }}"></script>
@endsection