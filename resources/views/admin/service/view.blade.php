@extends('layouts.adminlayout')

@section('content')
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Project</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-3">
                            <p class="text-right">
                                <a href="{{ route('new-project') }}"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                            </p>
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($projects as $projectDetails)
                                            <tr class="clickable-row" data-href="{{ route('show-project', $projectDetails->id) }}">
                                                <td>
                                                    {{ $projectDetails->name }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="panel tabbed-panel panel-primary">
                            <div class="panel-heading clearfix">
                                <div class="panel-title pull-left">
                                    Project Details
                                </div>
                                <div class="pull-right">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Overview</a></li>
                                        <li class=""><a href="#tab-primary-2" data-toggle="tab" aria-expanded="false">Files</a></li>
                                        <li class="dropdown">
                                            <a href="#" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="#tab-primary-3" data-toggle="tab" aria-expanded="false"><i class="fa fa-file-text"></i> Inclusions</a>
                                                </li>
                                                <li>
                                                    <a href="#tab-primary-4" data-toggle="tab" aria-expanded="false"><i class="fa fa-book"></i> Requirements</a>
                                                </li>
                                                <li>
                                                    <a href="{{ route('update-project', $project->id) }}"><i class="fa fa-edit"></i> Edit</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:deleteProject({{ $project->id }})"><i class="fa fa-close"></i> Delete</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab-primary-1">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h3>{{ $project->name }} <a href="{{ route('update-project', $project->id) }}" class="text-right"><i class="fa fa-edit imanila-color"></i> </a></h3>
                                                <hr></hr>
                                                {{ $project->description }}
                                                <h4>Project Owner</h4>
                                                @if (empty($project->accounts->name)) 
                                                    N/A
                                                @else
                                                    {{ $project->accounts->name }}
                                                @endif
                                                <h4>Project Status</h4>
                                                {{ $projectStatus }}
                                                <h4>Project Manager</h4>
                                                @if (empty($project->users->name)) 
                                                    N/A
                                                @else
                                                    {{ $project->users->name }}
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-primary-2">
                                        <form action="{{ route('upload-project-files', $project->id) }}" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-4">
                                                        {{ Form::select('file_folder', $fileFolders, '',['id' => 'file_folder',  'class'=>'']) }}
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <input class="pull-left" type="file" name="file_projects[]" id="file_projects" multiple>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <button type="submit" class="btn btn-primary pull-right">Upload</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <hr></hr>
                                                    @if(count($fileFolders) > 0)
                                                        @foreach($fileFolders as $key => $name)
                                                            <div class="col-lg-6">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <div class="row">
                                                                            <div class="col-xs-12" style="padding-bottom: 5px;">
                                                                                <div style="word-wrap: break-word;" class="imanila-color"><strong>{{ $name }}</strong></div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <a href="#">
                                                                        <div class="panel-footer">
                                                                            <a href="{{ route('view-files', [$project->id, $key]) }}">
                                                                                <span class="pull-left"><small>View Files</small></span>
                                                                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                                                <div class="clearfix"></div>
                                                                            </a>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <div class="alert alert-info">
                                                            <i class="fa fa-hand-o-right"></i>
                                                            <small>No file available.</small>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="tab-primary-3">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                {!! $project->inclusions !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-primary-4">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                {!! $project->requirements !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>
                <script src="{{ asset('js/projects/projects.js') }}"></script>
                <script>
                    $(document).ready(function () {
                        var url = window.location.href;
                        var activeTab = url.substring(url.indexOf("#") + 1);
                        if (url.indexOf("#") >= 0) {
                            $(".tab-pane").removeClass("active in");
                            $('a[href="#'+ activeTab +'"]').tab('show')
                        }
                    });
                </script>
@endsection