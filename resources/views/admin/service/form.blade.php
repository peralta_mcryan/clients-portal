@extends('layouts.adminlayout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">{{ $pageName }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Service Details
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form" method="POST" action="{{ $route }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name"><span class="asterisk-marker">*</span> Service Name</label>
                                    <input id="name" name="name" class="form-control" placeholder="Project Name" value="{{ $service->name }}" required>
                                    @if ($errors->has('name'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="project_owner">Service Type</label>
                                    <br>
                                    {{ Form::select('service_type', $serviceTypes, $service->service_type_id,['id' => 'service_type',  'class'=>'']) }}
                                    @if ($errors->has('service_type'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('service_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="account">Email Template</label>
                                    <textarea name="email_template" id="email_template">{{ $service->email_template }}</textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="<?= URL::previous() ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/projects/projects.js') }}"></script>

    <script>
        $(document).ready(function () {
            CKEDITOR.replace( 'email_template' );
        });
    </script>
@endsection
