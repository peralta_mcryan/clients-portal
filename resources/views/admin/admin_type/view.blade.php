@extends('layouts.adminlayout')

@section('content')
<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Administrator Types</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-3">
                            <p class="text-right">
                                <a href="{{ route('admin-types.create') }}"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                            </p>
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($adminTypes as $adminTypeList)
                                            <tr class="clickable-row" data-href="{{ route('admin-types.show', $adminTypeList->id) }}">
                                                <td>
                                                    {{ $adminTypeList->name }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $adminTypes->links() }}
                            </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Service Type Details
                             </div>
                            <div class="panel-body">
                                <p class="text-right">
                                    <a href="{{ route('admin-types.edit', $adminType->id) }}">
                                        <i class="fa fa-edit"> Edit</i></a>
                                        | <a href="javascript:deleteServiceType({{ $adminType->id }})">
                                        <i class="fa fa-close"> Delete</i></a>
                                </p>
                                <p class="lead">
                                    {{ $adminType->name }}
                                </p>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>

                <script src="{{ asset('js/service_type.js') }}"></script>
@endsection