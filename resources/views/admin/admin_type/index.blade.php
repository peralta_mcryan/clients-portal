@extends('layouts.adminlayout')

@section('content')
<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Administrator Roles</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                            <p class="text-right">
                                <a href="{{ route('admin-types.create') }}"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                            </p>
                            <div class="dataTable_wrapper">
                                <?php 
                                    $ctr = 1;
                                ?>
                                <div class="row">
                                    @foreach ($adminTypes as $adminType)
                                        <div class="col-lg-4 col-md-6">
                                            <div class="panel panel-primary">
                                                <a href="{{ route('admin-types.show', $adminType->id) }}">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div><strong>{{ $adminType->name }}</strong></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="panel-footer">
                                                    <small>
                                                        <a href="{{ route('admin-types.edit', $adminType->id) }}">
                                                            <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <span class="pull-left">&nbsp;Edit Details</span>
                                                        </a>
                                                        <a class="pull-right ">
                                                            <span class="pull-right imanila-color" style="cursor:pointer;"><i class="fa fa-close" onclick="javascript:deleteServiceType({{ $adminType->id }})"></i></span>
                                                        </a>
                                                    </small>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    @if($ctr % 3 == 0)
                                        </div>
                                        <div class="row">
                                    @endif
                                    <?php $ctr++; ?>
                                @endforeach
                                <div class="col-lg-12 text-center">
                                    {{ $adminTypes->links() }}
                                </div>
                            </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <script src="{{ asset('js/admin-types.js') }}"></script>
@endsection