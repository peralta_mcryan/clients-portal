@extends('layouts.adminlayout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">{{ $pageName }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Department Details
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" method="POST" action="{{ $route }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name"><span class="asterisk-marker">*</span> Department Name</label>
                                    <input id="name" name="name" placeholder = "Department Name" class="form-control" value="{{old('name') ? old('name') :(!empty($department->name)?$department->name:'') }}" required>
                                    @if ($errors->has('name'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="email"><span class="asterisk-marker">*</span> Department Email</label>
                                    <input id="email" name="email" placeholder = "Email" class="form-control" value="{{old('email') ? old('email') :(!empty($department->email)?$department->email:'') }}" required>
                                    @if ($errors->has('email'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="<?= route('client-project-tabular') ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/projects/projects.js') }}"></script>

    <script>
        $(document).ready(function () {
            CKEDITOR.replace( 'inclusions' );
            CKEDITOR.replace( 'requirements' );
        });
    </script>
@endsection



