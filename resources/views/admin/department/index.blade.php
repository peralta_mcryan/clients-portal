@extends('layouts.adminlayout')

@section('content')
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Departments</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                            <div class="col-lg-6">
                              
                            </div>
                            <div class="col-lg-6">
                                <p class="text-right">
                                    <a href="{{ route('departments.create') }}"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                                </p>
                            </div>
                            <div class="dataTable_wrapper">
                                <?php 
                                    $ctr = 1;
                                ?>
                                <div class="row">
                                    @foreach ($departments as $department)
                                        <div class="col-lg-4 col-md-6">
                                            <div class="panel panel-primary">
                                                <a href="{{ route('show-project', $department->id) }}">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div><strong>{{ $department->name }}</strong></div>
                                                                <div><small>Email: {{ $department->email }}</small></div>
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="panel-footer">
                                                    <small>
                                                        <a href="{{ route('departments.edit', $department->id) }}">
                                                            <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <span class="pull-left">&nbsp;Edit Details</span>
                                                        </a>
                                                    </small>
                                                   
                                                    <br>
                                                    <small>
                                                        <a href="#" onclick="javascript:deleteDepartment({{ $department->id }})">
                                                            <span class="pull-left"><i class="fa fa-close imanila-color"></i></span>
                                                            <span class="pull-left">&nbsp;Delete</span>
                                                        </a>
                                                    </small>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    @if($ctr % 3 == 0)
                                        </div>
                                        <div class="row">
                                    @endif
                                    <?php $ctr++; ?>
                                @endforeach
                            </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        {{ $departments->links() }}
                    </div>
                </div>
                <script>
                    var backurl = "{{ route('departments.index') }}";
                </script>
                <script src="{{ asset('js/department.js') }}"></script>
@endsection