@extends('layouts.adminlayout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">{{ $pageName }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Project Details
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form" method="POST" action="{{ $route }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name"><span class="asterisk-marker">*</span> Project Name</label>
                                    <input id="name" name="name" class="form-control" placeholder="Project Name" value="{{ $project->name }}" required>
                                    @if ($errors->has('name'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="description"><span class="asterisk-marker">*</span> Project Description</label>
                                    <textarea class="form-control" id="description" name="description" required>{{ $project->description }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="project_owner">Project Owner</label>
                                    <br>
                                    @if($account->id)
                                        <input type="hidden" name="project_owner" value="{{ $account->id }}">
                                        {{ $account->name }}
                                    @else
                                        {{ Form::select('project_owner', $accounts, $project->account_id,['id' => 'project_owner',  'class'=>'']) }}
                                        @if ($errors->has('project_owner'))
                                            <span class="error-block">
                                                <strong>{{ $errors->first('project_owner') }}</strong>
                                            </span>
                                        @endif
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="project_status">Project Status</label>
                                    {{ Form::select('project_status', $projectStatus, $project->status,['id' => 'project_status',  'class'=>'']) }}
                                    @if ($errors->has('project_status'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('project_status') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="assigned_project_manager">Assigned Project Manager</label>
                                    <br>
                                    {{ Form::select('assigned_project_manager', $users, $project->user_id,['id' => 'assigned_project_manager',  'class'=>'']) }}
                                    @if ($errors->has('assigned_project_manager'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('assigned_project_manager') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="assigned_ae">Assigned AE</label>
                                    <br>
                                    {{ Form::select('assigned_ae', $ae, $project->assigned_ae,['id' => 'assigned_ae',  'class'=>'']) }}
                                    @if ($errors->has('assigned_ae'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('assigned_ae') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="assigned_project_manager">Service Type</label>
                                    <br>
                                    <select multiple="multiple" name="service_type[]" id="service_type">
                                        @foreach($serviceTypes as $key => $value)
                                            <option value="{{ $key }}" {{ ( in_array($key, $project->projectServiceType()->pluck('service_type_id')->toArray()) ? 'selected' : '') }}>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('service_type'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('service_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 col-md-3">
                                        <label for="account"><span class="asterisk-marker">*</span> Service Contract
                                            @if(!empty($serviceContract))
                                                <a href="{{ $serviceContract }}" class="imanila-color">{{ $project->service_contract }}</a>
                                            @endif
                                        </label>
                                        <input type="file" id="service_contract" name="service_contract" placeholder="Service Contract">
                                         @if ($errors->has('service_contract'))
                                            <span class="error-block">
                                                <strong>{{ $errors->first('service_contract') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label for="project_start"><span class="asterisk-marker">*</span> Project Start</label>
                                        <input type="date" id="project_start" name="project_start" class="form-control" placeholder="Project Start" value="{{ $project->project_start == null ? Carbon\Carbon::now()->format('Y-m-d') : Carbon\Carbon::parse($project->project_start)->format('Y-m-d') }}" >
                                        @if ($errors->has('project_start'))
                                            <span class="error-block">
                                                <strong>{{ $errors->first('project_start') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label for="project_days"><span class="asterisk-marker">*</span> Project Man-Days</label>
                                        {{Form::select('project_days',array(10=>10, 45 => 45, 60 =>60),$project->project_days,['id'=>'project_days','placeholder'=>'Man days'])}}
                                        @if ($errors->has('project_days'))
                                            <span class="error-block">
                                                <strong>{{ $errors->first('project_days') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label for="project_end">Project End</label>
                                        <input type="date" id="project_end" name="project_end" class="form-control disable" placeholder="Project End" value="{{ trim($project->project_end) }}" readonly>
                                        @if ($errors->has('project_end'))
                                            <span class="error-block">
                                                <strong>{{ $errors->first('project_end') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                   
                                </div>
                                <div class="form-group">
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label for="inclusions">Inclusions</label>
                                    <textarea name="inclusions" id="inclusions">{{ $project->inclusions }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="requirements">Requirements</label>
                                    <textarea name="requirements" id="requirements">{{ $project->requirements }}</textarea>
                                </div>

                                @if ($formSettings['show_update_remarks_field']) 
                                    <div class="form-group">
                                        <label for="update_remarks"><span class="asterisk-marker">*</span> Update Remarks</label>
                                        <textarea name="update_remarks" id="update_remarks" class="form-control" required></textarea>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <input type="checkbox" name="send_welcome_email" id="send_welcome_email" value="1" {{ $formSettings['welcome_email_default'] == 1 ? 'checked' : '' }}> Check this box to send the welcome email to the contacts associated to this project. Otherwise, remove the check mark.
                                </div>
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="<?= route('client-project-tabular') ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/projects/projects.js') }}"></script>

    <script>
        $(document).ready(function () {
            CKEDITOR.replace( 'inclusions' );
            CKEDITOR.replace( 'requirements' );
        });
    </script>
@endsection
