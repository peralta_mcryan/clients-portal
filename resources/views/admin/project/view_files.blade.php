@extends('layouts.adminlayout')

@section('content')
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ $project->name }}</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel tabbed-panel panel-primary">
                            <div class="panel-heading clearfix">
                                <div class="panel-title pull-left">
                                    {{ $fileFolder->folder_name }}
                                </div>
                                <div class="pull-right">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Files</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab-primary-1">
                                        <form action="{{ route('upload-project-files', $project->id) }}" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <span class="pull-left"><h4>{{ $fileFolder->folder_name }} Files</h4></span>
                                                    <a href="{{ route('show-project',$project->id) }}#tab-primary-2" class="btn btn-primary pull-right">Back</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <hr></hr>
                                                    @if(count($files) > 0)
                                                        @foreach($files as $file)
                                                            <?php
                                                                $fileUploaderId = 0;
                                                            ?>
                                                            <div class="col-lg-6">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <div class="row">
                                                                            <div class="col-xs-12" style="padding-bottom: 5px;">
                                                                                <div style="word-wrap: break-word;" class="imanila-color"><strong>{{ $file->filename }}</strong></div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <a href="#">
                                                                        <div class="panel-footer">
                                                                            <a href="#" style="cursor:default">
                                                                                <span class="pull-left"><small>Uploaded By</small></span>
                                                                                <span class="pull-right">
                                                                                    <small>
                                                                                        @if($file->users)
                                                                                            {{ $file->users->name }}
                                                                                            <?php 
                                                                                                $fileUploaderId = $file->users->id;
                                                                                            ?>
                                                                                        @elseif($file->contacts)
                                                                                            {{ $file->contacts->first_name }} {{ $file->contacts->last_name }}
                                                                                            <?php 
                                                                                                $fileUploaderId = $file->contacts->id;
                                                                                            ?>
                                                                                        @endif
                                                                                    </small>
                                                                                </span>
                                                                                <div class="clearfix"></div>
                                                                            </a>
                                                                            <a href="#" style="cursor:default;">
                                                                                <span class="pull-left"><small>Upload Date</small></span>
                                                                                <span class="pull-right">
                                                                                    <small>
                                                                                        {{ $file->created_at }}
                                                                                    </small>
                                                                                </span>
                                                                                <div class="clearfix"></div>
                                                                            </a>
                                                                            <a href="{{ route('download-project-files', [$project->id, $fileFolder->id, $file->filename]) }}">
                                                                                <span class="pull-left"><small>Download</small></span>
                                                                                <span class="pull-right"><i class="fa fa-download"></i></span>
                                                                                <div class="clearfix"></div>
                                                                            </a>
                                                                            @if((Auth::user()->id == $fileUploaderId) && ($file->contact_id == 0))
                                                                            <a href="{{ route('delete-project-files', [$project->id, $fileFolder->id, $file->filename]) }}" onclick="deleteFiles(event)">
                                                                                <span class="pull-left"><small>Remove</small></span>
                                                                                <span class="pull-right"><i class="fa fa-close imanila-color"></i></span>
                                                                                <div class="clearfix"></div>
                                                                            </a>
                                                                            @endif
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <div class="alert alert-info">
                                                            <i class="fa fa-hand-o-right"></i>
                                                            <small>No file available.</small>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>
                <script src="{{ asset('js/projects/projects.js') }}"></script>
                <script>
                    function deleteFiles(e) {
                        if (confirm('Are you sure you want to delete this file?')) {
                            return true;
                        } else {
                            e.preventDefault();
                        }
                    }
                </script>
@endsection