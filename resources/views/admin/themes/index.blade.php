@extends('layouts.adminlayout')

@section('content')
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Themes</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                      
                            <form id = "frm_themes" role="form" method="POST" action="{{ $route }}" >
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label for="theme_group"><span class="asterisk-marker">*</span> Theme Group: &nbsp;</label>
                                        <p>
                                            {{ Form::select($drName, config('constants.theme_type'),$group,['id' => $drName,  'class'=>'']) }}
                                        </p>
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="website_name"><span class="asterisk-marker">*</span> Website Title: &nbsp;</label>
                                        <input type="text" id = "website_name" name = "website_name" class = "form-control" value = "{{$themeSite->name}}" required>
                                        @if ($errors->has('website_name'))
                                            <span class="error-block">
                                                <strong>{{ $errors->first('website_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-8">     
                                        <label for="website_name"><span class="asterisk-marker">*</span> Website Link: &nbsp;</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">http://</span>
                                            <input type="text" id = "website_link" name = "website_link" class = "form-control" value = "{{$themeSite->website_links}}" required >
                                        </div>
                                        @if ($errors->has('website_link'))
                                            <span class="error-block">
                                                <strong>{{ $errors->first('website_link') }}</strong>
                                            </span>
                                        @endif
                                    </div> 
                                    <div class="col-lg-4">           
                                        <div class="row">
                                            <div class="col-md-4">      
                                                <label for="">&nbsp;</label>                                           
                                                <input id = 'submitButton' type="submit" class="btn btn-success form-control" value="{{$buttonName}}"  >
                                            </div>
                                            <div class="col-md-4">      
                                                <label for="">&nbsp;</label>     
                                                <button type="button" class="btn btn-danger form-control" onclick="resetFields();">Reset Field </button>   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                         
                          <div class="col-lg-12">&nbsp;</div>
                     <hr>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Website Link</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($themes as $theme)
                                        <tr  data-href="">
                                            <td class="col-md-3">
                                                <a href="">
                                                    {{ $theme->name }}
                                                </a>
                                            </td>
                                            <td class="col-md-8">
                                                
                                                 {{ $theme->website_links }}
                                            </td>   
                                            <td class="col-md-1">
                                            <a target = "_blank" href="http://{{$theme->website_links}}">
                                                    <span class="pull-left"> &nbsp;<i class="fa fa-external-link" title = "Open Link"></i> &nbsp;</span>
                                                </a> &nbsp;
                                                <a href="javascript:void();" onclick="editSite({{$group}},{{$theme->id}})">
                                                    <span class="pull-left"> &nbsp;<i class="fa fa-edit" title="Edit Website"></i> &nbsp;</span>
                                                </a> &nbsp;
                                                <a href="#" onclick="javascript:deleteSite({{ $theme->id }})" title="Delete">
                                                    <span class="pull-left"> &nbsp;<i class="fa fa-close imanila-color"></i></span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        {{ $themes->links() }}
                    </div>
                </div>

<script src="{{ asset('js/themes/website-themes.js') }}"></script>
@endsection