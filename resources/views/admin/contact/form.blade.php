@extends('layouts.adminlayout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">{{ $pageName }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Contact Details
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form" method="POST" action="{{ $route }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="first_name"><span class="asterisk-marker">*</span> First Name</label>
                                    <input id="first_name" name="first_name" class="form-control" placeholder="First Name" value="{{ $contact->first_name }}" required>
                                    @if ($errors->has('first_name'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="last_name"><span class="asterisk-marker">*</span> Last Name</label>
                                    <input id="last_name" name="last_name" class="form-control" placeholder="Last Name" value="{{ $contact->last_name }}" required>
                                    @if ($errors->has('last_name'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="email_address"><span class="asterisk-marker">*</span> Email Address</label>
                                    <input id="email_address" name="email_address" class="form-control" placeholder="Email Address" value="{{ $contact->email }}" type="email" required>
                                    @if ($errors->has('email_address'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('email_address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="work_phone"><span class="asterisk-marker">*</span> Work Phone</label>
                                    <input id="work_phone" name="work_phone" class="form-control" placeholder="Work Phone" value="{{ $contact->work_phone }}" required>
                                    @if ($errors->has('work_phone'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('work_phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="mobile_phone"><span class="asterisk-marker">*</span> Mobile Phone</label>
                                    <input id="mobile_phone" name="mobile_phone" class="form-control" placeholder="Mobile Phone" value="{{ $contact->mobile_phone }}" required>
                                    @if ($errors->has('mobile_phone'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('mobile_phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="designation">Designation</label>
                                    <input id="designation" name="designation" class="form-control" placeholder="Designation" value="{{ $contact->designation }}">
                                    @if ($errors->has('designation'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('designation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="department">Department</label>
                                    <input id="department" name="department" class="form-control" placeholder="Department" value="{{ $contact->department }}">
                                    @if ($errors->has('department'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('department') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="account">Account</label>
                                    <br>
                                    @if($account->id)
                                        <input type="hidden" name="account" value="{{ $account->id }}">
                                        {{ $account->name }}
                                    @else
                                        {{ Form::select('account', $accounts, $contact->account_id,['id' => 'acccount',  'class'=>'']) }}
                                        @if ($errors->has('account'))
                                            <span class="error-block">
                                                <strong>{{ $errors->first('account') }}</strong>
                                            </span>
                                        @endif
                                    @endif
                                </div>
                                <div class="form-group" style="display:none;">
                                    <label for="portal_access">Portal Access</label>
                                    {{ Form::select('portal_access', $portalAccess, $contact->allow_access,['id' => 'portal_access',  'class'=>'']) }}
                                    @if ($errors->has('portal_access'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('portal_access') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="portal_access">Contact Type</label>
                                    {{ Form::select('contact_type', $contactType, $contact->contact_type,['id' => 'contact_type',  'class'=>'']) }}
                                    @if ($errors->has('contact_type'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('contact_type') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                @if ($settings['include_password'])
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password">Password</label>
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm">Confirm Password</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                                @endif

                                @if ($settings['show_email_checkbox'])
                                <div class="form-group">
                                    <input type="checkbox" name="send_welcome_email" id="send_welcome_email" value="1" @if($settings['show_email_default_value']) {{ "checked" }} @endif> Check this box to auto-generate a new password and send the welcome email. Otherwise, remove the check mark.
                                </div>
                                @endif
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="<?= route('client-contact') ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <script src="{{ asset('js/accounts/accounts.js') }}"></script>
@endsection
