@extends('layouts.adminlayout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Contacts</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
            <div class="col-lg-6">
                <p class="text-left">
                    <a href="{{ route('client-contact') }}"><i class="fa fa-columns fa-1x text-left"></i></a>
                    <a href="{{ route('client-contact-tabular') }}"><i class="fa fa-list fa-1x text-left"></i></a>
                </p>
            </div>
            <p class="text-right">
                <a href="{{ route('new-contact') }}"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
            </p>
            <div class="dataTable_wrapper">
                <?php 
                    $ctr = 1;
                ?>
                <div class="row">
                    @foreach ($contacts as $contact)
                        <div class="col-lg-4 col-md-6">
                            <div class="panel panel-primary">
                                <a href="{{ route('show-contact', $contact->id) }}">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div><strong>{{ $contact->first_name }} {{ $contact->last_name }}</strong></div>
                                                <br>
                                                <div>
                                                    <small>
                                                        <i class="fa fa-building"></i> 
                                                        @if (!empty($contact->account->name))
                                                            {{ $contact->account->name }}
                                                        @else
                                                            Not associated to any client
                                                        @endif
                                                    </small>
                                                </div>
                                                <div>
                                                    <small>
                                                        <i class="fa fa-phone"></i>
                                                        @if (!empty($contact->work_phone))
                                                            {{ $contact->work_phone }}
                                                        @else
                                                            Not Available
                                                        @endif
                                                    </small>
                                                </div>
                                                <div>
                                                    <small>
                                                        <i class="fa fa-mobile-phone"></i> 
                                                        @if (!empty($contact->mobile_phone))
                                                            {{ $contact->mobile_phone }}
                                                        @else
                                                            Not Available
                                                        @endif
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="panel-footer">
                                    <small>
                                        <a href="{{ route('update-contact', $contact->id) }}">
                                            <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                            <span class="pull-left">&nbsp;Edit Details</span>
                                        </a>
                                        <a class="pull-right ">
                                            <span class="pull-right imanila-color" style="cursor:pointer;"><i class="fa fa-close" onclick="javascript:deleteContact({{ $contact->id }})"></i></span>
                                        </a>
                                    </small>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    @if($ctr % 3 == 0)
                        </div>
                        <div class="row">
                    @endif
                    <?php $ctr++; ?>
                @endforeach
            </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12 text-center">
        {{ $contacts->links() }}
    </div>
</div>
<script src="{{ asset('js/contacts/contacts.js') }}"></script>
@endsection