@extends('layouts.adminlayout')

@section('content')
<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Contacts</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-3">
                            <p class="text-right">
                                <a href="{{ route('new-contact') }}"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                            </p>
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($contacts as $contactDetails)
                                            <tr class="clickable-row" data-href="{{ route('show-contact', $contactDetails->id) }}">
                                                <td>
                                                    {{ $contactDetails->first_name }} {{ $contactDetails->last_name }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center">
                                {{ $contacts->links() }}
                            </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="panel tabbed-panel panel-primary">
                            <div class="panel-heading clearfix">
                                <div class="panel-title pull-left">
                                    Project Details
                                </div>
                                <div class="pull-right">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Overview</a></li>
                                        <li class="dropdown">
                                            <a href="#" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="{{ route('contact-change-pass', $contact->id) }}"><i class="fa fa-key"></i> Change Password</a>
                                                </li>
                                                <li>
                                                    <a href="{{ route('update-contact', $contact->id) }}"><i class="fa fa-edit"></i> Edit</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:deleteContact({{ $contact->id }})"><i class="fa fa-close"></i> Delete</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab-primary-1">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h3>{{ $contact->first_name }} {{ $contact->last_name }} <a href="{{ route('update-contact', $contact->id) }}" class="text-right"><i class="fa fa-edit imanila-color"></i> </a></h3>
                                                <hr></hr>
                                                <div class="panel panel-green">
                                                    <div class="panel-heading" style="padding-bottom: 5px;">
                                                        Contact Numbers
                                                    </div>
                                                    <div class="panel-body">
                                                        <strong>Email Address</strong>
                                                        <div>
                                                            <small>
                                                                <i class="fa fa-address-card-o"></i> 
                                                                @if (!empty($contact->email))
                                                                    {{ $contact->email }}
                                                                @else
                                                                    Not Available
                                                                @endif
                                                            </small>
                                                        </div>
                                                        <br>
                                                        <strong>Work Phone</strong>
                                                        <div>
                                                            <small>
                                                                <i class="fa fa-phone"></i>
                                                                @if (!empty($contact->work_phone))
                                                                    {{ $contact->work_phone }}
                                                                @else
                                                                    Not Available
                                                                @endif
                                                            </small>
                                                        </div>
                                                        <br>
                                                        <strong>Mobile Phone</strong>
                                                        <div>
                                                            <small>
                                                                <i class="fa fa-mobile-phone"></i>
                                                                @if (!empty($contact->mobile_phone))
                                                                    {{ $contact->mobile_phone }}
                                                                @else
                                                                    Not Available
                                                                @endif
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>

                <script src="{{ asset('js/contacts/contacts.js') }}"></script>
@endsection