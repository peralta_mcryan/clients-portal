@extends('layouts.adminlayout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Contacts</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-6">
            <p class="text-left">
                <a href="{{ route('client-contact') }}"><i class="fa fa-columns fa-1x text-left"></i></a>
                <a href="{{ route('client-contact-tabular') }}"><i class="fa fa-list fa-1x text-left"></i></a>
            </p>
        </div>
            <p class="text-right">
                <a href="{{ route('new-contact') }}"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
            </p>
            <div class="dataTable_wrapper">
                <?php 
                    $ctr = 1;
                ?>
                <div class="row">
                    <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Contact Name</th>
                            <th>Department</th>
                            <th>Designation</th>
                            <th>Email</th>
                            <th>Work Phone</th>
                            <th>Mobile Phone</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($contacts as $contact)
                        <tr>
                            <td>{{$contact->first_name}} {{ $contact->last_name }} </td>
                            <td>{{!empty($contact->department)?$contact->department:"N/A"}}</td>
                            <td>{{!empty($contact->designation)?$contact->designation:"N/A"}}</td>
                            <td>{{$contact->email}}</td>
                            <td>{{!empty($contact->work_phone)?$contact->work_phone:"N/A"}}</td>
                            <td>{{!empty($contact->mobile_phone)?$contact->mobile_phone:"N/A"}}</td>
                            <td> 
                         
                                        <a href="{{ route('update-contact', $contact->id) }}">
                                            <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                            <span class="pull-left"></span>
                                        </a>
                                        <a class="pull-right ">
                                            <span class="pull-right imanila-color" style="cursor:pointer;"><i class="fa fa-close" onclick="javascript:deleteContact({{ $contact->id }})"></i></span>
                                        </a>
                                    
                           </td>
                           <td>&nbsp; &nbsp; &nbsp;</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>                
            </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12 text-center">
        {{ $contacts->links() }}
    </div>
</div>
<script src="{{ asset('js/contacts/contacts.js') }}"></script>
@endsection