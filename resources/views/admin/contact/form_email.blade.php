@extends('layouts.adminlayout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Welcome Email</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Email Template
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form" method="POST" action="{{ $route }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <textarea name="email_template" id="email_template"></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary" onclick="confirmSending(event)">Send</button>
                                <a href="<?= URL::previous() ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    <script>
        $(document).ready(function () {
            CKEDITOR.replace( 'email_template' );
        });

        function confirmSending(e) {
            if (confirm('Are you sure you want to send this email? Please double check the details.')) {
                return true;
            } else {
                e.preventDefault();
            }
        }
    </script>
@endsection
