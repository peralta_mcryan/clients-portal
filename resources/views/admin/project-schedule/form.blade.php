@extends('layouts.adminlayout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">{{ $pageName }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Schedule Details
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form" method="POST" action="{{ $route }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group col-md-4">
                                    <label for="order"><span class="asterisk-marker">*</span> Order</label>
                                    <input id="order" name="order" class="form-control" placeholder="Order" value="{{ $projectSchedule->phase_no }}" required>
                                    @if ($errors->has('order'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('order') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="phase_name"><span class="asterisk-marker">*</span> Phase Name</label>
                                    <input id="phase_name" name="phase_name" class="form-control" placeholder="Phase Name"  value="{{ $projectSchedule->phase_name }}" required>
                                    @if ($errors->has('phase_name'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('phase_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                {{--  <div class="form-group col-md-4">
                                    <label for="start_date"> Start Date</label>
                                    <input id="start_date" name="start_date" class="form-control" placeholder="Start Date" type="date">
                                    @if ($errors->has('start_date'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('start_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="end_date"> End Date</label>
                                    <input id="end_date" name="end_date" class="form-control" placeholder="End Date" type="date">
                                    @if ($errors->has('end_date'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('end_date') }}</strong>
                                        </span>
                                    @endif
                                </div>  --}}
                                <div class="form-group col-md-4">
                                    <label for="working_days"><span class="asterisk-marker">*</span> Working Days</label>
                                    <input id="working_days" name="working_days" class="form-control" placeholder="Working Days"  value="{{ $projectSchedule->working_days }}" required>
                                    @if ($errors->has('working_days'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('working_days') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="description"> Description</label>
                                    <textarea name="description" id="description">{{ $projectSchedule->description }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                    <a href="<?= route('client-project-tabular') ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                                </div>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <div class="col-lg-12">
            <hr>
        </div>

        @if($chartData != '[]')
            <div class="col-lg-11">
                <div id="chart_div"></div>
            </div>
        @else
            <div class="col-lg-12 text-center">
                No Project Timeline
                <hr>
            </div>
        @endif
        <?php 
            $ctr = 1;
        ?>
        <div class="row">
        <div class="col-lg-12">
        @foreach($projectScheduleList as $projectSchedule)
            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="pull-left"><strong>{{ $projectSchedule->phase_name }}</strong></span> 
                        <span class="pull-right"><strong>{{ $projectSchedule->working_days }} Days</strong></span>
                        &nbsp;
                        {{--  <hr style="margin-bottom: 5px; margin-top: 25px;">  --}}
                        {{--  <div class="text-center">{{ $projectSchedule->phase_name }}</div>   --}}
                    </div>
                    <div class="panel-body">
                        {!! $projectSchedule->description !!}
                    </div>
                    <div class="panel-footer">
                        <small>
                            <a href="{{ route('project-timeline.edit', [$projectSchedule->id]) }}">
                                <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                <span class="pull-left">&nbsp;Edit Details</span>
                            </a>
                            <a class="pull-right ">
                                <span class="pull-right imanila-color" style="cursor:pointer;"><i class="fa fa-close" onclick="javascript:deleteProjectSchedule({{ $project->id }}, {{ $projectSchedule->id }})"></i></span>
                            </a>
                        </small>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            @if($ctr % 3 == 0)
                </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
            @endif
            <?php $ctr++; ?>
        @endforeach
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script>
        $(document).ready(function () {
            CKEDITOR.replace( 'description' );
            @if($chartData != '[]')
                google.charts.load('visualization', '1', {packages: ['corechart']});
                google.charts.setOnLoadCallback(drawChart);            
            @endif
        });

        function drawChart() {
            var jsonChartData = {!! $chartData !!};
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Name');
            data.addColumn('number', 'spacer');
            data.addColumn('number', 'Working Days');
            data.addRows(jsonChartData);
            
            var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
            chart.draw(data, {
                isStacked: true,
                series: {
                    0: {
                        visibleInLegend: false,
                        color: 'transparent'
                    }
                },
                hAxis: {
                    minValue: 1
                }

            });
        }

        function setWorkingDays() {
            var startDate = $('#start_date').val();
            var endDate = $('#end_date').val();

            if ((startDate != '') && (endDate != '')) {
                // $()
            }
        }

        function deleteProjectSchedule(projectId, projectScheduleId) {
            if (confirm('Are you sure you want to delete this schedule?')) {
                $.ajax({
                    type:'GET',
                    url:'/project-timeline/' + projectScheduleId + '/destroy',
                    success:function(data){
                        window.location = '/project-timeline/'+projectId;
                    }
                });
            }
        }
    </script>
@endsection
