@extends('layouts.adminlayout')

@section('content')
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Ticket List</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="text-left">
                                    <a href="{{ route('client-project') }}"><i class="fa fa-columns fa-1x text-left"></i></a>
                                    <a href="{{ route('client-project-tabular') }}"><i class="fa fa-list fa-1x text-left"></i></a>
                                </p>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-right">
                                    <a href="{{ route('tickets.create') }}"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                                </p>
                            </div>
                        </div>
                         <form role="form" method="POST" action="{{ route('tickets.index') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-4">
                                    
                                    {{Form::select('project[]',$projects,old('project'),['di'=>'project','multiple'=>'multiple','placeholder'=>'All Projects'])}}
                                </div>

                                <div class="col-lg-2">
                                    
                                    {{Form::select('status[]',config('constants.ticket_status_def'),old('status'),['id'=>'status','multiple'=>'multiple','placeholder'=>'All Status'])}}
                                </div>

                                <div class="col-lg-2">
                                    
                                    {{Form::select('priority[]',config('constants.ticket_priority_def'),old('priority'),['id'=>'priority','multiple'=>'multiple','placeholder'=>'All Priority'])}}
                                </div>
                                <div class="col-lg-2">
                                      <input type="submit" class="btn btn-success btn-rectangle pull-left" value="Search">
                                </div>
                            </div>
                        </form>
                         <br>
                            <div class="dataTable_wrapper">
                                <?php 
                                    $ctr = 1;
                                ?>
                                <div class="row">
                                   
                                    @foreach ($issues as $issue)
                                        <div class="col-lg-4 col-md-6">
                                            <div class="panel panel-primary">
                                                <a href="{{ route('tickets.show', $issue->id) }}">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <div><strong>{{ $issue->control_no }}</strong></div>                     
                                                            </div>
                                                            @if ($issue->status == config('constants.ticket_status.active'))                                               
                                                                <span class="ticket-status ticket-status-active pull-right"><span class = "ticket-code">&nbsp;&nbsp;{{config('constants.ticket_status_def.'.$issue->status)}}&nbsp;&nbsp;</span></span>
                                                            @elseif ($issue->status == config('constants.ticket_status.onhold'))
                                                                <span class="ticket-status ticket-status-hold pull-right"><span class = "ticket-code">&nbsp;&nbsp;{{config('constants.ticket_status_def.'.$issue->status)}}&nbsp;&nbsp;</span></span>
                                                            @elseif ($issue->status == config('constants.ticket_status.cancelled'))
                                                                <span class="ticket-status ticket-status-cancelled pull-right"><span class = "ticket-code">&nbsp;&nbsp;{{config('constants.ticket_status_def.'.$issue->status)}}&nbsp;&nbsp;</span></span>
                                                            @endif
                                                              
                                                            &nbsp;
                                                            @if ($issue->priority == config('constants.ticket_priority.high'))                                               
                                                                <span class="ticket-priority ticket-priority-high pull-right"><span class = "ticket-code">&nbsp;&nbsp;{{config('constants.ticket_priority_def.'.$issue->priority)}}&nbsp;&nbsp;</span></span>
                                                            @elseif ($issue->priority == config('constants.ticket_priority.medium'))
                                                                <span class="ticket-priority ticket-priority-medium pull-right"><span class = "ticket-code">&nbsp;&nbsp;{{config('constants.ticket_priority_def.'.$issue->priority)}}&nbsp;&nbsp;</span></span>
                                                            @elseif ($issue->priority == config('constants.ticket_priority.low'))
                                                                <span class="ticket-priority ticket-priority-low pull-right"><span class = "ticket-code">&nbsp;&nbsp;{{config('constants.ticket_priority_def.'.$issue->priority)}}&nbsp;&nbsp;</span></span>
                                                            @endif
                                                            
                                                         
                                                            <div class="col-xs-12">
                                                               
                                                                <div><small>Project: {{$issue->project->name}}</small></div>
                                                                <div><small>Assigned to: {{$issue->department->name}}</small></div>
                                                                <div><small>Created Date: {{ \Carbon\Carbon::parse($issue->created_at)->format('M d, Y') }}</small></div>
                                                                <div><small>Aging:&nbsp;{{$issue->getAgingDays()}} days</small></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="panel-footer">
                                                    <small>
                                                        <a href="{{ route('tickets.edit', $issue->id) }}">
                                                            <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <span class="pull-left">&nbsp;Edit Details</span>
                                                        </a>
                                                    </small>    
                                                    <br>
                                                    <small>
                                                        <a href="javascript:void(0);" onclick="deleteTicket({{ $issue->id }});">
                                                            <span class="pull-left"><i class="fa fa-close imanila-color"></i></span>
                                                            <span class="pull-left">&nbsp;Delete</span>
                                                        </a>
                                                    </small>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    @if($ctr % 3 == 0)
                                        </div>
                                        <div class="row">
                                    @endif
                                    <?php $ctr++; ?>
                                @endforeach
                            </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        {{ $issues->links() }}
                    </div>
                </div>
                <script>
                    var backurl = "{{ route('tickets.index') }}";
                </script>
                <script src="{{ asset('js/tickets.js') }}"></script>
@endsection