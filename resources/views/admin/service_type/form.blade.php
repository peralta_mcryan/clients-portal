@extends('layouts.adminlayout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Service Types</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $pageName }}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-horizontal" method="POST" action="{{ $route }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-2 control-label"><span class="asterisk-marker">*</span> Name</label>

                                    <div class="col-md-10">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ $serviceType->name }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                  <div class="form-group{{ $errors->has('broad_type') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-2 control-label"></label>

                                    <div class="col-md-10">
                                        @if($serviceType->broad_type == 0)
                                            <div class="col-md-2"><input type="radio" name="broad_type" value="0" checked>None</div>
                                        @else
                                            <div class="col-md-2"><input type="radio" name="broad_type" value="0">None</div>
                                        @endif

                                        @if($serviceType->broad_type == 1)
                                            <div class="col-md-3"><input type="radio" name="broad_type" value="1" checked>Digital Marketing</div>
                                        @else
                                            <div class="col-md-3"><input type="radio" name="broad_type" value="1">Digital Marketing</div>
                                        @endif

                                        @if($serviceType->broad_type == 2)
                                            <div class="col-md-3"><input type="radio" name="broad_type" value="2" checked>Website</div>
                                        @else
                                           <div class="col-md-3"><input type="radio" name="broad_type" value="2">Website</div>
                                        @endif

                                    </div>
                                    @if ($errors->has('broad_type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('broad_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                               

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-2 control-label"> Files</label>
                                    @foreach($fileFolders as $key => $value)
                                        <div class="col-md-2">
                                            &nbsp;
                                        </div>
                                        <div class="col-md-10">
                                            <input id="service_type_file_{{ $key }}" type="checkbox" name="service_type_file[]" value="{{ $key }}" {{ (in_array($key, $serviceTypeFile) ? 'checked' : '') }}> <label for="service_type_file_{{ $key }}">&nbsp;{{ $value }}</label>
                                        </div>
                                    @endforeach
                                </div>

                                <div class="form-group">
                                    <div class="col-md-10 col-md-offset-2">
                                        <button type="submit" class="btn btn-primary">
                                            Save
                                        </button>
                                        <a href="{{ route('service-types.index') }}">
                                            <button type="button" class="btn btn-default">
                                                Cancel
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
@endsection
