@extends('layouts.adminlayout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">{{ $pageName }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Company Details
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" method="POST" action="{{ $route }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name"><span class="asterisk-marker">*</span> Company Name</label>
                                    <input id="name" name="name" class="form-control" placeholder="Company Name" value="{{ $account->name }}" required>
                                    @if ($errors->has('name'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="address"><span class="asterisk-marker">*</span> Company Address</label>
                                    <input id="address" name="address" class="form-control" placeholder="Company Address" value="{{ $account->address }}" required>
                                    @if ($errors->has('address'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="work_phone"><span class="asterisk-marker">*</span> Work Phone</label>
                                    <input id="work_phone" name="work_phone" class="form-control" placeholder="Work Phone" value="{{ $account->work_phone }}" required>
                                    @if ($errors->has('work_phone'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('work_phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="mobile_phone">Mobile Phone</label>
                                    <input id="mobile_phone" name="mobile_phone" class="form-control" placeholder="Mobile Phone" value="{{ $account->mobile_phone }}">
                                    @if ($errors->has('mobile_phone'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('mobile_phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="country">Country</label>
                                    {{ Form::select('country', $countries, $account->country_id,['id' => 'country',  'class'=>'']) }}
                                    @if ($errors->has('country'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('country') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="province">Province</label>
                                    {{ Form::select('province', $provinces, $account->province_id,['id' => 'province',  'class'=>'']) }}
                                    @if ($errors->has('province'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('province') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="city">City</label>
                                    {{ Form::select('city', $cities, $account->city_id,['id' => 'city',  'class'=>'']) }}
                                    @if ($errors->has('city'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="<?= route('client-account') ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <script src="{{ asset('js/accounts/accounts.js') }}"></script>
@endsection
