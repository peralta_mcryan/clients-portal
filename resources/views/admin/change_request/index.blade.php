@extends('layouts.adminlayout')

@section('content')
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Change Requests</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                            <div class="col-lg-6">
                                <p class="text-left">
                                    <a href="{{ route('client-project') }}"><i class="fa fa-columns fa-1x text-left"></i></a>
                                    <a href="{{ route('client-project-tabular') }}"><i class="fa fa-list fa-1x text-left"></i></a>
                                </p>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-right">
                                    <a href="{{ route('change-requests.create') }}"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                                </p>
                            </div>

                            <form  method="POST" action="{{ route('change-requests.index') }}">
                                {{ csrf_field() }}

                                <div class="col-lg-4">
                                    {{Form::select('client_accounts[]',$searchFilters['clientAccounts'],old('client_accounts'),['multiple'=>'multiple','id'=>'client_accounts','placeholder'=>'All Accounts'])}}
                                    {{-- <select multiple="multiple" name="client_accounts[]" id="client_accounts" placeholder="Select Accounts">
                                        @foreach($searchFilters['clientAccounts'] as $key => $value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select> --}}
                                </div>
                                <div class="col-lg-4">
                                    {{Form::select('projects[]',$searchFilters['projects']->pluck('name','id'),old('projects'),['multiple'=> 'multiple','id'=>'projects','placeholder'=>'All Projects'])}}
                                    {{-- <select multiple="multiple" name="projects[]" id="projects" placeholder="Select Project">
                                        @foreach($searchFilters['projects']->pluck('name','id') as $key => $value)
                                    
                                            <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select> --}}
                                </div>
    
                                <div class="col-lg-4">
                                    <input type="submit" class="btn btn-success btn-rectangle pull-left" value="Search">
                                </div>
                            </form>

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Account</th>
                                        <th>Project</th>
                                        <th colspan = "3">Task</th>
                                        <th>Man Hours</th>
                                        <th>Price</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($CRO as $item =>$cr )
                                        <tr>
                                            <td>{{$cr->project->accounts->name}}</td>
                                            <td>{{$cr->project->name}}</td>
                                            <td  colspan = "3">
                                                <a href = "#">Please view details... </a>
                                            </td>
                                            <td>{{number_format($cr->man_hours,2)}}</td>
                                            <td>{{number_format($cr->price,2)}}</td>
                                            <td>
                                                <?php
                                                    $changerequest = $cr->id;
                                                ?>
                                                <a href="{{route('change-requests.show',$cr->id)}}">
                                                    <span class="pull-left"> &nbsp;<i class="fa fa-arrow-circle-right" title="View Request Details"></i> &nbsp;</span>
                                                </a> &nbsp;
                                                <a href="{{route('change-requests.edit',$changerequest)}}">
                                                    <span class="pull-left"> &nbsp;<i class="fa fa-edit" title="Edit change request"></i> &nbsp;</span>
                                                </a> &nbsp;
                                                <a href="javasctript:void(0);" onclick="deleteRequest({{$changerequest}})" title="Delete">
                                                    <span class="pull-left"> &nbsp;<i class="fa fa-close imanila-color"></i></span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        {{-- {{ $projects->links() }} --}}
                    </div>
                </div>
                <script>
                    function deleteRequest(reqId) {
                        if (confirm('Are you sure you want to delete this change request?')) {
                            $.ajax({
                                type:'GET',
                                url:'/change-requests/delete/'+reqId,
                                success:function(data){
                                    window.location = '{{route('change-requests.index')}}';
                                }
                            });
                        }
                    }
                </script>
              
                
@endsection