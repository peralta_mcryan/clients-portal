@extends('layouts.adminlayout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">{{ $pageName }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Change Request Details
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" method="POST" action="{{ $route }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name"><span class="asterisk-marker">*</span>Project</label>
                                  {{Form::select('project',$projectArray->pluck('name','id'),!empty(old('project'))?old('project'):((!empty($changerequest->project_id)?$changerequest->project_id:'')))}}
                                    </select>
                                      @if ($errors->has('project'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('project') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="task">Task</label>
                                <textarea name="task" id="task" class="form-control" rows="4" placeholder = "Enter task here...">{{!empty(old('task'))?old('task'):(((!empty($changerequest->task)?$changerequest->task:'')))}}</textarea>
                                      @if ($errors->has('task'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('task') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                  <div class="form-group">
                                    <label for="man_hours">Man Hours</label>
                                  <input name="man_hours" id="man_hours" class="form-control" value = "{{!empty(old('man_hours'))?old('man_hours'):((!empty($changerequest->man_hours)?str_replace(',','',$changerequest->man_hours):'0.00'))}}">
                                      @if ($errors->has('man_hours'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('man_hours') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="price">Price</label>
                                    <input name="price" id="price" class="form-control" value = "{{!empty(old('price'))?old('price'):((!empty($changerequest->price)?str_replace(',','',$changerequest->price):'0.00'))}}">
                                    @if ($errors->has('price'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('price') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="<?= route('change-requests.index') ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    <script>
        $('select').selectize();
        $(document).ready(function () {
            CKEDITOR.replace( 'task' );
        });
    </script>
@endsection
