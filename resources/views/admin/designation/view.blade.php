@extends('layouts.adminlayout')

@section('content')
<div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Designations</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                            <p class="text-right">
                                <a href="{{ route('designations.create') }}"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                            </p>
                            <div class="dataTable_wrapper">
                                <?php 
                                    $ctr = 1;
                                ?>
                                <div class="row">
                                    @if(count($designations) > 0)
                                        @foreach ($designations as $designation)
                                            <div class="col-lg-4 col-md-6">
                                                <div class="panel panel-primary">
                                                    <a href="{{ route('service-types.show', $designation->id) }}">
                                                        <div class="panel-heading">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <div><strong>{{ $designation->designation }}</strong></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="panel-footer">
                                                        <small>
                                                            <a href="{{ route('designations.edit', $designation->id) }}">
                                                                <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                                                <span class="pull-left">&nbsp;Edit Details</span>
                                                            </a>
                                                            <a class="pull-right ">
                                                                <span class="pull-right imanila-color" style="cursor:pointer;"><i class="fa fa-close" onclick="javascript:deletedesignation({{ $designation->id }})"></i></span>
                                                            </a>
                                                        </small>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($ctr % 3 == 0)
                                                </div>
                                                <div class="row">
                                            @endif
                                            <?php $ctr++; ?>
                                        @endforeach
                                    @else
                                                    <h2>Empty Designation List</h2>
                                    @endif
                                <div class="col-lg-12 text-center">
                                    {{ $designations->links() }}
                                </div>
                            </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

<script>
    
function deletedesignation(designationId) {
	if (confirm('Are you sure you want to delete this designation?')) {
		$.ajax({
			type:'GET',
			url:'/designations/delete/'+designationId,
			success:function(data){
                alert(data)
				window.location = '/designations';
			}
		});
	}
}
</script>

@endsection