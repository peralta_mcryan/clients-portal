@extends('layouts.adminlayout')

@section('content')
<div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Administrator Accounts</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-3">
                            <p class="text-right">
                                <a href="{{ route('administrator-accounts-create') }}"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
                            </p>
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($users as $userList)
                                            <tr class="clickable-row" data-href="{{ route('administrator-accounts-show', $userList->id) }}">
                                                <td>
                                                    {{ $userList->name }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center">
                                {{ $users->links() }}
                            </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Account Details
                             </div>
                            <div class="panel-body">
                                <p class="text-right">
                                    <a href="{{ route('administrator-accounts-edit', $user->id) }}">
                                        <i class="fa fa-edit"> Edit</i></a> | 
                                    <a href="{{ route('administrator-accounts-change-pass', $user->id) }}">
                                        <i class="fa fa-key"> Change Password</i></a> 
                                    @if (Auth::user()->id == $user->id) 
                                        | <a href="javascript:deleteAdminAccount({{ $user->id }})">
                                        <i class="fa fa-close"> Delete</i></a>
                                    @endif
                                </p>
                                <p class="lead">
                                    {{ $user->name }}
                                </p>
                                <p>
                                    <strong>
                                        {{ $user->email }}
                                    </strong>
                                </p>
                                <p>
                                    <strong>
                                        @if(!empty($user->userDesignation->name))
                                        {{ $user->userDesignation->name }}
                                        @else
                                        ---
                                        @endif
                                    </strong>
                                </p>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>

                <script src="{{ asset('js/admin_account.js') }}"></script>
@endsection