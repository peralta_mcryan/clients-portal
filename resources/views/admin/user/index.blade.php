@extends('layouts.adminlayout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Administrator Accounts</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
            <p class="text-right">
                <a href="{{ route('administrator-accounts-create') }}"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a>
            </p>
            <div class="dataTable_wrapper">
                <?php 
                    $ctr = 1;
                ?>
                <div class="row">
                    @foreach ($users as $user)
                        <div class="col-lg-4 col-md-6">
                            <div class="panel panel-primary">
                                <a href="{{ route('administrator-accounts-show', $user->id) }}">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div><strong>{{ $user->name }}</strong></div>
                                                <br>
                                                <div>
                                                    <small>
                                                        @if (!empty($user->email))
                                                            {{ $user->email }}
                                                        @else
                                                            Not Available
                                                        @endif
                                                    </small>
                                                </div>
                                                <div>
                                                    <small>
                                                        @if (!empty($user->userDesignation->name))
                                                            {{ $user->userDesignation->name }}
                                                        @else
                                                            &nbsp;
                                                        @endif
                                                    </small>
                                                </div>
                                                <div>
                                                    <small>
                                                        @if (!empty($user->admin_type_id))
                                                            {{ !empty($user->adminType->name)?$user->adminType->name : '---' }}
                                                        @else
                                                            &nbsp;
                                                        @endif
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="panel-footer">
                                    <small>
                                        <a href="{{ route('administrator-accounts-edit', $user->id) }}">
                                            <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                            <span class="pull-left">&nbsp;Edit Details</span>
                                        </a>
                                        <a class="pull-right ">
                                            <span class="pull-right imanila-color" style="cursor:pointer;"><i class="fa fa-close" onclick="javascript:deleteAdminAccount({{ $user->id }})"></i></span>
                                        </a>
                                    </small>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    @if($ctr % 3 == 0)
                        </div>
                        <div class="row">
                    @endif
                    <?php $ctr++; ?>
                @endforeach
            </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12 text-center">
        {{ $users->links() }}
    </div>
</div>
<script src="{{ asset('js/admin_account.js') }}"></script>
@endsection