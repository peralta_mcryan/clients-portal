@extends('layouts.techlayout')

@section('content')
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Projects</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <p class="text-left">
                                    <a href="{{ route('tech-account.index') }}"><i class="fa fa-columns fa-1x text-left"></i></a>
                                    <a href="{{ route('techTeam-project-tabular') }}"><i class="fa fa-list fa-1x text-left"></i></a>
                                </p>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-right">
                                    {{-- <a href="{{ route('new-project') }}"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a> --}}
                                </p>
                            </div>
                        </div>
                            <div class = "col-lg-12">
                            <form role="form" method="GET" action="{{ route('techTeam-project-tabular') }}" enctype="multipart/form-data">
                                {{-- {{ csrf_field() }} --}}

                                <div class="col-lg-4">
                                    {{Form::select('client_accounts[]',$dropDowns['clientAccounts'],$searchFilters['client_accounts'],['id'=>'client_accounts','multiple'=>'multiple','placeholder'=>'All Client Accounts'])}}
                           
                                </div>

                                <div class="col-lg-4">
                                    {{Form::select('project_status[]',$dropDowns['projectStatuses'],$searchFilters['project_status'],['id'=>'project_status','multiple'=>'multiple','placeholder'=>'All Status'])}}

                                </div>
                                    <div class="col-lg-4">
                                        <input type="submit" class="btn btn-success btn-rectangle pull-left" value="Search">
                                    </div>
                            </form>
                           
                            @if(count($projects->toArray())>0)
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Project Name</th>
                                            <th>Description</th>
                                            <th>Status</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Aging</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($projects as $project)
                                            <tr class="clickable-row" data-href="{{ route('techTeam-show-project', $project->id) }}">
                                                <td>
                                                    <a href="{{ route('techTeam-show-project', $project->id) }}">
                                                        {{ $project->name }}
                                                    </a>
                                                </td>

                                            <td title = "{{empty($project->description)?'N / A':$project->description}}">
                                                    @if (empty($project->description)) 
                                                        N / A
                                                    @else
                                                        {{ $view = substr($project->description,0,50)}}
                                                    {{$view = strlen($project->description) > 50?'...':''}}
                                                    @endif
                                                </td>

                                                <td>
                                                    {{ config('constants.project_status.'.$project->status) }}
                                                </td>

                                                <td>
                                                    {{ !empty($project->project_start)?\Carbon\Carbon::parse($project->project_start)->format('M d, Y'):'---' }}
                                                </td>
                                                <td>
                                                    {{ \Carbon\Carbon::parse($project->project_end)->format('M d, Y') }}
                                                </td>


                                                <td>
                                                    {{ $project->getAgingDays() }} days
                                                </td>

                                                <td>
                                                    <a href="{{ route('techTeam-show-project', $project->id) }}">
                                                        <span class="pull-left"> &nbsp;<i class="fa fa-arrow-circle-right" title="View Project"></i> &nbsp;</span>
                                                    </a> &nbsp;
                                                
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>   
                            @else
                                <h3>No Result Found.</h3>
                            @endif
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                           {{ $projects->appends($searchFilters)->links() }}
                    </div>
                </div>
                <script>
                    var backurl = "{{ route('client-project-tabular') }}";
                </script>
                <script src="{{ asset('js/projects/projects.js') }}"></script>
@endsection