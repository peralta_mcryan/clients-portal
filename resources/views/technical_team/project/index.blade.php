@extends('layouts.techlayout')

@section('content')
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Projects</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                            <div class="col-lg-6">
                                <p class="text-left">
                                    <a href="{{ route('tech-account.index') }}"><i class="fa fa-columns fa-1x text-left"></i></a>
                                    <a href="{{ route('techTeam-project-tabular') }}"><i class="fa fa-list fa-1x text-left"></i></a>
                                </p>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-right">
                                    {{-- <a href="{{ route('new-project') }}"><button type="button" class="btn btn-success btn-rectangle"><i class="fa fa-plus"> New</i></button></a> --}}
                                </p>
                            </div>
                    </div>
                    <form role="form" method="GET" action="{{ route('tech-account.index') }}" enctype="multipart/form-data">
                        {{-- {{ csrf_field() }} --}}

                        <div class="col-lg-4">
                            {{Form::select('client_accounts[]',$dropDowns['clientAccounts'],$searchFilters['client_accounts'],['id'=>'client_accounts','multiple'=>'multiple','placeholder'=>'All Client Accounts'])}}
                           
                        </div>

                        <div class="col-lg-4">
                             {{Form::select('project_status[]',$dropDowns['projectStatuses'],$searchFilters['project_status'],['id'=>'project_status','multiple'=>'multiple','placeholder'=>'All Status'])}}

                        </div>
                        <div class="col-lg-4">
                            <input type="submit" class="btn btn-success btn-rectangle pull-left" value="Search">
                        </div>
                    </form>
                  
                    <div class = "col-lg-12">
                          <br>
                            <div class="dataTable_wrapper">
                                <?php 
                                    $ctr = 1;
                                ?>
                                <div class="row">
                                    
                                    @if(count($projects->toArray()['data'])>0)
                                        @foreach ($projects as $project)
                                            <div class="col-lg-4 col-md-6">
                                                <div class="panel panel-primary">
                                                    <a href="{{ route('techTeam-show-project', $project->id) }}">
                                                        <div class="panel-heading">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <div><strong>{{ $project->name }}</strong></div>
                                                                    <br>
                                                                    <div>
                                                                        <small>
                                                                            @if (empty($project->description))
                                                                                No project description
                                                                                @else
                                                                                <span title = "{{ empty($project->description)?'No project description':'Project Description: '.$project->description}}">
                                                                                    {{ $view = substr($project->description,0,50)}}
                                                                                    {{$view = strlen($project->description) > 50?'...':''}}
                                                                                </span>
                                                                            @endif
                                                                        </small>
                                                                    </div>
                                                                    <div><small>Status: {{ config('constants.project_status.'.$project->status) }}</small></div>
                                                                    <div><small>Start Date: {{ !empty($project->project_start)?\Carbon\Carbon::parse($project->project_start)->format('M d, Y'):'---' }}</small></div>
                                                                    <div><small>End Date: {{ \Carbon\Carbon::parse($project->project_end)->format('M d, Y') }}</small></div>
                                                                    <div><small>Aging: {{ $project->getAgingDays() }} days</small></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="panel-footer">
                                                        <small>
                                                            {{-- <a href="{{ route('update-project', $project->id) }}">
                                                                <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                                                <span class="pull-left">&nbsp;Edit Details</span>
                                                            </a> --}}
                                                        </small>
                                                        <br>
                                                        <small>
                                                            {{-- <a href="{{ route('project-timeline.index', $project->id) }}">
                                                                <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                                                <span class="pull-left">&nbsp;Timeline</span>
                                                            </a> --}}
                                                        </small>
                                                        <br>
                                                        <small>
                                                            {{-- <a href="#" onclick="javascript:deleteProject({{ $project->id }})">
                                                                <span class="pull-left"><i class="fa fa-close imanila-color"></i></span>
                                                                <span class="pull-left">&nbsp;Delete</span>
                                                            </a> --}}
                                                        </small>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($ctr % 3 == 0)
                                                </div>
                                                <div class="row">
                                            @endif
                                            <?php $ctr++; ?>
                                        @endforeach
                                    @else
                                        <h3>No Result Found.</h3>
                                    @endif
                            </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        {{ $projects->appends($searchFilters)->links() }}
                    </div>
                </div>
                <script>
                    var backurl = "{{ route('client-project-tabular') }}";
                </script>
                <script src="{{ asset('js/projects/projects.js') }}"></script>
@endsection