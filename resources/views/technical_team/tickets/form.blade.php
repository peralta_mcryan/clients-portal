@extends('layouts.techlayout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">{{ $pageName }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ticket Details
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" method="POST" action="{{ $route }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="ctrl"> CTRL #</label>
                                    <input id="ctrl" name="ctrl" class="form-control" value="{{ !empty(old('ctrl'))?old('ctrl'):(!empty($issueList->control_no)?$issueList->control_no:'---') }}" required readonly>
                                    @if ($errors->has('name'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                 <div class="form-group">
                                    <label for="ctrl"><span class="asterisk-marker">*</span> Project</label>
                                    {{Form::select('project',$projects,!empty(old('project'))?old('project'):(!empty($issueList->project_id)?$issueList->project_id:''),['placeholder'=>'Select Project','class'=>''])}}
                                    @if ($errors->has('project'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('project') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="ctrl"><span class="asterisk-marker">*</span> Assigned To</label>
                                    {{Form::select('assigned_department',$users,!empty(old('assigned_department'))?old('assigned_department'):(!empty($issueList->assigned_to)?$issueList->assigned_to:''),['placeholder'=>'Select Assignee','class'=>''])}}
                                    @if ($errors->has('assigned_department'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('assigned_department') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="status"><span class="asterisk-marker">*</span> Status</label>
                                    {{Form::select('status',config('constants.ticket_status_def'),!empty(old('status'))?old('status'):(!empty($issueList->status)?$issueList->status:''),['placeholder'=>'Select Status','class'=>''])}}
                                    @if ($errors->has('status'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="priority"><span class="asterisk-marker">*</span> Priority</label>
                                    {{Form::select('priority',config('constants.ticket_priority_def'),!empty(old('priority'))?old('priority'):(!empty($issueList->priority)?$issueList->priority:''),['placeholder'=>'Select Priority','class'=>''])}}
                                    @if ($errors->has('priority'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('priority') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="details"><span class="asterisk-marker">*</span> Details</label>
                                    {{Form::textarea('details',!empty(old('details'))?old('details'):(!empty($issueList->description)?$issueList->description:''),['placeholder'=>'Detail goes here','class'=>'form-control'])}}
                                    @if ($errors->has('details'))
                                        <span class="error-block">
                                            <strong>{{ $errors->first('details') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="<?= route('tickets.index') ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/tickets.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('select').selectize();
            CKEDITOR.replace('details');
        });
    </script>
@endsection



