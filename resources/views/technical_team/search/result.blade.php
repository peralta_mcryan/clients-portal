@extends('layouts.techlayout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h4 class="page-header">Search Results for <strong><i>{{ $searchText }}</i></strong></h4>
    </div>
</div>
<!-- /.row -->
<div class="row search-result">
    <div class="col-lg-12">
        <div class="panel tabbed-panel panel-primary">
            <div class="panel-heading clearfix">
                <div class="pull-left">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Projects ({{ count($projectSearchResults) }})</a></li>
                        <li class=""><a href="#tab-primary-2" data-toggle="tab" aria-expanded="false">Files ({{ count($fileSearchResults) }})</a></li>
                        <li class=""><a href="#tab-primary-3" data-toggle="tab" aria-expanded="false">Tickets ({{ count($ticketSearchResults) }})</a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-primary-1">
                        <div class="row">
                            <div class="col-lg-12">
                                @if (count($projectSearchResults) > 0)
                                    @foreach ($projectSearchResults as $projectSearchResult)
                                        <a href="{{ route('techTeam-show-project', $projectSearchResult->id) }}">
                                        <div class="col-lg-12 col-md-12">
                                            <strong>{{ $projectSearchResult->name }}</strong>
                                            <hr></hr>
                                        </div>
                                    </a>
                                    @endforeach
                                @else
                                    <strong class="imanila-color">No results found.</strong>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab-primary-2">
                        <div class="row">
                            <div class="col-lg-12">
                                @if(count($fileSearchResults) > 0)
                                    @foreach ($fileSearchResults as $fileSearchResult)
                                        <a href="{{ route('download-project-files', [$fileSearchResult->project_id, $fileSearchResult->file_folder_id, $fileSearchResult->filename]) }}">
                                            <div class="col-lg-12 col-md-12">
                                                <strong>{{ $fileSearchResult->filename }}</strong>
                                                <hr></hr>
                                            </div>
                                        </a>
                                    @endforeach
                                @else
                                    <strong class="imanila-color">No results found.</strong>
                                @endif
                            </div>
                        </div>
                    </div>

                     <div class="tab-pane" id="tab-primary-3">
                        <div class="row">
                            <div class="col-lg-12">
                                @if(count($ticketSearchResults) > 0)
                                    @foreach ($ticketSearchResults as $ticketSearchResult)
                                        <a href="{{ route('tickets.show', $ticketSearchResult->id) }}">
                                            <div class="col-lg-12 col-md-12">
                                                <strong>{{ $ticketSearchResult->control_no }}</strong>
                                                <hr></hr>
                                            </div>
                                        </a>
                                    @endforeach
                                @else
                                    <strong class="imanila-color">No results found.</strong>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection