            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                <a class="navbar-brand" href="{{ config('app.url', '#') }}">{{ config('app.name', 'iManila') }}</a>
                </div>

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                </ul>

                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> 
                                @component('components.who')
                                @endcomponent
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <!-- Authentication Links -->
                            @if (Auth::guard('ae')->guest())
                                <li><a href="{{ route('login') }}">Login</a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>
                            @else
                                @component('components.logout')
                                @endcomponent
                            @endif
                        </ul>
                    </li>
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <form role="form" method="GET" action="{{ route('search') }}">
                                    <div class="input-group custom-search-form">
                                        <input type="text" name="search_text" class="form-control" placeholder="Search..." required>
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </li> 
                            <!-- <li>
                                <a href="{{ route('home') }}" class="active"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li> -->
                            {{-- <li class="sidebar-search">
                                <form role="form" method="GET" action="{{ route('search') }}">
                                    <div class="input-group custom-search-form">
                                        <input type="text" name="search_text" class="form-control" placeholder="Search..." required>
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                                <!-- /input-group -->
                            </li> --}}
                            <li>
                                <a href="{{ route('my-account.index') }}"><i class="fa fa-folder-o" aria-hidden="true"></i> Projects</a>
                            </li>
                             {{-- <li>
                                <a href="{{ route('tickets.index') }}"><i class="fa fa-ticket" aria-hidden="true"></i> Tickets</a>
                            </li> --}}
                        </ul>
                    </div>
                </div>
            </nav>