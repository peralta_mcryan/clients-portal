@extends('layouts.clientlayout')

@section('content')
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Projects</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Account</th>
                                <th>Status</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Aging</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($projects as $project)
                                <tr class="clickable-row" data-href="{{ route('my-project-show', $project->id) }}">
                                    <td>
                                        <a href="{{ route('my-project-show', $project->id) }}">
                                            {{ $project->name }}
                                        </a>
                                    </td>

                                    <td>
                                        @if (empty($project->accounts->name)) 
                                            N / A
                                        @else
                                            {{ $project->accounts->name }}
                                        @endif
                                    </td>

                                    <td>
                                        {{ config('constants.project_status.'.$project->status) }}
                                    </td>

                                    <td>
                                          {{ !empty($project->project_start)?\Carbon\Carbon::parse($project->project_start)->format('M d, Y'):'---' }}
                                    </td>
                                    <td>
                                        {{ \Carbon\Carbon::parse($project->project_end)->format('M d, Y') }}
                                    </td>


                                    <td>
                                        {{ $project->getAgingDays() }} days
                                    </td>

                                    <td>
                                        <a href="{{ route('my-project-show', $project->id) }}#tab-primary-2">
                                            <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                            <span class="pull-left">&nbsp;Upload Files</span>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('my-project-show', $project->id) }}#tab-primary-5">
                                            <span class="pull-left"><i class="fa fa-window-maximize"></i></span>
                                            <span class="pull-left">&nbsp;Select Theme</span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        {{ $projects->links() }}
                    </div>
                </div>
                <script src="{{ asset('js/projects/projects.js') }}"></script>
@endsection