@extends('layouts.clientlayout')

@section('content')
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Official Receipts</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                            <div class="dataTable_wrapper">
                                <?php 
                                    $ctr = 1;
                                ?>
                                <div class="row">
                                    @foreach ($projects as $project)
                                        <div class="col-lg-4 col-md-6">
                                            <div class="panel panel-primary">
                                                <a href="{{ route('my-project-show', $project->id) }}">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <i class="fa fa-suitcase fa-4x"></i>
                                                            </div>
                                                            <div class="col-xs-9 text-right">
                                                                <div>{{ $project->name }}</div>
                                                                <br>
                                                                <div>
                                                                    <small>
                                                                        <i class="fa fa-building"></i>
                                                                        @if (empty($project->accounts->name)) 
                                                                            Not associated to a client
                                                                        @else
                                                                            {{ $project->accounts->name }}
                                                                        @endif
                                                                    </small>
                                                                </div>
                                                                <div><small>{{ config('constants.project_status.'.$project->status) }}</small></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="panel-footer">
                                                    <small>
                                                        <a href="{{ route('client-view-files', [$project->id, 13]) }}">
                                                            <span class="pull-left"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <span class="pull-left">&nbsp;View Receipts</span>
                                                        </a>
                                                    </small>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    @if($ctr % 3 == 0)
                                        </div>
                                        <div class="row">
                                    @endif
                                    <?php $ctr++; ?>
                                @endforeach
                            </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <script src="{{ asset('js/projects/projects.js') }}"></script>
@endsection