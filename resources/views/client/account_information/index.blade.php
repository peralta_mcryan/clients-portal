@extends('layouts.clientlayout')

@section('content')
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Account Information</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel tabbed-panel panel-primary">
                            <div class="panel-heading clearfix">
                                <div class="panel-title pull-left">
                                    Company Details
                                </div>
                                <div class="pull-right">
                                    @if(!empty($contact->account))
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Overview</a></li>
                                        <li class=""><a href="#tab-primary-2" data-toggle="tab" aria-expanded="false">Contacts</a></li>
                                    </ul>
                                    @else
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Overview</a></li>
                                    </ul>
                                    @endif
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    @if(!empty($contact->account))
                                    <div class="tab-pane fade active in" id="tab-primary-1">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h3>{{ $contact->account->name }} </h3>
                                                <hr></hr>

                                                <div class="panel panel-green">
                                                    <div class="panel-heading" style="padding-bottom: 5px;">
                                                        Address
                                                    </div>
                                                    <div class="panel-body">
                                                        <address>
                                                            <strong>{{ $contact->account->name }}</strong>
                                                            <br>
                                                            <small>
                                                                {{ $contact->account->address }}
                                                                <br>
                                                                <abbr title="Work Phone">W:</abbr>
                                                                {{ $contact->account->work_phone }}
                                                                <br>
                                                                <abbr title="Mobile Phone">M:</abbr>
                                                                {{$contact->account->mobile_phone }}
                                                            </small>
                                                        </address>

                                                        <small>
                                                        @if (!empty($city->name))
                                                            {{ $city->name }},
                                                        @endif

                                                        @if (!empty($province->name))
                                                            {{ $province->name }},
                                                        @endif

                                                        @if (!empty($country->name))
                                                            {{ $country->name }}
                                                        @endif
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab-primary-2">
                                        <div class="col-lg-12">
                                            <h3>{{ $contact->account->name }} </h3>
                                            <hr></hr>

                                            @if(!empty($contact->account->contacts))
                                                @foreach($contact->account->contacts as $contact)
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-green">
                                                            <div class="panel-heading">
                                                                <div class="row">
                                                                    <div class="col-xs-3" style="padding-bottom: 5px;">
                                                                        <i class="fa fa-address-card-o fa-5x"></i>
                                                                    </div>
                                                                    <div class="col-xs-9 text-right">
                                                                        <div class="huge">{{ $contact->first_name }} {{ $contact->last_name }}</div>
                                                                        <div><small>{{ $contact->email }}</small></div>
                                                                        <div><small>{{ $contact->work_phone }}</small></div>
                                                                        <div><small>{{ $contact->mobile_phone }}</small></div>
                                                                        <div><small>{{ config('constants.contact_type.'.$contact->contact_type) }}</small></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <a href="#">
                                                                <div class="panel-footer">
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="alert alert-info">
                                                    <i class="fa fa-hand-o-right"></i> You don't have a contact associated to this account.
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    @else
                                    <div class="tab-pane fade active in" id="tab-primary-1">
                                        <div class="col-lg-12">
                                            <div class="alert alert-info">
                                                <i class="fa fa-hand-o-right"></i> You are not assicated to any account. Please wait until the administrator is finished updating your account. 
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
@endsection