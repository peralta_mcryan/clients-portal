<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>iManila</title>

        <!-- Fonts -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css"> -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet">

        <!-- Styles -->
        <style>
            * {
                font-family: 'Open Sans', sans-serif;
                font-weight: 300;
            }

            html, body {
                background-color: #fff;
                color: #636b6f;
                /*font-family: Arial, Helvetica, sans-serif;*/
                
                /*font-weight: 100;*/
                
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 400;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/ae/login') }}">Account Executive</a>
                        <a href="{{ url('/client/login') }}">Client Login</a>
                        <a href="{{ url('/login') }}">Administrator</a>
                        <a href="{{ url('technical/login') }}">Technical Team</a>
                    @endif
                    <a href="https://imanila.ph/lets-talk/" target="_blank">Contact Us</a>
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <img src="{{ url('/images/imanila-logo.png') }}" style="width:300px;">
                </div>

                <div class="links">
                    <a href="https://imanila.ph/web/" target="_blank">Web Design</a>
                    <a href="https://imanila.ph/apps/" target="_blank">Web Applications</a>
                    <a href="https://imanila.ph/digital-marketing/" target="_blank">Digital Marketing</a>
                    <a href="https://imanila.ph/hosting/" target="_blank">Hosting</a>
                    <a href="https://imanila.ph/tech-support-service/" target="_blank">Tech Support Services</a>
                </div>
            </div>
        </div>
    </body>
</html>
