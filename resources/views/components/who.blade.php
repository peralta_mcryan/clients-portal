@if (Auth::guard('web')->check()) 
<span id = "currentuser">{{ Auth::user()->name }}</span>
@else
    @if (Auth::guard('ae')->check())
       <span id = "currentuser"> {{ Auth::guard('ae')->user()->name }}</span>
    @elseif (Auth::guard('techTeam')->check())
       <span id = "currentuser"> {{ Auth::guard('techTeam')->user()->name }}</span>
    @else
    <span id = "currentuser">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</span>
    @endif 

@endif