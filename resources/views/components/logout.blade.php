@if (Auth::guard('web')->check()) 
<li>
    <a href="{{ route('administrator-accounts-change-pass', Auth::user()->id) }}">
        <i class="fa fa-gear fa-fw"></i> Change Password
    </a>
</li>
<li>
    <a href="{{ route('admin-logout') }}"
        onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();">
        <i class="fa fa-sign-out fa-fw"></i> Logout
    </a>

    <form id="logout-form" action="{{ route('admin-logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</li>
@else
    @if (Auth::guard('ae')->check())
        <li>
            <a href="{{ route('ae-accounts-change-pass', Auth::guard('ae')->user()->id) }}">
                <i class="fa fa-gear fa-fw"></i> Change Password
            </a>
        </li>
        <li>
            <a href="{{ route('ae-logout') }}"
                onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out fa-fw"></i> Logout
            </a>

            <form id="logout-form" action="{{ route('ae-logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>

    @elseif (Auth::guard('techTeam')->check())
        <li>
            <a href="{{ route('techTeam-accounts-change-pass', Auth::guard('techTeam')->user()->id) }}">
                <i class="fa fa-gear fa-fw"></i> Change Password
            </a>
        </li>
        <li>
            <a href="{{ route('techTeam-logout') }}"
                onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out fa-fw"></i> Logout
            </a>

            <form id="logout-form" action="{{ route('techTeam-logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>


    @else    
   
        <li>
            <a href="{{ route('client-contact-change-pass', Auth::user()->id) }}">
                <i class="fa fa-gear fa-fw"></i> Change Password
            </a>
        </li>
        <li>
            <a href="{{ route('client-logout') }}"
                onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out fa-fw"></i> Logout
            </a>

            <form id="logout-form" action="{{ route('client-logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    @endif
@endif