<?php
 $loggedUser = '';
 if(Auth::guard('web')->check())
    $loggedUser = Auth::guard('web')->user()->id;
 elseif (Auth::guard('techTeam')->check())
    $loggedUser = Auth::guard('techTeam')->user()->id;
 elseif (Auth::guard('ae')->check())
    $loggedUser = Auth::guard('ae')->user()->id;
 ?>
<div class="panel tabbed-panel panel-primary">
    <div class="panel-heading clearfix">
        <div class="panel-title text-center">
            Topics
        </div>
    </div>
    
    <div class="panel-body">
        <div class=" pull-right">
            {{-- <button type="button" onclick="showNew();" id = "btnNewTopic" class = "btn btn-info">Create Topic</button> --}}
        </div>
        <br>
        <br>
        @php($threads = $issueThread->toArray())
        @if(count($threads['data'])> 0)
            @foreach ($issueThread as $kry => $forum )
                <div  class="panel tabbed-panel panel-primary">
                    <div class="panel-heading  discussion-head clearfix">
                        <div class="panel-title  pull-left">
                            {{date('F d, Y',strtotime($forum->created_at))}}
                        </div>
                    </div>
                    <div class="panel-body">
                        {!!$forum->details!!}
                    </div>
                    <div style="display:none;" id = "footer_{{$forum->id}}" class="panel-footer issue-thread-expand">
                        <form action="{{route('issue-thread.sendReply',$forum->id)}}" method="post">
                            {{ csrf_field() }}
                            @foreach ($forum->thread_discussion()->orderBy('created_at')->get() as $k => $discussion )
                            
                            @if($discussion->created_by == $loggedUser)    
                                <div class="col-lg-12 chat-logs-me">
                                    <div class="col-lg-8">
                                        <div class="panel tabbed-panel">
                            @else   
                                <div class="col-lg-12 chat-logs-others">
                                    <div class="col-lg-8">     
                                        <div class="panel tabbed-panel">      
                            @endif    
                                            <div class=" panel-discussion panel-heading clearfix">
                                                <div class="panel-title text-center">
                                                    {{date('F d, Y',strtotime($discussion->created_at))}} -- {{$discussion->user->name}}
                                                </div>
                                            </div>
                                                <div class="panel-body thread_discussion">
                                                @if ($discussion->reply_to > 0)
                                                    <div class="panel panel-body">
                                                        <span style = "font-style: italic;">&nbsp;{!! App\ThreadDiscussion::where('id',$discussion->reply_to)->pluck('details')->first()!!}&nbsp;</span>
                                                    </div>  
                                                @endif
                                                <span id = "current_message_{{$discussion->id}}">{!!$discussion->details!!}</span>
                                            </div>
                                            <div class="panel-footer">
                                                <a href="javascript::void(0);" class = "text-right" onclick="addReply({{$discussion->id}},{{$forum->id}});">Reply</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach 

                            <div id = "label_{{$forum->id}}" style="display:none;" class="panel panel-body thread-reply">
                                Reply for: <span id = "reply_for_{{$forum->id}}"></span> <span class = "pull-right"><a href = "javascript:void(0);" onclick="resetReply({{$forum->id}})"><i class="fa fa-close imanila-color"></i></a></span>
                            </div>
                                <input type="hidden" name = "reply_to_{{$forum->id}}" id = "reply_to_{{$forum->id}}" value="0">
                                    <textarea name="addComment_{{$forum->id}}" id="addComment_{{$forum->id}}" cols="30" rows="10"></textarea>
                                        <br>
                                            <input type="submit" value = "Post" class = "btn btn-info"></button>
                    
                                                <script>
                                                    $(document).ready(function () {
                                                        CKEDITOR.replace( 'addComment_'+"{{$forum->id}}" );
                                                    });
                                                </script>

                        </form>
                    </div>
                    <div class="discussion-footer panel-footer text-center">
                        <a href="javascript:void(0);" onclick="showFooter({{$forum->id}});">Show Discussions</a>
                    </div>
                </div>
            @endforeach
            <div class="text-center">
                {!!$issueThread->links()!!}
            </div>
            <div class="panel-body">
            </div>

        @else
            <h3>No Topics</h3>
       @endif
    </div>
</div>



{{-- THIS IS FOR REAL-TIME CHAT USING VUE JS ON LARAVEL --}}


 {{-- <div id = "vue-components-load">
            <input type="hidden" name="threadId" id="threadId" value = {{$issue}}>
            <chat-log :messages="messages" color='warning'></chat-log>
           
            <chat-composer v-on:messagesent="addMessage"></chat-composer>
        </div> --}}