@extends('layouts.aelayout')

@section('content')
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ $project->name }}</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-8">
                        <div class="panel tabbed-panel panel-primary">
                            <div class="panel-heading clearfix">
                                <div class="panel-title pull-left">
                                    Project Details
                                </div>
                                <div class="pull-right">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Overview</a></li>
                                        <li class=""><a href="#tab-primary-2" data-toggle="tab" aria-expanded="false">Files</a></li>
                                        <li class=""><a href="#tab-primary-3" data-toggle="tab" aria-expanded="false">Inclusions</a></li>
                                        <li class=""><a href="#tab-primary-4" data-toggle="tab" aria-expanded="false">Requirements</a></li>
                                        <li class=""><a href="#tab-primary-5" data-toggle="tab" aria-expanded="false">Themes</a></li>
                                        <li class=""><a href="#tab-primary-6" data-toggle="tab" aria-expanded="false">Tickets</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab-primary-1">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h3>{{ $project->name }} </h3>
                                                <hr></hr>

                                                <div class="col-xs-12 col-lg-12">
                                                        <small>
                                                            <strong>Description</strong>
                                                            <br>
                                                            {{ $project->description }}
                                                        </small>
                                                        <hr></hr>
    
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-xs-12 col-lg-4">
                                                                <small>
                                                                    <strong>Project Owner</strong>
                                                                    <br>
                                                                    @if (empty($project->accounts->name)) 
                                                                        N/A
                                                                    @else
                                                                        {{ $project->accounts->name }}
                                                                    @endif
                                                                </small>
                                                            </div>
            
                                                            <div class="col-xs-12 col-lg-4">
                                                                <small>
                                                                    <strong>Project Status</strong>
                                                                    <br>
                                                                    {{ $projectStatus }}
                                                                </small>
                                                            </div>
            
                                                            <div class="col-xs-12 col-lg-4">
                                                                <small>
                                                                    <strong>Project Manager</strong>
                                                                    <br>
                                                                    @if (empty($project->users->name)) 
                                                                        N/A
                                                                    @else
                                                                        {{ $project->users->name }}
                                                                    @endif
                                                                </small>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <p>
                                                            &nbsp;
                                                        </p>
                                                    </div>

                                                    <div class="col-xs-12 col-lg-4">
                                                        <small>
                                                            <strong>Service Contract</strong>
                                                            <br>
                                                            @if(!empty($serviceContract))
                                                                <a href="{{ $serviceContract }}" class="imanila-color">{{ $project->service_contract }}</a>
                                                            @else
                                                                N/A
                                                            @endif
                                                        </small>
                                                    </div>
    
                                                    <div class="col-xs-12 col-lg-4">
                                                        <small>
                                                            <strong>Project Start</strong>
                                                            <br>
                                                             {{ !empty($project->project_start)?\Carbon\Carbon::parse($project->project_start)->format('F j, Y, g:i a'):'---' }}
                                                          
                                                        </small>
                                                    </div>
    
                                                    <div class="col-xs-12 col-lg-4">
                                                        <small>
                                                            <strong>Project End</strong>
                                                            <br>
                                                            {{ \Carbon\Carbon::parse($project->project_end)->format('F j, Y, g:i a') }}
                                                        </small>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <p>
                                                            &nbsp;
                                                        </p>
                                                    </div>

                                                    <div class=
                                                    "col-xs-12 col-lg-4">
                                                        <small>
                                                            <strong>Assigned AE</strong>
                                                            <br>
                                                            {{ !empty($ae->name)?$ae->name:'---' }}
                                                        </small>
                                                    </div>
                                                    <div class="col-xs-12 col-lg-4">
                                                        <small>
                                                            <strong>Selected Themes</strong>
                                                            <br>
                                                            @php($display = '---')
                                                            @foreach ($themes as $theme )
                                                            <?php
                                                                if(in_array($theme->id,$selected))
                                                                {
                                                                    if($display == '---')
                                                                    {
                                                                        $display = $theme->name;
                                                                    }else{
                                                                        $display .= ','.$theme->name;
                                                                    }
                                                                }
                                                                
                                                            ?> 
                                                            @endforeach
                                                            {{$display}}
                                                        </small>
                                                    </div>
                                                <div class="col-xs-12">
                                                    <hr>
                                                        <h4 class="text-center">Timeline</h4>
                                                    <hr>
                                                </div>

                                                @if($chartData != '[]')
                                                <div class="col-lg-11">
                                                    <div id="chart_div"></div>
                                                </div>
                                                @else
                                                    <div class="col-lg-12 text-center">
                                                        No Project Timeline
                                                        <hr>
                                                    </div>
                                                @endif
                                                <?php 
                                                    $ctr = 1;
                                                ?>
                                                <div class="row">
                                                <div class="col-lg-12">
                                                @foreach($projectScheduleList as $projectSchedule)
                                                    <div class="col-sm-6">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <span class="pull-left"><strong>{{ $projectSchedule->phase_name }}</strong></span> 
                                                                <span class="pull-right"><strong>{{ $projectSchedule->working_days }} Days</strong></span>
                                                                &nbsp;
                                                                {{--  <hr style="margin-bottom: 5px; margin-top: 25px;">  --}}
                                                                {{--  <div class="text-center">{{ $projectSchedule->phase_name }}</div>   --}}
                                                            </div>
                                                            <div class="panel-body">
                                                                {!! $projectSchedule->description !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @if($ctr % 2 == 0)
                                                        </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                    @endif
                                                    <?php $ctr++; ?>
                                                @endforeach
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-primary-2">
                                        <form action="{{ route('ae-upload-project-files', $project->id) }}" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-4">
                                                        {{ Form::select('file_folder', $fileFolders, '',['id' => 'file_folder',  'class'=>'']) }}
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <input class="pull-left" type="file" name="file_projects[]" id="file_projects" multiple>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div><a href="{{ route('download-project-files-all', $project->id) }}" class="btn btn-primary pull-right">Download All</a></div>
                                                        <div><button  style="margin-right: 10px;" type="submit" class="btn btn-primary pull-right">Upload</button></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <hr></hr>
                                                    @if(count($fileFolders) > 0)
                                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                        <thead>
                                                            <tr>
                                                                <th>File Category</th>
                                                                <th>File Count</th>
                                                                <th>Download</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($fileFolders as $key => $name)
                                                                <tr class="clickable-row" data-href="{{ route('ae-view-files', [$project->id, $key]) }}">
                                                                    <td>
                                                                        {{ $name }}
                                                                    </td>

                                                                    <td>
                                                                        {{ $project->attachments->where('file_folder_id',$key)->count() }}
                                                                    </td>

                                                                    <td class="text-center">
                                                                        @if ($project->attachments->where('file_folder_id',$key)->count() > 0) 
                                                                            <a href="{{ route('download-project-files-category', [$project->id, $key]) }}"><span class=""><small>Download Files</small></span></a>
                                                                        @else 
                                                                        @endif
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    @else
                                                        <div class="alert alert-info">
                                                            <i class="fa fa-hand-o-right"></i>
                                                            <small>No file available.</small>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="tab-primary-3">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                {!! $project->inclusions !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-primary-4">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                {!! $project->requirements !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-primary-5">
                                        <form action="{{ route('my-project-themes', $project->id) }}" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-6">
                                                        {{-- <label for="theme_group" >Theme:</label>
                                                        <p>
                                                            
                                                            {{ Form::input('theme_group',config('constants.theme_type'), $group,['id' => 'province',  'class'=>'', 'onchange'=> 'changeThemeGroup(this,'.$project->id.')']) }}
                                                        </p> --}}
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="col-lg-12">
                                                            {{-- <label for="remarks" >Remarks:</label>
                                                            <p>
                                                                <textarea class = "form-control" name="remarks" id="remarks"  >{{!empty($clientTheme->remarks)?$clientTheme->remarks:''}}</textarea>
                                                            </p> --}}
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="col-lg-6">&nbsp;</div>
                                                        <div class="col-lg-6">
                                                            <div class="col-lg-12">   
                                                            <p>
                                                                {{-- <input type = "submit" class="btn btn-primary pull-right" value = "Save"> --}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            @if(count($themes->toArray())>0)
                                                @foreach ($themes as $theme)
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <h4>{{$theme->name}} 
                                                                        <a target = "_blank" href="http://{{$theme->website_links}}">
                                                                            <span class="pull-right"> &nbsp;<i class="fa fa-external-link" title = "Open Link"></i> &nbsp;</span>
                                                                        </a> &nbsp;
                                                                    </h4>
                                                                </div>   
                                                            </div>
                                                        </div>
                                                        <div class="panel-body">
                                                             <div class="col-lg-12">
                                                                {{ $theme->website_links }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @else
                                                <h2>No Selected Themes</h2>
                                            @endif  
                                        </form>
                                        <div class="row">
                                            <div class="col-lg-12 text-center">
                                               
                                            </div>
                                        </div>

                                    </div>
                                    <div class="tab-pane" id="tab-primary-6">
                                        <form action="{{ route('my-project-themes', $project->id) }}" method="post" enctype="multipart/form-data">
                                           @foreach($project->issue_list()->get() as $key => $issue)
                                            <div class="col-md-6">
                                                <div  class="panel tabbed-panel panel-danger">
                                                    <div class="panel-heading clearfix">
                                                        <div class="panel-title pull-left">
                                                            {{date('F d, Y',strtotime($issue->created_at))}}
                                                        </div>
                                                    </div>
                                                <a href="{{route('ae-ticket-show',$issue->id)}}">
                                                        <div class="panel-body">
                                                            {{-- {{ $view = substr($issue->description,0,100)}}
                                                            {{$view = strlen($issue->description) > 100?'...':''}} --}}
                                                            <h4><strong> Issue Ctrl #: {{$issue->control_no}} </strong></h4>
                                                            Issue date : {{date('F d, Y',strtotime($issue->created_at))}}
                                                            <br>
                                                            Created by : {{$issue->user->name}}
                                                        </div>
                                                    </a>
                                               </div>
                                            </div>
                                           @endforeach     
                                        </form>
                                        <div class="row">
                                            <div class="col-lg-12 text-center">
                                               
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <div class="col-lg-4">
                            <div class="dataTable_wrapper">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        Project Activity
                                    </div>
                                    <div class="panel-body">
                                        @if(!empty($projectActivity))
                                            @foreach($projectActivity as $key => $value)
                                                <div class="panel panel-primary">
                                                    <div class="panel-body">
                                                        <small>{{ $value->created_at->format('F j, Y, g:i a') }}</small>
                                                        <br>
                                                        <small>{{ $value->remarks }}</small>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script src="{{ asset('js/projects/projects.js') }}"></script>
                <script>
                    $(document).ready(function () {
                        var url = window.location.href;
                        var activeTab = url.substring(url.indexOf("#") + 1);
                        if (url.indexOf("#") >= 0) {
                            $(".tab-pane").removeClass("active in");
                            $('a[href="#'+ activeTab +'"]').tab('show')
                        }
                    });
                </script>
                <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

                <script>
                    $(document).ready(function () {
                        @if($chartData != '[]')
                            google.charts.load('visualization', '1', {packages: ['corechart']});
                            google.charts.setOnLoadCallback(drawChart);            
                        @endif
                    });
            
                    function drawChart() {
                        var jsonChartData = {!! $chartData !!};
                        var data = new google.visualization.DataTable();
                        data.addColumn('string', 'Name');
                        data.addColumn('number', 'spacer');
                        data.addColumn('number', 'Working Days');
                        data.addRows(jsonChartData);

                        
                        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
                        chart.draw(data, {
                            isStacked: true,
                            series: {
                                0: {
                                    visibleInLegend: false,
                                    color: 'transparent'
                                }
                            },
                            hAxis: {
                                title: 'Working Days',
                                viewWindow: {
                                    min: 1
                                },
                                gridlines: {
                                    color: '#000'
                                }
                            },
                            vAxis: {
                                title: 'Phases',
                                gridlines: {
                                    color: '#000'
                                }
                            }

            
                        });
                    }
            
                    function setWorkingDays() {
                        var startDate = $('#start_date').val();
                        var endDate = $('#end_date').val();
            
                        if ((startDate != '') && (endDate != '')) {
                            // $()
                        }
                    }
            
                    function deleteProjectSchedule(projectId, projectScheduleId) {
                        if (confirm('Are you sure you want to delete this schedule?')) {
                            $.ajax({
                                type:'GET',
                                url:'/project-timeline/' + projectScheduleId + '/destroy',
                                success:function(data){
                                    window.location = '/project-timeline/'+projectId;
                                }
                            });
                        }
                    }
                </script>
@endsection