@extends('layouts.aelayout')

@section('content')
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ $issueList->project->name }}</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-10">
                        <div class="panel tabbed-panel panel-primary">
                            <div class="panel-heading clearfix">
                                <div class="panel-title pull-left">
                                    Issue Details
                                </div>
                                <div class="pull-right">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-primary-1" data-toggle="tab" aria-expanded="false">Overview</a></li>
                                        <li ><a href="#tab-primary-2" data-toggle="tab" aria-expanded="false">Logs</a></li>
                                        <li ><a href="#tab-primary-3" data-toggle="tab" aria-expanded="false">New Topic</a></li>
                                        
                                       
                                    </ul>
                                </div>
                            </div>
                           
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab-primary-1">
                                        <div class="row">
                                            <div class="col-lg-12">
                                             <div class="col-lg-8">
                                                    <h3>Issue Ctrl #:&nbsp;{{ $issueList->control_no }}  &nbsp;</h3>
                                                </div>
                                                 <div class="pull-right">
                                                    @if(!empty($alerts))
                                                    <label for="alert"> Notify Me! </label>
                                                    <form id="change-status" action="{{route('update-alert-status',$issueList->id)}}" method="post">
                                                        {{ csrf_field() }}
                                                        <label class="switch">
                                                            @if($alerts->status == 1)
                                                                <input type="checkbox" name = "alert" id = "alert" onchange="submitForm();" value = 1 checked>
                                                                 <span class="slider round"></span>
                                                            @else
                                                                <input type="checkbox" name = "alert" id = "alert" onchange="submitForm();" value = 1>
                                                                <span class="slider round"></span>
                                                            @endif
                                                          
                                                        </label>
                                                    </form>
                                                    @endif
                                                </div>
                                                <div class="row">
                                                     <div class="col-md-12">
                                                        <div class="col-xs-12 col-lg-12">
                                                            <small>
                                                                <strong>Project Name</strong>
                                                                <br>
                                                            
                                                                {{ $issueList->project->name }}
                                                        
                                                            </small>
                                                            <hr></hr>
                                                        </div> 
                                                   
                                                        <div class="col-xs-12 col-lg-4">
                                                            <small>
                                                                <strong>Date Issued</strong>
                                                                <br>
                                                                    {{ $issueList->created_at }}
                                                                </small>
                                                        </div>
        
                                                        <div class="col-xs-12 col-lg-4">
                                                            <small>
                                                                <strong>Created By</strong>
                                                                <br>
                                                                {{ $issueList->user->name }}
                                                            </small>
                                                        </div>
                                                        
                                                        <div class="col-xs-12 col-lg-4">
                                                            <small>
                                                                <strong>Assigned To</strong>
                                                                <br>
                                                                {{ $issueList->assignee->name }}
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <p>
                                                        &nbsp;
                                                    </p>
                                                </div>  
                                                <div class="col-xs-12 col-lg-12">
                                                    <small>
                                                        <strong>Description</strong>
                                                        <br>
                                                        <div class="border-container">
                                                        {!! $issueList->description !!}
                                                        </div>
                                                    </small>
                                                    <hr></hr>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade in" id="tab-primary-2"> 
                                        <div id = "mainpanel" class="panel-body">
                                            @foreach ($issueLogs as $log )
                                                <div class="panel panel-primary">    
                                                    <div class="panel-heading">
                                                                                                    
                                                        <span calss ="text-left">{{date('F d, Y',strtotime($log->created_at))}}</span>
                                                    {{-- <div class="col-md-6">
                                                        Created date :
                                                    </div> --}}
                                                    </div>
                                                    <div class = "panel-body">
                                                        <div class="col-lg-12">
                                                        <?php $logs = json_decode($log->details)?>       
                                                        <ul>
                                                            @foreach ($logs as $logdetail )
                                                                <li>{{$logdetail}}</li>
                                                            @endforeach
                                                        </ul>
                                                        </div>
                                                    </div>
                                                </div>  
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="tab-pane fade in" id="tab-primary-3">                              
                                        <div class="panel-body">
                                                <form id = "frm_forum" action="{{route('ae-ticket-post-topic',$issueList->id)}}" method="post">
                                                   {{ csrf_field() }}    
                                                    <textarea name="details" id="details"></textarea>
                                                    <br>
                                                    <div class="pull-right">
                                                        <input type="submit" value="Submit" class = "btn btn-success"/>
                                                    </div>
                                                </form>  
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                         <div class="col-lg-10">
                            @component('components.chat',['issueThread' => $issueThread])
                            @endcomponent
                        </div>
  
                        <!-- /.panel -->
                    </div>
{{-- <script src="{{ asset("js/app.js") }}"></script> --}}

<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
     
        $(document).ready(function () {
            CKEDITOR.replace( 'details' );
        });
</script>
<script>
    $(document).ready(function(){
        
        $('#employee_sched').slideDown(300); 
    });

    function showFooter(id) {
        if($('#footer_'+id).is(':visible')){
            $('#footer_'+id).slideUp(300);
        }else{
            $('#footer_'+id).slideDown(300);
        }
    }

    function addReply(id,forum) {
        var  ctr = $('#current_message_'+id).html()
         $('#reply_to_'+forum).val(id);
        // $('#addComment_'+forum).val($('#current_message_'+id).html())
        if($('#reply_to_'+forum).val() == 0){
            $('#label_'+forum).slideUp(300);
        }else{
            $('#reply_for_'+forum).html(ctr);
            $('#label_'+forum).slideDown(300);
        }
        
    
    }
    function resetReply(forum)
    {
       
        $('#reply_to_'+forum).val(0);
        $('#label_'+forum).slideUp(300);
    }

        function submitForm()
    {
        $('#change-status').submit();
    }
</script>
@endsection