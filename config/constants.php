<?php
	return [
	    'project_status' => [
			0 => '',
			1 => "Sales Kick-off",
			2 => "Client Kick-off",
			3 => "Briefing Documentation",
	    	4 => 'Data Gathering',
	        5 => 'Sitemap',
	        6 => 'Initial Mock up',
	        7 => 'Full Mock up',
	        8 => 'Full Mock up Revisions',
			9 => 'Full Development',
			10 => "Documentation",
			11 => "Acceptance",
			12 => "Warranty",
			13 => "Closed",
	        14 => 'Full Development Revisions',
	        15 => 'Live Publication',
	        16 => 'Training',
	    ],
	    'portal_access' => [
	    	1 => 'Allow',
	        2 => 'Restrict'
		],

		 'role_type' => [
		 'ae'	=>	1,
		 'api'	=>	2,
		 'pm'	=>	3,
		 'tech'	=>	4
		],
	
		 'theme_group' => [
		 'Irepublic'	=>	1,
		 'Imanila'	=>	2, 
		],

		 'theme_type' => [
		 		1 	=> 'Irepublic',
				2	=> 'Imanila', 
		],
		
		  
	    'contact_type' => [
	    	1 => 'Primary',
	        2 => 'Administrator',
	        3 => 'Techincal',
		],
		'pagination' => 9,
		'project_activity_type' => [
	    	'project' => ['id' => 1, 'value' => 'Project'],
		],

		'ticket_status' =>[
			'active' => 1,
			'onhold' => 2,
			'cancelled' =>3,
		],

		'ticket_status_def' =>[
			1 => 'ACTIVE',
			2 => 'ON-HOLD',
			3 => 'CANCELLED',
		],

		'ticket_priority' =>[
			'high' => 1,
			'medium' => 2,
			'low' =>3,
		],

		'ticket_priority_def' =>[
			1 => 'HIGH',
			2 => 'MEDIUM',
			3 => 'LOW',
		]
	];
?>